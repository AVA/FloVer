#!/bin/bash

###############################################################################
#                                                                             #
# Affine Arithmetic Experiments for FMCAD 2018 paper                          #
# First run Daisy on all files found in the folder given as third argument    #
# certificates and log roundoff errors in a csv file.                         #
# Then benchmark each of the FloVer implementations by measuring runtime      #
# when checking the generated certificate.                                    #
#                                                                             #
###############################################################################

arr=()
while IFS=  read -r -d $'\0'; do
    arr+=("$REPLY")
# FIXME: Use a proper directory or a parameter?
done < <(find $3 -name "*.scala" -print0)

DATE=`date +%H%M%d%m%y`

mkdir /tmp/artifact_affine_$DATE

for file in "${arr[@]}"
do
  #Get the filename
  FILEPATH=${file/daisy\//.\/}
  FILE=${file/*\//}
  FILENAME=${FILE/.scala/}
  FILENAME=${FILENAME/Let/}
  PMAP=${FILE/scala/txt}
  CERTNAME="certificate_$FILENAME"
  # Generate a certificate with Daisy
  echo -n $FILENAME >>$1
  cd ./daisy
  if [ ! -f testcases/mixed-precision-maps/$PMAP ];
  then
      echo "Doing single precision eval for $FILENAME" >>$2
      RESULT=$( { /usr/bin/time -f ", %e" \
                                ./daisy $FILEPATH --certificate=coq \
                                --rangeMethod=affine \
                                --errorMethod=interval \
                                --results-csv=$FILENAME.csv \
                                >>$2 \
                  ; } 2>&1 )
      echo -n $RESULT >> $1
  else
      echo "Doing mixed-precision eval for $FILENAME" >>$2
      RESULT=$( { /usr/bin/time -f ", %e" \
                                ./daisy $FILEPATH --certificate=coq \
                                --rangeMethod=affine \
                                --errorMethod=interval \
                                --mixed-precision=testcases/mixed-precision-maps/$PMAP \
                                --results-csv=$FILENAME.csv \
                                >>$2 \
                  ; } 2>&1)
      echo -n $RESULT >> $1
  fi

  mv ./output/$FILENAME.csv /tmp/artifact_affine_$DATE

  cd ..

  #run coq checker 3 times
  echo "Working on Coq $CERTNAME" >>$2
  RESULT=$( { /usr/bin/time -f ", %e" coqc -R ./coq Flover \
                            ./daisy/output/$CERTNAME.v; } 2>&1)
  echo -n $RESULT >>$1

  rm ./daisy/output/$CERTNAME.vo
  rm ./daisy/output/$CERTNAME.glob

  RESULT=$( { /usr/bin/time -f ", %e" coqc -R ./coq Flover \
                            ./daisy/output/$CERTNAME.v; } 2>&1)
  echo -n $RESULT >>$1

  rm ./daisy/output/$CERTNAME.vo
  rm ./daisy/output/$CERTNAME.glob

  RESULT=$( { /usr/bin/time -f ", %e" coqc -R ./coq Flover \
                            ./daisy/output/$CERTNAME.v; } 2>&1)
  echo $RESULT >>$1

  mv ./daisy/output/certificate_* /tmp/artifact_affine_$DATE/

done
