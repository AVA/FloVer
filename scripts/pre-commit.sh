#!/bin/bash

# Check that the HOLCOMMIT variable and the HOL commit coincide:
DIR=`pwd`
FLOVERDIR=${DIR/FloVer\/*/FloVer\/}

HOLCOMMIT_LOCAL="$(cat $FLOVERDIR/hol4/.HOLCOMMIT)"

cd $HOLDIR
HOLCOMMIT_GLOBAL="$(git rev-parse HEAD)"
cd $DIR

if [[ "$HOLCOMMIT_LOCAL" = "$HOLCOMMIT_GLOBAL" ]]
then
    exit 0
else
    cat <<\EOF
    Error: You are trying to commit a state where ./hol4/.HOLCOMMIT and the
           commit hash of HOL4 in $HOLDIR have diverged. Please make sure
           that your changes either compile with the state specified in
           ./hol4/.HOLCOMMIT or update the file.
EOF
  exit 1
fi
