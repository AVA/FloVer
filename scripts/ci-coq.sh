#!/bin/sh


cd ./coq

#opam switch coq8.7.2
#eval `opam config env`
#./configure_coq.sh
#make -j

#if [[ "$?" != "0" ]];
#then
#    echo "Compilation with Coq 8.7.2 failed"
#    exit 1
#fi

#make clean

opam switch coq8.8
eval `opam config env`
./configure_coq.sh
make -j
