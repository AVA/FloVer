#!/bin/bash
function display_help {
    echo "configure_hol.sh - Check for environment variables and HOL4"
    echo "                   commit hash to be correct"
    echo ""
    echo -e "--help\t Show this text"
    echo -e " --init\t Initialize CakeML submodule to latest state"
}

INIT=no
while [ $# != 0 ]; do
   case "$1" in
   --help)  display_help;;
   --init) INIT=yes;;
   *)
   esac
   shift
 done

if [[ "$HOLDIR" = "" ]];
then
    echo "ERROR: HOLDIR variable not set. Please configure the HOLDIR variable first"
    exit 1
fi

DIR="$(pwd)"
HOLCOMMIT="$(cat ./.HOLCOMMIT)"

if [[ "$INIT" = "yes" ]];
then
    echo "Checking out CakeML submodule to correct state"
    git submodule init
    git submodule update -f cakeml

    echo "Checking out correct HOL4 version"
    cd $HOLDIR
    git pull
    git checkout $HOLCOMMIT
    bin/build cleanAll
    poly < $HOLDIR/tools/smart-configure.sml
    bin/build 
    cd $DIR
else
    cd $HOLDIR
    CURR_HOL="$(git rev-parse HEAD)"

    if [[ "$CURR_HOL" = "$HOLCOMMIT" ]]
    then
        echo "All set. HOL4 is on the correct commit."
    else
        echo "Please make sure HOL4 uses the commit $HOLCOMMIT"
        echo "or consider running the script with the --init parameter."
        exit 1
    fi
fi
