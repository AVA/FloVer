#!/bin/bash

#Set HOL4 commit variable
HOLCOMMIT="$(cat ./hol4/.HOLCOMMIT)"
FLOVERDIR="$(pwd)"

##HOL4 always cloned exactly here --> must be in this folder
#if [ ! -d "./HOL4" ];
#then
#    echo "HOL4 not installed, starting to install HOL4 into current directory."
#    echo "If this is not intended, please abort with Ctrl-c"
#    sleep 5
#
#    git clone https://github.com/HOL-Theorem-Prover/HOL.git HOL4
#    cd HOL4
#    git checkout $HOLCOMMIT
#    poly < tools/smart-configure.sml
#    bin/build
#    export HOLDIR="$(pwd)"
#    cd ../
#else
#    cd ./HOL4
#    export HOLDIR="$(pwd)"
#    git pull
#    CURRHOL="$(git rev-parse HEAD)"
#    if [[ "$CURRHOL" != "$HOLCOMMIT" ]];
#    then
#        echo "Updating HOL4 to latest version specified in .HOLCOMMIT"
#        git checkout $HOLCOMMIT
#        bin/build cleanAll
#        poly < tools/smart-configure.sml
#        bin/build
#    else
#        echo "HOL4 up-to-date"
#    fi
#    cd $FLOVERDIR
#fi

cd ./hol4

Holmake -j1

#cd ./binary

#echo "Downloading CakeML"
#git submodule init cakeml
#git submodule update -f cakeml

#$HOLDIR/bin/Holmake checkerBinaryTheory.uo
#$HOLDIR/bin/Holmake checker.S
#$HOLDIR/bin/Holmake cake_checker
