open preamble

open machine_ieeeTheory binary_ieeeTheory lift_ieeeTheory realTheory

open MachineTypeTheory ExpressionsTheory RealSimpsTheory FloverTactics CertificateCheckerTheory
open FPRangeValidatorTheory IntervalValidationTheory TypingTheory ErrorValidationTheory IntervalArithTheory AbbrevsTheory


val f64_plus_preserves_finite = store_thm ("f64_plus_preserves_finite",
  ``!f1 f2.
      fp64_isFinite (fp64_add dmode f1 f2) ==>
      fp64_isFinite f1 /\ fp64_isFinite f2 ``,
  rw [fp64_isFinite_def, fp64_add_def, fp64_to_float_float_to_fp64,
      float_add_def, float_is_finite_def]
  \\ Cases_on `float_value (fp64_to_float f1)`
  \\ Cases_on `float_value (fp64_to_float f2)`
  \\ fs [float_values]);

val f64_sub_preserves_finite = store_thm ("f64_plus_preserves_finite",
  ``!f1 f2.
      fp64_isFinite (fp64_sub dmode f1 f2) ==>
      fp64_isFinite f1 /\ fp64_isFinite f2 ``,
  rw [fp64_isFinite_def, fp64_sub_def, fp64_to_float_float_to_fp64,
      float_sub_def, float_is_finite_def]
  \\ Cases_on `float_value (fp64_to_float f1)` \\ Cases_on `float_value (fp64_to_float f2)`
  \\ fs [float_values]
  \\ Cases_on `float_value (float_negate (fp64_to_float f2))`
  \\ fs [float_values]
  \\ `(fp64_to_float f2).Exponent = UINT_MAXw`
    by (fs [float_value_def]
        \\ Cases_on `(fp64_to_float f2).Exponent = -(1w)`
        \\ Cases_on `(fp64_to_float f2).Significand = 0w`
        \\ fs [])
  \\ `(fp64_to_float f2).Significand = 0w`
    by (fs [float_value_def]
        \\ Cases_on `(fp64_to_float f2).Exponent = -(1w)`
        \\ Cases_on `(fp64_to_float f2).Significand = 0w`
        \\ fs [])
  \\ fs [float_negate_def, float_value_def]);

val f64_mul_preserves_finite = store_thm ("f64_plus_preserves_finite",
  ``!f1 f2.
      fp64_isFinite (fp64_mul dmode f1 f2) ==>
      fp64_isFinite f1 /\ fp64_isFinite f2 ``,
  rw [fp64_isFinite_def, fp64_mul_def, fp64_to_float_float_to_fp64,
      float_mul_def, float_is_finite_def]
  \\ Cases_on `float_value (fp64_to_float f1)` \\ Cases_on `float_value (fp64_to_float f2)`
  \\ fs [float_values]
  \\ Cases_on `r = 0`
  \\ fs [float_values]);

val f64_div_preserves_finite = store_thm ("f64_plus_preserves_finite",
  ``!f1 f2.
      fp64_isFinite (fp64_div dmode f1 f2) /\
      (float_value (fp64_to_float f2) = Infinity ==> F)==>
      fp64_isFinite f1 /\ fp64_isFinite f2 ``,
  rw [fp64_isFinite_def, fp64_div_def, fp64_to_float_float_to_fp64,
      float_div_def, float_is_finite_def]
  \\ Cases_on `float_value (fp64_to_float f1)` \\ Cases_on `float_value (fp64_to_float f2)`
  \\ fs [float_values]
  \\ Cases_on `r = 0`
  \\ fs [float_values]);

val real_to_float_float_id = Q.prove (
  `!f.
     fp64_isFinite f /\
     fp64_isNormal f /\
     ~ fp64_isZero f ==>
     float_to_fp64 (real_to_float dmode (float_to_real (fp64_to_float f))) = f`,
rpt strip_tac
\\ fs[fp64_isFinite_def, fp64_isZero_def, fp64_isNormal_def]
\\ fs[real_to_float_id]
\\ fs[float_to_fp64_fp64_to_float]);


``!ff:half.
    validFloatValue (float_to_real ff) M16 ==>
    float_to_real ((float_round roundTiesToEven F (float_to_real ff)):single) = float_to_real ((float_round roundTiesToEven F (float_to_real ff)):half)``
  rw[]
  \\ fs[validFloatValue_def]
  >- (`float_is_finite ff` by (irule normal16_to_real_implies_finiteness \\ fs[])
      \\ Q.ISPEC_THEN `ff` impl_subgoal_tac round_float_to_real_id \\ fs[]
      >- (cheat)
      \\ `float_value ff = Float (float_to_real ff)`  by (irule normal16_is_float_value \\ fs[])
      \\ Cases_on `float_to_real ff = 0`
      \\ fs[]
      >- (`2 * abs 0 <= ulp (:10#5)`
             by (fs[ulp_def, ULP_def])
          \\ `2 * abs 0 <= ulp (:23#8)`
               by (fs[ulp_def, ULP_def])
          \\ fs[round_roundTiesToEven_is_plus_zero, zero_to_real])
      \\ rw[float_round_def, zero_to_real, float_is_zero_to_real]
      >- (fs[round_def]
          \\ every_case_tac \\ fs[]
          \\ rveq
          \\ TRY (
               fs[float_minus_infinity_def, float_plus_infinity_def, float_to_real_def, threshold_def, float_negate_def, float_value_def]
               \\ FAIL_TAC "")
          \\ qpat_x_assum `closest_such _ _ _ = _` MP_TAC
          (* \\ qpat_x_assum `float_to_real (closest_such _ _ _) = _` MP_TAC *)
          \\ fs[closest_such_def]
          \\ SELECT_ELIM_TAC
          \\ rw[]
          >- (qexists_tac `ff`
              \\ rw[is_closest_def, IN_DEF, realTheory.ABS_POS]
              \\ first_x_assum (qspec_then `ff` mp_tac)
              \\ fs[realTheory.REAL_SUB_REFL]
              \\ strip_tac
              \\ fs[float_to_real_eq]
              \\ rveq
              \\ rfs[]
              \\ fs[float_is_zero_to_real])
          \\ CCONTR_TAC \\ fs[] \\ rveq
          \\ fs[is_closest_def, IN_DEF]
          (* (* \\ qpat_x_assum `!x._ ` (qspec_then `ff` assume_tac) mp_tac *) *)
          \\ fs[]
          \\ qpat_x_assum `float_to_real _ = _` mp_tac
          \\ SELECT_ELIM_TAC
          \\ rw[]
          cheat
          >- (Cases_on `ff`
              \\ qexists_tac `float c (0w @@ c0) (0w @@ c1)`
              \\ rw[is_closest_def, IN_DEF, realTheory.ABS_POS, word_concat_0]
              >- (fs[float_is_finite_def, float_value_def]
                  \\ every_case_tac \\ fs[float_Significand, float_Exponent, float_Sign, float_to_real_def]

(*           \\ CCONTR_TAC \\ fs[] \\ rveq *)
(*           \\ qpat_x_assum `!b. float_is_finite b ==> _` (qspec_then `x` impl_subgoal_tac) *)
(*           \\ fs[] *)

(* \\ fs[] *)

(* ) *)
