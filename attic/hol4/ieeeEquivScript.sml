open preamble

open machine_ieeeTheory binary_ieeeTheory

open ExpressionsTheory RealSimpsTheory

val dmode_def = Define `dmode = roundTiesToEven`;

val convert_env_def = Define `
  convert_env (E:num -> word64 option) =
    \n. case E n of
          | SOME v => SOME (float_to_real (fp64_to_float v))
          | NONE => NONE`;

val eval_exp_float_def = Define `
  eval_exp_float (e:real exp) (E:num -> word64 option) =
  case e of
    | Var n => E n
    | Const v => SOME (real_to_fp64 dmode v)
    | Binop b e1 e2 =>
      (case ((eval_exp_float e1 E), (eval_exp_float e2 E)) of
       | SOME v1, SOME v2 =>
                  (case b of
                     | Plus => SOME (fp64_add dmode v1 v2)
                     | Sub => SOME (fp64_sub dmode v1 v2)
                     | Mult => SOME (fp64_mul dmode v1 v2)
                     | Div => SOME (fp64_div dmode v1 v2))
                 |_, _ => NONE)
    | _ => NONE`;

val eval_agrees = store_thm ("eval_agrees",
  ``!(e:real exp) (E:num -> word64 option) (v:word64).
       eval_exp_float e E = SOME v ==>
       eval_exp machineEpsilon (convert_env E) e (float_to_real (fp64_to_float v))``,
Induct_on `e` \\ rpt strip_tac
>- (fs [eval_exp_float_def, convert_env_def]
    \\ Cases_on `E n` \\ fs []
    \\ match_mp_tac Var_load
    \\ fs [])
>- (fs [eval_exp_float_def, convert_env_def, real_to_fp64_def, real_to_float_def, dmode_def]
    \\ first_x_assum (fn thm => rw[GSYM thm])
    \\ once_rewrite_tac [fp64_to_float_float_to_fp64]
    \\ cheat)
>- (cheat)
>- (cheat));
