(**
    Interval arithmetic checker and its soundness proof.
    The function validIntervalbounds checks wether the given analysis result is
    a valid range arithmetic for each sub term of the given exprression e.
    The computation is done using our formalized interval arithmetic.
    The function is used in CertificateChecker.v to build the full checker.
**)
From Coq
     Require Import QArith.QArith QArith.Qreals QArith.Qminmax Lists.List
     micromega.Psatz.

From Flover
     Require Import Infra.Abbrevs Infra.RationalSimps Infra.RealRationalProps
     Infra.Ltacs Infra.RealSimps Infra.Results TypeValidator.

From Flover
     Require Export IntervalArithQ IntervalArith ssaPrgs RealRangeArith.

Fixpoint inferIntervalbounds (e:expr Q) (A: analysisResult) (* (P: FloverMap.t intv) *)
         : (result intv * analysisResult) :=
  match FloverMap.find e A with
  | Some (iv, _) => (Succes iv, A)
  | None =>
    let (rIv, newA) :=
        match e with
        | Var _ v => (Fail _ "variable not found", A)
        | Const _ n => (Succes (n,n), A)
        | Unop o f1 =>
          match inferIntervalbounds f1 A with
          | (Succes iv1, A1) =>
            match o with
            | Neg => (Succes (negateIntv iv1), A1)
            | Inv =>
              if (((Qleb (ivhi iv1) 0) && (negb (Qeq_bool (ivhi iv1) 0))) ||
                  ((Qleb 0 (ivlo iv1)) && (negb (Qeq_bool (ivlo iv1) 0))))
              then (Succes (invertIntv iv1), A1)
              else (Fail _ "div 0 in Inv", A1)
            end
          | fail1 => fail1
          end
        | Binop op f1 f2 =>
          match inferIntervalbounds f1 A with
          | (Succes iv1, A1) =>
            match inferIntervalbounds f2 A1 with
            | (Succes iv2, A2) =>
              match op with
              | Plus => (Succes (addIntv iv1 iv2), A2)
              | Sub => (Succes (subtractIntv iv1 iv2), A2)
              | Mult => (Succes (multIntv iv1 iv2), A2)
              | Div =>
                if (((Qleb (ivhi iv2) 0) && (negb (Qeq_bool (ivhi iv2) 0))) ||
                    ((Qleb 0 (ivlo iv2)) && (negb (Qeq_bool (ivlo iv2) 0))))
                then (Succes (divideIntv iv1 iv2), A2)
                else (Fail _ "div 0 in Div", A2)
              end
            | fail2 => fail2
            end
          | fail1 => fail1
          end
        | Fma f1 f2 f3 =>
          match inferIntervalbounds f1 A with
          | (Succes iv1, A1) =>
            match inferIntervalbounds f2 A1 with
            | (Succes iv2, A2) =>
              match inferIntervalbounds f3 A2 with
              | (Succes iv3, A3) => (Succes (addIntv (multIntv iv1 iv2) iv3), A3)
              | fail3 => fail3
              end
            | fail2 => fail2
            end
          | fail1 => fail1
          end
        | Downcast _ f1 => inferIntervalbounds f1 A
        | Let _ x f1 f2 =>
          match inferIntervalbounds f1 A with
          | (Succes iv1, A1) => inferIntervalbounds f2 (FloverMap.add (Var Q x) (iv1, 0) A1)
          | fail1 => fail1
          end
        end
    in match rIv with
       | Succes iv => (rIv, FloverMap.add e (iv, 0) newA)
       | failInfer => (failInfer, newA)
       end
  end.

(*
Lemma inferBoundsDiv_uneq_zero e1 e2 A P V ivlo_e2 ivhi_e2 err:
  FloverMap.find (elt:=intv * error) e2 A = Some (ivlo_e2, ivhi_e2) ->
  inferIntervalbounds (Binop Div e1 e2) A = (Succes iv, newA) ->
  (ivhi_e2 < 0) \/ (0 < ivlo_e2).
Proof.
  intros A_eq validBounds; cbn in *.
  Flover_compute; try congruence.
  apply le_neq_bool_to_lt_prop; auto.
Qed.
*)

Theorem inferIntervalbounds_sound (f:expr Q) (A newA: analysisResult) iv
        inVars outVars (E:env) Gamma:
  ssa f inVars outVars ->
  inferIntervalbounds f A = (Succes iv, newA) ->
  (forall e r, FloverMap.find e A = Some r ->
           validRanges e A E (toRTMap (toRExpMap Gamma))) ->
  validTypes f Gamma ->
  validRanges f newA E (toRTMap (toRExpMap Gamma)).
Proof.
  induction f in E, inVars, A, newA |- *;
    intros ssa_f infer_success init_valid valid_types;
    unfold inferIntervalbounds in infer_success; Flover_compute; inversion infer_success; subst;
      try eapply init_valid; eauto.
  - split; auto.
    exists (v, v), 0, (Q2R v); repeat split; try (cbn; lra).
    + rewrite FloverMapFacts.P.F.add_eq_o; auto. admit.
    + eapply Const_dist'; eauto; try reflexivity.
      cbn. rewrite Rabs_R0. lra.
  - destruct r; inversion infer_success; subst.
  (*
  - destruct (NatSet.mem n dVars) eqn:?; cbn in *; split; auto.
    + destruct (valid_definedVars n)
        as [vR [iv_n [err_n [env_valid [map_n bounds_valid]]]]];
        try set_tac.
      rewrite map_n in *.
      kill_trivial_exists.
      eexists; split; [auto| split; try eauto ].
      eapply Var_load; simpl; try auto.
      destruct (types_defined) as [m [find_m _]].
      eapply toRExpMap_some in  find_m; simpl; eauto.
      match_simpl; auto.
    + Flover_compute.
      destruct (valid_freeVars n i0) as [free_n [vR [env_valid bounds_valid]]];
        auto using find_in_precond; set_tac.
      kill_trivial_exists.
      eexists; split; [auto | split].
      * econstructor; try eauto.
        destruct types_defined as [m [find_m _]].
        eapply toRExpMap_some in find_m; simpl; eauto.
        match_simpl; auto.
      * canonize_hyps; cbn in *. lra.
  - split; [auto |].
    Flover_compute; canonize_hyps; simpl in *.
    kill_trivial_exists.
    exists (perturb (Q2R v) REAL 0).
    split; [eauto| split].
    + econstructor; try eauto.
      cbn. rewrite Rabs_R0; lra.
    + unfold perturb; simpl; lra.
  - Flover_compute; simpl in *; try congruence.
    assert (validRanges f A E (toRTMap (toRExpMap Gamma))) as valid_e.
    { inversion ssa_f. eapply IHf; eauto.
      destruct types_defined as [? [? [[? ?] ?]]]; auto. }
    split; try auto.
    apply validRanges_single in valid_e.
    destruct valid_e as [iv_f [err_f [vF [iveq_f [eval_f valid_bounds_f]]]]].
    rewrite Heqo0 in iveq_f; inversion iveq_f; subst.
    inversion iveq_f; subst.
    destruct u; try andb_to_prop R; simpl in *.
    + kill_trivial_exists.
      exists (evalUnop Neg vF); split;
        [auto | split ; [econstructor; eauto | ]].
      * cbn; auto.
      *  canonize_hyps.
         rewrite Q2R_opp in *.
         simpl; lra.
    + rename L0 into nodiv0.
      apply le_neq_bool_to_lt_prop in nodiv0.
      kill_trivial_exists.
      exists (perturb (evalUnop Inv vF) REAL 0); split;
        [destruct i; auto | split].
      * econstructor; eauto.
        { auto. }
        { simpl. rewrite Rabs_R0; lra. }
        (* Absence of division by zero *)
        { hnf. destruct nodiv0 as [nodiv0 | nodiv0]; apply Qlt_Rlt in nodiv0;
               rewrite Q2R0_is_0 in nodiv0; lra. }
      * canonize_hyps.
        pose proof (interval_inversion_valid ((Q2R (fst iv_f)),(Q2R (snd iv_f)))
                                             (a :=vF))
          as inv_valid.
         unfold invertInterval in inv_valid; simpl in *.
         destruct inv_valid; try auto.
         { destruct nodiv0; rewrite <- Q2R0_is_0; [left | right]; apply Qlt_Rlt; auto. }
         { split; eapply Rle_trans.
           - apply L1.
           - rewrite Q2R_inv; try auto.
             destruct nodiv0; try lra.
             hnf; intros.
             assert (0 < Q2R (snd iv_f))%R.
             { eapply Rlt_le_trans.
               apply Qlt_Rlt in H1.
               rewrite <- Q2R0_is_0.
               apply H1. lra.
             }
             rewrite <- Q2R0_is_0 in H3.
             apply Rlt_Qlt in H3.
             lra.
           - apply H0.
           - rewrite <- Q2R_inv; try auto.
             hnf; intros.
             destruct nodiv0; try lra.
             assert (Q2R (fst iv_f) < 0)%R.
             { rewrite <- Q2R0_is_0.
               apply Qlt_Rlt in H2.
               eapply Rle_lt_trans; try eauto.
               lra. }
             rewrite <- Q2R0_is_0 in H3.
             apply Rlt_Qlt in H3. lra. }
  - destruct types_defined
      as [? [? [[? [? ?]]?]]].
    inversion ssa_f.
    assert (validRanges f1 A E (toRTMap (toRExpMap Gamma))) as valid_f1
        by (Flover_compute; try congruence; eapply IHf1; eauto; set_tac).
    assert (validRanges f2 A E (toRTMap (toRExpMap Gamma))) as valid_f2
        by (Flover_compute; try congruence; eapply IHf2; eauto; set_tac).
    repeat split;
      [ intros; subst; Flover_compute; congruence |
                       auto | auto |].
    apply validRanges_single in valid_f1;
      apply validRanges_single in valid_f2.
    destruct valid_f1 as [iv_f1 [err1 [vF1 [env1 [eval_f1 valid_f1]]]]].
    destruct valid_f2 as [iv_f2 [err2 [vF2 [env2 [eval_f2 valid_f2]]]]].
    Flover_compute; try congruence.
    kill_trivial_exists.
    exists (perturb (evalBinop b vF1 vF2) REAL 0);
      split; [destruct i; auto | ].
    inversion env1; inversion env2; subst.
      destruct b; simpl in *.
    * split;
        [eapply Binop_dist' with (delta := 0%R); eauto; try congruence;
         try rewrite Rabs_R0; cbn; try auto; try lra|].
      pose proof (interval_addition_valid ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                          (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_add.
      specialize (valid_add vF1 vF2 valid_f1 valid_f2).
      unfold isSupersetIntv in R.
      andb_to_prop R.
      canonize_hyps; simpl in *.
      repeat rewrite <- Q2R_plus in *;
        rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold perturb. lra.
    * split;
        [eapply Binop_dist' with (delta := 0%R); eauto; try congruence;
         try rewrite Rabs_R0; cbn; try auto; lra|].
      pose proof (interval_subtraction_valid ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                             (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_sub.
      specialize (valid_sub vF1 vF2 valid_f1 valid_f2).
      unfold isSupersetIntv in R.
      andb_to_prop R.
      canonize_hyps; simpl in *.
      repeat rewrite <- Q2R_opp in *;
        repeat rewrite <- Q2R_plus in *;
        rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold perturb; lra.
    * split;
        [eapply Binop_dist' with (delta := 0%R); eauto; try congruence;
         try rewrite Rabs_R0; cbn; try auto; lra|].
      pose proof (interval_multiplication_valid ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                                (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_mul.
      specialize (valid_mul vF1 vF2 valid_f1 valid_f2).
      unfold isSupersetIntv in R.
      andb_to_prop R.
      canonize_hyps; simpl in *.
      repeat rewrite <- Q2R_mult in *;
        rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold perturb; lra.
    * andb_to_prop R.
      canonize_hyps.
      apply le_neq_bool_to_lt_prop in L.
      split;
        [eapply Binop_dist' with (delta := 0%R); eauto; try congruence;
         try rewrite Rabs_R0; cbn; try auto; try lra|].
      (* No division by zero proof *)
      { hnf; intros.
        destruct L as [L | L]; apply Qlt_Rlt in L; rewrite Q2R0_is_0 in L; lra. }
      { pose proof (interval_division_valid (a:=vF1) (b:=vF2)
                                            ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                            (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_div.
        simpl in *.
        destruct valid_div; try auto.
        - destruct L; rewrite <- Q2R0_is_0; [left | right]; apply Qlt_Rlt; auto.
        - assert (~ (snd iv_f2) == 0).
          { hnf; intros. destruct L; try lra.
            assert (0 < (snd iv_f2)) by (apply Rlt_Qlt; apply Qlt_Rlt in H7; lra).
            lra. }
          assert (~ (fst iv_f2) == 0).
          { hnf; intros; destruct L; try lra.
            assert ((fst iv_f2) < 0) by (apply Rlt_Qlt; apply Qlt_Rlt in H8; lra).
            lra. }
          repeat (rewrite <- Q2R_inv in *; try auto).
          repeat rewrite <- Q2R_mult in *.
          rewrite <- Q2R_min4, <- Q2R_max4 in *.
          unfold perturb.
          lra.
      }
  - destruct types_defined
      as [mG [find_mg [[validt_f1 [validt_f2 [validt_f3 valid_join]]] valid_exec]]].
    inversion ssa_f.
    assert (validRanges f1 A E (toRTMap (toRExpMap Gamma))) as valid_f1
        by (Flover_compute; try congruence; eapply IHf1; eauto; set_tac).
    assert (validRanges f2 A E (toRTMap (toRExpMap Gamma))) as valid_f2
        by (Flover_compute; try congruence; eapply IHf2; eauto; set_tac).
    assert (validRanges f3 A E (toRTMap (toRExpMap Gamma))) as valid_f3
        by (Flover_compute; try congruence; eapply IHf3; eauto; set_tac).
    repeat split; try auto.
    apply validRanges_single in valid_f1;
      apply validRanges_single in valid_f2;
      apply validRanges_single in valid_f3.
    destruct valid_f1 as [iv_f1 [err1 [vF1 [env1 [eval_f1 valid_f1]]]]].
    destruct valid_f2 as [iv_f2 [err2 [vF2 [env2 [eval_f2 valid_f2]]]]].
    destruct valid_f3 as [iv_f3 [err3 [vF3 [env3 [eval_f3 valid_f3]]]]].
    Flover_compute; try congruence.
    kill_trivial_exists.
    exists (perturb (evalFma vF1 vF2 vF3) REAL 0); split; try auto.
    inversion env1; inversion env2; inversion env3; subst.
    split; [auto | ].
    * eapply Fma_dist' with (delta := 0%R); eauto; try congruence; cbn;
        try rewrite Rabs_R0; try auto; lra.
    * pose proof (interval_multiplication_valid (Q2R (fst iv_f1),Q2R (snd iv_f1)) (Q2R (fst iv_f2), Q2R (snd iv_f2))) as valid_mul.
      specialize (valid_mul vF1 vF2 valid_f1 valid_f2).
      remember (multInterval (Q2R (fst iv_f1), Q2R (snd iv_f1)) (Q2R (fst iv_f2), Q2R (snd iv_f2))) as iv_f12prod.
      pose proof (interval_addition_valid (fst iv_f12prod, snd iv_f12prod)) (Q2R (fst iv_f3),Q2R (snd iv_f3)) as valid_add.
      specialize (valid_add (vF1 * vF2)%R vF3 valid_mul valid_f3).
      rewrite Heqiv_f12prod in valid_add, valid_mul.
      unfold multIntv in valid_add.
      canonize_hyps.
      simpl in *.
      repeat rewrite <- Q2R_mult in *;
        repeat rewrite <- Q2R_min4, <- Q2R_max4 in *;
        repeat rewrite <- Q2R_plus in *;
        repeat rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold evalFma, evalBinop, perturb.
      lra.
  - destruct types_defined
      as [mG [find_mG [[validt_f _] _]]].
    inversion ssa_f.
    assert (validRanges f A E (toRTMap (toRExpMap Gamma))) as valid_f1
        by (Flover_compute; try congruence; eapply IHf; eauto).
    split; try auto.
    apply validRanges_single in valid_f1.
    destruct valid_f1 as [iv_f [err_f [vF [env_f [eval_f valid_f]]]]].
    Flover_compute.
    inversion env_f; subst.
    kill_trivial_exists.
    exists (perturb vF REAL 0).
    split; [destruct i; auto |].
    canonize_hyps.
    split; [| cbn in *; lra].
    eapply Downcast_dist'; try eassumption; auto; auto.
    cbn; rewrite Rabs_R0. lra.
  - destruct types_defined
      as [mG [find_mG [[validt_f1 [validt_f2 valid_join]] valid_exec]]].
    Flover_compute; try congruence.
    inversion ssa_f; subst.
    canonize_hyps.
    assert (validRanges f1 A E (toRTMap (toRExpMap Gamma))) as valid_f1
        by (Flover_compute; try congruence; eapply IHf1; eauto; set_tac).
    pose proof (validRanges_single _ _ _ _ valid_f1) as valid_single_f1.
    destruct valid_single_f1 as [iv_f1 [err_f1 [v [find_v [eval_f1 valid_bounds_f1]]]]].
    rewrite find_v in *; inversion Heqo0; subst.
    assert (validRanges f2 A (updEnv n v E) (toRTMap (toRExpMap Gamma))) as valid_f2.
    { eapply IHf2; eauto.
      - eapply ssa_equal_set; eauto.
        intros x. split; set_tac; intros; tauto.
      - intros v0 mem_v0.
        unfold updEnv.
        case_eq (v0 =? n); intros v0_eq.
        + rename L1 into eq_lo;
            rename R into eq_hi.
          rewrite Nat.eqb_eq in v0_eq; subst.
          exists v; eexists; eexists. repeat split; eauto; simpl in *; lra.
        + apply valid_definedVars.
          set_tac.
          destruct mem_v0 as [? | [? ?]]; try auto.
          rewrite Nat.eqb_neq in v0_eq.
          congruence.
      - intros x x_contained.
        set_tac.
        repeat split; auto.
        + right. split; auto. hnf; intros. apply H0; set_tac.
        + hnf; intros. apply H0; set_tac.
      - hnf. intros x ? ?.
        unfold updEnv.
        case_eq (x =? n); intros case_x; auto.
        rewrite Nat.eqb_eq in case_x. subst.
        set_tac.
        assert (NatSet.In n fVars) as in_free
            by (apply preVars_free; eapply preIntvVars_sound; eauto).
        exfalso. apply H5. set_tac. }
    repeat split; auto.
    + intros vR ?.
      assert (vR = v) by (eapply meps_0_deterministic; eauto); now subst.
    + apply validRanges_single in valid_f2.
      destruct valid_f2 as [? [? [v2 [find_v2 [? ?]]]]].
      rewrite find_v2 in *; inversion Heqo2; subst.
      cbn in *.
      eexists. eexists. exists v2.
      repeat split; eauto; try lra.
      econstructor; eauto; try reflexivity.
      rewrite Rabs_R0. cbn; lra.
  (* - Flover_compute. congruence. *)
Qed.
*)
Abort.

(*
Theorem validIntervalboundsCmd_sound (f:cmd Q) (A:analysisResult):
  forall Gamma E fVars dVars outVars P,
    ssa f (NatSet.union fVars dVars) outVars ->
    dVars_range_valid dVars E A ->
    P_intv_sound E P ->
    NatSet.Subset (preIntvVars P) fVars ->
    NatSet.Subset (NatSet.diff (Commands.freeVars f) dVars) fVars ->
    validIntervalboundsCmd f A P dVars = true ->
    validTypesCmd f Gamma ->
    validRangesCmd f A E (toRTMap (toRExpMap Gamma)).
Proof.
  induction f;
    intros *  ssa_f dVars_sound fVars_valid preIntvVars_free usedVars_subset
                    valid_bounds_f valid_types_f;
    cbn in *.
  - Flover_compute.
    inversion ssa_f; subst.
    canonize_hyps.
    pose proof (validIntervalbounds_sound e Gamma (E:=E) (fVars:=fVars) L) as validIV_e.
    destruct valid_types_f
             as [[mG [find_mG [_ [_ [validt_e validt_f]]]]] _].
    assert (validRanges e A E (toRTMap (toRExpMap Gamma))) as valid_e.
    { apply validIV_e; try auto.
      set_tac. repeat split; auto.
      hnf; intros; subst.
      set_tac. }
    assert (NatSet.Equal (NatSet.add n (NatSet.union fVars dVars)) (NatSet.union fVars (NatSet.add n dVars))).
    { hnf. intros a; split; intros in_set; set_tac.
      - destruct in_set as [ ? | [? ?]]; try auto; set_tac.
        destruct H0; auto.
      - destruct in_set as [? | ?]; try auto; set_tac.
        destruct H as [? | [? ?]]; auto. }
    pose proof (validRanges_single _ _ _ _ valid_e) as valid_single_e.
    destruct valid_single_e as [iv_e [err_v [v [find_v [eval_e valid_bounds_e]]]]].
    rewrite find_v in *; inversion Heqo; subst.
    specialize (IHf Gamma (updEnv n v E) fVars (NatSet.add n dVars)) as IHf_spec.
    assert (validRangesCmd f A (updEnv n v E) (toRTMap (toRExpMap Gamma))).
    { eapply IHf_spec; eauto.
      - eapply ssa_equal_set. symmetry in H. apply H. apply H7.
      - intros v0 mem_v0.
        unfold updEnv.
        case_eq (v0 =? n); intros v0_eq.
        + rename R1 into eq_lo;
            rename R0 into eq_hi.
          rewrite Nat.eqb_eq in v0_eq; subst.
          exists v; eexists; eexists; repeat split; try eauto; simpl in *; lra.
        + apply dVars_sound.
          set_tac.
          destruct mem_v0 as [? | [? ?]]; try auto.
          rewrite Nat.eqb_neq in v0_eq.
          congruence.
      - hnf. intros x ? ?.
        unfold updEnv.
        case_eq (x =? n); intros case_x; auto.
        rewrite Nat.eqb_eq in case_x. subst.
        set_tac.
        assert (NatSet.In n fVars) as in_free
            by (apply preIntvVars_free; eapply preIntvVars_sound; eauto).
          (* by (destruct (fVars_valid n iv); auto; set_tac). *)
        exfalso. apply H6. set_tac.
      - intros x x_contained.
        set_tac.
        repeat split; try auto.
        + hnf; intros; subst. apply H1; set_tac.
        + hnf; intros. apply H1; set_tac. }
    (*
        destruct x_contained as [x_free | x_def].
        + destruct (types_valid x) as [m_x Gamma_x]; try set_tac.
          exists m_x.
          unfold updDefVars. case_eq (x =? n); intros eq_case; try auto.
          rewrite Nat.eqb_eq in eq_case.
          subst.
          exfalso; apply H6; set_tac.
        + set_tac.
          destruct x_def as [x_n | x_def]; subst.
          *  exists REAL; unfold updDefVars; rewrite Nat.eqb_refl; auto.
          * destruct x_def. destruct (types_valid x) as [m_x Gamma_x].
            { rewrite NatSet.union_spec; auto. }
            { exists m_x.
              unfold updDefVars; case_eq (x =? n); intros eq_case; try auto.
              rewrite Nat.eqb_eq in eq_case; subst.
              congruence. }
    { clear L R1 R0 R IHf.
        hnf. intros a a_freeVar.
        rewrite NatSet.diff_spec in a_freeVar.
        destruct a_freeVar as [a_freeVar a_no_dVar].
        apply usedVars_subset.
        simpl.
        rewrite NatSet.diff_spec, NatSet.remove_spec, NatSet.union_spec.
        repeat split; try auto.
        + hnf; intros; subst.
          apply a_no_dVar.
          rewrite NatSet.add_spec; auto.
        + hnf; intros a_dVar.
          apply a_no_dVar.
          rewrite NatSet.add_spec; auto. } *)
    { repeat split; try auto.
      - intros vR ?.
        assert (vR = v) by (eapply meps_0_deterministic; eauto); subst.
        auto.
      - apply validRangesCmd_single in H0.
        destruct H0 as [? [? [? [? [? ?]]]]].
        repeat eexists; eauto.
        econstructor; try eauto.
        + lra.
        + lra. }
  - unfold validIntervalboundsCmd in valid_bounds_f.
    pose proof (validIntervalbounds_sound e (E:=E) Gamma valid_bounds_f dVars_sound usedVars_subset) as valid_iv_e.
    destruct valid_types_f as [? ?].
    assert (validRanges e A E (toRTMap (toRExpMap Gamma))) as valid_e by (apply valid_iv_e; auto).
    split; try auto.
    apply validRanges_single in valid_e.
    destruct valid_e as [?[?[? [? [? ?]]]]]; try auto.
    simpl in *. repeat eexists; repeat split; try eauto; [econstructor; eauto| | ]; lra.
Qed.
*)
