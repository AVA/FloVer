From Flover
     Require Import Infra.RealSimps Infra.RationalSimps Infra.RealRationalProps
     Infra.Ltacs RealRangeArith RealRangeValidator RoundoffErrorValidator
     Environments TypeValidator FPRangeValidator ExpressionAbbrevs ResultChecker
     IntervalArithQ.
(*
From Flover
     Require Import Infra.RealRationalProps Environments ExpressionSemantics
     IntervalArithQ SMTArith RealRangeArith RealRangeValidator RoundoffErrorValidator
     ssaPrgs TypeValidator ErrorAnalysis ResultChecker.
*)

Require Import List.

Fixpoint resultLeq e (A1 A2: analysisResult) :=
  match e with
  | Unop _ e1 | Downcast _ e1 => resultLeq e1 A1 A2
  | Binop _ e1 e2 | Let _ _ e1 e2 => resultLeq e1 A1 A2 && resultLeq e2 A1 A2
  | Fma e1 e2 e3 => resultLeq e1 A1 A2 && resultLeq e2 A1 A2 && resultLeq e3 A1 A2
  | _ => true
  end &&
      match FloverMap.find e A1, FloverMap.find e A2 with
      | Some (ivA1, errA1), Some (ivA2, errA2) => isSupersetIntv ivA1 ivA2 && (Qleb errA1 errA2)
      | _, _ => false
      end.

Lemma resultLeq_sound e A1 A2 :
  resultLeq e A1 A2 = true ->
  exists iv1 err1 iv2 err2,
    FloverMap.find e A1 = Some (iv1, err1) /\
    FloverMap.find e A2 = Some (iv2, err2) /\
    isSupersetIntv iv1 iv2 = true /\
    err1 <= err2.
Proof.
  intros H.
  assert (
      match FloverMap.find e A1, FloverMap.find e A2 with
      | Some (ivA1, errA1), Some (ivA2, errA2) => isSupersetIntv ivA1 ivA2 && (Qleb errA1 errA2)
      | _, _ => false
      end = true).
  { unfold resultLeq in H. destruct e;
    apply andb_prop in H as [_ H]; auto. }
  Flover_compute.
  repeat eexists; auto.
  - unfold isSupersetIntv. now rewrite L0.
  - apply (reflect_iff _ _ (Z.leb_spec0 _ _)). auto.
Qed.

(* TODO: maybe put this somewhere else *)
Lemma approxEnv_empty_dVars A1 A2 Gamma fVars dVars E1 E2 :
  NatSet.Empty dVars ->
  approxEnv E1 Gamma A1 fVars dVars E2 ->
  approxEnv E1 Gamma A2 fVars dVars E2.
Proof.
  intros He H.
  induction H.
  - constructor.
  - econstructor; eauto.
  - exfalso. eapply He. set_tac.
Qed.

Definition checkSubdivsStep e absenv defVars Gamma (b: bool) (PA: precond * analysisResult) :=
  let (P, A) := PA in
  b && (RangeErrorChecker e A P (FloverMap.empty(SMTLogic * SMTLogic)) defVars Gamma) &&
    resultLeq e A absenv.

Definition checkSubdivs e absenv subdivs defVars Gamma :=
  fold_left (checkSubdivsStep e absenv defVars Gamma) subdivs true.

Lemma checkSubdivs_false_fp e absenv subdivs defVars Gamma :
  fold_left (checkSubdivsStep e absenv defVars Gamma) subdivs false = false.
Proof.
  induction subdivs as [|[P A] subdivs]; auto.
Qed.

Lemma checkSubdivs_sound e absenv subdivs defVars Gamma P A :
  checkSubdivs e absenv subdivs defVars Gamma = true ->
  In (P, A) subdivs ->
  RangeErrorChecker e A P (FloverMap.empty(SMTLogic * SMTLogic)) defVars Gamma = true /\
  resultLeq e A absenv = true.
Proof.
  intros H Hin.
  induction subdivs; cbn in Hin; auto.
  destruct Hin; subst; auto.
  - cbn in *.
    destruct (RangeErrorChecker e A P (FloverMap.empty (SMTLogic * SMTLogic)) defVars Gamma) eqn:?;
             try (rewrite checkSubdivs_false_fp in H; discriminate H).
    split; auto.
    destruct (resultLeq e A absenv) eqn: ?; auto.
    rewrite checkSubdivs_false_fp in H; auto.
  - apply IHsubdivs; auto. cbn in H.
    unfold checkSubdivs.
    destruct (checkSubdivsStep e absenv defVars Gamma true a) eqn:?; auto.
    rewrite checkSubdivs_false_fp in H. discriminate H.
Qed.

(*
Definition joinIntv iv1 iv2 : result (option intv) :=
  if isSupersetIntv iv2 iv1 then Succes (Some iv1) else
    if Qeqb (ivhi iv1) (ivlo iv2) then Succes (Some (ivlo iv1, ivhi iv2)) else Fail _ "intervals don't align".

Definition checkDimensionStep x (ivAcc: result (option intv)) P (* (ivNew: option intv) *) :=
  let ivNew := FloverMap.find (Var Q x) P in
  match ivAcc with
  | Succes ivAcc => match ivNew with
                 | Some ivNew => optionBind ivAcc (fun iv => joinIntv iv ivNew) (Succes (Some ivNew))
                 | None => Fail _ "var not found in precond"
                 end
  | f => f
  end.

Definition checkDimension x iv Ps :=
  match fold_left (checkDimensionStep x) Ps (Succes None) with
  | Succes (Some ivU) => if isSupersetIntv iv ivU then Succes true else Fail _ "var not covered"
  | Succes None => Fail _ "no subdivisions given"
  | Fail _ msg | FailDet msg _ => Fail _ msg
  end.

Definition checkAllDimensionsStep Ps b (p: expr Q * intv) :=
  match p with
  | (Var _ x, iv) => resultBind b (fun _ => checkDimension x iv Ps) (* b && checkDimension x iv Ps *)
  | _ => b
  end.

Definition checkAllDimensions P Ps :=
  fold_left (checkAllDimensionsStep Ps) P (Succes false).

Lemma checkAllDimensionsStep_fail_fp Ps P s :
  fold_left (checkAllDimensionsStep Ps) P (Fail bool s) = Fail bool s.
Proof.
  induction P as [|p P]; auto.
  destruct p as [e iv]. destruct e; auto.
Qed.

Lemma checkAllDimensionsStep_faildet_fp Ps P s b :
  fold_left (checkAllDimensionsStep Ps) P (FailDet s b) = FailDet s b.
Proof.
  induction P as [|p P]; auto.
  destruct p as [e iv]. destruct e; auto.
Qed.
*)

Fixpoint coverIntv iv intvs :=
  match intvs with
  | nil => false
  | intv :: nil => isSupersetIntv iv intv
  | intv :: intvs =>
    Qleb (ivlo intv) (ivlo iv) && coverIntv (ivhi intv, ivhi iv) intvs
  end.

Lemma coverIntv_sound iv intvs v :
  coverIntv iv intvs = true ->
  (Q2R (ivlo iv) <= v <= Q2R (ivhi iv))%R ->
  exists intv, In intv intvs /\ (Q2R (ivlo intv) <= v <= Q2R (ivhi intv))%R.
Proof.
  induction intvs as [|intv0 intvs] in iv |- *; [intros H; discriminate H|].
  destruct intvs as [|intv1 intvs]; cbn; intros H Hin.
  - exists intv0. split; auto. andb_to_prop H. canonize_hyps. cbn in *. lra.
  - destruct (Rle_or_lt v (Q2R (ivhi intv0))) as [Hle|Hlt].
    + andb_to_prop H.
      canonize_hyps.
      exists intv0. split; cbn; auto.
      cbn in *. lra.
    + andb_to_prop H.
      destruct (IHintvs (ivhi intv0, ivhi iv)) as [intv [inl Hin1]]; eauto.
      cbn in *. lra.
Qed.

Definition addToBucket x iv P :=
  match P with
  | nil => Fail _ "var not found in precond"
  | (e, ivE) :: P =>
    match Q_orderedExps.exprCompare e (Var Q x) with
    | Eq =>
      if isSupersetIntv iv ivE then Succes (iv, Some P) else Succes (ivE, None)
    | _ => Fail _ "var not found in precond"
    end
  end.

Fixpoint mergeDim_aux x Ps iv b :=
  match Ps with
  | nil => Succes ((iv, rev b) :: nil)
  | P :: Ps =>
    match addToBucket x iv P with
    | Succes (ivN, Some P') => mergeDim_aux x Ps iv (P' :: b)
    | Succes (ivN, None) => resultBind (mergeDim_aux x Ps ivN nil)
                                      (fun rest => Succes ((iv, rev b) :: rest))
    | Fail _ msg | FailDet msg _ => Fail _ msg
    end
  end.

(* x := current variable being checked
   Ps := list of preconditions for the subdivision 
*)
Definition mergeDim x Ps :=
  match Ps with
  | nil => Fail _ "precond not found"
  | nil :: _ => Fail _ "var not found in precond"
  | ((e, iv) :: P) :: _ => mergeDim_aux x Ps iv nil
  end.

(* x := current variable being checked
   Px := global precondition for x
   Ps := list of preconditions for the subdivision 
   last_iv := option of last seen interval
*)
(*
Definition checkDim x Px Ps curr_iv :=
  match Ps with
  | nil => (* check Px upper bounded by curr_iv *)
  | Ps1::Ps_rem => 
    let iv_x := find (fun e => isEq (Q_orderedExps.exprCompare (Var Q x) e)) Ps1 in
    match iv_x with
    | None => Fail _ "var not found"
    | Some iv_x => 

                     
  match Ps with
  | nil => Fail _ "precond not found"
  | nil :: _ => Fail _ "var not found in precond"
  | ((e, iv) :: P) :: _ => checkDim_aux x Px Ps iv nil
  end.
 *)

Lemma mergeDim_aux_sound x Ps iv b res :
  mergeDim_aux x Ps iv b = Succes res ->
  forall ivb Psb P,
    In (ivb, Psb) res ->
    In P Psb ->
    (In P b -> forall iv', ~ In  ((Var Q x, iv') :: P) Ps \/ isSupersetIntv ivb iv' = false) /\
    (~ In P b -> exists iv', In ((Var Q x, iv') :: P) Ps /\ isSupersetIntv ivb iv' = true).
Proof.
  induction Ps as [|P Ps] in iv, b, res |- *.
  - cbn; intros H; inversion H. intros * Hin HinR.
    inversion Hin; inversion H0; subst. admit. (* exfalso. now eapply Hnin, in_rev. *)
  - cbn; intros H ivb Psb Pb inRes inPsb.
    destruct (addToBucket x iv P) eqn:Heq; try discriminate H.
    destruct p as [ivN [P'|]].
    + (* specialize (IHPs _ _ _ H ivb Psb Pb inRes inPsb). *)
      (* P = (Var _ x, ivE) :: P' by addToBucket_sound *)
      (* assert (Pb = P') as HPb by admit; subst. *)

      assert (Pb <> P') as Hneq by admit.
      edestruct IHPs as [iv' [Hin Hsub]]; eauto.
      intros [<- | inb]; auto.
    + destruct (mergeDim_aux x Ps ivN nil) eqn:Hrec; try discriminate H.
      cbn in *.
      symmetry in H.
      inversion H; subst.
      destruct inRes as [beq|inL].
      * exfalso. inversion beq; subst. now apply Hnin, in_rev.
      * specialize (IHPs _ _ _ Hrec ivb Psb Pb inL inPsb).
        destruct IHPs as [iv' [Hin Hsub]]; eauto.
Admitted.

Lemma mergeDim_sound x Ps res :
  mergeDim x Ps = Succes res ->
  forall ivb Psb P,
    In (ivb, Psb) res ->
    In P Psb ->
    exists iv', In ((Var Q x, iv') :: P) Ps /\ isSupersetIntv ivb iv' = true.
Proof.
  intros chk.
  destruct Ps as [|[|[e iv] P] Ps]; try discriminate chk.
  intros.
  eapply mergeDim_aux_sound; eauto.
Qed.

Fixpoint covers (P: list (expr Q * intv)) Ps :=
  match P with
  | nil => match Ps with nil :: nil => true | _ => false end
  | (Var _ x, iv) :: P =>
    match mergeDim x Ps with
    | Succes res =>
      let intvs := map fst res in
      let buckets := map snd res in
      forallb (covers P) buckets && coverIntv iv intvs
    | _ => false
    end
  | _ :: P => false
  end.
  
Lemma in_subdivs_intv P Ps E :
  covers P Ps = true ->
  (forall (x : nat) (iv : intv),
      In (Var Q x, iv) P ->
      exists vR : R, E x = Some vR /\ (Q2R (fst iv) <= vR <= Q2R (snd iv))%R) ->
  exists P', In P' Ps /\
        (forall (x : nat) (iv : intv),
            In (Var Q x, iv) P' ->
            exists vR : R, E x = Some vR /\ (Q2R (fst iv) <= vR <= Q2R (snd iv))%R).
Proof.
  induction P as [|[e iv] P] in Ps |- *; intros chk H.
  - destruct Ps as [|P' Ps]; cbn in chk; try discriminate chk.
    exists P'; split; [now constructor |].
    destruct P'; try discriminate chk. auto.
  - cbn in chk.
    destruct e; try discriminate chk.
    destruct (mergeDim n  Ps) as [res| |] eqn:Hres; try discriminate chk.
    apply andb_prop in chk as [rec curr].
    destruct (H n iv) as [v [Hf Hc]]; try now constructor.
    destruct (coverIntv_sound _ _ curr Hc) as [iv' [Hin Hc']].
    apply in_map_iff in Hin as [[iv0 b] [Hiv0 Hin]].
    cbn in *; subst.
    eapply forallb_forall in rec; [| apply in_map_iff; eauto].
    destruct (IHP _ rec) as [P' [inb valid_P']]; auto.
    destruct (mergeDim_sound _ _ Hres _ _ _ Hin inb) as [iv0 [inPs Hsub]].
    eexists; split; eauto.
    intros x iv1 [Heq| inP'].
    + inversion Heq; subst.
      eexists; split; eauto.
      Flover_compute.
      canonize_hyps.
      cbn in *. lra.
    + auto.
Qed.

Definition checkCoverage (subdivs: list precond) (P: precond) :=
  let Piv := FloverMap.elements (fst P) in
  let Ps := map (fun P => FloverMap.elements (fst P)) subdivs in
  covers Piv Ps && true. (* TODO check additional constraints *)

Lemma in_subdivs (subdivs: list (precond * analysisResult)) E P :
  checkCoverage (map fst subdivs) P = true ->
  eval_precond E P ->
  exists PA, In PA subdivs /\ eval_precond E (fst PA).
Proof.
  intros H [Pintv Hadd].
  andb_to_prop H.
  eapply in_subdivs_intv in L; eauto.
  destruct L as [Pl1 [Hin H]].
  apply in_map_iff in Hin as [P1 [Heq1 Hin]].
  apply in_map_iff in Hin as [PA1 [Heq2 Hin]].
  exists PA1. split; auto.
  split.
  - rewrite Heq2.
    now rewrite <- Heq1 in H.
  - rewrite Heq2. admit.
Admitted.

(* TODO: check precond sound wrt. subdivs *)
(* TODO: merge hd and tl again *)
(** Interval subdivisions checking function **)
Definition SubdivsChecker (e: expr Q) (absenv: analysisResult)
           (P: precond) hd tl (defVars: FloverMap.t  mType) Gamma :=
  validSSA e (preVars P) &&
           checkCoverage (map fst (hd :: tl)) P &&
           checkSubdivs e absenv (hd :: tl) defVars Gamma.

(**
   Soundness proof for the interval subdivs checker.
**)
Theorem subdivs_checking_sound (e: expr Q) (absenv: analysisResult) P hd_subdivs tl_subdivs defVars Gamma:
  forall (E1 E2: env) DeltaMap,
    (forall (e': expr R) (m': mType),
        exists d: R, DeltaMap e' m' = Some d /\ (Rabs d <= mTypeToR m')%R) ->
    eval_precond E1 P ->
    getValidMap defVars e (FloverMap.empty mType) = Succes Gamma ->
    SubdivsChecker e absenv P hd_subdivs tl_subdivs defVars Gamma = true ->
    exists Gamma,
      approxEnv E1 (toRExpMap Gamma) absenv (freeVars e) NatSet.empty E2 ->
      exists iv err vR vF m,
        FloverMap.find e absenv = Some (iv, err) /\
        eval_expr E1 (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) vR REAL /\
        eval_expr E2 (toRExpMap Gamma) DeltaMap (toRExp e) vF m /\
        (forall vF m,
            eval_expr E2 (toRExpMap Gamma) DeltaMap (toRExp e) vF m ->
            (Rabs (vR - vF) <= Q2R err))%R.
Proof.
  intros * deltas_matched P_valid valid_typeMap chk.
  apply andb_prop in chk as [chk valid_subdivs].
  apply andb_prop in chk as [valid_ssa valid_cover].
  apply (in_subdivs (hd_subdivs :: tl_subdivs)) in P_valid as [[P' A] [in_subdivs P_valid]]; auto.
  (* preVars P == preVars P' should hold *)
  assert (validSSA e (preVars P') = true) as valid_ssa' by admit.
  pose proof (checkSubdivs_sound  e _ _ _ _ _ _ valid_subdivs in_subdivs) as [range_err_check A_leq].
  assert (ResultChecker e A P' (FloverMap.empty(SMTLogic * SMTLogic))  defVars Gamma = true) as res_check
    by (unfold ResultChecker; now rewrite valid_ssa', range_err_check).
  exists Gamma; intros approxE1E2.
  assert (approxEnv E1 (toRExpMap Gamma) A (freeVars e) NatSet.empty E2) as approxE1E2'
      by (eapply approxEnv_empty_dVars; eauto).
  assert (validRanges e A E1 (toRTMap (toRExpMap Gamma))) as validRange.
  { eapply result_checking_sound; eauto.
    admit. (* unsat_queries of empty map *) }
  assert (validErrorBounds e E1 E2 A Gamma DeltaMap) as Hsound.
  { eapply result_checking_sound; eauto.
    admit. (* unsat_queries of empty map *) }
  eapply validRanges_single in validRange.
  destruct validRange as [iv [err [vR [Hfind [eval_real validRange]]]]].
  eapply validErrorBounds_single in Hsound; eauto.
  destruct Hsound as [[vF [mF eval_float]] err_bounded]; auto.
  destruct (resultLeq_sound _ _ _ A_leq) as [iv1 [err1 [iv2 [err2 H]]]].
  destruct H as [F1 [F2 [sub err_leq]]].
  exists iv2, err2, vR, vF, mF; repeat split; auto.
  assert (err = err1) by congruence; subst.
  apply Qle_Rle in err_leq.
  intros vF0 m Heval.
  specialize (err_bounded vF0 m Heval).
  lra.
Admitted.