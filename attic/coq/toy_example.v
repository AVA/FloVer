(**
  Toy Example to understand what certificate we will need for a given program
 **)
Require Import Coq.Reals.Reals.
Require Import Flover.flover_lang Flover.exps Flover.abs_err.

Definition prg :cmd R :=
  Ret R (Binop Mult (Const (3%R)) (Var R 1)).

(*
old bigger program
Definition prg :cmd R :=
  Let R 1 (Binop Mult (Const (2%R)) (Var R 2))
      (Ret R (Binop Mult (Const (3%R)) (Var R 1))).
*)

Definition abs_err :R := 1%R.
(* TODO: Prove exp syntactic eq decidable *)
Definition ienv := fun x:exp R =>
                     match x with
                     | Var _ n => if n =? 1 then ((1%R,2%R),0%R) else if n =? 0 then ((0%R,0%R),abs_err) else ((0%R,0%R),1%R)
                     | _ =>  ((0%R,0%R),1%R)
                     end.

Lemma Rmax_eq:
  forall a, Rmax a a = a.
Proof.
  intros a.
  apply Rmax_left.
  unfold Rle.
  right; auto.
Qed.

Lemma Rmin_eq:
  forall a, Rmin a a = a.
Proof.
  intros a.
  apply Rmin_left.
  unfold Rle.
  right; auto.
Qed.

Lemma foo:
  AbsErrPrg prg ienv (ErrFromEnv ienv (Binop Mult (Const (3%R)) (Var R 1))).
Proof.
  unfold prg.
  apply AbsErrRet.
  apply AbsErrBinop; simpl.
  - apply AbsErrConst; simpl.
    unfold isValidErr.
    simpl.
    pose proof Rplus_ne as Rplus_0s.
    destruct (Rplus_0s machineEpsilon).
    rewrite H0.
    rewrite Rminus_0_l.
    rewrite Rabs_Ropp.
    erewrite Rmax_eq.
    unfold machineEpsilon.
     .
  - apply AbsErrVar.
    unfold isValidErr.
    simpl.
    (* There is a flaw on how we handle variable errors currently as far as it seems *)
     .
  - split.
    + unfold isValidIntv.
      split.
      * simpl.
        repeat rewrite Rmult_0_l.
        repeat rewrite Rmult_0_r.
        unfold interval_arith.max4, interval_arith.min4.
        repeat erewrite Rmin_eq.
        repeat erewrite Rmax_eq.
        repeat erewrite Rplus_0_r.
        repeat erewrite Rmin_eq.
        repeat erewrite Rmax_eq.
        unfold Rge.
        right; auto.
      * simpl.
        unfold interval_arith.max4, interval_arith.min4.
        repeat rewrite Rmult_0_l.
        repeat rewrite Rmult_0_r.
        repeat erewrite Rmin_eq.
        repeat erewrite Rmax_eq.
        repeat erewrite Rplus_0_r.
        repeat erewrite Rmin_eq.
        repeat erewrite Rmax_eq.
        repeat erewrite Rmin_eq.
        unfold Rle.
        right; auto.
    +

Lemma bound_correct (env:nat->R) (env_R:nat->R) (env_F:nat->R):
  bstep prg env (0%R) (Nop R) env_R ->
  bstep prg env machineEpsilon (Nop R) env_F ->
  AbsErrPrg prg ienv (getErr ienv (Var R 0)) ->
  (Rabs ((env_R (0%nat)) - (env_F (0%nat))) <= getErr ienv (Var R 0))%R.
Proof.
  intros eval_real_valued eval_float_valued abs_err_valid.
  unfold getErr; simpl.
  unfold prg in *.
  inversion eval_real_valued; inversion eval_float_valued; subst.
  inversion H0; subst.
  inversion H6; subst.
  (* real valued eval *)
  apply Rabs_0_impl_eq in H3; subst.
  apply Rabs_0_impl_eq in H1; subst.
  unfold perturb in H6.
  rewrite Rplus_0_r in H6.
  rewrite Rmult_1_r in H6.
  unfold perturb in H0; rewrite Rplus_0_r in H0. repeat rewrite Rmult_1_r in H0.
  inversion H7 as [ v v_one env_eq | | ]; subst.
  inversion H0.
  apply Rabs_0_impl_eq in H4.
  subst.
  unfold perturb in H3; unfold perturb, upd_env; simpl.
  rewrite Rplus_0_r.
  repeat rewrite Rmult_1_r.
  (*cleanup for readability *)
  clear H6 H7 H0 H8 H0 H3.
  clear H9.
  inversion  H5 as [ | | op c d e].
  (* float valued eval *)

Qed.
