(**
 Formalization of the Abstract Syntax Tree of a subset used in the Flover framework
 **)
Require Import Coq.Reals.Reals.
Require Import Flover.Infra.Abbrevs Flover.Expressions.
(**
  Next define what a program is.
  Currently no loops, only conditionals and assignments
  Final return statement
**)
Inductive cmd (V:Type) :Type :=
Let: nat -> exp V -> cmd V -> cmd V
| Ite: bexp V -> cmd V -> cmd V -> cmd V
| Ret: exp V -> cmd V
| Nop: cmd V.

(**
   Small Step semantics for Flover language, parametric by evaluation function.
**)
Inductive sstep : cmd R -> env_ty -> R -> cmd R -> env_ty -> Prop :=
  let_s x e s env v eps:
    eval_exp eps env e v ->
    sstep (Let R x e s) env eps s (updEnv x v env)
 |condtrue_s c s t eps env:
    bval eps env c True ->
         sstep (Ite R c s t) env eps s env
 |condfalse_s c s t eps env:
    (~bval eps env c True) ->
    sstep (Ite R c s t) env eps s env
 |ret_s e v eps env:
    eval_exp eps env e v ->
    sstep (Ret R e) env eps (Nop R) (updEnv 0 v env).
(**
  Analogously define Big Step semantics for the Flover language,
  parametric by the evaluation function
 **)
Inductive bstep : cmd R -> env_ty -> R -> cmd R -> env_ty -> Prop :=
  let_b x e s s' env v eps env2:
    eval_exp eps env e v ->
    bstep s (updEnv x v env) eps s' env2 ->
    bstep (Let R x e s) env eps s' env2
 |condtrue_b c s1 s2 t eps env env2:
    bval eps env c True ->
    bstep s1 env eps s2 env2 ->
    bstep (Ite R c s1 t) env eps s2 env2
 |condgalse_b c s t1 t2 eps env env2:
    (~ bval eps env c True) ->
    bstep t1 env eps t2 env2 ->
    bstep (Ite R c s t1) env eps t2 env2
 |ret_b e v eps env:
    eval_exp eps env e v ->
    bstep (Ret R e) env eps (Nop R) (updEnv 0 v env).