Lemma Qred_eq_frac:
  forall q1 q2,
    Is_true (Qleb q1 q2) <-> Is_true (Qleb (Qred q1) q2).
Proof.
  intros; split; intros.
  - apply Is_true_eq_left. apply Is_true_eq_true in H.
    unfold Qleb in *; apply Qle_bool_iff. apply Qle_bool_iff in H.
    rewrite Qred_correct; auto.
  - apply Is_true_eq_left. apply Is_true_eq_true in H. unfold Qleb in *.
    rewrite Qle_bool_iff.
    rewrite Qle_bool_iff in H.
    rewrite Qred_correct in H.
    auto.
Qed.