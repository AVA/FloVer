Require Import Flover.CertificateChecker.

(*
[  Info  ]
[  Info  ] Starting specs preprocessing phase
[  Info  ] Finished specs preprocessing phase
[  Info  ]
[  Info  ]
[  Info  ] Starting range-error phase
[  Info  ] Machine epsilon 1.1102230246251565E-16
[  Info  ] (1657)/(5): [331.4, 331.4],(1657)/(45035996273704960)
[  Info  ] u: [-100.0, 100.0],(25)/(2251799813685248)
[  Info  ] ((1657)/(5) * u).propagatedError = [-7.358558207215538E-12, 7.358558207215538E-12]
[  Info  ] ((1657)/(5) * u): [-33140.0, 33140.0],(2016477162795049297422773199443075165)/(182687704666362864775460604089535377456991567872)
[  Info  ] Finished range-error phase
[  Info  ]
[  Info  ] Starting info phase
[  Info  ] doppler
[  Info  ] abs-error: (2016477162795049297422773199443075165)/(182687704666362864775460604089535377456991567872), range: [-33140.0, 33140.0],
 *)

(** TODO MOVE TO FILE
Ltac prove_constant := unfold realFromNum, negativePower; interval.
Ltac rw_asm H Lem := rewrite Lem; rewrite Lem in H.

Lemma bounds_abs_maxAbs (a:R) (b:R) (x:R) :
  contained x (mkInterval a b) ->
  (Rabs x <= Rmax (Rabs a) (Rabs b))%R.
Proof.
  intros [a_le_x x_leq_b].
  apply RmaxAbs; auto.
Qed.

Lemma Rplus_minus_general:
  forall a b c,
    (a + b + (- a) + c = b + c)%R.
Proof.
  intros a b c.
  repeat rewrite Rplus_assoc.
  assert (b + (-a + c) = (b + c) + - a)%R.
  - rewrite Rplus_comm.
    rewrite Rplus_assoc.
    assert (c + b = b + c)%R by (rewrite Rplus_comm; auto).
    rewrite H.
    rewrite Rplus_comm; auto.
  - rewrite H.
    rewrite <- Rplus_assoc.
    rewrite (Rplus_assoc a (b + c)).
    rewrite <- Rsub_eq_Ropp_Rplus.
    rewrite Rplus_minus; auto.
Qed.

 **)


Definition u:nat := 1.
(** 1655/5 = 331; 0,4 = 2/5 **)
Definition cst1:Q := 1657 # 5.

(** Define abbreviations **)
Definition varU:exp Q := Param Q u.
Definition valCst:exp Q := Const cst1.
Definition valCstAddVarU:exp Q := Binop Mult valCst varU.

(** Error values **)
Definition errCst1 := (1657)#(45035996273704960).
Definition errVaru := (25)#(2251799813685248).
Definition lowerBoundMultUCst:Q := - (33140#1).
Definition upperBoundMultUCst:Q := (33140#1).
Definition errMultUCst :=(2016477162795049297422773199443075165)#(182687704666362864775460604089535377456991567872).

Definition absEnv : analysisResult :=
  fun (e:exp Q) =>
    match e with
    |Const n => (cst1,cst1,errCst1)
    |Param v => (-(100#1),(100#1),errVaru)
    |Binop _ _ _ => (lowerBoundMultUCst,upperBoundMultUCst,errMultUCst)
    | _ => (0,0,0) end.

Definition precondition :precond := fun _ => (-(100#1),(100#1)).

Definition machineEpsilon := (1#(2^53)).

Eval compute in CertificateChecker valCst absEnv precondition.
Eval compute in CertificateChecker varU absEnv precondition.
Eval compute in CertificateChecker valCstAddVarU absEnv precondition.

(* OLD STUFF BEGINS HERE
(** The added assertion becomes the precondition for us **)
Definition precondition := fun env:nat -> R => (- 100 <= env u)%R /\ (env u <= 100)%R.

(** As stated, we need to define the absolute environment now as an inductive predicate **)
Inductive absEnv : abs_env :=
  theCst: absEnv valCst (mkInterval cst1 cst1) errCst1
|theVar: absEnv varU (mkInterval (- 100) (100)) errVaru
|theAddition: absEnv valCstMultVarU (mkInterval lowerBoundMultUCst upperBoundMultUCst) errMultUCst.


(** Show only the result for the expression as a test,
  since lifting to a program will only add additional predicate unfolding in this example
 **)
Goal forall (cenv:nat->R) (vR:R) (vF:R),
       precondition cenv ->
       eval_exp (0)%R cenv valCstMultVarU vR ->
       eval_exp machineEpsilon cenv valCstMultVarU vF ->
       AbsErrExp valCstMultVarU absEnv errMultUCst /\
       (Rabs (vR - vF) <= errMultUCst)%R.
Proof.
  intros cenv vR vF precond eval_real eval_float.
  rewrite Rabs_simplify.
  split.
  (* Proving Absolute Error correct *)
  - unfold valCstMultVarU.
    apply (AbsErrBinop _ _ (mkInterval cst1 cst1) errCst1
                       _ (mkInterval (- 100) (100)) errVaru
                       (mkInterval lowerBoundMultUCst upperBoundMultUCst) errMultUCst);
      [ constructor | constructor | constructor | | | ].
    + unfold valCst.
      apply (AbsErrConst cst1 (mkInterval cst1 cst1) errCst1); [constructor | ].
      unfold isSoundErr; simpl.
      unfold errCst1, cst1, machineEpsilon.
      unfold rationalFromNum, negativePower.
      rewrite Rmax_left; [ |apply Req_le; auto].
      assert (Rabs 100 = 100)%R by (unfold Rabs; destruct Rcase_abs; lra).
      rewrite H.
      (* TODO: What about vlaues that are actually floats ? *)  .
    + apply (AbsErrParam u (mkInterval (- 100) (100)) errVaru); [constructor | ].
      unfold isSoundErr; simpl.
      unfold Expressions.machineEpsilon, errVaru.
      unfold rationalFromNum.
      unfold negativePower.
      assert (Rabs (-100) = 100%R) by (unfold Rabs; destruct Rcase_abs; lra).
      rewrite H.
      assert (Rabs 100 = 100)%R by (unfold Rabs; destruct Rcase_abs; lra).
      rewrite H0.
      rewrite Rmax_left; [ | apply Req_le; auto].
      interval.
    + unfold isSoundErrBin, valCst; split; try split; simpl; [unfold min4| unfold max4| ].
      * assert (Rmin (cst1 * - 100) (cst1 * 100) = (cst1 * - 100))%R by (unfold cst1; apply Rmin_left; interval).
        rewrite H.
        rewrite Rmin_comm in H.
        rewrite H.
        rewrite Rmin_left; [ | apply Req_le; auto].
        unfold cst1, lowerBoundMultUCst.
        prove_constant. (* Here it works... *)
      * assert (Rmax (cst1 * - 100) (cst1 * 100) = (cst1 * 100))%R by (unfold cst1; apply Rmax_right; interval).
        assert (Rmax (cst1 * 100) (cst1 * 100) = cst1 * 100)%R by (apply Rmax_left; apply Req_le; auto). (* TODO: lemma *)
        rewrite H.
        rewrite H0.
        rewrite H.
        unfold cst1, upperBoundMultUCst.
        field_simplify.
        prove_constant. (* And here too *)
      * unfold isSoundErr; simpl.
        unfold lowerBoundMultUCst, upperBoundMultUCst, errMultUCst.
        unfold Expressions.machineEpsilon.
        assert (- rationalFromNum 10000 0 0 <= 0)%R by prove_constant.
        assert (0 <= rationalFromNum 10000 0 0) %R by prove_constant.
        rewrite Rabs_left1; auto.
        rewrite Rabs_pos_eq; auto.
        rewrite Ropp_involutive.
        rewrite Rmax_left; [ | apply Req_le; auto].
        prove_constant.

  (* Semantic judgement  Rabs_triang_gen, Rabs_triang! triangle property? *)
  - inversion eval_real as
        [ | | | op e1 e2 v1 v2 delta delta_leq_0 eval_cst1 eval_param_u [op_eq e1_eq e2_eq] vR_eq];
      subst.
    rewrite delta_0_deterministic in eval_real; auto.
    inversion eval_cst1; subst.
    rewrite delta_0_deterministic in eval_real; auto.
    inversion eval_param_u; subst.
    repeat rewrite delta_0_deterministic; auto.
    rewrite delta_0_deterministic in eval_real , eval_param_u, eval_cst1; auto.
    clear delta delta0 delta1 delta_leq_0 H0 H1.
    inversion eval_float; subst.
    inversion H4; subst.
    inversion H5; subst.
    pose proof (const_abs_err_bounded _ _ _ _ eval_cst1 H4).
    pose proof (param_abs_err_bounded _ _ _ _ eval_param_u H5).
    unfold eval_binop.
    unfold perturb in *; simpl in *.
    rewrite Rabs_err_simpl in *.
    repeat rewrite Rmult_plus_distr_l.
    repeat rewrite Rmult_1_r.
    repeat rewrite Rmult_plus_distr_r.
    rewrite Rsub_eq_Ropp_Rplus.
    repeat rewrite Ropp_plus_distr.
    repeat rewrite <- Rplus_assoc.
    rewrite Rplus_assoc at 1.
    repeat rewrite Rplus_minus_general.
    rewrite <- Rplus_assoc.
    assert (forall a b, a + - a + b = b)%R by (intros; lra).
    erewrite H6.
    repeat rewrite <- Rsub_eq_Ropp_Rplus.
    (** Lots of subtractions and we have positive numbers **)
    right.
    split.
    + eapply Rge_trans.
      eapply Fcore_Raux.Rabs_le_inv in H0; eapply Fcore_Raux.Rabs_le_inv in H2.
      destruct H2, H0.

    (* TODO *)
      (*
    assert (
        Rabs
          (- (cst1 * delta0) + - (cenv u * delta1) + - (cst1 * delta) + - (cst1 * delta0 * delta) + - (cenv u * delta) +
           - (cenv u * delta1 * delta)) <=
         Rabs (cst1 * delta0) + Rabs (cenv u * delta1) + Rabs (cst1 * delta) + Rabs (cst1 * delta0 * delta) +
         Rabs (cenv u * delta) + Rabs (cenv u * delta1 * delta)
        )%R.
  - eapply Rle_trans.
    eapply Rabs_triang.
    assert (Rabs (- (cst1 * delta0) + - (cenv u * delta1) + - (cst1 * delta) + - (cst1 * delta0 * delta) + - (cenv u * delta)) <= Rabs (- (cst1 * delta0)) + Rabs (- (cenv u * delta1)) + Rabs (- (cst1 * delta)) + Rabs (- (cst1 * delta0 * delta)) + Rabs (- (cenv u * delta)))%R.
    + eapply Rle_trans.
      eapply Rabs_triang.
      assert (Rabs (- (cst1 * delta0) + - (cenv u * delta1) + - (cst1 * delta) + - (cst1 * delta0 * delta)) <= Rabs (- (cst1 * delta0)) + Rabs (- (cenv u * delta1)) + Rabs (- (cst1 * delta)) + Rabs (- (cst1 * delta0 * delta)))%R.
      * eapply Rle_trans.
        eapply Rabs_triang.
        assert (Rabs (- (cst1 * delta0) + - (cenv u * delta1) + - (cst1 * delta)) <= Rabs (- (cst1 * delta0)) + Rabs (- (cenv u * delta1)) + Rabs (- (cst1 * delta)))%R.
        {
          eapply Rle_trans.
          eapply Rabs_triang.
          pose proof (Rabs_triang (- (cst1 * delta0)) (- (cenv u * delta1))).
          eapply Rplus_le_compat; [auto | apply Req_le; auto].
        }
        {
          eapply Rplus_le_compat; [auto | apply Req_le; auto].
        }
      * eapply Rplus_le_compat; [auto | apply Req_le; auto].
    +  repeat rewrite Rabs_Ropp in H7; auto.
       eapply Rle_trans.
       eapply Rplus_le_compat_r.
       apply H7.
      eapply Rplus_le_compat; [auto | apply Req_le; auto].

      rewrite Rabs_Ropp; auto.
    + eapply Rle_trans. apply H7.
      repeat rewrite Rplus_assoc.
      eapply Rle_trans.
      apply Rplus_le_compat. apply H.
      apply Req_le; auto.
      eapply Rle_trans.
      eapply Rplus_le_compat.
      eapply Req_le; auto.
      eapply Rplus_le_compat.
      apply H3.
      apply Req_le; auto.
      rewrite (Rabs_mult (cst1 * delta0) _).
      rewrite (Rabs_mult (cenv u * delta1) _).
      rewrite (Rabs_mult cst1 delta).
      rewrite (Rabs_mult (cenv u) delta).

      (* Now prove interval bounds *)
      unfold precondition in precond.
      destruct precond.
      assert (contained (cenv u) (mkInterval (-100) (100))) as uContained by (split; auto).
      assert (contained (cst1) (mkInterval cst1 cst1)) as cst1Contained by (apply validPointInterval).
      assert (contained (cenv u + cst1) (addInterval  (mkInterval (-100) (100)) (mkInterval cst1 cst1)))
        as containedAdd
          by (apply interval_addition_valid; auto).
      unfold contained in *; simpl in *.
      destruct uContained, cst1Contained, containedAdd.
      assert (Rabs cst1 = cst1) by (assert (0 <= cst1)%R by (unfold cst1; interval); rewrite Rabs_pos_eq; auto).
      repeat rewrite Rabs_mult.
      repeat rewrite H15.
      assert (Rabs (cenv u) <= Rmax (Rabs (-100)) (Rabs (100)))%R by (apply RmaxAbs; auto).
      assert (Rabs (-100) = 100)%R by (unfold Rabs; destruct Rcase_abs; lra).
      assert (Rabs 100 = 100)%R by (unfold Rabs; destruct Rcase_abs; lra).
      rewrite H17, H18 in H16.
      rewrite Rmax_left in H16; [ | lra].
      assert (forall eps:R, 0 <= eps -> Rabs (cenv u) * eps <= 100 * eps)%R by (intros; apply Rmult_le_compat_r; auto).
      assert (cst1 * Rabs delta0 * Rabs delta <= cst1 * machineEpsilon * machineEpsilon)%R.
      * assert (cst1 * Rabs delta0 <= cst1 * machineEpsilon)%R by (apply Rmult_le_compat_l; [unfold cst1, rationalFromNum, negativePower; interval | auto]).
        repeat rewrite Rmult_assoc.
        apply Rmult_le_compat_l; [unfold cst1; prove_constant | ].
        apply Rmult_le_compat; auto using Rabs_pos.
      * assert (Rabs (cenv u) * Rabs delta1 * Rabs delta <= Rabs (cenv u) * machineEpsilon * machineEpsilon)%R.
        repeat rewrite Rmult_assoc.
        apply Rmult_le_compat_l; try auto using Rabs_pos.
        apply Rmult_le_compat; auto using Rabs_pos.
        assert (Rabs (cenv u) * Rabs delta1 * Rabs delta <= 100 * machineEpsilon * machineEpsilon)%R.
        {
          eapply Rle_trans.
          apply H21.
          rewrite Rmult_assoc.
          rewrite (Rmult_assoc 100 _).
          apply Rmult_le_compat_r; [unfold machineEpsilon; prove_constant | auto ].
        }
        {
          (* bound manipulation *)
          eapply Rle_trans.
          eapply Rplus_le_compat.
          apply Req_le; auto.
          eapply Rplus_le_compat.
          apply H19.
          unfold machineEpsilon; prove_constant.
          eapply Rplus_le_compat.
          eapply Rmult_le_compat_l.
          unfold cst1; prove_constant.
          apply H2.
          eapply Rplus_le_compat.
          apply H20.
          eapply Rplus_le_compat.
          eapply Rmult_le_compat; try auto using Rabs_pos.
          apply H16.
          apply H2.
          apply H22.
          unfold cst1, errAddUCst, machineEpsilon; prove_constant.
        }
Qed. *)

Admitted.
*)
