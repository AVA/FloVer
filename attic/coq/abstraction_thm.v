Require Import Coq.Reals.Reals.
Require Import Flover.Infra.abbrevs Flover.flover_lang Flover.abs_err Flover.exps.

(**
  Notes:
  - We need to relate the concrete environment and the abstract environment
  - we need to relate the environment with which the float and the real valued execution are done
**)

Theorem AbstractionTheorem (s:cmd R) (env:nat->R) (env_R:nat->R) (env_F:nat->R) (abserr:err) (absenv:abs_env) :
  bstep s env 0%R (Nop R) env_R
  ->bstep s env machineEpsilon (Nop R) env_F
  ->AbsErrPrg s absenv abserr
  -> (Rabs (env_R 0%nat - env_F 0%nat) <= abserr)%R.
Proof.
  intros term_real term_float abs_err_valid.

  revert term_real term_float.
  revert env env_R env_F.
  induction abs_err_valid.
  - intros envc env_R env_F bstep_R bstep_fl.
    inversion bstep_R; subst.