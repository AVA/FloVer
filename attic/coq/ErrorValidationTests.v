 Definition u:nat := 1.
  (** 1655/5 = 331; 0,4 = 2/5 **)
  Definition cst1:Q := 1657 # 5.

(** Define abbreviations **)
  Definition varU:exp Q := Param Q u.
  Definition valCst:exp Q := Const cst1.
  Definition valCstAddVarU:exp Q := Binop Plus valCst varU.

  (** Error values **)
  Definition errCst1 :Q := rationalFromNum (36792791036077685#1) 16 14.
  Definition errVaru := rationalFromNum (11102230246251565#1) 16 14.
  Definition lowerBoundAddUCst:Q := 1157 # 5.
  Definition upperBoundAddUCst:Q := 2157 # 5.
  Definition errAddUCst := rationalFromNum (9579004256465851#1) 15 14.

  (** The added assertion becomes the precondition for us **)
  Definition precondition := fun env:nat -> R => (- 100 <= env u)%R /\ (env u <= 100)%R.

  (** As stated, we need to define the absolute environment now as an inductive predicate
  Inductive absEnv : abs_env :=
    theCst: absEnv valCst (mkIntv cst1 cst1) errCst1
   |theVar: absEnv varU (mkIntv (- 100) (100)) errVaru
   |theAddition: absEnv valCstAddVarU (mkIntv lowerBoundAddUCst upperBoundAddUCst) errAddUCst. **)

  Definition validConstant := Eval compute in validErrorbound (valCst) (fun x => ((cst1,cst1),errCst1)).
  Definition validParam := Eval  compute in validErrorbound (varU) (fun x => (((-100) # 1,100 # 1),errVaru)).
  Definition validAddition := Eval compute in validErrorbound (valCstAddVarU)
                                                              (fun x => match x with
                                                                     |Param _ => ((cst1,cst1),errCst1)
                                                                     |Const _ => (((-100) # 1,100 # 1),errVaru)
                                                                     |_ => ((lowerBoundAddUCst,upperBoundAddUCst),errAddUCst) end).
