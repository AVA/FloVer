(**
 Formalization of the Abstract Syntax Tree of a subset used in the Flover framework
 **)
Require Import Coq.Reals.Reals Coq.QArith.QArith.
Require Export Flover.ExpressionSemantics.
Require Export Flover.Infra.ExpressionAbbrevs Flover.Infra.NatSet.

(**
  Next define what a program is.
  Currently no loops, or conditionals.
  Only assignments and return statement
**)
Inductive cmd (V:Type) :Type :=
Let: mType -> nat -> cmd V -> cmd V -> cmd V
| Ret: expr V -> cmd V.

Fixpoint getRetExp (V:Type) (f:cmd V) :=
  match f with
  |Let m x e g => getRetExp g
  | Ret e => e
  end.

Fixpoint toRCmd (f:cmd Q) :=
  match f with
  |Let m x e g => Let m x (toRCmd e) (toRCmd g)
  |Ret e => Ret (toRExp e)
  end.

Fixpoint toREvalCmd (f:cmd R) :=
  match f with
  |Let m x e g => Let REAL x (toREvalCmd e) (toREvalCmd g)
  |Ret e => Ret (toREval e)
  end.

(*
UNUSED!
   Small Step semantics for Flover language
Inductive sstep : cmd R -> env -> R -> cmd R -> env -> Prop :=
  let_s x e s E v eps:
    eval_expr eps E e v ->
    sstep (Let x e s) E eps s (updEnv x v E)
 |ret_s e E v eps:
    eval_expr eps E e v ->
    sstep (Ret e) E eps (Nop R) (updEnv 0 v E).
 *)

(**
  Define big step semantics for the Flover language, terminating on a "returned"
  result value
 **)
Inductive bstep : cmd R -> env -> (expr R -> option mType) -> (R -> mType -> option R) ->
                  R -> mType -> Prop :=
  let_b m m' x e s E v res defVars DeltaMap:
    bstep e E defVars DeltaMap v m ->
    bstep s (updEnv x v E) defVars DeltaMap res m' -> (* (updDefVars (Var R x) m defVars) res m' -> *)
    bstep (Let m x e s) E defVars DeltaMap res m'
 |ret_b m e E v defVars DeltaMap:
    eval_expr E defVars DeltaMap e v m ->
    bstep (Ret e) E defVars DeltaMap v m.

(**
  The free variables of a command are all used variables of exprressions
  without the let bound variables
**)
Fixpoint freeVars V (f:cmd V) :NatSet.t :=
  match f with
  | Let _ x e1 g => (freeVars e1 ) ∪ (NatSet.remove x (freeVars g))
  | Ret e => Expressions.usedVars e
  end.

(**
  The defined variables of a command are all let bound variables
**)
Fixpoint definedVars V (f:cmd V) :NatSet.t :=
  match f with
  | Let _ x _ g => NatSet.add x (definedVars g)
  | Ret _ => NatSet.empty
  end.

(**
  The live variables of a command are all variables which occur on the right
  hand side of an assignment or at a return statement
 **)
Fixpoint liveVars V (f:cmd V) :NatSet.t :=
  match f with
  | Let _ _ e g => NatSet.union (liveVars e) (liveVars g)
  | Ret e => usedVars e
  end.

Lemma bstep_eq_env f:
  forall E1 E2 Gamma DeltaMap v m,
  (forall x, E1 x = E2 x) ->
  bstep f E1 Gamma DeltaMap v m ->
  bstep f E2 Gamma DeltaMap v m.
Proof.
  induction f; intros * eq_envs bstep_E1;
    inversion bstep_E1; subst; simpl in *.
  - econstructor; eauto.
    apply (IHf2 (updEnv n v0 E1)); auto.
    intros; unfold updEnv.
    destruct (x=? n); auto.
  - apply ret_b. eapply eval_eq_env; eauto.
Qed.

Lemma swap_Gamma_bstep f E vR m Gamma1 Gamma2 DeltaMap:
  (forall n, Gamma1 n = Gamma2 n) ->
  bstep f E Gamma1 DeltaMap vR m ->
  bstep f E Gamma2 DeltaMap vR m.
Proof.
  revert E Gamma1 Gamma2 DeltaMap vR m;
  induction f; intros * Gamma_eq eval_f.
  - inversion eval_f; subst.
    econstructor; try eauto.
  - inversion eval_f; subst.
    econstructor; try eauto.
    eapply swap_Gamma_eval_expr; eauto.
Qed.

Lemma bstep_Gamma_det f:
  forall E1 E2 Gamma DeltaMap v1 v2 m1 m2,
  bstep f E1 Gamma DeltaMap v1 m1 ->
  bstep f E2 Gamma DeltaMap v2 m2 ->
  m1 = m2.
Proof.
  induction f; intros * eval_f1 eval_f2;
    inversion eval_f1; subst;
      inversion eval_f2; subst; try auto.
  - eapply IHf2; eauto.
  - eapply Gamma_det; eauto.
Qed.

Lemma bstep_det f:
  forall E Gamma DeltaMap v1 v2 m,
  bstep f E Gamma DeltaMap v1 m ->
  bstep f E Gamma DeltaMap v2 m ->
  v1 = v2.
Proof.
  induction f; intros * eval_f1 eval_f2;
    inversion eval_f1; subst;
      inversion eval_f2; subst; try auto.
  - replace v with v0 in * by eauto using eval_expr_functional.
    eapply IHf2; eauto.
  - eapply eval_expr_functional; eauto.
Qed.
