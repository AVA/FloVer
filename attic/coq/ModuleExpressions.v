(**
Formalization of the base expression language for the flover framework
 **)
Require Import Coq.Reals.Reals Coq.micromega.Psatz Coq.QArith.QArith Interval.Interval_tactic.
Require Import Flover.Infra.RealConstruction Flover.Infra.RealSimps Flover.Infra.Abbrevs.
Set Implicit Arguments.

Module Type Expression.
  (* Value Type *)
  Parameter V:Type.
  Parameter plus: V -> V -> V.
  Parameter minus: V -> V -> V.
  Parameter mult: V -> V -> V.
  Parameter div: V -> V -> V.
  Parameter le : V -> V -> Prop.
  Parameter abs: V -> V.
  Parameter V1 :V.

(**
  Expressions will use binary operators.
  Define them first
**)
Inductive binop : Type := Plus | Sub | Mult | Div.
(**
  Next define an evaluation function for binary operators on reals.
  Errors are added on the expression evaluation level later.
 **)
Fixpoint eval_binop (o:binop) (v1:V) (v2:V) :=
  match o with
  | Plus => plus v1 v2
  | Sub => minus v1 v2
  | Mult => mult v1 v2
  | Div => div v1 v2
  end.
(**
  Define expressions parametric over some value type V.
  Will ease reasoning about different instantiations later.
  Note that we differentiate between wether we use a variable from the environment or a parameter.
  Parameters do not have error bounds in the invariants, so they must be perturbed, but variables from the
  program will be perturbed upon binding, so we do not need to perturb them.
**)
Inductive exp (V:Type): Type :=
  Var: nat -> exp V
| Param: nat -> exp V
| Const: V -> exp V
| Binop: binop -> exp V -> exp V -> exp V.
(**
  Define a perturbation function to ease writing of basic definitions
**)
Definition perturb (r:V) (e:V) :=
  mult r (plus V1 e).
(**
  Abbreviation for the type of an environment
 **)
Definition env_ty := nat -> V.

(**
  Define expression evaluation relation parametric by an "error" delta.
  This value will be used later to express float computations using a perturbation
  of the real valued computation by (1 + d)
**)
Inductive eval_exp (eps:V) (env:env_ty) : (exp V) -> V -> Prop :=
  Var_load x: eval_exp eps env (Var V x) (env x)
| Param_acc x delta:
    le (abs delta) eps ->
    eval_exp eps env (Param V x) (perturb (env x) delta)
| Const_dist n delta:
    le (abs delta) eps ->
    eval_exp eps env (Const n) (perturb n delta)
|Binop_dist op e1 e2 v1 v2 delta:
   le (abs delta) eps ->
                eval_exp eps env e1 v1 ->
                eval_exp eps env e2 v2 ->
                eval_exp eps env (Binop op e1 e2) (perturb (eval_binop op v1 v2) delta).

End Expression.

Lemma delta_0_deterministic (v:R) (delta:R):
  (Rabs delta <= 0)%R ->
  perturb v delta = v.
Proof.
  intros abs_0; apply Rabs_0_impl_eq in abs_0; subst.
  unfold perturb.
  rewrite Rmult_plus_distr_l.
  rewrite Rmult_0_r.
  rewrite Rmult_1_r.
  rewrite Rplus_0_r; auto.
Qed.

Lemma eval_det (e:exp R) (env:env_ty):
  forall v1 v2,
  eval_exp R0 env e v1 ->
  eval_exp R0 env e v2 ->
  v1 = v2.
Proof.
  induction e; intros v1 v2 eval_v1 eval_v2;
    inversion eval_v1; inversion eval_v2; [ auto | | | ];
      repeat try rewrite delta_0_deterministic; auto.
  subst.
  rewrite (IHe1 v0 v4); auto.
  rewrite (IHe2 v3 v5); auto.
Qed.

(**
  Using the parametric expressions, define boolean expressions for conditionals
**)
Inductive bexp (V:Type) : Type :=
  leq: exp V -> exp V -> bexp V
| less: exp V -> exp V -> bexp V.
(**
  Define evaluation of booleans for reals
 **)
Inductive bval (eps:R) (env:env_ty) : (bexp R) -> Prop -> Prop :=
  leq_eval (e1:exp R) (e2:exp R) (v1:R) (v2:R):
    eval_exp eps env e1 v1 ->
    eval_exp eps env e2 v2 ->
    bval eps env (leq e1 e2) (Rle v1 v2)
|less_eval (e1:exp R) (e2:exp R) (v1:R) (v2:R):
    eval_exp eps env e1 v1 ->
    eval_exp eps env e2 v2 ->
    bval eps env (less e1 e2) (Rlt v1 v2).
(**
 Simplify arithmetic later by making > >= only abbreviations
**)
Definition gr := fun (V:Type) (e1: exp V) (e2: exp V) => less e2 e1.
Definition greq := fun (V:Type) (e1:exp V) (e2: exp V) => leq e2 e1.