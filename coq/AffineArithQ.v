(*
  Formalization of Affine Arithmetic for FloVer. Original author: Raphael Monat.
*)

Require Import Coq.QArith.QArith Coq.QArith.Qreals QArith.Qminmax  Coq.QArith.Qabs Coq.Lists.List Coq.micromega.Psatz.
Require Import Coq.ZArith.ZArith Coq.ZArith.Zbool.
Require Import Recdef.
Require Import Flover.AffineForm Flover.Infra.Abbrevs Flover.Infra.RationalSimps Flover.Infra.Ltacs Flover.IntervalArithQ.

Fixpoint radius (a: affine_form Q): Q := match a with
| Const v => 0
| Noise _ v a' => (Qabs v) + radius a'
end.

Lemma radius_nonneg (a: affine_form Q):
  radius a >= 0.
Proof.
  induction a; simpl in *; try lra.
  assert (0 == 0 + 0) as tmp by lra; rewrite tmp; clear tmp.
  eapply Qplus_le_compat.
  apply Qabs_nonneg.
  auto.
Qed.

Definition toIntv (a: affine_form Q): intv := mkIntv (get_const a - radius a) (get_const a + radius a).

Lemma valid_to_intv (af: affine_form Q):
  valid (toIntv af).
Proof.
  unfold valid.
  unfold toIntv.
  simpl.
  pose proof (radius_nonneg af).
  apply Qplus_le_compat; try lra.
Qed.

Definition fromIntv (iv: intv) (noise: nat): affine_form Q :=
  let ivMid := (ivhi iv / (2#1)) + (ivlo iv / (2#1)) in
  let ivRad := Qmax (ivMid - (ivlo iv))%Q ((ivhi iv) - ivMid)%Q in
  if Qeq_bool (ivlo iv) (ivhi iv) then Const ivMid
  else Noise noise ivRad (Const (ivMid)).

Lemma to_from_intv_eq (iv: intv) (n: nat):
  valid iv ->
  isEqIntv (toIntv (fromIntv iv n)) iv.
Proof.
  intros vld.
  unfold isEqIntv, valid, fromIntv in *.
  unfold ivlo, ivhi in vld.
  simpl ivlo.
  simpl ivhi.
  destruct (Qeq_bool (fst iv) (snd iv)) eqn: Heq'.
  - rewrite Qeq_bool_iff in Heq'.
    split; simpl; rewrite Heq'; field.
  - apply Qeq_bool_neq in Heq'.
    simpl.
    rewrite Qplus_0_r.
    apply Q.max_case_strong.
    intros x y A; rewrite A; auto.
    Set Default Goal Selector "all".
    intros A.
    split; apply Qabs_case; intros B; try lra.
    ring_simplify.
    repeat rewrite Qmult_div_r by lra.
    lra.
    Set Default Goal Selector "1".
Qed.

Lemma to_intv_functional a x1 x2:
  toIntv a = x1 -> toIntv a = x2 -> x1 = x2.
Proof.
  congruence.
Qed.

Definition above_zero a := (ivlo (toIntv a)) > 0.
Definition below_zero a := (ivhi (toIntv a)) < 0.

Lemma above_zero_monotone a n v :
  above_zero (Noise n v a) -> above_zero a.
Proof.
  unfold above_zero.
  simpl.
  intros noise.
  unfold Qminus in *.
  rewrite <- Qlt_minus_iff in *.
  pose proof (Qabs_nonneg v) as nnabs.
  pose proof (radius_nonneg a) as nnrad.
  lra.
Qed.

Lemma above_ivhi a:
  above_zero a -> ivhi (toIntv a) > 0.
Proof.
  unfold above_zero.
  pose proof (valid_to_intv a) as validiv.
  unfold valid in validiv.
  lra.
Qed.

Lemma below_zero_monotone a n v :
  below_zero (Noise n v a) -> below_zero a.
Proof.
  unfold below_zero.
  simpl.
  intros noise.
  pose proof (Qabs_nonneg v) as nnabs.
  pose proof (radius_nonneg a) as nnrad.
  lra.
Qed.

Lemma below_ivlo a:
  below_zero a -> ivlo (toIntv a) < 0.
Proof.
  unfold below_zero.
  pose proof (valid_to_intv a) as validiv.
  unfold valid in validiv.
  lra.
Qed.

Definition noise_type := {q: Q | -(1) <= q /\ q <= 1}.
Definition noise_val (q: noise_type): Q := proj1_sig q.

Coercion noise_val : noise_type >-> Q.

Lemma zero_is_valid_noise:
  (-1#1) <= 0 <= 1.
Proof.
  lra.
Qed.

Lemma one_is_valid_noise:
  (-1#1) <= 1 <= 1.
Proof.
  lra.
Qed.

Definition noise_zero := exist (fun (x: Q) => (-1#1) <= x /\ x <= 1) 0 zero_is_valid_noise.
Definition noise_one := exist (fun (x: Q) => (-1#1) <= x /\ x <= 1) 1 one_is_valid_noise.

Definition noise_mapping := nat -> option noise_type.

Definition updMap (m: noise_mapping) (n: nat) (q: noise_type): noise_mapping :=
  fun x => if x =? n then Some q else m x.

Lemma upd_sound map n q:
  updMap map n q n = Some q.
Proof.
  unfold updMap.
  assert (n =? n = true) as H by apply Nat.eqb_refl.
  now rewrite H.
Qed.

Fixpoint eval_aff (a: affine_form Q) (map: noise_mapping): option Q :=
  match a with
  | Const v => Some v
  | Noise n v a' => match (map n), eval_aff a' map with
                       | None, _ => None
                       | _, None => None
                       | Some res, Some res_a' =>
                         Some ((res * v) + res_a')
                       end
  end.

Definition af_evals (a: affine_form Q) (v: Q) (map: noise_mapping): Prop :=
  plet v_aff := eval_aff a map in v_aff == v.

Lemma constant_evals_unchanged v1 v2 map:
  af_evals (Const v1) v2 map -> v1 == v2.
Proof.
  intros A.
  unfold af_evals in A.
  now simpl in A.
Qed.

Lemma af_evals_noise_compat a v1 v2 n map:
  af_evals (Noise n v1 a) v2 map ->
  exists q, map n = Some q /\ af_evals a (v2 - q * v1) map.
Proof.
  intros A.
  unfold af_evals in *.
  simpl in A.
  destruct (map n); try contradiction.
  destruct (eval_aff a map); try contradiction.
  simpl in A |-*.
  exists n0.
  rewrite <- A.
  split; auto; ring.
Qed.

Lemma bounded_by_abs_pos v (q: noise_type):
  0 < v -> - Qabs v <= q * v <= Qabs v.
Proof.
  intros v_pos % Qlt_le_weak; destruct q as [q [q_l q_h]]; simpl; rewrite Qabs_pos; try lra; split.
  - assert (q * v == - (- q * v)) by lra.
    rewrite H.
    apply Qopp_le_compat.
    pose proof (Qmult_le_compat_r _ _ _ q_l v_pos); lra.
  - pose proof (Qmult_le_compat_r _ _ _ q_h v_pos); lra.
Qed.

Lemma bounded_by_abs v (q: noise_type):
  - Qabs v <= q * v <= Qabs v.
Proof.
  case_eq (Qcompare v 0); intros.
  - apply Qeq_alt in H; rewrite H.
    simpl.
    rewrite Qmult_0_r.
    lra.
  - apply Qlt_alt in H; simpl.
    assert (0 < - v) by lra.
    destruct (bounded_by_abs_pos (v:=-v) q) as [v_l v_h]; try auto.
    rewrite Qabs_opp in v_h, v_l.
    assert (q * v == - (q * -v)) by lra.
    assert (Qabs v == - - Qabs v) by lra.
    rewrite H1.
    rewrite H2 at 2.
    split; apply Qopp_le_compat; auto.
  - apply Qgt_alt in H; simpl.
    apply bounded_by_abs_pos; auto.
Qed.

(* Interval containment property *)
Lemma bounded_aff a map v:
  (eval_aff a map) = (Some v) ->
  (get_const a) - (radius a) <= v /\ v <= (get_const a) + (radius a).
Proof.
  revert v; induction a; intros v0 eval; simpl in *; subst.
  - inversion eval; subst; lra.
  - case_eq (map n); intros; rewrite H in eval; try congruence.
    case_eq (eval_aff a map); intros; rewrite H0 in eval; inversion eval.
    subst.
    specialize (IHa _ H0); destruct IHa as [IHal IHar].
    assert (- Qabs v <= n0 * v <= Qabs v) as [v_l v_h] by eauto using bounded_by_abs.
    split.
    + pose proof (Qplus_le_compat _ _ _ _ IHal v_l); lra.
    + pose proof (Qplus_le_compat _ _ _ _ IHar v_h); lra.
Qed.

Lemma bounded_aff_abs a map v:
  (eval_aff a map) = Some v ->
  Qabs (v - get_const a) <= radius a.
Proof.
  intros.
  apply Qabs_Qle_condition.
  pose proof (bounded_aff a map H).
  lra.
Qed.

Lemma bounded_af_evals_abs a map v:
  af_evals a v map ->
  Qabs (v - get_const a) <= radius a.
Proof.
  unfold af_evals.
  destruct (eval_aff a map) eqn: Hv.
  - intros A.
    simpl in A. 
    rewrite <- A.
    eauto using bounded_aff_abs.
  - simpl; intros; contradiction.
Qed.

Lemma to_intv_containment a v map:
  af_evals a v map ->
  ivlo (toIntv a) <= v <= ivhi (toIntv a).
Proof.
  intros evals.
  simpl.
  unfold af_evals in evals.
  destruct (eval_aff a map) eqn: Hq; simpl in evals; try contradiction.
  rewrite <- evals.
  now apply bounded_aff with (map := map).
Qed.

Lemma radius_0_const (a: affine_form Q) v map:
  af_evals a v map ->
  radius a == 0 ->
  v == get_const a.
Proof.
  unfold af_evals.
  intros evals rad.
  destruct (eval_aff a map) eqn: Hev; try contradiction; simpl in evals.
  apply bounded_aff in Hev.
  rewrite evals in *.
  rewrite rad in Hev.
  lra.
Qed.

(***********************************************)
(******** Addition of two affine forms *********)
(***********************************************)

Function plus_aff_tuple (a: affine_form Q * affine_form Q) { measure aff_length_tuple a }: affine_form Q :=
  let (a1, a2) := a in
  match a1, a2 with
  | Const v1, Const v2 => Const (v1 + v2)
  | Const v1, Noise n2 v2 a2' => Noise n2 v2 (plus_aff_tuple (a1, a2'))
  | Noise n1 v1 a1', Const v2 => Noise n1 v1 (plus_aff_tuple (a1', a2))
  | Noise n1 v1 a1', Noise n2 v2 a2' =>
   if (n1 =? n2)%nat then
     Noise n1 (v1+v2) (plus_aff_tuple (a1', a2'))
   else if n1 <? n2%nat then
          Noise n2 v2 (plus_aff_tuple (a1, a2'))
        else
          Noise n1 v1 (plus_aff_tuple (a1', a2))
  end.
Proof.
  - intros. unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
Defined.

Definition plus_aff (a1 a2: affine_form Q): affine_form Q := plus_aff_tuple (a1, a2).

Lemma plus_aff_tuple_sound (a: affine_form Q * affine_form Q) (v1 v2: Q) map:
  af_evals (fst a) v1 map ->
  af_evals (snd a) v2 map ->
  af_evals (plus_aff_tuple a) (v1+v2) map.
Proof.
  revert v1 v2; unfold af_evals.
  functional induction (plus_aff_tuple a); simpl in *; intros v3 v4 G1 G2.
  - lra.
  - case_eq (map n2); intros; rewrite H in *; [ | inversion G2 ].
    destruct (eval_aff a2' map); [ | inversion G2].
    specialize (IHa0 _ _ G1 (Qeq_refl q)).
    case_eq (eval_aff (plus_aff_tuple (Const v1, a2')) map); intros; rewrite H0 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
  - case_eq (map n1); intros; rewrite H in *; [ | inversion G1 ].
    destruct (eval_aff a1' map); [ | inversion G1 ].
    specialize (IHa0 _ _ (Qeq_refl q) G2).
    case_eq (eval_aff (plus_aff_tuple (a1', Const v2)) map); intros; rewrite H0 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
  - apply beq_nat_true in e2; subst.
    case_eq (map n2); intros; rewrite H in *; [ | inversion G1 ].
    destruct (eval_aff a1' map); [ | inversion G1 ].
    destruct (eval_aff a2' map); [ | inversion G2 ].
    specialize (IHa0 _ _ (Qeq_refl q) (Qeq_refl q0)).
    case_eq (eval_aff (plus_aff_tuple (a1', a2')) map); intros; rewrite H0 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
  - case_eq (map n1); intros; rewrite H in *; [ | inversion G1 ].
    case_eq (map n2); intros; rewrite H0 in *; [ | inversion G2 ].
    destruct (eval_aff a1' map); [ | inversion G1 ].
    destruct (eval_aff a2' map); [ | inversion G2 ].
    specialize (IHa0 _ _ (Qeq_refl (n * v1 + q)) (Qeq_refl q0)).
    case_eq (eval_aff (plus_aff_tuple (Noise n1 v1 a1', a2')) map); intros; rewrite H1 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
  - case_eq (map n1); intros; rewrite H in *; [ | inversion G1 ].
    case_eq (map n2); intros; rewrite H0 in *; [ | inversion G2 ].
    destruct (eval_aff a1' map); [ | inversion G1 ].
    destruct (eval_aff a2' map); [ | inversion G2 ].
    specialize (IHa0 _ _ (Qeq_refl q) (Qeq_refl (n0 * v2 + q0))).
    case_eq (eval_aff (plus_aff_tuple (a1', Noise n2 v2 a2')) map); intros; rewrite H1 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
Qed.

Lemma plus_aff_sound (a1: affine_form Q) (a2: affine_form Q) (v1 v2: Q) map:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  af_evals (plus_aff a1 a2) (v1 + v2) map.
Proof.
  eauto using plus_aff_tuple_sound.
Qed.

Lemma plus_aff_comm (a1 a2: affine_form Q) v1 v2 map:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  af_evals (plus_aff a1 a2) (v1 + v2) map <-> af_evals (plus_aff a2 a1) (v2 + v1) map.
Proof.
  intros evals1 evals2.
  pose proof (plus_aff_sound _ _ _ _ _ evals1 evals2) as evalspl.
  pose proof (plus_aff_sound _ _ _ _ _ evals2 evals1) as evalspr.
  split; intros; Flover_compute; simpl in *; try auto.
Qed.

Lemma plus_aff_fresh_compat a1 a2 n:
  fresh n a1 ->
  fresh n a2 ->
  fresh n (plus_aff a1 a2).
Proof.
  unfold plus_aff.
  remember (a1, a2) as a12.
  assert (fst a12 = a1 /\ snd a12 = a2) as Havals by now rewrite Heqa12.
  destruct Havals as [Heqa1 Heqa2].
  rewrite <- Heqa1, <- Heqa2.
  clear Heqa1 Heqa2 Heqa12 a1 a2.
  intros fresh1 fresh2.
  functional induction (plus_aff_tuple a12); simpl in *; eauto using fresh_noise_gt, fresh_noise_compat, fresh_noise.
  pose proof (fresh_noise_gt fresh1) as freshn1.
  apply fresh_noise_compat in fresh1.
  pose proof (fresh_noise_gt fresh2) as freshn2.
  apply fresh_noise_compat in fresh2.
  specialize (IHa fresh1 fresh2).
  now apply (fresh_noise _ freshn1).
Qed.

(***********************************************)
(***** Multiplication of two affine forms ******)
(***********************************************)

Function mult_aff_aux (a: affine_form Q * affine_form Q) { measure aff_length_tuple a }: affine_form Q :=
  let (a1, a2) := a in
  let c_a1 := get_const a1 in
  let c_a2 := get_const a2 in
  match a1, a2 with
  | Const v1, Const v2 => Const (v1 * v2)
  | Const v1, Noise n2 v2 a2' => Noise n2 (v2 * v1) (mult_aff_aux (a1, a2'))
  | Noise n1 v1 a1', Const v2 => Noise n1 (v1 * v2) (mult_aff_aux (a1', a2))
  | Noise n1 v1 a1', Noise n2 v2 a2' =>
   if (n1 =? n2) then
     Noise n1 (v1 * c_a2 + v2 * c_a1) (mult_aff_aux (a1', a2'))
   else if (n1 <? n2) then
          Noise n2 (v2 * c_a1) (mult_aff_aux (a1, a2'))
        else
          Noise n1 (v1 * c_a2) (mult_aff_aux (a1', a2))
  end.
Proof.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
Defined.

Definition mult_aff (a1 a2: affine_form Q) (next_noise: nat): affine_form Q :=
  if ((Qeq_bool (radius a1) 0) || (Qeq_bool (radius a2) 0)) then
    mult_aff_aux (a1, a2)
  else 
    Noise next_noise (radius a1 * radius a2) (mult_aff_aux (a1, a2)).

Lemma mult_aff_aux_sound_helper1 (a1 a2: affine_form Q) (val1 val2 v1 v2 q: Q) (q1 qIHa: noise_type):
  q == (val1 - q1 * v1) * (val2 - q1 * v2) - qIHa * radius a1 * radius a2 ->
  Qabs (val1 - q1 * v1 - get_const a1) <= radius a1 ->
  Qabs (val2 - get_const a2) <= Qabs v2 + radius a2 ->
  Qabs (val1 * val2 - q1 * (v1 * get_const a2 + v2 * get_const a1) - q) <=
  (Qabs v1 + radius a1) * (Qabs v2 + radius a2).
Proof.
  intros Hq bounds1 bounds2.
  rewrite Hq.
  field_rewrite (val1 * val2 - q1 * (v1 * get_const a2 + v2 * get_const a1) - ((val1 - q1 * v1) * (val2 - q1 * v2) - qIHa * radius a1 * radius a2) == - q1 * v1 * q1 * v2 + q1 * v2 * (val1 - get_const a1) + q1 * v1 * (val2 - get_const a2) + qIHa * (radius a1 * radius a2)).
  field_rewrite ((Qabs v1 + radius a1) * (Qabs v2 + radius a2) == Qabs v1 * Qabs v2 + radius a1 * Qabs v2 + Qabs v1 * radius a2 + radius a1 * radius a2).
  eapply Qle_trans; try eapply Qabs_triangle.
  eapply Qplus_le_compat.
  - field_rewrite (- q1 * v1 * q1 * v2 + q1 * v2 * (val1 - get_const a1) + q1 * v1 * (val2 - get_const a2) == q1 * v2 * (val1 - q1 * v1 - get_const a1) + q1 * v1 * (val2 - get_const a2)).
    field_rewrite (Qabs v1 * Qabs v2 + radius a1 * Qabs v2 + Qabs v1 * radius a2 == Qabs v2 * radius a1 + Qabs v1 * (Qabs v2 + radius a2)).
    eapply Qle_trans; try eapply Qabs_triangle.
    eapply Qplus_le_compat.
    + eapply Qle_trans.
      * pose proof (Qabs_bounded _ (v2 * (val1 - q1 * v1 - get_const a1)) (proj2_sig q1)) as H.
        rewrite Qmult_assoc in H.
        exact H.
      * rewrite Qabs_Qmult.
        apply Qmult_le_compat_l; eauto using Qabs_nonneg.
    + eapply Qle_trans.
      * pose proof (Qabs_bounded _ (v1 * (val2 - get_const a2)) (proj2_sig q1)) as H.
        rewrite Qmult_assoc in H.
        exact H.
      * rewrite Qabs_Qmult.
        apply Qmult_le_compat_l; eauto using Qabs_nonneg.
  - eapply Qle_trans; try apply (Qabs_bounded _ (radius a1 * radius a2) (proj2_sig qIHa)).
    rewrite Qabs_Qmult.
    pose proof (radius_nonneg a1) as H1.
    pose proof (radius_nonneg a2) as H2.
    repeat rewrite Qabs_pos; try lra.
Qed.

Lemma mult_aff_aux_sound_helper2 (a1 a2: affine_form Q) (val1 val2 v1 v2 q: Q) (q1 qIHa: noise_type):
  q == val1 * (val2 - q1 * v2) - qIHa * (Qabs v1 + radius a1) * radius a2 ->
  Qabs (val1 - get_const a1) <= Qabs v1 + radius a1 ->
  Qabs (val1 * val2 - q1 * (v2 * get_const a1) - q) <=
  (Qabs v1 + radius a1) * (Qabs v2 + radius a2).
Proof.
  intros Hq bounds1.
  rewrite Hq.
  field_rewrite (val1 * val2 - q1 * (v2 * get_const a1) - (val1 * (val2 - q1 * v2) - qIHa * (Qabs v1 + radius a1) * radius a2) == q1 * v2 * (val1 - get_const a1) + qIHa * (Qabs v1 * radius a2) + qIHa * (radius a1 * radius a2)).
  field_rewrite ((Qabs v1 + radius a1) * (Qabs v2 + radius a2) == Qabs v2 * (Qabs v1 + radius a1) + Qabs v1 * radius a2 + radius a1 * radius a2).
  eapply Qle_trans; try eapply Qabs_triangle.
  eapply Qplus_le_compat.
  - eapply Qle_trans; try eapply Qabs_triangle.
    eapply Qplus_le_compat.
    + eapply Qle_trans.
      * pose proof (Qabs_bounded _ (v2 * (val1 - get_const a1)) (proj2_sig q1)) as H.
        rewrite Qmult_assoc in H.
        exact H.
      * rewrite Qabs_Qmult.
        apply Qmult_le_compat_l; eauto using Qabs_nonneg.
    + eapply Qle_trans.
      * epose proof (Qabs_bounded _ _ (proj2_sig qIHa)).
        exact H.
      * rewrite Qabs_Qmult.
        rewrite (Qabs_pos (radius a2) (radius_nonneg a2)).
        rewrite (Qabs_pos (Qabs v1) (Qabs_nonneg v1)).
        lra.
  - eapply Qle_trans.
    apply (Qabs_bounded qIHa (radius a1 * radius a2) (proj2_sig qIHa)).
    rewrite Qabs_Qmult.
    pose proof (radius_nonneg a1) as H1.
    pose proof (radius_nonneg a2) as H2.
    repeat rewrite Qabs_pos; lra.
Qed.

Lemma mult_aff_aux_sound_helper3 (a1 a2: affine_form Q) (val1 val2 v1 v2 q: Q) (q1 qIHa: noise_type):
  q == (val1 - q1 * v1) * val2 - qIHa * radius a1 * (Qabs v2 + radius a2) ->
  Qabs (val2 - get_const a2) <= Qabs v2 + radius a2 ->
  Qabs (val1 * val2 - q1 * (v1 * get_const a2) - q) <=
  (Qabs v1 + radius a1) * (Qabs v2 + radius a2).
Proof.
  field_rewrite ((val1 - q1 * v1) * val2 - qIHa * radius a1 * (Qabs v2 + radius a2) == val2 * (val1 - q1 * v1) - qIHa * (Qabs v2 + radius a2) * radius a1).
  field_rewrite (val1 * val2 - q1 * (v1 * get_const a2) - q == val2 * val1 - q1 * (v1 * get_const a2) - q).
  field_rewrite ((Qabs v1 + radius a1) * (Qabs v2 + radius a2) == (Qabs v2 + radius a2) * (Qabs v1 + radius a1)).
  eauto using mult_aff_aux_sound_helper2.
Qed.

Lemma mult_aff_aux_sound (a1: affine_form Q) (a2: affine_form Q) (v1 v2: Q) map:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  exists (q: noise_type), af_evals (mult_aff_aux (a1, a2)) (v1 * v2 - q * (radius a1) * (radius a2)) map.
Proof.
  revert v1 v2.
  remember (a1, a2) as a12.
  assert (fst a12 = a1 /\ snd a12 = a2) as Havals by now rewrite Heqa12.
  destruct Havals as [Heqa1 Heqa2].
  rewrite <- Heqa1, <- Heqa2.
  clear Heqa1 Heqa2 Heqa12 a1 a2.
  functional induction (mult_aff_aux a12); intros val1 val2 evals1 evals2; simpl in *.
  - unfold af_evals in *; simpl in *.
    exists noise_zero; rewrite evals1, evals2; ring.
  - apply af_evals_noise_compat in evals2 as [q2 [Hq2 evals2]].
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    apply constant_evals_unchanged in evals1.
    exists qIHa.
    unfold af_evals in *; simpl; rewrite Hq2.
    destruct (eval_aff (mult_aff_aux (Const v1, a2')) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    rewrite IHa; ring_simplify; rewrite evals1; ring.
  - (* Same as the previous case *)
    apply af_evals_noise_compat in evals1 as [q1 [Hq1 evals1]].
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    apply constant_evals_unchanged in evals2.
    exists qIHa.
    unfold af_evals in *; simpl; rewrite Hq1.
    destruct (eval_aff (mult_aff_aux (a1', Const v2)) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    rewrite IHa; ring_simplify; rewrite evals2; ring.
  - apply beq_nat_true in e2.
    rewrite <- e2 in *.
    pose proof (bounded_af_evals_abs _ _ _ evals2) as bounds2;
      simpl get_const in bounds2; simpl radius in bounds2.
    apply af_evals_noise_compat in evals1 as [q1 [Hq1 evals1]].
    apply af_evals_noise_compat in evals2 as [q2 [Hq2 evals2]].
    pose proof (bounded_af_evals_abs _ _ _ evals1) as bounds1;
      simpl get_const in bounds1; simpl radius in bounds1.
    assert (q1 = q2) as Heqq by congruence; rewrite <- Heqq in *.
    clear Heqq e2 q2.
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    unfold af_evals in IHa |-*; simpl; rewrite Hq1.
    destruct (eval_aff (mult_aff_aux (a1', a2')) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    pose proof (radius_nonneg a1') as nnrad1; pose proof (radius_nonneg a2') as nnrad2.
    pose proof (Qabs_nonneg v1) as nnabsv1; pose proof (Qabs_nonneg v2) as nnabsv2.
    destruct (Qeq_bool (radius a1') 0) eqn: rad1; destruct (Qeq_bool (radius a2') 0) eqn: rad2;
      destruct (Qeq_bool v1 0) eqn: v1zero; destruct (Qeq_bool v2 0) eqn: v2zero;
        first [rewrite Qeq_bool_iff in v1zero | apply Qeq_bool_neq in v1zero];
          first [rewrite Qeq_bool_iff in v2zero | apply Qeq_bool_neq in v2zero];
            first [rewrite Qeq_bool_iff in rad1 | apply Qeq_bool_neq in rad1];
              first [rewrite Qeq_bool_iff in rad2 | apply Qeq_bool_neq in rad2].
    all: try pose proof (radius_0_const _ _ _ evals1 rad1) as const1.
    all: try pose proof (radius_0_const _ _ _ evals2 rad2) as const2.
    all: try (exists noise_zero;
      simpl;
      rewrite IHa;
      try rewrite <- const1; try rewrite <- const2;
      try rewrite rad1; try rewrite rad2; try rewrite v1zero; try rewrite v2zero; simpl;
      ring).
    all: pose (needed_noise_expr := (val1 * val2 - q1 * (v1 * get_const a2' + v2 * get_const a1') - q) / ((Qabs v1 + radius a1') * (Qabs v2 + radius a2'))).
    all: pose (nominator := val1 * val2 - q1 * (v1 * get_const a2' + v2 * get_const a1') - q).
    all: fold nominator in needed_noise_expr.
    all: pose (denominator := (Qabs v1 + radius a1') * (Qabs v2 + radius a2')).
    all: fold denominator in needed_noise_expr.
    all: unfold needed_noise_expr.
    all: try pose proof (Qabs_nonzero_positive v1 v1zero).
    all: try pose proof (Qabs_nonzero_positive v2 v2zero).
    all: try assert (Qabs v1 + radius a1' > 0) by lra.
    all: try assert (Qabs v2 + radius a2' > 0) by lra.
    all: pose proof mult_aff_aux_sound_helper1 as noise_bounds.
    all: specialize (noise_bounds a1' a2' val1 val2 v1 v2 q q1 qIHa IHa bounds1 bounds2).
    all: rewrite Qabs_Qle_condition in noise_bounds.
    all: fold nominator denominator in noise_bounds.
    all: assert (denominator > 0) as denominator_pos by eauto using Qmult_nonneg.
    all: assert (1 * denominator == denominator) as tmp by lra; rewrite <- tmp in noise_bounds; clear tmp.
    all: rewrite <- (Qdiv_le_bounds nominator _ 1 denominator_pos) in noise_bounds.
    all: fold needed_noise_expr in noise_bounds.
    all: pose (needed_noise := exist (fun x => -(1#1) <= x <= 1) needed_noise_expr noise_bounds).
    all: exists needed_noise; simpl.
    all: unfold needed_noise_expr, nominator, denominator.
    all: field; lra.
  - pose proof (bounded_af_evals_abs _ _ _ evals1) as bounds1;
      simpl get_const in bounds1; simpl radius in bounds1.
    pose proof (bounded_af_evals_abs _ _ _ evals2) as bounds2;
      simpl get_const in bounds2; simpl radius in bounds2.
    apply af_evals_noise_compat in evals2 as [q2 [Hq2 evals2]].
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    apply af_evals_noise_compat in evals1 as [q1 [Hq1 evals1]].
    unfold af_evals in IHa |-*; simpl; rewrite Hq2.
    pose proof (radius_nonneg a1') as nnrad1; pose proof (radius_nonneg a2') as nnrad2.
    pose proof (Qabs_nonneg v1) as nnabsv1; pose proof (Qabs_nonneg v2) as nnabsv2.
    destruct (eval_aff (mult_aff_aux (Noise n1 v1 a1', a2')) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    destruct (Qeq_bool (radius a1') 0) eqn: rad1; destruct (Qeq_bool (radius a2') 0) eqn: rad2;
      destruct (Qeq_bool v1 0) eqn: v1zero; destruct (Qeq_bool v2 0) eqn: v2zero;
        first [rewrite Qeq_bool_iff in v1zero | apply Qeq_bool_neq in v1zero];
          first [rewrite Qeq_bool_iff in v2zero | apply Qeq_bool_neq in v2zero];
            first [rewrite Qeq_bool_iff in rad1 | apply Qeq_bool_neq in rad1];
              first [rewrite Qeq_bool_iff in rad2 | apply Qeq_bool_neq in rad2].
    all: try pose proof (radius_0_const _ _ _ evals1 rad1) as const1.
    all: try pose proof (radius_0_const _ _ _ evals2 rad2) as const2.
    all: try (exists noise_zero;
      simpl;
      rewrite IHa;
      try rewrite <- const1; try rewrite <- const2;
      try rewrite rad1; try rewrite rad2; try rewrite v1zero; try rewrite v2zero; simpl;
      ring).
    all: pose (needed_noise_expr := (val1 * val2 - q2 * (v2 * get_const a1') - q) / ((Qabs v1 + radius a1') * (Qabs v2 + radius a2'))).
    all: pose (nominator := val1 * val2 - q2 * (v2 * get_const a1') - q).
    all: fold nominator in needed_noise_expr.
    all: pose (denominator := (Qabs v1 + radius a1') * (Qabs v2 + radius a2')).
    all: fold denominator in needed_noise_expr.
    all: unfold needed_noise_expr.
    all: try pose proof (Qabs_nonzero_positive v1 v1zero).
    all: try pose proof (Qabs_nonzero_positive v2 v2zero).
    all: try assert (Qabs v1 + radius a1' > 0) by lra.
    all: try assert (Qabs v2 + radius a2' > 0) by lra.
    all: pose proof mult_aff_aux_sound_helper2 as noise_bounds.
    all: specialize (noise_bounds a1' a2' val1 val2 v1 v2 q q2 qIHa IHa bounds1).
    all: rewrite Qabs_Qle_condition in noise_bounds.
    all: fold nominator denominator in noise_bounds.
    all: assert (denominator > 0) as denominator_pos by eauto using Qmult_nonneg.
    all: assert (1 * denominator == denominator) as tmp by lra; rewrite <- tmp in noise_bounds; clear tmp.
    all: rewrite <- (Qdiv_le_bounds nominator _ 1 denominator_pos) in noise_bounds.
    all: fold needed_noise_expr in noise_bounds.
    all: pose (needed_noise := exist (fun x => -(1#1) <= x <= 1) needed_noise_expr noise_bounds).
    all: exists needed_noise; simpl.
    all: unfold needed_noise_expr, nominator, denominator.
    all: field; lra.
  - (* Same as the previous case *)
    pose proof (bounded_af_evals_abs _ _ _ evals1) as bounds1;
      simpl get_const in bounds1; simpl radius in bounds1.
    pose proof (bounded_af_evals_abs _ _ _ evals2) as bounds2;
      simpl get_const in bounds2; simpl radius in bounds2.
    apply af_evals_noise_compat in evals1 as [q1 [Hq1 evals1]].
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    apply af_evals_noise_compat in evals2 as [q2 [Hq2 evals2]].
    unfold af_evals in IHa |-*; simpl; rewrite Hq1.
    pose proof (radius_nonneg a1') as nnrad1; pose proof (radius_nonneg a2') as nnrad2.
    pose proof (Qabs_nonneg v1) as nnabsv1; pose proof (Qabs_nonneg v2) as nnabsv2.
    destruct (eval_aff (mult_aff_aux (a1', Noise n2 v2 a2')) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    destruct (Qeq_bool (radius a1') 0) eqn: rad1; destruct (Qeq_bool (radius a2') 0) eqn: rad2;
      destruct (Qeq_bool v1 0) eqn: v1zero; destruct (Qeq_bool v2 0) eqn: v2zero;
        first [rewrite Qeq_bool_iff in v1zero | apply Qeq_bool_neq in v1zero];
          first [rewrite Qeq_bool_iff in v2zero | apply Qeq_bool_neq in v2zero];
            first [rewrite Qeq_bool_iff in rad1 | apply Qeq_bool_neq in rad1];
              first [rewrite Qeq_bool_iff in rad2 | apply Qeq_bool_neq in rad2].
    all: try pose proof (radius_0_const _ _ _ evals1 rad1) as const1.
    all: try pose proof (radius_0_const _ _ _ evals2 rad2) as const2.
    all: try (exists noise_zero;
      simpl;
      rewrite IHa;
      try rewrite <- const1; try rewrite <- const2;
      try rewrite rad1; try rewrite rad2; try rewrite v1zero; try rewrite v2zero; simpl;
      ring).
    all: pose (needed_noise_expr := (val1 * val2 - q1 * (v1 * get_const a2') - q) / ((Qabs v1 + radius a1') * (Qabs v2 + radius a2'))).
    all: pose (nominator := val1 * val2 - q1 * (v1 * get_const a2') - q).
    all: fold nominator in needed_noise_expr.
    all: pose (denominator := (Qabs v1 + radius a1') * (Qabs v2 + radius a2')).
    all: fold denominator in needed_noise_expr.
    all: unfold needed_noise_expr.
    all: try pose proof (Qabs_nonzero_positive v1 v1zero).
    all: try pose proof (Qabs_nonzero_positive v2 v2zero).
    all: try assert (Qabs v1 + radius a1' > 0) by lra.
    all: try assert (Qabs v2 + radius a2' > 0) by lra.
    all: pose proof mult_aff_aux_sound_helper3 as noise_bounds.
    all: specialize (noise_bounds a1' a2' val1 val2 v1 v2 q q1 qIHa IHa bounds2).
    all: rewrite Qabs_Qle_condition in noise_bounds.
    all: fold nominator denominator in noise_bounds.
    all: assert (denominator > 0) as denominator_pos by eauto using Qmult_nonneg.
    all: assert (1 * denominator == denominator) as tmp by lra; rewrite <- tmp in noise_bounds; clear tmp.
    all: rewrite <- (Qdiv_le_bounds nominator _ 1 denominator_pos) in noise_bounds.
    all: fold needed_noise_expr in noise_bounds.
    all: pose (needed_noise := exist (fun x => -(1#1) <= x <= 1) needed_noise_expr noise_bounds).
    all: exists needed_noise; simpl.
    all: unfold needed_noise_expr, nominator, denominator.
    all: field; lra.
Qed.

Lemma bounded_value_mult_aff_aux (a1: affine_form Q) (a2: affine_form Q) (v1 v2: Q) map v_aux:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  eval_aff (mult_aff_aux (a1, a2)) map = Some v_aux ->
  Qabs (v1 * v2 - v_aux) <= (radius a1 * radius a2).
Proof.
  intros evals1 evals2 evalsm.
  pose proof (mult_aff_aux_sound a1 a2 v1 v2 map evals1 evals2) as boundm.
  destruct boundm as [q boundm].
  unfold af_evals in boundm; rewrite evalsm in boundm; simpl in boundm; rewrite boundm.
  field_rewrite (v1 * v2 - (v1 * v2 - q * radius a1 * radius a2) == q * (radius a1 * radius a2)).
  eapply Qle_trans; try apply (Qabs_bounded _ (radius a1 * radius a2) (proj2_sig q)).
  rewrite Qabs_Qmult.
  repeat rewrite Qabs_pos; eauto using radius_nonneg; try lra.
Qed.

Lemma eval_updMap_compat a map n q:
  fresh n a ->
  eval_aff a map = eval_aff a (updMap map n q).
Proof.
  intros freshn.
  induction a.
  - trivial.
  - pose proof (fresh_noise_gt freshn) as ngt.
    apply fresh_noise_compat in freshn.
    specialize (IHa freshn).
    simpl.
    rewrite IHa.
    unfold updMap.
    assert (~ n0 = n) as neqn by lia.
    apply Nat.eqb_neq in neqn.
    now rewrite neqn.
Qed.

Lemma mult_aff_aux_fresh_compat a1 a2 n:
  fresh n a1 ->
  fresh n a2 ->
  fresh n (mult_aff_aux (a1, a2)).
Proof.
  remember (a1, a2) as a12.
  assert (fst a12 = a1 /\ snd a12 = a2) as Havals by now rewrite Heqa12.
  destruct Havals as [Heqa1 Heqa2].
  rewrite <- Heqa1, <- Heqa2.
  clear Heqa1 Heqa2 Heqa12 a1 a2.
  intros fresh1 fresh2.
  functional induction (mult_aff_aux a12); simpl in *; eauto using fresh_noise_gt, fresh_noise_compat, fresh_noise.
  pose proof (fresh_noise_gt fresh1) as freshn1.
  apply fresh_noise_compat in fresh1.
  pose proof (fresh_noise_gt fresh2) as freshn2.
  apply fresh_noise_compat in fresh2.
  specialize (IHa fresh1 fresh2).
  now apply (fresh_noise _ freshn1).
Qed.

Lemma mult_aff_sound (a1: affine_form Q) (a2: affine_form Q) (v1 v2: Q) map (n: nat):
  fresh n a1 ->
  fresh n a2 ->
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  exists q, af_evals (mult_aff a1 a2 n) (v1 * v2) (updMap map n q).
Proof.
  intros fresh1 fresh2 evals1 evals2.
  unfold mult_aff.
  pose proof (mult_aff_aux_sound a1 a2 v1 v2 map evals1 evals2) as [q bounds].
  exists q.
  unfold af_evals in *.
  assert (af_evals (mult_aff_aux (a1, a2)) (v1 * v2 - q * radius a1 * radius a2) (updMap map n q))
    as Heq.
  { pose proof (mult_aff_aux_fresh_compat fresh1 fresh2) as freshmult.
    pose proof (eval_updMap_compat map q freshmult) as Heq'.
    rewrite Heq' in bounds.
    unfold af_evals.
    Flover_compute; simpl in *; try auto.
  }
  destruct (Qeq_bool (radius a1) 0) eqn: H1; destruct (Qeq_bool (radius a2) 0) eqn: H2; simpl;
    first [rewrite Qeq_bool_iff in H1 | apply Qeq_bool_neq in H1];
    first [rewrite Qeq_bool_iff in H2 | apply Qeq_bool_neq in H2].
  all: try rewrite upd_sound.
  all: (destruct (eval_aff (mult_aff_aux (a1, a2)) (updMap map n q)) eqn: Hn; try contradiction).
  all: simpl in *.
  all: unfold af_evals in Heq; rewrite Hn in Heq; simpl in Heq.
  all: try rewrite Heq.
  all: try rewrite H1.
  all: try rewrite H2.
  all: lra.
Qed.

(********************************************)
(*************** Subtraction ****************)
(********************************************)

Definition mult_aff_const (x: affine_form Q) (a: Q): affine_form Q :=
  mult_aff x (Const a) (get_max_index x + 1).

Lemma mult_aff_const_sound (af: affine_form Q) map v c:
  af_evals af v map ->
  af_evals (mult_aff_const af c) (v * c) map.
Proof.
  intros evals1.
  assert (af_evals (Const c) c map) as evals2.
  { unfold af_evals; now simpl. }
  unfold mult_aff_const, mult_aff.
  assert (radius (Const c) == 0) as rad2 by now simpl.
  rewrite <- Qeq_bool_iff in rad2; rewrite rad2; rewrite orb_true_r.
  assert (fresh (get_max_index af + 1) af) as fresh1 by (unfold fresh; omega).
  assert (fresh (get_max_index af + 1) (Const c)) as fresh2
      by (unfold fresh, get_max_index; simpl; omega).
  pose proof (mult_aff_sound _ _ _ fresh1 fresh2 evals1 evals2) as [q evalsm].
  unfold af_evals in evalsm |-*.
  pose proof (mult_aff_aux_fresh_compat fresh1 fresh2) as freshm.
  pose proof (eval_updMap_compat map q freshm) as mapcompat.
  unfold mult_aff in evalsm |-*.
  rewrite mapcompat.
  rewrite rad2, orb_true_r in evalsm.
  Flover_compute.
Qed.

Definition plus_aff_const (x: affine_form Q) (a: Q): affine_form Q :=
  plus_aff x (Const a).

Lemma plus_aff_const_sound (af: affine_form Q) map v c:
  af_evals af v map ->
  af_evals (plus_aff_const af c) (v + c) map.
Proof.
  intros evals1.
  assert (af_evals (Const c) c map) as evals2.
  { unfold af_evals; now simpl. }
  unfold plus_aff_const.
  apply (plus_aff_sound _ _ _ _ _ evals1 evals2).
Qed.

Definition negate_aff a: affine_form Q :=
  mult_aff_const a (-(1#1)).

Lemma negate_aff_sound a v map:
  af_evals a v map ->
  af_evals (negate_aff a) (-v) map.
Proof.
  intros evals.
  pose proof (mult_aff_const_sound _ _ _ (-(1#1)) evals) as evalsm.
  unfold af_evals in evalsm |-*.
  unfold negate_aff.
  Flover_compute.
  replace q0 with q by congruence.
  lra.
Qed.

Definition subtract_aff a1 a2: affine_form Q :=
  plus_aff a1 (negate_aff a2).

Lemma subtract_aff_sound a1 a2 v1 v2 map:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  af_evals (subtract_aff a1 a2) (v1 - v2) map.
Proof.
  intros evals1 evals2.
  unfold subtract_aff.
  pose proof (negate_aff_sound _ _ _ evals2) as evalsm.
  pose proof (plus_aff_sound _ _ _ _ _ evals1 evalsm) as evalsp. 
  unfold af_evals in evalsp |-*.
  Flover_compute.
  replace q0 with q by congruence; lra.
Qed.

(********************************************)
(******** Inverse of an affine form *********)
(********************************************)

(* Concerning the inverse of an affine form x lying in [a; b], it *)
(* should be alpha x + beta +- delta, with (explanation should be in the *)
(* annex of my report): *)

(*   alpha = -1/b^2 *)
(*   beta = 0.5(1/a+1/b) - 0.5 alpha (a + b) *)
(*   delta = 0.5(1/a-1/b - alpha (a - b) *)

(* *This is the case when 0 < a < b* *)

(* Then, if v \in x, to prove that 1/v in 1/x, the new noise term *)
(* should be 1/delta ( 1/v - alpha * v - beta ) *)

Definition inverse_aff (x: affine_form Q) (n: nat): affine_form Q :=
  let ivx := toIntv x in
  let a := minAbs ivx in
  let b := maxAbs ivx in
  let alpha := - (1/(b*b)) in
  let beta := (1/a + 1/b - alpha * (a+b)) / (2#1) in
  let delta := (1/a - 1/b - alpha * (a-b)) / (2#1) in
  let alphax := mult_aff_const x alpha in
  if (Qlt_bool (ivhi ivx) 0) then
    Noise n delta (plus_aff_const alphax (-beta))
  else
    Noise n delta (plus_aff_const alphax beta).

Lemma inverse_aff_sound (a: affine_form Q) (v: Q) map n:
  fresh n a ->
  above_zero a \/ below_zero a ->
  af_evals a v map ->
  exists q, af_evals (inverse_aff a n) (1/v) (updMap map n q).
Proof.
  intros freshn bounds evals.
  pose proof (valid_to_intv a) as validiv; unfold valid in validiv.
  pose proof (to_intv_containment _ _ _ evals) as containv.
  unfold inverse_aff, af_evals, toIntv; simpl.
  destruct bounds as [above | below]; unfold above_zero, below_zero in *.
  - pose proof (above_ivhi above) as above'.
    simpl in above, above', containv.
    assert (~ ((get_const a + radius a)) < 0) as not_below by lra.
    rewrite <- Qlt_bool_iff in not_below.
    apply eq_true_not_negb in not_below.
    rewrite negb_true_iff in not_below.
    rewrite not_below.
    simpl.
    pose (alpha := (- (1 / (maxAbs (get_const a - radius a, get_const a + radius a) *
                       maxAbs (get_const a - radius a, get_const a + radius a))))).
    fold alpha.
    pose (beta := ((1 / minAbs (get_const a - radius a, get_const a + radius a) +
                   1 / maxAbs (get_const a - radius a, get_const a + radius a) -
                   alpha *
                   (minAbs (get_const a - radius a, get_const a + radius a) +
                    maxAbs (get_const a - radius a, get_const a + radius a))) / 
                  (2 # 1))).
    fold beta.
    pose (delta := ((1 / minAbs (get_const a - radius a, get_const a + radius a) -
                   1 / maxAbs (get_const a - radius a, get_const a + radius a) -
                   alpha *
                   (minAbs (get_const a - radius a, get_const a + radius a) -
                    maxAbs (get_const a - radius a, get_const a + radius a))) / 
                  (2 # 1))).
    fold delta.
    destruct (Qeq_dec (radius a) 0) as [Hrad | Hrad].
    + exists noise_zero.
      assert (af_evals a v map) as constev by assumption.
      apply radius_0_const in constev; try assumption.
      rewrite upd_sound.
      pose proof (eval_updMap_compat map noise_zero freshn) as Heq'.
      unfold af_evals in evals.
      rewrite Heq' in evals.
      fold af_evals in evals.
      pose proof (mult_aff_const_sound a _ v alpha evals) as evalsm.
      pose proof (plus_aff_const_sound (mult_aff_const a alpha) _ _ beta evalsm) as evalsp.
      unfold af_evals in evalsp.
      destruct (eval_aff (plus_aff_const (mult_aff_const a alpha) beta) (updMap map n noise_zero)); try contradiction; simpl in evalsp.
      simpl.
      rewrite evalsp; clear evalsm evalsp Heq'.
      ring_simplify.
      unfold beta, alpha.
      unfold minAbs, maxAbs.
      simpl fst; simpl snd.
      rewrite Qabs_pos; try (simpl; lra).
      rewrite Q.min_l, Q.max_r; try (rewrite Qabs_pos; lra).
      rewrite Qabs_pos; try lra.
      rewrite Hrad, <- constev.
      field_simplify; try lra.
      field_simplify; try lra.
    + pose (needed_expr := (1/v - alpha * v - beta) / delta).
      assert (radius a > 0) as radgtzero by lra.
      unfold minAbs, maxAbs in *.
      simpl fst in *; simpl snd in *.
      assert (get_const a - radius a < get_const a + radius a) as intvnotpoint by lra.
      assert (v > 0) as vpos by lra.
      enough ((-(1#1)) <= needed_expr <= 1) as noise_bounds.
      * pose (needed_noise := exist (fun x => -(1#1) <= x <= 1) needed_expr noise_bounds).
        exists needed_noise.
        rewrite upd_sound.
        pose proof (eval_updMap_compat map needed_noise freshn) as Heq'.
        unfold af_evals in evals.
        rewrite Heq' in evals.
        fold af_evals in evals.
        pose proof (mult_aff_const_sound a _ v alpha evals) as evalsm.
        pose proof (plus_aff_const_sound (mult_aff_const a alpha) _ _ beta evalsm) as evalsp.
        unfold af_evals in evalsp.
        destruct (eval_aff (plus_aff_const (mult_aff_const a alpha) beta) (updMap map n needed_noise)); try contradiction; simpl in evalsp.
        simpl.
        rewrite evalsp; clear evalsm evalsp Heq'.
        unfold needed_expr.
        field_simplify; try lra.
        split; try lra.
        unfold delta, alpha.
        rewrite Qabs_pos; try (simpl; lra).
        rewrite Q.min_l, Q.max_r; try (rewrite Qabs_pos; lra).
        rewrite Qabs_pos; try lra.
        field_rewrite (get_const a - radius a - (get_const a + radius a) == (-(2#1)) * radius a).
        assert (get_const a > radius a) as constrad by lra.
        pose proof (Qpos_1_div_pos _ above) as frac1.
        pose proof (Qpos_1_div_pos _ above') as frac2.
        pose proof (Qlt_inverse_lt _ _ above' above intvnotpoint) as inversebounds.
        assert (1 / (get_const a - radius a) - 1 / (get_const a + radius a) > 0) as bounds1 by lra.
        field_rewrite (- (1 / ((get_const a + radius a) * (get_const a + radius a))) * (- (2 # 1) * radius a) == (((2 # 1) * radius a) / ((get_const a + radius a) * (get_const a + radius a)))).
        field_rewrite (1 / (get_const a - radius a) - 1 / (get_const a + radius a) == ((2#1) * radius a) / ((get_const a - radius a) * (get_const a + radius a))).
        field_rewrite (((2 # 1) * radius a / ((get_const a - radius a) * (get_const a + radius a)) -
   (2 # 1) * radius a / ((get_const a + radius a) * (get_const a + radius a))) / 
  (2 # 1) == (radius a / ((get_const a - radius a) * (get_const a + radius a)) -
   radius a / ((get_const a + radius a) * (get_const a + radius a)))).
        intros zeroeq.
        field_simplify_eq in zeroeq; try field_simplify; try lra.
        apply Qmult_integral in zeroeq.
        destruct zeroeq as [ zeroeq | zeroeq]; try lra.
        unfold Qpower, Qpower_positive, pow_pos in zeroeq.
        apply Qmult_integral in zeroeq; destruct zeroeq; lra.
      * rewrite <- Qabs_Qle_condition.
        unfold needed_expr, delta, beta, alpha.
        rewrite (Qabs_pos _ (Qlt_le_weak _ _ above)); try (simpl; lra).
        rewrite (Qabs_pos _ (Qlt_le_weak _ _ above')); try (simpl; lra).
        rewrite Q.min_l, Q.max_r; try (apply Qlt_le_weak; lra).
        pose (c := get_const a).
        pose (r := radius a).
        fold c r in above', above, intvnotpoint, containv, not_below, Hrad, radgtzero |-*.
        field_rewrite (c - r - (c + r) == (-(2#1)) * r).
        field_rewrite (- (1 / ((c + r) * (c + r))) * (- (2 # 1) * r) == (((2 # 1) * r) / ((c + r) * (c + r)))).
        field_rewrite (1 / (c - r) - 1 / (c + r) == ((2#1) * r) / ((c - r) * (c + r))).
        field_rewrite (((2 # 1) * r / ((c - r) * (c + r)) - (2 # 1) * r / ((c + r) * (c + r))) / (2 # 1) == (r / ((c - r) * (c + r)) - r / ((c + r) * (c + r)))).
        field_rewrite (c - r + (c + r) == (2#1) * c).
        field_rewrite (1 / (c - r) + 1 / (c + r) == ((2#1) * c) / ((c - r) * (c + r))).
        field_rewrite (1 / v - - (1 / ((c + r) * (c + r))) * v == 1 / v + (v / ((c + r) * (c + r)))).
        field_rewrite (((2 # 1) * c / ((c - r) * (c + r)) -
       - (1 / ((c + r) * (c + r))) * ((2 # 1) * c)) == ((2 # 1) * c / ((c - r) * (c + r)) + (((2 # 1) * c) / ((c + r) * (c + r))))).
        field_rewrite (((2 # 1) * c / ((c - r) * (c + r)) + (2 # 1) * c / ((c + r) * (c + r))) / (2 # 1) == (c / ((c - r) * (c + r)) + c / ((c + r) * (c + r)))).
        field_rewrite ((c / ((c - r) * (c + r)) + c / ((c + r) * (c + r))) == ((2#1) * c * c / ((c - r) * (c + r) * (c + r)))).
        field_rewrite (1 / v + v / ((c + r) * (c + r)) == ((c + r) * (c + r) + v * v) / (v * (c + r) * (c + r))).
        field_rewrite (((c + r) * (c + r) + v * v) / (v * (c + r) * (c + r)) == ((c + r) * (c + r) * (c - r) + v * v * (c - r)) / (v * (c + r) * (c + r) * (c - r))).
        field_rewrite ((2 # 1) * c * c / ((c - r) * (c + r) * (c + r)) == (2 # 1) * v * c * c / (v * (c - r) * (c + r) * (c + r))).
        field_rewrite ((((c + r) * (c + r) * (c - r) + v * v * (c - r)) / (v * (c + r) * (c + r) * (c - r)) - (2 # 1) * v * c * c / (v * (c - r) * (c + r) * (c + r))) == (((c + r) * (c + r) * (c - r) + v * v * (c - r) - (2 # 1) * v * c * c) / (v * (c - r) * (c + r) * (c + r)))).
        field_rewrite ((r / ((c - r) * (c + r)) - r / ((c + r) * (c + r))) == ((2#1) * r * r / ((c - r) * (c + r) * (c + r)))).
        assert ((c - r) * (c + r) * (c + r) > 0) as prodnonzero1
            by (field_rewrite (0 == 0 * (c + r) * (c + r)); erewrite Qmult_lt_r; try assumption;
                erewrite Qmult_lt_r; try assumption).
        assert ((2#1) * r * r > 0) as prodnonzero2
            by (field_rewrite (0 == 0 * r * r); erewrite Qmult_lt_r; try lra;
                erewrite Qmult_lt_r; try lra).
        assert (((2 # 1) * r * r / ((c - r) * (c + r) * (c + r))) > 0) as divnonzero
          by (apply Qdiv_pos; lra).
        rewrite (Qdiv_abs_le_bounds _ _ _ divnonzero).
        assert (v * (c - r) * (c + r) * (c + r) > 0) as prodnonzero3
            by (field_rewrite (0 == 0 * (c - r) * (c + r) * (c + r)); erewrite Qmult_lt_r; try lra;
                erewrite Qmult_lt_r; try lra; erewrite Qmult_lt_r; try lra).
        rewrite (Qdiv_abs_le_bounds _ _ _ prodnonzero3).
        rewrite Qmult_1_l.
        field_rewrite (((2 # 1) * r * r / ((c - r) * (c + r) * (c + r))) * (v * (c - r) * (c + r) * (c + r)) == ((2 # 1) * r * r * v)).
        assert ((2 # 1) * r * r * v > 0) as prodnonzero4
            by (field_rewrite (0 == 0 * r * r * v); erewrite Qmult_lt_r; try lra;
                erewrite Qmult_lt_r; try lra; erewrite Qmult_lt_r; try lra).
        field_rewrite ((c + r) * (c + r) * (c - r) + v * v * (c - r) == ((c + r) * (c + r) + v * v) * (c - r)).
        rewrite Qabs_Qle_condition.
        split.
        -- rewrite Qle_minus_iff.
           rewrite Qopp_involutive.
           field_rewrite (((c + r) * (c + r) + v * v) * (c - r) - (2 # 1) * v * c * c + (2 # 1) * r * r * v == ((c + r) * (c + r) + v * v - (2 # 1) * v * (c + r)) * (c - r)).
           apply Qmult_le_0_compat; try lra.
           field_rewrite ((c + r) * (c + r) + v * v - (2 # 1) * v * (c + r) == (c + r - v) * (c + r - v)).
           apply Qmult_le_0_compat; try lra.
        -- rewrite Qminus_le_iff.
           field_rewrite (((c + r) * (c + r) + v * v) * (c - r) - (2 # 1) * v * c * c + - ((2 # 1) * r * r * v) == ((c + r - v) * (c + r - v)) * (c - r) + - ((((2 # 1) * r) * ((2 # 1) * r)) * v)).
           rewrite <- Qminus_le_iff.
           destruct (Qeq_dec (c + r) v) as [vbound | vbound].
           ** rewrite <- vbound.
              field_rewrite ((c + r - (c + r)) * (c + r - (c + r)) * (c - r) == 0).
              field_rewrite ((2 # 1) * r * ((2 # 1) * r) * (c + r) == ((4 # 1) * r * r) * (c + r)).
              apply Qmult_le_0_compat; lra.
           ** apply Qmult_piecewise_le; try apply Qmult_nonneg; try lra.
              apply Qmult_piecewise_le; try lra.
  - pose proof (below_ivlo below) as below'.
    simpl in below, below', containv.
    assert (get_const a + radius a < 0) as below_bool by lra.
    rewrite <- Qlt_bool_iff in below_bool.
    rewrite below_bool.
    simpl.
    pose (alpha := (- (1 / (maxAbs (get_const a - radius a, get_const a + radius a) *
                       maxAbs (get_const a - radius a, get_const a + radius a))))).
    fold alpha.
    pose (beta := ((1 / minAbs (get_const a - radius a, get_const a + radius a) +
                   1 / maxAbs (get_const a - radius a, get_const a + radius a) -
                   alpha *
                   (minAbs (get_const a - radius a, get_const a + radius a) +
                    maxAbs (get_const a - radius a, get_const a + radius a))) / 
                  (2 # 1))).
    fold beta.
    pose (delta := ((1 / minAbs (get_const a - radius a, get_const a + radius a) -
                   1 / maxAbs (get_const a - radius a, get_const a + radius a) -
                   alpha *
                   (minAbs (get_const a - radius a, get_const a + radius a) -
                    maxAbs (get_const a - radius a, get_const a + radius a))) / 
                  (2 # 1))).
    fold delta.
    destruct (Qeq_dec (radius a) 0) as [Hrad | Hrad].
    + exists noise_zero.
      assert (af_evals a v map) as constev by assumption.
      apply radius_0_const in constev; try assumption.
      rewrite upd_sound.
      pose proof (eval_updMap_compat map noise_zero freshn) as Heq'.
      unfold af_evals in evals.
      rewrite Heq' in evals.
      fold af_evals in evals.
      pose proof (mult_aff_const_sound a _ v alpha evals) as evalsm.
      pose proof (plus_aff_const_sound (mult_aff_const a alpha) _ _ (- beta) evalsm) as evalsp.
      unfold af_evals in evalsp.
      destruct (eval_aff (plus_aff_const (mult_aff_const a alpha) (- beta)) (updMap map n noise_zero)); try contradiction; simpl in evalsp.
      simpl.
      rewrite evalsp; clear evalsm evalsp Heq'.
      ring_simplify.
      unfold beta, alpha.
      unfold minAbs, maxAbs.
      simpl fst; simpl snd.
      rewrite Qabs_neg; try (simpl; lra).
      rewrite Q.min_l, Q.max_r; try (rewrite Qabs_neg; lra).
      rewrite Qabs_neg; try lra.
      rewrite Hrad, <- constev.
      field_simplify; try lra.
      field_simplify; try lra.
    + pose (needed_expr := (1/v - alpha * v + beta) / delta).
      assert (radius a > 0) as radgtzero by lra.
      unfold minAbs, maxAbs in *.
      simpl fst in *; simpl snd in *.
      assert (get_const a - radius a < get_const a + radius a) as intvnotpoint by lra.
      assert (v < 0) as vpos by lra.
      enough ((-(1#1)) <= needed_expr <= 1) as noise_bounds.
      * pose (needed_noise := exist (fun x => -(1#1) <= x <= 1) needed_expr noise_bounds).
        exists needed_noise.
        rewrite upd_sound.
        pose proof (eval_updMap_compat map needed_noise freshn) as Heq'.
        unfold af_evals in evals.
        rewrite Heq' in evals.
        fold af_evals in evals.
        pose proof (mult_aff_const_sound a _ v alpha evals) as evalsm.
        pose proof (plus_aff_const_sound (mult_aff_const a alpha) _ _ (-beta) evalsm) as evalsp.
        unfold af_evals in evalsp.
        destruct (eval_aff (plus_aff_const (mult_aff_const a alpha) (-beta)) (updMap map n needed_noise)); try contradiction; simpl in evalsp.
        simpl.
        rewrite evalsp; clear evalsm evalsp Heq'.
        unfold needed_expr.
        field_simplify; try lra.
        split; try lra.
        unfold delta, alpha.
        rewrite Qabs_neg; try (simpl; lra).
        rewrite Qabs_neg; try (simpl; lra).
        rewrite Q.min_r, Q.max_l; try lra.
        field_rewrite (- (get_const a + radius a) - - (get_const a - radius a) == (-(2#1)) * radius a).
        assert (get_const a < radius a) as constrad by lra.
        assert (-(get_const a + radius a) > 0) as neg_below by lra.
        assert (-(get_const a - radius a) > 0) as neg_below' by lra.
        pose proof (Qpos_1_div_pos _ neg_below) as frac1.
        pose proof (Qpos_1_div_pos _ neg_below') as frac2.
        assert (-(get_const a - radius a) > -(get_const a + radius a)) as intvnotpoint' by lra.
        pose proof (Qlt_inverse_lt _ _ neg_below' neg_below intvnotpoint') as inversebounds.
        assert (1 / - (get_const a - radius a) - 1 / - (get_const a + radius a) < 0) as bounds1 by lra.
        field_rewrite (- (1 / (- (get_const a - radius a) * - (get_const a - radius a))) * (- (2 # 1) * radius a) == (((2 # 1) * radius a) / ((get_const a - radius a) * (get_const a - radius a)))).
        field_rewrite (1 / - (get_const a + radius a) - 1 / - (get_const a - radius a) == ((2#1) * radius a) / ((get_const a - radius a) * (get_const a + radius a))).
        field_rewrite (((2 # 1) * radius a / ((get_const a - radius a) * (get_const a + radius a)) -
   (2 # 1) * radius a / ((get_const a - radius a) * (get_const a - radius a))) / 
  (2 # 1) == (radius a / ((get_const a - radius a) * (get_const a + radius a)) -
   radius a / ((get_const a - radius a) * (get_const a - radius a)))).
        intros zeroeq.
        field_simplify_eq in zeroeq; try field_simplify; try lra.
        apply Qmult_integral in zeroeq.
        destruct zeroeq as [ zeroeq | zeroeq]; try lra.
        unfold Qpower, Qpower_positive, pow_pos in zeroeq.
        apply Qmult_integral in zeroeq; destruct zeroeq; lra.
      * rewrite <- Qabs_Qle_condition.
        unfold needed_expr, delta, beta, alpha.
        rewrite (Qabs_neg _ (Qlt_le_weak _ _ below)); try (simpl; lra).
        rewrite (Qabs_neg _ (Qlt_le_weak _ _ below')); try (simpl; lra).
        rewrite Q.min_r, Q.max_l; try (apply Qlt_le_weak; lra).
        pose (c := get_const a).
        pose (r := radius a).
        fold c r in below', below, intvnotpoint, containv, Hrad, radgtzero |-*.
        field_rewrite (-(c + r) + - (c - r) == (-(2#1)) * c).
        field_rewrite (- (1 / (- (c - r) * - (c - r))) * (- (2 # 1) * c) == (((2 # 1) * c) / ((c - r) * (c - r)))).
        field_rewrite (1 / - (c + r) + 1 / - (c - r) == - ((2#1) * c) / ((c - r) * (c + r))).
        field_rewrite ((- ((2 # 1) * c) / ((c - r) * (c + r)) - (2 # 1) * c / ((c - r) * (c - r))) / (2 # 1) == (-c / ((c - r) * (c + r)) - c / ((c - r) * (c - r)))).
        field_rewrite (- (c + r) - - (c - r) == -(2#1) * r).
        field_rewrite (1 / - (c + r) - 1 / - (c - r) == ((2#1) * r) / ((c - r) * (c + r))).
        field_rewrite (1 / v - - (1 / (- (c - r) * - (c - r))) * v == 1 / v + (v / ((c - r) * (c - r)))).
        field_rewrite (((2 # 1) * r / ((c - r) * (c + r)) - - (1 / (- (c - r) * - (c - r))) * (- (2 # 1) * r)) == ((2 # 1) * r / ((c - r) * (c + r)) - (((2 # 1) * r) / ((c - r) * (c - r))))).
        field_rewrite (((2 # 1) * r / ((c - r) * (c + r)) - (2 # 1) * r / ((c - r) * (c - r))) / (2 # 1) == (r / ((c - r) * (c + r)) - r / ((c - r) * (c - r)))).
        field_rewrite ((- c / ((c - r) * (c + r)) - c / ((c - r) * (c - r))) == (-(2#1) * c * c / ((c - r) * (c - r) * (c + r)))).
        field_rewrite (1 / v + v / ((c - r) * (c - r)) == ((c - r) * (c - r) + v * v) / (v * (c - r) * (c - r))).
        field_rewrite (((c - r) * (c - r) + v * v) / (v * (c - r) * (c - r)) == ((c - r) * (c - r) * (c + r) + v * v * (c + r)) / (v * (c - r) * (c - r) * (c + r))).
        field_rewrite (- (2 # 1) * c * c / ((c - r) * (c - r) * (c + r)) == - (2 # 1) * v * c * c / (v * (c - r) * (c - r) * (c + r))).
        field_rewrite ((((c - r) * (c - r) * (c + r) + v * v * (c + r)) / (v * (c - r) * (c - r) * (c + r)) + - (2 # 1) * v * c * c / (v * (c - r) * (c - r) * (c + r))) == (((c - r) * (c - r) * (c + r) + v * v * (c + r) - (2 # 1) * v * c * c) / (v * (c - r) * (c - r) * (c + r)))).
        field_rewrite ((r / ((c - r) * (c + r)) - r / ((c - r) * (c - r))) == (- (2#1) * r * r / ((c - r) * (c - r) * (c + r)))).
        field_rewrite (- (2 # 1) * r * r / ((c - r) * (c - r) * (c + r)) == - (2 # 1) * v * r * r / (v * (c - r) * (c - r) * (c + r))).
        assert (v * (c - r) * ((c - r) * (c + r)) > 0) as prodnonzero1
            by (apply Qmult_nonneg with (x := v * (c - r)); apply Qmult_neg_nonneg; assumption). 
        assert (- (2#1) * v * r * r > 0) as prodnonzero2
            by (field_rewrite (0 == 0 * r * r); erewrite Qmult_lt_r; try lra;
                erewrite Qmult_lt_r; try lra).
        assert ((- (2 # 1) * v * r * r / (v * (c - r) * (c - r) * (c + r))) > 0) as divnonzero
          by (apply Qdiv_pos; lra).
        rewrite (Qdiv_abs_le_bounds _ _ _ divnonzero).
        assert (v * (c - r) * (c - r) * (c + r) > 0) as prodnonzero3 by lra.
        rewrite (Qdiv_abs_le_bounds _ _ _ prodnonzero3).
        rewrite Qmult_1_l.
        field_rewrite ((- (2 # 1) * v * r * r / (v * (c - r) * (c - r) * (c + r))) * (v * (c - r) * (c - r) * (c + r)) == (- (2 # 1) * r * r * v)).
        field_rewrite ((c - r) * (c - r) * (c + r) + v * v * (c + r) == ((c - r) * (c - r) + v * v) * (c + r)).
        rewrite Qabs_Qle_condition.
        split.
        -- rewrite Qle_minus_iff.
           rewrite Qopp_involutive.
           field_rewrite (((c - r) * (c - r) + v * v) * (c + r) - (2 # 1) * v * c * c + - (2 # 1) * r * r * v == (c - r - v) * ((c - r - v) * (c + r)) + - (((2 # 1) * r) * (((2 # 1) * r) * v))).
           rewrite <- Qle_minus_iff.
           destruct (Qeq_dec (c - r) v) as [vbound | vbound].
           ** rewrite <- vbound.
              field_rewrite ((c - r - (c - r)) * ((c - r - (c - r)) * (c + r)) == 0).
              field_rewrite ((2 # 1) * r * ((2 # 1) * r * (c - r)) == ((4 # 1) * r * r) * (c - r)).
              assert ((4#1) * r * r > 0) by (apply Qmult_nonneg; lra).
              field_rewrite (0 == (4#1) * r * r * 0).
              apply Qmult_le_l; try lra.
           ** field_rewrite ((2 # 1) * r * ((2 # 1) * r * v) == --((2 # 1) * r * ((2 # 1) * r * v))).
              field_rewrite (((c - r - v) * ((c - r - v) * (c + r))) == --((c - r - v) * ((c - r - v) * (c + r)))).
              apply Qopp_le_compat.
              field_rewrite (- ((c - r - v) * ((c - r - v) * (c + r))) == -(c - r - v) * (-(c - r - v) * - (c + r))).
              field_rewrite (- ((2 # 1) * r * ((2 # 1) * r * v)) == (2 # 1) * r * ((2 # 1) * r * -v)).
              apply Qmult_piecewise_le; try apply Qmult_nonneg; try lra.
              apply Qmult_piecewise_le; try lra.
        -- rewrite Qle_minus_iff.
           field_rewrite (- (2 # 1) * r * r * v + - (((c - r) * (c - r) + v * v) * (c + r) - (2 # 1) * v * c * c) == ((c - r) * (c - r) + v * v - (2 # 1) * v * (c - r)) * -(c + r)).
           apply Qmult_le_0_compat; try lra.
           field_rewrite ((c - r) * (c - r) + v * v - (2 # 1) * v * (c - r) == (c - r - v) * (c - r - v)).
           apply Qsquare_nonneg.
Qed.

(********************************************)
(***************** Division *****************)
(********************************************)

Lemma mult_aff_const_fresh_compat a c n:
  fresh n a ->
  fresh n (mult_aff_const a c).
Proof.
  intros fr.
  unfold mult_aff_const, mult_aff.
  simpl.
  assert (Qeq_bool 0 0 = true) by trivial.
  rewrite H.
  rewrite orb_true_r.
  apply mult_aff_aux_fresh_compat; try assumption.
  unfold fresh, get_max_index in *.
  simpl; omega.
Qed.

Lemma plus_aff_const_fresh_compat a c n:
  fresh n a ->
  fresh n (plus_aff_const a c).
Proof.
  intros fr.
  unfold plus_aff_const, plus_aff.
  apply plus_aff_fresh_compat; try assumption.
  unfold fresh, get_max_index in *.
  simpl; omega.
Qed.

Lemma inverse_aff_fresh_compat a (n: nat):
  fresh n a ->
  fresh (n + 1) (inverse_aff a n).
Proof.
  intros fr.
  unfold inverse_aff.
  destruct (Qlt_bool (ivhi (toIntv a)) 0).
  Set Default Goal Selector "all".
  apply fresh_noise; try omega.
  apply plus_aff_const_fresh_compat.
  apply mult_aff_const_fresh_compat.
  now apply fresh_inc.
  Set Default Goal Selector "1".
Qed.

Lemma above_below_nonzero a v map:
  above_zero a \/ below_zero a ->
  af_evals a v map ->
  ~ v == 0.
Proof.
  intros bounds evals.
  destruct a.
  - unfold above_zero, below_zero in bounds.
    simpl in bounds.
    apply constant_evals_unchanged in evals.
    destruct bounds as [bound | bound]; simpl in bound; lra.
  - apply bounded_af_evals_abs in evals.
    simpl radius in evals.
    simpl get_const in evals.
    destruct bounds as [bound | bound].
    + unfold above_zero in bound; simpl in bound.
      assert (get_const a > Qabs q + radius a) as constbound by lra.
      destruct (Qlt_le_dec (v - get_const a) 0).
      * rewrite Qabs_neg in evals; try lra.
      * rewrite Qabs_pos in evals; try lra.
    + unfold below_zero in bound; simpl in bound.
      destruct (Qlt_le_dec (v - get_const a) 0).
      * rewrite Qabs_neg in evals; try lra.
      * rewrite Qabs_pos in evals; try lra.
Qed.

Definition divide_aff a1 a2 n :=
  mult_aff a1 (inverse_aff a2 n) (n + 1).

Lemma divide_aff_sound a1 a2 v1 v2 n map:
  fresh n a1 ->
  fresh n a2 ->
  above_zero a2 \/ below_zero a2 ->
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  exists qI qM, af_evals (divide_aff a1 a2 n) (v1 / v2) (updMap (updMap map n qI) (n + 1) qM).
Proof.
  intros fresh1 fresh2 bounds2 evals1 evals2.
  pose proof inverse_aff_sound as inv.
  pose proof (above_below_nonzero _ bounds2 evals2) as v2nonzero.
  specialize (inv a2 v2 map n fresh2 bounds2 evals2) as [qInv evalsi].
  pose proof mult_aff_sound as mul.
  apply inverse_aff_fresh_compat in fresh2.
  unfold af_evals in evals1.
  rewrite (eval_updMap_compat _ qInv fresh1) in evals1.
  apply fresh_inc in fresh1.
  specialize (mul a1 (inverse_aff a2 n) v1 (1 / v2) (updMap map n qInv)
                  ((n + 1)%nat) fresh1 fresh2 evals1 evalsi) as [qMul evalsm].
  unfold af_evals, divide_aff in evalsm |-*.
  exists qInv, qMul.
  Flover_compute.
  - replace q1 with q by congruence.
    rewrite evalsm.
    field; auto.
  - replace q0 with q by congruence.
    rewrite evalsm.
    field; auto.
Qed.
