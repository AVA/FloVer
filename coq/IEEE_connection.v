From Coq
     Require Import Reals.Reals QArith.QArith QArith.Qabs micromega.Psatz
     QArith.Qreals.

From Flover
     Require Import Expressions Infra.RationalSimps TypeValidator IntervalValidation
     ErrorAnalysis CertificateChecker SubdivsChecker FPRangeValidator RoundoffErrorValidator
     Environments Infra.RealRationalProps Infra.Ltacs.

From Flocq
     Require Import Core.Generic_fmt Core.FLT Core.Zaux Calc.Round
     IEEE754.Binary IEEE754.Bits.

From Flocq.Prop
     Require Import Relative.

(**
   Definitions of static values.
   dmode is the rounding mode used for the proofs;
   fl64 specifies the type of binary, 64-bit floating-point words;
   flt_exp_64 is the Flocq library function FLT_exp for 64-bit floats
**)
Definition dmode := mode_NE.
Definition fl64:Type := binary_float 53 1024.
Definition flt_exp_64 := FLT_exp (3 - 1024 - 53) 53.

(* Additional assumptions necessary to obtain relative error later *)
Lemma valid_flt_64:
  Valid_exp flt_exp_64.
Proof.
  apply FLT_exp_valid.
  unfold FLX.Prec_gt_0. omega.
Qed.

Lemma valid_flt_64_assum:
  forall k : Z,
    (-1022 < k)%Z ->
    (53 <= k - (flt_exp_64 k))%Z.
Proof.
  intros k k_pos.
  unfold flt_exp_64, Core.FLT.FLT_exp; simpl.
  destruct (Z.max_spec_le (k - 53) (-1074)); omega.
Qed.

(** Flocq relative error theorem instantiated for 64-bit floats *)
Definition relative_error_64_ex :=
  @relative_error_N_ex radix2 flt_exp_64
                       valid_flt_64
                       (-1022) 53 valid_flt_64_assum
                       (fun x => negb (Z.even x)).

(**
   Our deterministic semantics uses a map to define roundoff errors.
   We define an IEEE-754 correct delta map and show that it correctly
   provides a roundoff error
**)
Definition IeeeDeltaMap (x:R) (m:mType): option R :=
  match m with
  | M64 => if (Raux.Rle_bool (Raux.bpow radix2 (-1022)) (Rabs x))
          then Some (((round radix2 flt_exp_64 (round_mode dmode) x) - x) / x)%R
          else Some 0%R (* failsafe case, no normal number provided *)
  | _ => Some 0%R
  end.

Ltac solve_0_tac :=
  exists 0%R; split; try auto; rewrite Rabs_R0; apply mTypeToR_pos_R.

Lemma IeeeDeltaMap_valid:
  forall (v : R) (m : mType),
  exists d : R, IeeeDeltaMap v m = Some d /\ (Rabs d <= mTypeToR m)%R.
Proof.
  intros. unfold IeeeDeltaMap.
  destruct m; try (solve_0_tac; fail).
  destruct (Raux.Rle_bool (Raux.bpow radix2 (-1022)) (Rabs v)) eqn:?.
  (* Normal number case *)
  - pose proof (relative_error_64_ex v) as rel_error_ex.
    pose proof (Raux.Rle_bool_spec (Raux.bpow radix2 (-1022)) (Rabs v)) as Rle_reflect.
    rewrite Heqb in *.
    inversion Rle_reflect.
    destruct rel_error_ex as (eps & rel_error_bounded & rel_error_def); [auto|].
    unfold dmode.
    assert (round_mode mode_NE = Znearest (fun x => negb (Z.even x)))
      as round_mode_def by auto.
    rewrite round_mode_def.
    rewrite rel_error_def.
    assert (v <> 0%R).
    { hnf; intros. subst. rewrite Rabs_R0 in *.
      pose proof (Raux.bpow_gt_0 radix2 (-1022)). lra. }
    rewrite Rmult_plus_distr_l, Rmult_1_r.
    field_rewrite (v + v * eps - v = v * eps)%R.
    field_rewrite (v * eps / v = eps)%R.
    exists eps; split; try auto.
    cut (mTypeToR M64 = / 2 * Raux.bpow radix2 (- (53) + 1))%R.
    + intros HeqType; rewrite HeqType; auto.
    + unfold mTypeToR.
      change (- (53) + 1)%Z with (- (52))%Z.
      rewrite Raux.bpow_opp.
      rewrite <- Rinv_mult_distr; try lra.
      2: {
        pose proof (Raux.bpow_gt_0 radix2 52).
        hnf; intros; lra. }
      rewrite <- inv_eq_1_div. f_equal.
      rewrite Raux.bpow_powerRZ.
      field_rewrite (2 ^ 53 = 2 * 2 ^ 52)%R.
      f_equal.
  - solve_0_tac.
Qed.

Definition normal_or_zero v :=
   (v = 0 \/ (Q2R (minValue_pos M64)) <= (Rabs v))%R.

Definition updFlEnv x v E :=
  fun y => if y =? x
        then Some (A:=(binary_float 53 1024)) v
        else E y.

(**
   Definition of IEEE 754 evaluation for FloVer expressions
**)
Fixpoint eval_expr_float (e:expr (binary_float 53 1024)) (E:nat -> option fl64):=
  match e with
  | Var _ x => E x
  | Const m v => Some v
  | Unop Neg e =>
    match eval_expr_float e E with
    |Some v1 => Some (b64_opp v1)
    |_ => None
    end
  | Unop Inv e => None
  | Binop b e1 e2 =>
    match eval_expr_float e1 E, eval_expr_float e2 E with
    | Some f1, Some f2 =>
      match b with
      | Plus => Some (b64_plus dmode f1 f2)
      | Sub => Some (b64_minus dmode f1 f2)
      | Mult => Some (b64_mult dmode f1 f2)
      | Div => Some (b64_div dmode f1 f2)
      end
    |_ , _ => None
    end
  | Fma e1 e2 e3 =>
    match eval_expr_float e1 E, eval_expr_float e2 E, eval_expr_float e3 E with
      (* | Some f1, Some f2, Some f3 => Some (b64_plus dmode f1 (b64_mult dmode f2 f3)) *)
      | _, _, _ => None
    end
  | Let m x e1 e2 =>
    olet res :=  eval_expr_float e1 E in
        eval_expr_float e2 (updFlEnv x res E)
  | _ => None
  end.

Definition isValid e :=
  plet v := e in
  normal_or_zero (B2R 53 1024 v).

Fixpoint eval_expr_valid (e:expr fl64) E :=
  match e with
  | Var _ x => True (*isValid (eval_expr_float (Var n) E)*)
  | Const m v => plet v := eval_expr_float (Const m v) E in
        if is_finite 53 1024 v then True else False
  | Unop u e => eval_expr_valid e E
  | Binop b e1 e2 =>
    (eval_expr_valid e1 E) /\ (eval_expr_valid e2 E) /\
    (let e1_res := eval_expr_float e1 E in
     let e2_res := eval_expr_float e2 E in
     optionBind e1_res
                (fun v1 =>
                   let v1_real := B2R 53 1024 v1 in
                   optionBind e2_res
                              (fun v2 =>
                                 let v2_real := B2R 53 1024 v2 in
                                 normal_or_zero (evalBinop b v1_real v2_real))
                              True)
                True)
  | Fma e1 e2 e3 =>
    (eval_expr_valid e1 E) /\ (eval_expr_valid e2 E) /\ (eval_expr_valid e3 E) /\
    (let e1_res := eval_expr_float e1 E in
     let e2_res := eval_expr_float e2 E in
     let e3_res := eval_expr_float e3 E in
     optionBind e1_res
                (fun v1 =>
                   let v1_real := B2R 53 1024 v1 in
                   optionBind e2_res
                              (fun v2 =>
                                 let v2_real := B2R 53 1024 v2 in
                                 optionBind e3_res
                                            (fun v3 =>
                                               let v3_real := B2R 53 1024 v3 in
                                               (* No support for fma yet *)
                                               (* normal_or_zero (evalFma v1_real v2_real v3_real)) *)
                                               False)
                                            True)
                              True)
                True)
  | Downcast m e => eval_expr_valid e E
  | Let m x e1 e2 =>
    eval_expr_valid e1 E /\
    (optionBind (eval_expr_float e1 E)
                (fun v_e => eval_expr_valid e2 (updFlEnv x v_e E))
                True)
  end.

Definition bpowQ (r:radix) (e: Z) :=
  match e with
  |0%Z => 1%Q
  | Zpos p => (Z.pow_pos r p) #1
  | Z.neg p => Qinv ((Z.pow_pos r p)#1)
  end.

Definition B2Q :=
  fun prec emax : Z =>
    let emin := (3 - emax - prec)%Z in
    let fexpr := FLT_exp emin prec in
    fun f : binary_float prec emax =>
      match f with
      | B754_infinity _ _ _ => bpowQ radix2 emax + 1%Q
      | B754_nan _ _ _ _ _ => bpowQ radix2 emax + 1%Q
      | B754_zero _ _ _ => 0%Q
      | B754_finite _ _ s m e _ =>
        let f_new: Core.Defs.float radix2 :=
            {| Core.Defs.Fnum := cond_Zopp s (Zpos m); Core.Defs.Fexp := e |} in
        (Core.Defs.Fnum f_new # 1) * bpowQ radix2 (Core.Defs.Fexp f_new)
      end.

Lemma B2Q_B2R_eq :
  forall v,
    is_finite 53 1024 v = true ->
    Q2R (B2Q v) = B2R 53 1024 v.
Proof.
  intros; unfold B2Q, B2R, is_finite in *.
  destruct v eqn:?; try congruence;
    try rewrite Q2R0_is_0; try lra.
  unfold Core.Defs.F2R.
  rewrite Q2R_mult.
  f_equal.
  - unfold Q2R.
    simpl. rewrite Rinv_1, Rmult_1_r.
    auto.
  - unfold Q2R; simpl.
    unfold bpowQ.
    destruct e; simpl; try lra.
    unfold IZR. unfold Qinv; simpl.
    destruct (Z.pow_pos 2 p) eqn:? ; try rewrite P2R_INR, INR_IPR;
      simpl; try lra.
    + unfold bounded in e0. simpl in e0. unfold canonical_mantissa in e0.
      simpl in e0.
      pose proof (Is_true_eq_left _ e0).
      apply Is_true_eq_true in H0; andb_to_prop H0.
      assert (0 < Z.pow_pos 2 p)%Z.
      { apply Zpower_pos_gt_0. cbv; auto. }
      rewrite Heqz in H0. inversion H0.
    + unfold IPR at 1. rewrite Rmult_1_l; auto.
    + rewrite <- Ropp_mult_distr_l, Ropp_mult_distr_r, Ropp_inv_permute;
        [ unfold IPR at 1; lra | ].
      hnf; intros. pose proof (pos_INR_nat_of_P p0).
      rewrite INR_IPR in *.
      rewrite H0 in H1; lra.
Qed.

Fixpoint B2Qexpr (e: expr fl64) :=
  match e with
  | Var _ x =>  Var Q x
  | Const m v => Const m (B2Q v)
  | Unop u e => Unop u (B2Qexpr e)
  | Binop b e1 e2 => Binop b (B2Qexpr e1) (B2Qexpr e2)
  | Fma e1 e2 e3 => Fma (B2Qexpr e1) (B2Qexpr e2) (B2Qexpr e3)
  | Downcast m e => Downcast m (B2Qexpr e)
  | Let m x e1 e2 => Let m x (B2Qexpr e1) (B2Qexpr e2)
  (* | Cond e1 e2 e3 => Cond (B2Qexpr e1) (B2Qexpr e2) (B2Qexpr e3) *)
  end.

Definition toREnv (E: nat -> option fl64) (x:nat):option R :=
  match E x with
  |Some v => Some (Q2R (B2Q v))
  |_ => None
  end.

Definition is64BitEnv (Gamma:FloverMap.t mType) :=
  forall (e:expr Q) (m:mType),
    FloverMap.find e Gamma = Some m ->
    m = M64.

Fixpoint is64BitEval (V:Type) (e:expr V) :=
  match e with
  | Var _ x => True
  | Const m e => m = M64
  | Unop u e => is64BitEval e
  | Binop b e1 e2 => is64BitEval e1 /\ is64BitEval e2
  | Fma e1 e2 e3 => is64BitEval e1 /\ is64BitEval e2 /\ is64BitEval e3
  | Downcast m e => m = M64 /\ is64BitEval e
  | Let m x e g => is64BitEval e /\ m = M64 /\ is64BitEval g
  (* | Cond e1 e2 e3 => is64BitEval e1 /\ is64BitEval e2 /\ is64BitEval e3 *)
  end.

Fixpoint noDowncast (V:Type) (e:expr V) :=
  match e with
  | Var _ x => True
  | Const m e => True
  | Unop u e => noDowncast e
  | Binop b e1 e2 => noDowncast e1 /\ noDowncast e2
  | Fma e1 e2 e3 => noDowncast e1 /\ noDowncast e2 /\ noDowncast e3
  | Downcast m e => False
  | Let m x e1 e2 => noDowncast e1 /\ noDowncast e2
  (* | Cond e1 e2 e3 => noDowncast e1 /\ noDowncast e2 /\ noDowncast e3 *)
  end.

Opaque mTypeToQ.

Lemma validValue_is_finite v:
  validFloatValue (Q2R (B2Q v)) M64 ->
  is_finite 53 1024 v = true.
Proof.
  intros validVal.
  unfold is_finite.
  unfold validFloatValue, B2Q in *.
  destruct v; try auto;
    destruct validVal; unfold Normal in *; unfold Denormal in *;
      unfold maxValue, minValue_pos, maxExponent, minExponentPos in*;
      rewrite Q2R_inv in *; unfold bpowQ in *.
  - cbn in *.
    destruct H; try lra.
    rewrite Rabs_right in H0.
    apply Rle_Qle in H0.
    + rewrite <- Qle_bool_iff in H0.
      cbv in H0; try congruence.
    + rewrite <- Q2R0_is_0.
      apply Rle_ge. apply Qle_Rle; rewrite <- Qle_bool_iff; cbv; auto.
  - vm_compute; intros; congruence.
  - destruct H.
    + destruct H. rewrite Rabs_right in H.
      * rewrite <- Q2R_inv in H.
        apply Rlt_Qlt in H.
        vm_compute in H.
        congruence.
        vm_compute; congruence.
      * rewrite <- Q2R0_is_0.
        apply Rle_ge. apply Qle_Rle; rewrite <- Qle_bool_iff; cbv; auto.
    + rewrite <- Q2R0_is_0 in H.
      apply eqR_Qeq in H.
       vm_compute in H; congruence.
  - vm_compute; congruence.
  - destruct H.
    rewrite Rabs_right in H0.
    + apply Rle_Qle in H0.
      rewrite <- Qle_bool_iff in H0.
      vm_compute in H0; auto.
    + rewrite <- Q2R0_is_0.
      apply Rle_ge. apply Qle_Rle; rewrite <- Qle_bool_iff; cbv; auto.
  - vm_compute; congruence.
  - destruct H.
    + rewrite Rabs_right in H.
      * destruct H. rewrite <- Q2R_inv in H.
        { apply Rlt_Qlt in H. rewrite Qlt_alt in H.
          vm_compute in H. congruence. }
        { vm_compute; congruence. }
      * rewrite <- Q2R0_is_0.
        apply Rle_ge. apply Qle_Rle; rewrite <- Qle_bool_iff; cbv; auto.
    + rewrite <- Q2R0_is_0 in H.
      apply eqR_Qeq in H. vm_compute in H; congruence.
  - vm_compute; congruence.
Qed.

Lemma typing_expr_64_bit e:
  forall Gamma,
    is64BitEnv Gamma ->
    validTypes e Gamma ->
    FloverMap.find e Gamma = Some M64.
Proof.
  destruct e; intros * Gamma_64Bit [mG [find_mG ?]];
    erewrite <- Gamma_64Bit; eauto.
Qed.

Lemma round_0_zero:
  (round radix2 flt_exp_64 (round_mode mode_NE) 0) = 0%R.
Proof.
  unfold round. simpl.
  unfold scaled_mantissa.
  rewrite Rmult_0_l.
  unfold Znearest.
  unfold Raux.Zfloor.
  assert (up 0 = 1%Z).
  { symmetry. apply tech_up; lra. }
  rewrite H.
  simpl. rewrite Rsub_eq_Ropp_Rplus. rewrite Rplus_opp_r.
  assert (Raux.Rcompare (0) (/ 2) = Lt).
  { apply Raux.Rcompare_Lt. lra. }
  rewrite H0.
  unfold cexp.
  unfold Defs.F2R; simpl.
  rewrite Rmult_0_l. auto.
Qed.

Ltac find_cases :=
  rewrite map_find_add;
  match goal with
  | [H: _ |- context[ Q_orderedExps.compare ?e1 ?e2]] =>
    destruct (Q_orderedExps.compare e1 e2) eqn:?
  end;
  try auto.

Lemma getValidMap_preserving e:
  forall defVars akk res,
    (forall e m, FloverMap.find e akk = Some m -> m = M64) ->
    is64BitEval e ->
    getValidMap defVars e akk = Succes res ->
    (forall e m, FloverMap.find e defVars = Some m -> m = M64) ->
    forall e m, FloverMap.find e res = Some m -> m = M64.
Proof.
  pose (eT := e);
  induction e; intros * valid_akk is64Bit_e getMap_succeeds find_defVars *;
    destruct (FloverMap.mem eT akk) eqn:case_mem;
    cbn in *; subst eT; rewrite case_mem in *;
      try (inversion getMap_succeeds; subst; now apply valid_akk).
  - Flover_compute.
    inversion getMap_succeeds; subst.
    assert (m0 = M64) by (eapply find_defVars; eauto).
    subst; find_cases; try (now apply valid_akk).
    congruence.
  - unfold isMonotone in *.
    Flover_compute.
    + destruct (mTypeEq m1 m) eqn:?; try congruence; type_conv; subst.
      inversion getMap_succeeds; subst.
      find_cases; try (now apply valid_akk).
      congruence.
    + inversion getMap_succeeds; subst.
      find_cases; try (now apply valid_akk).
      congruence.
  - destruct (getValidMap defVars e akk) eqn:?; simpl in *; try congruence.
    destruct (FloverMap.find e t) eqn:?; try congruence.
    assert (m0 = M64).
    { eapply IHe with (akk:=akk); eauto. }
    subst. cbn in getMap_succeeds.
    unfold addMono, isMonotone in *.
    destruct (FloverMap.find (Unop u e) defVars) eqn:?.
    + destruct (mTypeEq m0 M64) eqn:?; try congruence; type_conv; subst.
      Flover_compute; inversion getMap_succeeds; subst.
      find_cases; try (eapply IHe with (akk:=akk); eauto).
      congruence.
    + Flover_compute; inversion getMap_succeeds; subst.
      find_cases; try (eapply IHe with (akk:=akk); eauto).
      congruence.
  - destruct (getValidMap defVars e1 akk) eqn:?;
             simpl in *; try congruence.
    destruct (getValidMap defVars e2 t) eqn:?;
             simpl in *; try congruence.
    destruct (FloverMap.find e1 t0) eqn:?;
             try congruence.
    destruct (FloverMap.find e2 t0) eqn:?;
             try congruence.
    destruct is64Bit_e.
    assert (forall e m, FloverMap.find e t = Some m -> m = M64)
      as valid_t
           by (eapply IHe1 with (akk:=akk); eauto).
    assert (forall e m, FloverMap.find e t0 = Some m -> m = M64)
      as valid_t0
           by (eapply IHe2 with (akk:=t); eauto).
    clear IHe1 IHe2.
    assert (m0 = M64) by (eapply valid_t0; eauto).
    assert (m1 = M64) by (eapply valid_t0; eauto).
    subst; cbn in *.
    unfold isMonotone, addMono in *.
    destruct (FloverMap.find (Binop b e1 e2) defVars) eqn:?.
    + destruct (mTypeEq m0 M64) eqn:?; try congruence; type_conv; subst.
      Flover_compute.
      inversion getMap_succeeds; subst.
      find_cases; try (eapply valid_t0; now eauto).
      congruence.
    + Flover_compute.
      inversion getMap_succeeds; subst.
      find_cases; try (eapply valid_t0; now eauto).
      congruence.
  - destruct (getValidMap defVars e1 akk) eqn:?;
             simpl in *; try congruence.
    destruct (getValidMap defVars e2 t) eqn:?;
             simpl in *; try congruence.
    destruct (getValidMap defVars e3 t0) eqn:?;
             simpl in *; try congruence.
    destruct (FloverMap.find e1 t1) eqn:?;
             try congruence.
    destruct (FloverMap.find e2 t1) eqn:?;
             try congruence.
    destruct (FloverMap.find e3 t1) eqn:?;
             try congruence.
    destruct is64Bit_e as (? & ? & ?).
    assert (forall e m, FloverMap.find e t = Some m -> m = M64)
      as valid_t
           by (eapply IHe1 with (akk:=akk); eauto).
    assert (forall e m, FloverMap.find e t0 = Some m -> m = M64)
      as valid_t0
           by (eapply IHe2 with (akk:=t); eauto).
    assert (forall e m, FloverMap.find e t1 = Some m -> m = M64)
      as valid_t1
           by (eapply IHe3 with (akk:=t0); eauto).
    clear IHe1 IHe2 IHe3.
    assert (m0 = M64 /\ m1 = M64 /\ m2 = M64)
           as (? & ? & ?)
             by (repeat split; eapply valid_t1; eauto).
    subst; cbn in *.
    unfold isMonotone, addMono in *.
    destruct (FloverMap.find (Fma e1 e2 e3) defVars) eqn:?.
    + destruct (mTypeEq m0 M64) eqn:?; try congruence; type_conv; subst.
      Flover_compute.
      inversion getMap_succeeds; subst.
      find_cases; try (eapply valid_t1; now eauto).
      congruence.
    + Flover_compute.
      inversion getMap_succeeds; subst.
      find_cases; try (eapply valid_t1; now eauto).
      congruence.
  - destruct (getValidMap defVars e akk) eqn:?; simpl in *; try congruence.
    destruct (FloverMap.find e t) eqn:?; try congruence.
    destruct is64Bit_e; subst.
    assert (m1 = M64).
    { eapply IHe with (akk:=akk); eauto. }
    subst. cbn in getMap_succeeds.
    unfold addMono, isMonotone in *.
    destruct (FloverMap.find (Downcast M64 e) defVars) eqn:?.
    + destruct (mTypeEq m M64) eqn:?; try congruence; type_conv; subst.
      Flover_compute; inversion getMap_succeeds; subst.
      find_cases; try (eapply IHe with (akk:=akk); eauto).
      congruence.
    + Flover_compute; inversion getMap_succeeds; subst.
      find_cases; try (eapply IHe with (akk:=akk); eauto).
      congruence.
  - destruct (getValidMap defVars e1 akk) eqn:?;
             simpl in *; try congruence.
    destruct (FloverMap.find e1 t) eqn:?; [ | congruence].
    destruct is64Bit_e as (? & ? & ?); subst.
    assert (m1 = M64).
    { eapply IHe1 with (akk:=akk); eauto. }
    subst. cbn in getMap_succeeds.
    unfold addMono, isMonotone in *.
    destruct (FloverMap.find (Var Q n) t) eqn:?;
             simpl in *; [ congruence | ].
    destruct (getValidMap defVars e2 (FloverMap.add (Var Q n) M64 t)) eqn:?;
             simpl in *; try congruence.
    destruct (FloverMap.find e2 t0) eqn:?;
             try congruence.
    assert (forall e m, FloverMap.find e t = Some m -> m = M64)
      as valid_t
           by (eapply IHe1 with (akk:=akk); eauto).
    assert (forall e m, FloverMap.find e t0 = Some m -> m = M64)
      as valid_t0.
    { eapply IHe2 with (akk:=(FloverMap.add (Var Q n) M64 t)); try eauto.
      intros. rewrite map_find_add in H0.
      destruct (Q_orderedExps.compare (Var Q n) e0) eqn:?; [ congruence | | ];
        eapply valid_t; eauto. }
    clear IHe1 IHe2.
    assert (m = M64) by (eapply valid_t0; eauto).
    subst.
    destruct (FloverMap.find (Let M64 n e1 e2) defVars) eqn:?.
    + destruct (mTypeEq m M64) eqn:?; try congruence; type_conv; subst.
      Flover_compute.
      inversion getMap_succeeds; subst.
      find_cases; try (eapply valid_t0; now eauto).
      congruence.
    + Flover_compute.
      inversion getMap_succeeds; subst.
      find_cases; try (eapply valid_t0; now eauto).
      congruence.
Qed.

(*
Lemma getValidMapCmd_preserving f:
  forall defVars akk res,
    (forall e m, FloverMap.find e akk = Some m -> m = M64) ->
    is64BitBstep f ->
    getValidMapCmd defVars f akk = Succes res ->
    (forall e m, FloverMap.find e defVars = Some m -> m = M64) ->
    forall e m, FloverMap.find e res = Some m -> m = M64.
Proof.
  induction f; intros * valid_akk is64Bit_f validMap_succ find_def *;
    cbn in *; unfold addMono in *; Flover_compute.
  - destruct (mTypeEq m m1) eqn:?; try congruence.
    type_conv; subst.
    destruct is64Bit_f as (? & ? & ?); subst.
    eapply IHf with (akk:=t0); eauto.
    inversion Heqr0; subst.
    intros *.
    find_cases; try (eapply getValidMap_preserving with (akk:=akk); now eauto).
    congruence.
  - destruct is64Bit_f as (? & ? & ?); subst.
    destruct (mTypeEq M64 m1) eqn:?; congruence.
  - eapply getValidMap_preserving with (akk:=akk); eauto.
Qed.
 *)

Lemma validValue_bounded v delta:
  (Normal v M64\/ (v = 0)%R) ->
  (Rabs delta <= / 2 * Raux.bpow radix2 (- 53 + 1))%R ->
  delta = (((round radix2 flt_exp_64 (round_mode mode_NE) v)
            - v) / v)%R ->
  validFloatValue (v * (1 + delta)) M64 ->
  Raux.Rlt_bool (Rabs (round radix2 flt_exp_64 (round_mode mode_NE) v)) (Raux.bpow radix2 1024) = true.
Proof.
  simpl.
  pose proof relative_error_64_ex as rel_error_exists.
  intros [normal_v | zero_v] delta_small delta_eq validVal;
  apply Raux.Rlt_bool_true.
  - unfold Normal in *; destruct normal_v.
    specialize (rel_error_exists v).
    specialize (rel_error_exists) as [eps [bounded_eps round_eq]].
    + eapply Rle_trans; eauto.
      unfold minValue_pos.
      rewrite Q2R_inv.
      * apply Raux.Rinv_le.
        { rewrite <- Q2R0_is_0. apply Qlt_Rlt.
          apply Qlt_alt. vm_compute; auto. }
        { unfold Q2R.
          unfold Qnum, Qden. unfold minExponentPos.
          rewrite Rinv_1. rewrite Rmult_1_r.
          apply IZR_le; vm_compute; congruence. }
      *  vm_compute; congruence.
    + cbn in round_eq.
      rewrite round_eq in delta_eq.
      assert (v <> 0%R).
      {
        clear - H.
        intros v_zero.
        subst.
        rewrite Rabs_R0 in H.
        cbn in H.
        lra.
      }
      field_simplify in delta_eq; [ | lra].
      field_simplify in delta_eq; [ | lra].
      replace (delta / 1)%R with delta in delta_eq by lra.
      replace (eps / 1)%R with eps in delta_eq by lra.
      subst.
      simpl in *.
      rewrite round_eq.
      destruct validVal as [normal_v | [denormal_v | zero_v]]; try auto.
      * unfold Normal in *. destruct normal_v.
        eapply Rle_lt_trans; eauto.
        unfold maxValue, Raux.bpow. unfold maxExponent. unfold Q2R.
        unfold Qnum, Qden. unfold IZR.
        repeat rewrite <- INR_IPR. simpl.
        rewrite RMicromega.Rinv_1.
        vm_compute; lra.
      * unfold Denormal in *. destruct denormal_v.
        eapply Rlt_trans; eauto.
        unfold minValue_pos, minExponentPos, Raux.bpow.
        rewrite Q2R_inv.
        { unfold Q2R, Qnum, Qden, IZR.
          repeat rewrite <- INR_IPR; simpl. vm_compute; lra. }
        { vm_compute; congruence. }
      * rewrite zero_v. simpl. rewrite Rabs_R0.
        eapply IZR_lt. apply Zpower_pos_gt_0. vm_compute; congruence.
  - rewrite zero_v.
    pose proof round_0_zero. simpl in H. rewrite H.
    rewrite Rabs_R0.
    eapply IZR_lt. apply Zpower_pos_gt_0. vm_compute; congruence.
Qed.

Lemma validValue_bounded_old v:
  (Normal v M64\/ (v = 0)%R) ->
  (forall eps, (Rabs eps <= / 2 * Raux.bpow radix2 (- 53 + 1))%R ->
          validFloatValue (v * (1 + eps)) M64) ->
  Raux.Rlt_bool (Rabs (round radix2 flt_exp_64
                    (round_mode mode_NE)
                    v))
           (Raux.bpow radix2 1024) = true.
Proof.
  simpl.
  pose proof relative_error_64_ex as rel_error_exists.
  intros [normal_v | zero_v] validVal;
  apply Raux.Rlt_bool_true.
  - unfold Normal in *; destruct normal_v.
    specialize (rel_error_exists v).
    destruct (rel_error_exists) as [eps [bounded_eps round_eq]].
    + eapply Rle_trans; eauto.
      unfold minValue_pos.
      rewrite Q2R_inv.
      * apply Raux.Rinv_le.
        { rewrite <- Q2R0_is_0. apply Qlt_Rlt.
          apply Qlt_alt. vm_compute; auto. }
        { unfold Q2R.
          unfold Qnum, Qden. unfold minExponentPos.
          rewrite Rinv_1. rewrite Rmult_1_r.
          apply IZR_le; vm_compute; congruence. }
      *  vm_compute; congruence.
    + simpl in *.
      rewrite round_eq.
      destruct (validVal eps) as [normal_v | [denormal_v | zero_v]]; try auto.
      * unfold Normal in *. destruct normal_v.
        eapply Rle_lt_trans; eauto.
        unfold maxValue, Raux.bpow. unfold maxExponent. unfold Q2R.
        unfold Qnum, Qden, IZR.
        repeat rewrite <- INR_IPR. simpl.
        vm_compute; lra.
      * unfold Denormal in *. destruct denormal_v.
        eapply Rlt_trans; eauto.
        unfold minValue_pos, minExponentPos, Raux.bpow.
        rewrite Q2R_inv.
        { unfold Q2R, Qnum, Qden, IZR.
          repeat rewrite <- INR_IPR; simpl.
          vm_compute; lra. }
        { vm_compute; congruence. }
      * rewrite zero_v. simpl. rewrite Rabs_R0. vm_compute; lra.
  - rewrite zero_v.
    pose proof round_0_zero. simpl in H. rewrite H.
    rewrite Rabs_R0.
    vm_compute; lra.
Qed.

Lemma  Q2R_IZR_maxExponent:
  (Q2R (/ (Z.pow_pos 2 1022 # 1)) = / IZR (Z.pow_pos 2 1022))%R.
Proof.
    vm_compute; lra.
Qed.

Lemma Rmult_inv_id:
  forall (a b :R), (b <> 0 -> b * a / b = a)%R.
Proof.
  intros. unfold Rdiv.
  rewrite Rmult_comm. rewrite <- Rmult_assoc.
  rewrite Rinv_l; [ | auto].
  lra.
Qed.

Lemma Rzero_lt_maxExponent:
  (Raux.Rlt_bool (Rabs 0) (IZR (Z.pow_pos 2 1024)) = true)%R.
Proof.
  rewrite Rabs_R0. apply Raux.Rlt_bool_true.
  apply IZR_lt.
  vm_compute; auto.
Qed.

Lemma minExponentpos_positive:
  (0 < / (IZR (Z.pow_pos 2 1022)))%R.
Proof.
  apply Rinv_0_lt_compat. vm_compute; lra.
Qed.

Lemma eval_expr_gives_IEEE (e:expr fl64) :
  forall E1 E2 E2_real Gamma vR A fVars dVars outVars,
    (forall x, (toREnv E2) x = E2_real x) ->
    ssa (B2Qexpr e) (NatSet.union fVars dVars) outVars ->
    validTypes (B2Qexpr e) Gamma ->
    approxEnv E1 (toRExpMap Gamma) A fVars dVars E2_real ->
    validRanges (B2Qexpr e) A E1 (toRTMap (toRExpMap Gamma)) ->
    validErrorBoundsRec (B2Qexpr e) E1 E2_real A Gamma IeeeDeltaMap ->
    validFPRanges (B2Qexpr e) E2_real Gamma IeeeDeltaMap ->
    eval_expr (toREnv E2) (toRExpMap Gamma) IeeeDeltaMap (toRExp (B2Qexpr e)) vR M64 ->
    NatSet.Subset ((freeVars (B2Qexpr e)) -- dVars) fVars ->
    is64BitEval (B2Qexpr e) ->
    is64BitEnv Gamma ->
    noDowncast (B2Qexpr e) ->
    eval_expr_valid e E2 ->
    (forall v,
        NatSet.In v dVars ->
        exists vF m,
          (E2_real v = Some vF /\ FloverMap.find (Var Q v) Gamma = Some m /\
           validFloatValue vF m)) ->
    exists (v:binary_float 53 1024),
      eval_expr_float e E2 = Some v /\
      is_finite 53 1024 v = true /\
      (B2R 53 1024 v = vR).
Proof.
  Opaque validTypes mTypeToQ mTypeToR.
  induction e; simpl in *;
    intros * envs_eq ssa_e typecheck_e approxEnv_E1_E2_real valid_rangebounds
                            valid_roundoffs valid_float_ranges eval_e_float
                            usedVars_sound is64BitTEnv is64BitEval_e noDowncast_e
                            eval_IEEE_valid_e dVars_sound;
    (match_pat (eval_expr _ _ _ _ _ _) (fun H => inversion H; subst; simpl in * ));
     revert eval_IEEE_valid_e;
     Flover_compute_asm; try congruence; type_conv; subst;
     unfold optionBind;
     intros eval_IEEE_valid_e.
  Transparent validTypes.
  all:try validTypes_split.
  - unfold toREnv in *.
    destruct (E2 n) eqn:HE2; try congruence.
    exists f; split; try eauto.
    inversion H1.
    assert (is_finite 53 1024 f = true).
    { eapply validValue_is_finite.
      eapply valid_float_ranges; eauto using IeeeDeltaMap_valid.
      eapply eval_eq_env with (E1:=toREnv E2); eauto.
      inversion H1; subst. unfold toREnv; auto. }
    split; [ auto |].
    rewrite B2Q_B2R_eq; auto.
  - destruct (is_finite 53 1024 v) eqn:v_finite; [ | contradiction].
    rewrite B2Q_B2R_eq in *; auto.
    assert (delta = 0%R).
    { destruct (Raux.Rle_bool (/ IZR (Z.pow_pos 2 1022))
                              (Rabs (B2R 53 1024 v)))
               eqn:case_val;
        inversion H1; subst; [ | lra ].
        clear H1 H3.
        pose proof (round_generic radix2 flt_exp_64 (round_mode dmode)
                                  (B2R 53 1024 v))
          as round_id.
        simpl in round_id. rewrite round_id; [ f_equal; lra | ].
        apply generic_format_B2R. }
      subst. exists v; repeat split; try auto.
      lra.
  - destruct valid_rangebounds as [valid_e valid_intv].
    destruct m eqn:?; cbn in *; try congruence.
    subst.
    destruct valid_float_ranges; auto.
    inversion ssa_e; subst.
    edestruct IHe as (v_e & eval_float_e & finite_e & eval_rel_e); eauto; [intuition| ].
    rewrite eval_float_e.
    exists (b64_opp v_e); split; try auto.
    unfold b64_opp. rewrite (is_finite_Bopp).
    split; [ auto | ].
    rewrite B2R_Bopp. congruence.
  - (* No Inv allowed *)
    simpl in *. destruct valid_roundoffs; contradiction.
  - repeat (match goal with
            |H: _ /\ _ |- _ => destruct H
            end).
    move H1 at bottom.
    assert (FloverMap.find (B2Qexpr e1) Gamma = Some M64 /\
            FloverMap.find (B2Qexpr e2) Gamma = Some M64 /\
            FloverMap.find (Binop b (B2Qexpr e1) (B2Qexpr e2)) Gamma = Some M64)
      as [tMap_e1 [tMap_e2 tMap_b]].
    { repeat split; apply (typing_expr_64_bit _ (Gamma := Gamma));
        simpl; auto. }
    pose proof (validTypes_exec _ valid_arg tMap_e1 H6); subst.
    pose proof (validTypes_exec _ valid_arg0 tMap_e2 H9); subst.
    inversion ssa_e; subst.
    rename H27 into ssa_e1; rename H30 into ssa_e2; rename H31 into outvars_closed.
    edestruct (IHe1 E1 E2 E2_real Gamma v1 A fVars dVars)
      as (v_e1 & eval_float_e1 & finite_e1 & eval_rel_e1);
      try eauto; try set_tac.
    edestruct (IHe2 E1 E2 E2_real Gamma v2 A fVars dVars)
      as (v_e2 & eval_float_e2 & finite_e2 & eval_rel_e2);
      try eauto; try set_tac.
    clear tMap_e1 tMap_e2 tMap_b.
    pose proof (validRanges_single _ _ _ _ H23) as valid_e.
    destruct valid_e as [iv_2 [err_2 [nR2 [map_e2 [eval_real_e2 e2_bounded_real]]]]].
    rewrite eval_float_e1, eval_float_e2 in *.
    subst.
    destruct iv_2 as [ivlo_2 ivhi_2].
    assert (forall vF2 m2,
               eval_expr E2_real (toRExpMap Gamma) IeeeDeltaMap (toRExp (B2Qexpr e2)) vF2 m2 ->
               (Rabs (nR2 - vF2) <= Q2R err_2))%R as valid_err_e2.
    { eapply validErrorBoundsRec_single; try eauto; set_tac. }
    assert (contained (Q2R (B2Q v_e2))
                      (widenInterval
                         (Q2R ivlo_2, Q2R ivhi_2) (Q2R err_2))) as v_e2_contained.
    { eapply distance_gives_iv.
      - simpl. eapply e2_bounded_real.
      - eapply valid_err_e2. instantiate(1:=M64).
        rewrite B2Q_B2R_eq; [ | auto ].
        eapply eval_eq_env; eauto. }
    assert (b = Div -> (B2R 53 1024 v_e2) <> 0%R).
    {
      intros; subst; simpl in *.
      revert H16; intros no_div_zero.
      specialize (no_div_zero (eq_refl Div) (ivlo_2,ivhi_2) err_2 map_e2).
      apply le_neq_bool_to_lt_prop in no_div_zero.
      rewrite B2Q_B2R_eq in *; auto. }
    assert (validFloatValue
              (perturb (evalBinop b (Q2R (B2Q v_e1)) (Q2R (B2Q v_e2))) M64 delta) M64).
    { eapply H14; eauto using IeeeDeltaMap_valid.
      repeat (rewrite B2Q_B2R_eq; [ | auto ]).
      eapply eval_eq_env; eauto. }
    assert (validFloatValue (B2R 53 1024 v_e1) M64).
    { eapply (validFPRanges_single _ H13); eauto using IeeeDeltaMap_valid.
      eapply eval_eq_env; eauto. }
    assert (validFloatValue (B2R 53 1024 v_e2) M64).
    { eapply (validFPRanges_single _ H15); eauto using IeeeDeltaMap_valid.
      eapply eval_eq_env; eauto. }
    pose proof relative_error_64_ex as rel_error_exists.
    unfold optionBind, normal_or_zero in *; simpl in *.
    assert (Raux.Rlt_bool (Rabs ((evalBinop b (B2R 53 1024 v_e1) (B2R 53 1024 v_e2)) * (1 + delta)))
                          (IZR (Z.pow_pos 2 1024)) = true)
           as no_overflow_eval.
    { eapply Raux.Rlt_bool_true.
      revert H25.
      intros valid_res.
      repeat (rewrite B2Q_B2R_eq in valid_res; [ | auto ]).
      destruct valid_res; unfold Normal, Denormal in *; subst.
      - apply bounded_inAbs in H25. rewrite Rabs_Rabsolu in *.
        destruct H25; eapply Rle_lt_trans; try eauto.
        + unfold maxValue, maxExponent.
          unfold Q2R; simpl.
          rewrite Rinv_1, Rmult_1_r.
          rewrite Rabs_pos_eq.
          2:{ apply IZR_le. vm_compute; congruence. }
          apply IZR_lt. vm_compute; congruence.
        + unfold minValue_pos, minExponentPos.
          rewrite Q2R_inv; [ | vm_compute; congruence].
          unfold Q2R, Qnum, Qden. simpl.
          rewrite Rinv_1, Rmult_1_r.
          rewrite Rabs_pos_eq.
          2: { hnf. left. apply Rinv_0_lt_compat.
               apply IZR_lt. vm_compute; congruence. }
          vm_compute; lra.
      - destruct H25.
        + destruct H25.
          eapply Rlt_trans; eauto.
          unfold minValue_pos, minExponentPos.
          rewrite Q2R_inv; [ | vm_compute; congruence].
          unfold Q2R, Qnum, Qden. simpl.
          rewrite Rinv_1, Rmult_1_r.
          vm_compute; lra.
        + rewrite H25. rewrite Rabs_R0.
          apply IZR_lt. vm_compute; congruence. }
    destruct b; revert H1; intros case_eval;
      unfold evalBinop in *.
    (* Addition *)
    + unfold b64_plus.
      pose proof (Bplus_correct 53 1024 eq_refl eq_refl binop_nan_pl64 mode_NE
                                v_e1 v_e2 finite_e1 finite_e2)
        as addition_correct.
      destruct case_eval as [eval_zero | eval_normal].
      (* result is zero *)
      * rewrite eval_zero in *.
        pose proof round_0_zero. unfold flt_exp_64 in H1.
        simpl in *. rewrite H1 in *.
        rewrite Rzero_lt_maxExponent in *.
        destruct addition_correct as [ add_round [ finite_res _]].
        eexists; repeat split; try eauto.
        rewrite Rmult_0_l. auto.
      * rewrite Q2R_IZR_maxExponent in *.
        destruct (rel_error_exists (B2R 53 1024 v_e1 + B2R 53 1024 v_e2)%R)
          as [delta2 [ delta_bounded round_is_delta]];
          [ lra | ].
        assert (B2R 53 1024 v_e1 + B2R 53 1024 v_e2 <> 0)%R.
        { hnf; intros val_zero. rewrite val_zero in eval_normal.
          rewrite Rabs_R0 in eval_normal.
          pose proof minExponentpos_positive.
          lra. }
        apply Raux.Rle_bool_true in eval_normal.
        rewrite eval_normal in H3.
        assert (delta2 = delta)%R.
        { inversion H3. subst.
          rewrite Rmult_plus_distr_l in round_is_delta.
          rewrite Rmult_1_r in round_is_delta.
          rewrite round_is_delta.
          field_rewrite ((B2R 53 1024 v_e1 + B2R 53 1024 v_e2 + (B2R 53 1024 v_e1 + B2R 53 1024 v_e2) * delta2 -
    (B2R 53 1024 v_e1 + B2R 53 1024 v_e2)) =  (B2R 53 1024 v_e1 + B2R 53 1024 v_e2) * delta2)%R.
          rewrite Rmult_inv_id; auto. }
        subst; unfold flt_exp_64 in *; simpl in *.
        rewrite round_is_delta in *.
        rewrite  no_overflow_eval in addition_correct.
        destruct addition_correct as [add_round [finite_res _]].
        eexists; repeat split; eauto.
    (* Subtraction *)
    + unfold b64_minus.
      pose proof (Bminus_correct 53 1024 eq_refl eq_refl binop_nan_pl64 mode_NE
                                v_e1 v_e2 finite_e1 finite_e2)
        as subtraction_correct.
      destruct case_eval as [eval_zero | eval_normal].
      (* result is zero *)
      * rewrite eval_zero in *.
        pose proof round_0_zero. unfold flt_exp_64 in H1.
        simpl in *. rewrite H1 in *.
        rewrite Rzero_lt_maxExponent in *.
        destruct subtraction_correct as [ add_round [ finite_res _]].
        eexists; repeat split; try eauto.
        rewrite Rmult_0_l. auto.
      * rewrite Q2R_IZR_maxExponent in *.
        destruct (rel_error_exists (B2R 53 1024 v_e1 - B2R 53 1024 v_e2)%R)
          as [delta2 [ delta_bounded round_is_delta]];
          [ lra | ].
        assert (B2R 53 1024 v_e1 - B2R 53 1024 v_e2 <> 0)%R.
        { hnf; intros val_zero. rewrite val_zero in eval_normal.
          rewrite Rabs_R0 in eval_normal.
          pose proof minExponentpos_positive.
          lra. }
        apply Raux.Rle_bool_true in eval_normal.
        rewrite eval_normal in H3.
        assert (delta2 = delta)%R.
        { inversion H3. subst.
          rewrite Rmult_plus_distr_l in round_is_delta.
          rewrite Rmult_1_r in round_is_delta.
          rewrite round_is_delta.
          field_rewrite ((B2R 53 1024 v_e1 - B2R 53 1024 v_e2 + (B2R 53 1024 v_e1 - B2R 53 1024 v_e2) * delta2 -
    (B2R 53 1024 v_e1 - B2R 53 1024 v_e2)) = (B2R 53 1024 v_e1 - B2R 53 1024 v_e2) * delta2)%R.
          rewrite Rmult_inv_id; auto. }
        subst; unfold flt_exp_64 in *; simpl in *.
        rewrite round_is_delta in *.
        rewrite  no_overflow_eval in subtraction_correct.
        destruct subtraction_correct as [sub_round [finite_res _]].
        eexists; repeat split; eauto.
        (* Multiplication *)
    + unfold b64_mult.
      pose proof (Bmult_correct 53 1024 eq_refl eq_refl binop_nan_pl64 mode_NE
                                v_e1 v_e2)
        as mult_correct.
      destruct case_eval as [eval_zero | eval_normal].
      (* result is zero *)
      * rewrite eval_zero in *.
        pose proof round_0_zero. unfold flt_exp_64 in H1.
        simpl in *. rewrite H1 in *.
        rewrite Rzero_lt_maxExponent in *.
        destruct mult_correct as [ mult_round [ finite_res _]].
        eexists; repeat split; try eauto.
        { unfold dmode. rewrite finite_res.
          rewrite finite_e1, finite_e2. auto. }
        rewrite Rmult_0_l. auto.
      * rewrite Q2R_IZR_maxExponent in *.
        destruct (rel_error_exists (B2R 53 1024 v_e1 * B2R 53 1024 v_e2)%R)
          as [delta2 [ delta_bounded round_is_delta]];
          [ lra | ].
        assert (B2R 53 1024 v_e1 * B2R 53 1024 v_e2 <> 0)%R.
        { hnf; intros val_zero. rewrite val_zero in eval_normal.
          rewrite Rabs_R0 in eval_normal.
          pose proof minExponentpos_positive.
          lra. }
        apply Raux.Rle_bool_true in eval_normal.
        rewrite eval_normal in H3.
        assert (delta2 = delta)%R.
        { inversion H3. subst.
          rewrite Rmult_plus_distr_l in round_is_delta.
          rewrite Rmult_1_r in round_is_delta.
          rewrite round_is_delta.
          field_rewrite ((B2R 53 1024 v_e1 * B2R 53 1024 v_e2 + (B2R 53 1024 v_e1 * B2R 53 1024 v_e2) * delta2 -
    (B2R 53 1024 v_e1 * B2R 53 1024 v_e2)) = (B2R 53 1024 v_e1 * B2R 53 1024 v_e2) * delta2)%R.
          rewrite Rmult_inv_id; auto. }
        subst; unfold flt_exp_64 in *; simpl in *.
        rewrite round_is_delta in *.
        rewrite  no_overflow_eval in mult_correct.
        destruct mult_correct as [sub_round [finite_res _]].
        eexists; repeat split; eauto.
        unfold dmode.
        rewrite finite_res, finite_e1, finite_e2. auto.
    (* Division *)
    + unfold b64_div.
      pose proof (Bdiv_correct 53 1024 eq_refl eq_refl binop_nan_pl64 mode_NE
                               v_e1 v_e2)
        as division_correct.
      destruct case_eval as [eval_zero | eval_normal].
      (* result is zero *)
      * rewrite eval_zero in *.
        pose proof round_0_zero. unfold flt_exp_64 in H1.
        simpl in *. rewrite H1 in *.
        rewrite Rzero_lt_maxExponent in *.
        destruct division_correct as [ mult_round [ finite_res _]].
        { eapply H24; eauto. }
        eexists; repeat split; try eauto.
        rewrite Rmult_0_l. auto.
      * rewrite Q2R_IZR_maxExponent in *.
        destruct (rel_error_exists (B2R 53 1024 v_e1 / B2R 53 1024 v_e2)%R)
          as [delta2 [ delta_bounded round_is_delta]];
          [ lra | ].
        assert (B2R 53 1024 v_e1 / B2R 53 1024 v_e2 <> 0)%R.
        { hnf; intros val_zero. rewrite val_zero in eval_normal.
          rewrite Rabs_R0 in eval_normal.
          pose proof minExponentpos_positive.
          lra. }
        apply Raux.Rle_bool_true in eval_normal.
        rewrite eval_normal in H3.
        assert (delta2 = delta)%R.
        { inversion H3. subst.
          rewrite Rmult_plus_distr_l in round_is_delta.
          rewrite Rmult_1_r in round_is_delta.
          rewrite round_is_delta.
          assert ((B2R 53 1024 v_e1 / B2R 53 1024 v_e2 + (B2R 53 1024 v_e1 / B2R 53 1024 v_e2) * delta2 -
    (B2R 53 1024 v_e1 / B2R 53 1024 v_e2)) = (B2R 53 1024 v_e1 / B2R 53 1024 v_e2) * delta2)%R.
          { unfold Rdiv. field. apply H24; auto. }
          rewrite H28.
          rewrite Rmult_inv_id; auto. }
        subst; unfold flt_exp_64 in *; simpl in *.
        rewrite round_is_delta in *.
        rewrite  no_overflow_eval in division_correct.
        destruct division_correct as [sub_round [finite_res _]].
        { apply H24. auto. }
        eexists; repeat split; eauto.
  - repeat (match goal with
            |H: _ /\ _ |- _ => destruct H
            end).
    assert (FloverMap.find (B2Qexpr e1) Gamma = Some M64 /\
            FloverMap.find (B2Qexpr e2) Gamma = Some M64 /\
            FloverMap.find (B2Qexpr e3) Gamma = Some M64 /\
            FloverMap.find (Fma (B2Qexpr e1) (B2Qexpr e2) (B2Qexpr e3)) Gamma = Some M64)
      as [tMap_e1 [tMap_e2 [tMap_e3 tMap_fma]]].
    { repeat split; apply (typing_expr_64_bit _ (Gamma := Gamma)); simpl; auto. }
    pose proof (validTypes_exec _ valid_arg tMap_e1 H6); subst.
    pose proof (validTypes_exec _ valid_arg0 tMap_e2 H9); subst.
    pose proof (validTypes_exec _ valid_arg1 tMap_e3 H10); subst.
    inversion ssa_e; subst.
    edestruct (IHe1 E1 E2 E2_real Gamma v1 A fVars dVars)
      as [v_e1 [eval_float_e1 eval_rel_e1]];
      try eauto; try set_tac.
    edestruct (IHe2 E1 E2 E2_real Gamma v2 A fVars dVars)
      as [v_e2 [eval_float_e2 eval_rel_e2]];
      try eauto; try set_tac.
    edestruct (IHe3 E1 E2 E2_real Gamma v3 A fVars dVars)
      as [v_e3 [eval_float_e3 eval_rel_e3]];
      try eauto; try set_tac.
    rewrite eval_float_e1, eval_float_e2, eval_float_e3 in H7.
    contradiction H7.
  - inversion noDowncast_e.
  - repeat (match goal with
            |H: _ /\ _ |- _ => destruct H
            end).
    subst.
    assert (FloverMap.find (B2Qexpr e1) Gamma = Some M64 /\
            FloverMap.find (B2Qexpr e2) Gamma = Some M64 /\
            FloverMap.find (Let M64 n (B2Qexpr e1) (B2Qexpr e2)) Gamma = Some M64)
      as [tMap_e1 [tMap_e2 tMap_let]].
    { repeat split; apply (typing_expr_64_bit _ (Gamma := Gamma)); simpl; auto. }
    pose proof (validTypes_exec _ valid_arg tMap_e1 H6); subst.
    pose proof (validTypes_exec _ valid_arg0 tMap_e2 H7); subst.
    inversion ssa_e; subst.
    rename H25 into n_not_bound;
      rename H28 into ssa_e1; rename H29 into ssa_e2; rename H30 into outvars_closed.
    edestruct (IHe1 E1 E2 E2_real Gamma v1 A fVars dVars)
      as [v_e1 [eval_float_e1 [is_finite_e1 eval_rel_e1]]];
      try eauto; [ set_tac | ].
    rewrite eval_float_e1 in *.
    eapply validRanges_single in H16.
    destruct H16
      as (iv_e1 & err_e1 & vR_e1 & find_A_e1 & eval_real_e1 & bounded_e1).
    specialize (H19 _ eval_real_e1).
    eapply validErrorBoundsRec_single in H12; eauto.
    destruct H12
             as [(vF_e1 & mF_e1 & eval_float_rel_e1) valid_err_e1].
    assert (mF_e1 = M64).
    { symmetry. eapply (validTypes_exec _ valid_arg tMap_e1); eauto. }
    subst.
    specialize (H15 _ _ eval_real_e1 eval_float_rel_e1).
    cbn in typecheck_e. destruct typecheck_e as (mG & ? & (? & ? & ? & ?) & ?).
    destruct H22 as (? & ? & ? & ?).
    destruct H18 as (? & ? & ? & ? & find_e1New & find_A_n & ?).
    destruct (H14 _ _ _ _ find_e1New find_A_n) as (? & ? & ?).
    assert (forall x4 : nat, updEnv n vF_e1 E2_real x4 = toREnv (updFlEnv n v_e1 E2) x4).
    { intros *; unfold toREnv, updFlEnv, updEnv.
      destruct (x4 =? n) eqn:Heqx.
      * rewrite Nat.eqb_eq in Heqx; subst.
        f_equal. eapply eval_expr_functional; eauto.
        rewrite B2Q_B2R_eq; try auto.
        eapply eval_eq_env with (E1:= toREnv E2); auto.
      * rewrite Nat.eqb_neq in Heqx. rewrite <- envs_eq.
        unfold toREnv; auto. }
    eapply IHe2 with (E2:= updFlEnv n v_e1 E2) (E2_real:= updEnv n vF_e1 E2_real) (fVars:=fVars) (dVars:=NatSet.add n dVars); try eauto.
    + instantiate (1:=outVars2).
      eapply ssa_equal_set with (inVars:=NatSet.add n (NatSet.union fVars dVars));
        [ | auto].
      split; intros in_l; set_tac.
      * destruct in_l as [inl | inl]; try auto.
        set_tac. destruct inl as [inl | inl]; try auto.
        intuition; auto.
      * destruct in_l; try auto.
        intuition; try auto.
        set_tac. intuition.
    + eapply approxUpdBound; try eauto.
      * eapply toRExpMap_some with (e:=Var Q n); eauto.
      * unfold error in *.
        repeat (match goal with
                | H1: FloverMap.find ?e1 ?A = Some ?iv1,
                      H2: FloverMap.find ?e1 ?A = Some ?iv2 |- _ =>
                  rewrite H1 in H2; inversion H2; subst; clear H2
                end).
        rewrite <- H29. eapply valid_err_e1; eauto.
    + eapply eval_eq_env with (E1 := updEnv n (B2R 53 1024 v_e1) (toREnv E2)); try auto.
      assert (vF_e1 = B2R 53 1024 v_e1).
      { eapply eval_expr_functional; eauto.
        eapply eval_eq_env; eauto. }
      subst; auto.
      intros x. specialize (H30 x).
      rewrite <- H30.
      unfold updEnv.
      rewrite envs_eq; auto.
    + set_tac. split.
      * right; split; [ auto | ].
        hnf; intros. apply H32; subst. set_tac.
      * hnf; intros. apply H32; set_tac.
    + intros; set_tac.
      destruct H31 as [? | [v_neq in_dvars]]; unfold updEnv.
      * subst. rewrite Nat.eqb_refl; repeat eexists; try eauto.
        eapply validFPRanges_single in H9; eauto.
      * rewrite <- Nat.eqb_neq in v_neq. rewrite v_neq.
        apply dVars_sound. auto.
Qed.

Theorem IEEE_connection_expr e A P qMap subdivs E1 E2 defVars:
  is64BitEval (B2Qexpr e) ->
  is64BitEnv defVars ->
  noDowncast (B2Qexpr e) ->
  eval_expr_valid e E2 ->
  unsat_queries qMap ->
  (forall Qmap : usedQueries, In Qmap (queriesInSubdivs subdivs) -> unsat_queries Qmap) ->
  eval_precond E1 P ->
  CertificateChecker (B2Qexpr e) A P qMap subdivs defVars = true ->
  exists Gamma, (* m, currently = M64 *)
    approxEnv E1 Gamma A (freeVars (B2Qexpr e)) (NatSet.empty) (toREnv E2) ->
    exists iv err vR vF,
      FloverMap.find (B2Qexpr e) A = Some (iv, err) /\
      eval_expr E1 (toRTMap Gamma) DeltaMapR (toREval (toRExp (B2Qexpr e))) vR REAL /\
      eval_expr_float e E2 = Some vF /\
      eval_expr (toREnv E2) Gamma IeeeDeltaMap (toRExp (B2Qexpr e)) (Q2R (B2Q vF)) M64 /\
      (Rabs (vR - Q2R (B2Q vF )) <= Q2R err)%R.
Proof.
  intros * is64eval is64bitenv no_cast eval_valid unsat_qs unsat_subdivs
    P_valid certificate_valid.
  destruct (@Certificate_checking_is_sound_general (B2Qexpr e) A P qMap subdivs defVars
                                                   E1 (toREnv E2) IeeeDeltaMap)
    as [Gamma validCert_general];
    try eauto using IeeeDeltaMap_valid.
  exists (toRExpMap Gamma). intros approxEnv_E1E2.
  destruct validCert_general
    as (? & ? & ? & ? & ? & ? & find_e & eval_real & eval_finite & errors_valid &
        ssa_e &
        validTypes_e & getMap_e & validRanges_e & validErrors_e &
        validFPRanges_e);
    [ auto | ].
  specialize (validErrors_e IeeeDeltaMap IeeeDeltaMap_valid).
  assert (is64BitEnv Gamma) as Gamma_is_64bit.
  { unfold is64BitEnv.
    eapply getValidMap_preserving with (akk:=FloverMap.empty mType); try eauto.
    intros; cbn in *; congruence. }
  assert (x3 = M64).
  { eapply validTypes_single in validTypes_e.
    destruct validTypes_e as (? & ? & ?).
    assert (x3 = x5) by (eapply H0; eauto).
    subst.
    unfold is64BitEnv in Gamma_is_64bit.
    eapply Gamma_is_64bit; eauto. }
  subst.
  edestruct (eval_expr_gives_IEEE) as (? & ? & ? & ?); try eauto; try set_tac.
  - eapply ssa_equal_set with (inVars := freeVars (B2Qexpr e)); try auto.
    hnf; split; intros; set_tac.
    + destruct H; set_tac.
    + eapply ssa_e.
  - intros; set_tac.
  - subst.
    repeat eexists; try eauto;
    rewrite B2Q_B2R_eq in *; auto.
    eapply errors_valid; eauto.
Qed.