(**
    Interval arithmetic checker and its soundness proof.
    The function validIntervalbounds checks wether the given analysis result is
    a valid range arithmetic for each sub term of the given exprression e.
    The computation is done using our formalized interval arithmetic.
    The function is used in CertificateChecker.v to build the full checker.
**)
From Coq
     Require Import QArith.QArith QArith.Qreals QArith.Qminmax Lists.List
     micromega.Psatz.

From Flover
     Require Import Infra.Abbrevs Infra.RationalSimps Infra.RealRationalProps
     Infra.Ltacs Infra.RealSimps TypeValidator.

From Flover
     Require Export IntervalArithQ IntervalArith ssaPrgs RealRangeArith.

Fixpoint validIntervalbounds (e:expr Q) (A:analysisResult) (P: FloverMap.t intv)
         (validVars:NatSet.t) :bool:=
  match FloverMap.find e A with
  | None => false
  | Some (intv, _) =>
       match e with
       | Var _ v =>
         if NatSet.mem v validVars
         then true
         else
           match FloverMap.find e P with
           | None => false
           | Some new_iv => isSupersetIntv new_iv intv
                                          (* && (Qleb lo hi) *)
           end
       | Const _ n => isSupersetIntv (n,n) intv
       | Unop o f =>
         if validIntervalbounds f A P validVars
         then
           match FloverMap.find f A with
           | None => false
           | Some (iv, _) =>
          match o with
          | Neg =>
            let new_iv := negateIntv iv in
            isSupersetIntv new_iv intv
          | Inv =>
            if (((Qleb (ivhi iv) 0) && (negb (Qeq_bool (ivhi iv) 0))) ||
                ((Qleb 0 (ivlo iv)) && (negb (Qeq_bool (ivlo iv) 0))))
            then
              let new_iv := invertIntv iv in
              isSupersetIntv new_iv intv
            else false
          end
        end
      else false
    | Binop op f1 f2 =>
      if ((validIntervalbounds f1 A P validVars) &&
          (validIntervalbounds f2 A P validVars))
      then
        match FloverMap.find f1 A, FloverMap.find f2 A with
        | Some (iv1, _), Some (iv2, _) =>
          match op with
          | Plus =>
            let new_iv := addIntv iv1 iv2 in
            isSupersetIntv new_iv intv
          | Sub =>
            let new_iv := subtractIntv iv1 iv2 in
            isSupersetIntv new_iv intv
          | Mult =>
            let new_iv := multIntv iv1 iv2 in
            isSupersetIntv new_iv intv
          | Div =>
            if (((Qleb (ivhi iv2) 0) && (negb (Qeq_bool (ivhi iv2) 0))) ||
                ((Qleb 0 (ivlo iv2)) && (negb (Qeq_bool (ivlo iv2) 0))))
            then
              let new_iv := divideIntv iv1 iv2 in
              isSupersetIntv new_iv intv
            else false
          end
        | _, _ => false
        end
      else false
    | Fma f1 f2 f3 =>
      if ((validIntervalbounds f1 A P validVars) &&
          (validIntervalbounds f2 A P validVars) &&
          (validIntervalbounds f3 A P validVars))
      then
        match FloverMap.find f1 A, FloverMap.find f2 A, FloverMap.find f3 A with
        | Some (iv1, _), Some (iv2, _), Some (iv3, _) =>
          let new_iv := addIntv (multIntv iv1 iv2) iv3 in
          isSupersetIntv new_iv intv
        | _, _, _ => false
        end
      else false
    | Downcast _ f1 =>
      if (validIntervalbounds f1 A P validVars)
      then
        match FloverMap.find f1 A with
        | None => false
        | Some (iv1, _) =>
        isSupersetIntv iv1 intv
        end
      else
        false
    | Let _ x e1 e2 =>
      if validIntervalbounds e1 A P validVars &&
         validIntervalbounds e2 A P (NatSet.add x validVars)
      then
        match FloverMap.find e1 A, FloverMap.find (Var Q x) A, FloverMap.find e2 A with
        | Some ((e_lo,e_hi), _), Some ((x_lo, x_hi), _), Some (iv2, _) =>
          Qeq_bool (e_lo) (x_lo) &&
          Qeq_bool (e_hi) (x_hi) &&
          isSupersetIntv iv2 intv
        | _, _, _ => false
        end
      else false
    (* | Cond f1 f2 f3 => false *)
      (*
      if ((validIntervalbounds f1 A P validVars) &&
          (validIntervalbounds f2 A P validVars) &&
          (validIntervalbounds f3 A P validVars))
      then
        match FloverMap.find f1 A, FloverMap.find f2 A, FloverMap.find f3 A with
        | Some (iv1, _), Some (iv2, _), Some (iv3, _) =>
          let new_iv := (* TODO *) in
          isSupersetIntv new_iv intv
        | _, _, _ => false
        end
      else false
           *)
    end
  end.

Lemma validBoundsDiv_uneq_zero e1 e2 A P V ivlo_e2 ivhi_e2 err:
  FloverMap.find (elt:=intv * error) e2 A = Some ((ivlo_e2,ivhi_e2), err) ->
  validIntervalbounds (Binop Div e1 e2) A P V = true ->
  (ivhi_e2 < 0) \/ (0 < ivlo_e2).
Proof.
  intros A_eq validBounds; cbn in *.
  Flover_compute; try congruence.
  apply le_neq_bool_to_lt_prop; auto.
Qed.

Theorem validIntervalbounds_sound (f:expr Q) (A:analysisResult) (P: FloverMap.t intv)
        fVars dVars outVars (E:env) Gamma:
  ssa f (NatSet.union fVars dVars) outVars ->
  validIntervalbounds f A P dVars = true ->
  dVars_range_valid dVars E A ->
  NatSet.Subset (preIntvVars (FloverMap.elements P)) fVars ->
  NatSet.Subset ((Expressions.freeVars f) -- dVars) fVars ->
  eval_preIntv E (FloverMap.elements P) ->
  validTypes f Gamma ->
  validRanges f A E (toRTMap (toRExpMap Gamma)).
Proof.
  induction f in E, dVars |- *;
    intros ssa_f valid_bounds valid_definedVars preVars_free usedVars_subset valid_freeVars types_defined;
    cbn in *.
  - destruct (NatSet.mem n dVars) eqn:?; cbn in *; split; auto.
    + destruct (valid_definedVars n)
        as [vR [iv_n [err_n [env_valid [map_n bounds_valid]]]]];
        try set_tac.
      rewrite map_n in *.
      kill_trivial_exists.
      eexists; split; [auto| split; try eauto ].
      eapply Var_load; simpl; try auto.
      destruct (types_defined) as [m [find_m _]].
      eapply toRExpMap_some in  find_m; simpl; eauto.
      match_simpl; auto.
    + Flover_compute.
      destruct (valid_freeVars n i0) as [free_n [vR [env_valid bounds_valid]]];
        auto using find_in_precond; set_tac.
      kill_trivial_exists.
      eexists; split; [auto | split].
      * econstructor; try eauto.
        destruct types_defined as [m [find_m _]].
        eapply toRExpMap_some in find_m; simpl; eauto.
        match_simpl; auto.
      * canonize_hyps; cbn in *. lra.
  - split; [auto |].
    Flover_compute; canonize_hyps; simpl in *.
    kill_trivial_exists.
    exists (perturb (Q2R v) REAL 0).
    split; [eauto| split].
    + econstructor; try eauto.
      cbn. rewrite Rabs_R0; lra.
    + unfold perturb; simpl; lra.
  - Flover_compute; simpl in *; try congruence.
    assert (validRanges f A E (toRTMap (toRExpMap Gamma))) as valid_e.
    { inversion ssa_f. eapply IHf; eauto.
      destruct types_defined as [? [? [[? ?] ?]]]; auto. }
    split; try auto.
    apply validRanges_single in valid_e.
    destruct valid_e as [iv_f [err_f [vF [iveq_f [eval_f valid_bounds_f]]]]].
    rewrite Heqo0 in iveq_f; inversion iveq_f; subst.
    inversion iveq_f; subst.
    destruct u; try andb_to_prop R; simpl in *.
    + kill_trivial_exists.
      exists (evalUnop Neg vF); split;
        [auto | split ; [econstructor; eauto | ]].
      * cbn; auto.
      *  canonize_hyps.
         rewrite Q2R_opp in *.
         simpl; lra.
    + rename L0 into nodiv0.
      apply le_neq_bool_to_lt_prop in nodiv0.
      kill_trivial_exists.
      exists (perturb (evalUnop Inv vF) REAL 0); split;
        [destruct i; auto | split].
      * econstructor; eauto.
        { auto. }
        { simpl. rewrite Rabs_R0; lra. }
        (* Absence of division by zero *)
        { hnf. destruct nodiv0 as [nodiv0 | nodiv0]; apply Qlt_Rlt in nodiv0;
               rewrite Q2R0_is_0 in nodiv0; lra. }
      * canonize_hyps.
        pose proof (interval_inversion_valid ((Q2R (fst iv_f)),(Q2R (snd iv_f)))
                                             (a :=vF))
          as inv_valid.
         unfold invertInterval in inv_valid; simpl in *.
         destruct inv_valid; try auto.
         { destruct nodiv0; rewrite <- Q2R0_is_0; [left | right]; apply Qlt_Rlt; auto. }
         { split; eapply Rle_trans.
           - apply L1.
           - rewrite Q2R_inv; try auto.
             destruct nodiv0; try lra.
             hnf; intros.
             assert (0 < Q2R (snd iv_f))%R.
             { eapply Rlt_le_trans.
               apply Qlt_Rlt in H1.
               rewrite <- Q2R0_is_0.
               apply H1. lra.
             }
             rewrite <- Q2R0_is_0 in H3.
             apply Rlt_Qlt in H3.
             lra.
           - apply H0.
           - rewrite <- Q2R_inv; try auto.
             hnf; intros.
             destruct nodiv0; try lra.
             assert (Q2R (fst iv_f) < 0)%R.
             { rewrite <- Q2R0_is_0.
               apply Qlt_Rlt in H2.
               eapply Rle_lt_trans; try eauto.
               lra. }
             rewrite <- Q2R0_is_0 in H3.
             apply Rlt_Qlt in H3. lra. }
  - destruct types_defined
      as [? [? [[? [? ?]]?]]].
    inversion ssa_f; subst.
    assert (validRanges f1 A E (toRTMap (toRExpMap Gamma))) as valid_f1.
    { Flover_compute; try congruence. eapply IHf1; try eauto.
      - now extended_ssa.
      - set_tac. }
    assert (validRanges f2 A E (toRTMap (toRExpMap Gamma))) as valid_f2.
    { Flover_compute; try congruence. eapply IHf2; try eauto.
      - now extended_ssa.
      - set_tac. }
    repeat split;
      [ intros; subst; Flover_compute; congruence |
                       auto | auto |].
    apply validRanges_single in valid_f1;
      apply validRanges_single in valid_f2.
    destruct valid_f1 as [iv_f1 [err1 [vF1 [env1 [eval_f1 valid_f1]]]]].
    destruct valid_f2 as [iv_f2 [err2 [vF2 [env2 [eval_f2 valid_f2]]]]].
    Flover_compute; try congruence.
    kill_trivial_exists.
    exists (perturb (evalBinop b vF1 vF2) REAL 0);
      split; [destruct i; auto | ].
    inversion env1; inversion env2; subst.
      destruct b; simpl in *.
    * split;
        [eapply Binop_dist' with (delta := 0%R); eauto; try congruence;
         try rewrite Rabs_R0; cbn; try auto; try lra|].
      pose proof (interval_addition_valid ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                          (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_add.
      specialize (valid_add vF1 vF2 valid_f1 valid_f2).
      unfold isSupersetIntv in R.
      andb_to_prop R.
      canonize_hyps; simpl in *.
      repeat rewrite <- Q2R_plus in *;
        rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold perturb. lra.
    * split;
        [eapply Binop_dist' with (delta := 0%R); eauto; try congruence;
         try rewrite Rabs_R0; cbn; try auto; lra|].
      pose proof (interval_subtraction_valid ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                             (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_sub.
      specialize (valid_sub vF1 vF2 valid_f1 valid_f2).
      unfold isSupersetIntv in R.
      andb_to_prop R.
      canonize_hyps; simpl in *.
      repeat rewrite <- Q2R_opp in *;
        repeat rewrite <- Q2R_plus in *;
        rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold perturb; lra.
    * split;
        [eapply Binop_dist' with (delta := 0%R); eauto; try congruence;
         try rewrite Rabs_R0; cbn; try auto; lra|].
      pose proof (interval_multiplication_valid ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                                (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_mul.
      specialize (valid_mul vF1 vF2 valid_f1 valid_f2).
      unfold isSupersetIntv in R.
      andb_to_prop R.
      canonize_hyps; simpl in *.
      repeat rewrite <- Q2R_mult in *;
        rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold perturb; lra.
    * andb_to_prop R.
      canonize_hyps.
      apply le_neq_bool_to_lt_prop in L.
      split;
        [eapply Binop_dist' with (delta := 0%R); eauto; try congruence;
         try rewrite Rabs_R0; cbn; try auto; try lra|].
      (* No division by zero proof *)
      { hnf; intros.
        destruct L as [L | L]; apply Qlt_Rlt in L; rewrite Q2R0_is_0 in L; lra. }
      { pose proof (interval_division_valid (a:=vF1) (b:=vF2)
                                            ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                            (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_div.
        simpl in *.
        destruct valid_div; try auto.
        - destruct L; rewrite <- Q2R0_is_0; [left | right]; apply Qlt_Rlt; auto.
        - assert (~ (snd iv_f2) == 0).
          { hnf; intros. destruct L; try lra.
            assert (0 < (snd iv_f2)) by (apply Rlt_Qlt; apply Qlt_Rlt in H12; lra).
            lra. }
          assert (~ (fst iv_f2) == 0).
          { hnf; intros; destruct L; try lra.
            assert ((fst iv_f2) < 0) by (apply Rlt_Qlt; apply Qlt_Rlt in H13; lra).
            lra. }
          repeat (rewrite <- Q2R_inv in *; try auto).
          repeat rewrite <- Q2R_mult in *.
          rewrite <- Q2R_min4, <- Q2R_max4 in *.
          unfold perturb.
          lra.
      }
  - destruct types_defined
      as [mG [find_mg [[validt_f1 [validt_f2 [validt_f3 valid_join]]] valid_exec]]].
    inversion ssa_f.
    assert (validRanges f1 A E (toRTMap (toRExpMap Gamma))) as valid_f1.
    { Flover_compute; try congruence. eapply IHf1; try eauto.
      - now extended_ssa.
      - set_tac. }
    assert (validRanges f2 A E (toRTMap (toRExpMap Gamma))) as valid_f2.
    { Flover_compute; try congruence. eapply IHf2; try eauto.
      - now extended_ssa.
      - set_tac. }
    assert (validRanges f3 A E (toRTMap (toRExpMap Gamma))) as valid_f3.
    { Flover_compute; try congruence. eapply IHf3; try eauto.
      - now extended_ssa.
      - set_tac. }
    repeat split; try auto.
    apply validRanges_single in valid_f1;
      apply validRanges_single in valid_f2;
      apply validRanges_single in valid_f3.
    destruct valid_f1 as [iv_f1 [err1 [vF1 [env1 [eval_f1 valid_f1]]]]].
    destruct valid_f2 as [iv_f2 [err2 [vF2 [env2 [eval_f2 valid_f2]]]]].
    destruct valid_f3 as [iv_f3 [err3 [vF3 [env3 [eval_f3 valid_f3]]]]].
    Flover_compute; try congruence.
    kill_trivial_exists.
    exists (perturb (evalFma vF1 vF2 vF3) REAL 0); split; try auto.
    inversion env1; inversion env2; inversion env3; subst.
    split; [auto | ].
    * eapply Fma_dist' with (delta := 0%R); eauto; try congruence; cbn;
        try rewrite Rabs_R0; try auto; lra.
    * pose proof (interval_multiplication_valid (Q2R (fst iv_f1),Q2R (snd iv_f1)) (Q2R (fst iv_f2), Q2R (snd iv_f2))) as valid_mul.
      specialize (valid_mul vF1 vF2 valid_f1 valid_f2).
      remember (multInterval (Q2R (fst iv_f1), Q2R (snd iv_f1)) (Q2R (fst iv_f2), Q2R (snd iv_f2))) as iv_f12prod.
      pose proof (interval_addition_valid (fst iv_f12prod, snd iv_f12prod)) (Q2R (fst iv_f3),Q2R (snd iv_f3)) as valid_add.
      specialize (valid_add (vF1 * vF2)%R vF3 valid_mul valid_f3).
      rewrite Heqiv_f12prod in valid_add, valid_mul.
      unfold multIntv in valid_add.
      canonize_hyps.
      simpl in *.
      repeat rewrite <- Q2R_mult in *;
        repeat rewrite <- Q2R_min4, <- Q2R_max4 in *;
        repeat rewrite <- Q2R_plus in *;
        repeat rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold evalFma, evalBinop, perturb.
      lra.
  - destruct types_defined
      as [mG [find_mG [[validt_f _] _]]].
    inversion ssa_f.
    assert (validRanges f A E (toRTMap (toRExpMap Gamma))) as valid_f1
        by (Flover_compute; try congruence; eapply IHf; eauto).
    split; try auto.
    apply validRanges_single in valid_f1.
    destruct valid_f1 as [iv_f [err_f [vF [env_f [eval_f valid_f]]]]].
    Flover_compute.
    inversion env_f; subst.
    kill_trivial_exists.
    exists (perturb vF REAL 0).
    split; [destruct i; auto |].
    canonize_hyps.
    split; [| cbn in *; lra].
    eapply Downcast_dist'; try eassumption; auto; auto.
    cbn; rewrite Rabs_R0. lra.
  - destruct types_defined
      as [mG [find_mG [[validt_f1 [validt_f2 valid_join]] valid_exec]]].
    Flover_compute; try congruence.
    inversion ssa_f; subst.
    canonize_hyps.
    assert (validRanges f1 A E (toRTMap (toRExpMap Gamma))) as valid_f1.
    { Flover_compute; try congruence. eapply IHf1; try eauto.
      - eapply ssa_outVars_extensible; try eauto. set_tac.
      - set_tac. }
    pose proof (validRanges_single _ _ _ _ valid_f1) as valid_single_f1.
    destruct valid_single_f1 as [iv_f1 [err_f1 [v [find_v [eval_f1 valid_bounds_f1]]]]].
    rewrite find_v in *; inversion Heqo0; subst.
    assert (validRanges f2 A (updEnv n v E) (toRTMap (toRExpMap Gamma))) as valid_f2.
    { eapply IHf2; eauto.
      - eapply ssa_outVars_extensible with (outVars1:=outVars2); try eauto;
          [ | set_tac].
        eapply ssa_equal_set; eauto.
        intros x. split; set_tac; intros; tauto.
      - intros v0 mem_v0.
        unfold updEnv.
        case_eq (v0 =? n); intros v0_eq.
        + rename L1 into eq_lo;
            rename R into eq_hi.
          rewrite Nat.eqb_eq in v0_eq; subst.
          exists v; eexists; eexists. repeat split; eauto; simpl in *; lra.
        + apply valid_definedVars.
          set_tac.
          destruct mem_v0 as [? | [? ?]]; try auto.
          rewrite Nat.eqb_neq in v0_eq.
          congruence.
      - intros x x_contained.
        set_tac.
        repeat split; auto.
        + right. split; auto. hnf; intros. apply H0; set_tac.
        + hnf; intros. apply H0; set_tac.
      - hnf. intros x ? ?.
        unfold updEnv.
        case_eq (x =? n); intros case_x; auto.
        rewrite Nat.eqb_eq in case_x. subst.
        set_tac.
        assert (NatSet.In n fVars) as in_free
            by (apply preVars_free; eapply preIntvVars_sound; eauto).
        exfalso. apply H3. set_tac. }
    repeat split; auto.
    + repeat eexists; try eauto.
      simpl. apply eqR_Qeq in L1; apply eqR_Qeq in R.
      rewrite <- Qeq_bool_iff in *. intuition.
    + intros vR ?.
      assert (vR = v) by (eapply meps_0_deterministic; eauto); now subst.
    + apply validRanges_single in valid_f2.
      destruct valid_f2 as [? [? [v2 [find_v2 [? ?]]]]].
      rewrite find_v2 in *; inversion Heqo2; subst.
      cbn in *.
      eexists. eexists. exists v2.
      repeat split; eauto; lra.
Qed.