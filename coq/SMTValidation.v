(**
    SMT arithmetic checker and its soundness proof.
    The function validSMTIntervalbounds checks wether the given analysis result is
    a valid range arithmetic for each sub term of the given expression e.
    The computation is done using our formalized SMT arithmetic.
    The function is used in CertificateChecker.v to build the full checker.
**)
From Coq
     Require Import QArith.QArith QArith.Qreals QArith.Qminmax Lists.List
     micromega.Psatz.

From Flover
     Require Import Infra.Abbrevs Infra.RationalSimps Infra.RealRationalProps
     Infra.Ltacs Infra.RealSimps TypeValidator.

From Flover
     Require Export IntervalArithQ IntervalArith SMTArith ssaPrgs
     RealRangeArith.

Definition let_env := nat -> option (expr Q).

Definition lets_sound L E Gamma :=
  forall x e v,
    L x = Some e ->
    eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (Var _ x))) v REAL ->
    eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) v REAL.

Definition updLets x (e: expr Q) (L: let_env) (y: nat) :=
  if y =? x then Some e else L y.

Fixpoint inlineLets (L: let_env) (e: expr Q) :=
  match e with
  | Var _ x => match L x with
            | Some e' => e'
            | None => e
            end
  | Const m r => e
  | Unop u e' => Unop u (inlineLets L e')
  | Binop b e1 e2 => Binop b (inlineLets L e1) (inlineLets L e2)
  | Fma e1 e2 e3 => Fma (inlineLets L e1) (inlineLets L e2) (inlineLets L e3)
  | Downcast m e' => Downcast m (inlineLets L e')
  | Let m x e1 e2 => Let m x (inlineLets L e1) (inlineLets L e2)
  (* | Cond e1 e2 e3 => Cond (inlineLets L e1) (inlineLets L e2) (inlineLets L e3) *)
  end.

Lemma inlineLets_sound L E Gamma inVars outVars e v :
  lets_sound L E Gamma ->
  (forall x e, L x = Some e -> NatSet.Subset (NatSet.add x (freeVars e)) inVars) ->
  ssa e inVars outVars ->
  eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) v REAL ->
  eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (inlineLets L e))) v
    REAL.
Proof.
  induction e in E, inVars, v |- *; cbn; intros Lsound LfVars_valid ssa_e H.
  - Flover_compute; eauto.
  - auto.
  - inversion H; subst; econstructor; eauto.
    + destruct m; try discriminate.
      inversion ssa_e; subst.
      eauto.
    + destruct m; try discriminate.
      inversion ssa_e; subst.
      eauto.
  - inversion H; subst; econstructor; eauto.
    + assert (m1 = REAL) by (eapply toRTMap_eval_REAL; eauto).
      inversion ssa_e; subst.
      subst.
      eapply IHe1; try eauto.
      now extended_ssa.
    + assert (m2 = REAL) by (eapply toRTMap_eval_REAL; eauto).
      inversion ssa_e; subst.
      eapply IHe2; try eauto.
      now extended_ssa.
  - inversion H; subst; econstructor; eauto.
    + assert (m1 = REAL) by (eapply toRTMap_eval_REAL; eauto).
      inversion ssa_e; subst.
      eapply IHe1; try eauto.
      now extended_ssa.
    + assert (m2 = REAL) by (eapply toRTMap_eval_REAL; eauto).
      inversion ssa_e; subst.
      eapply IHe2; try eauto.
      now extended_ssa.
    + assert (m3 = REAL) by (eapply toRTMap_eval_REAL; eauto).
      inversion ssa_e; subst.
      eapply IHe3; try eauto.
      now extended_ssa.
  - inversion H; subst; econstructor; eauto.
    inversion ssa_e; subst.
    assert (m1 = REAL) by (eapply toRTMap_eval_REAL; eauto).
    subst. eauto.
  - inversion H; subst.
    inversion ssa_e; subst.
    econstructor; try eauto.
    + eapply IHe1; try eauto.
      now extended_ssa.
    + eapply (IHe2 _ (NatSet.add n inVars)); eauto.
      2: intros; set_tac; right; eapply LfVars_valid; eauto;
        set_tac; tauto.
      intros x e v'.
      unfold updEnv.
      destruct (x =? n) eqn: Heq.
      * intros Hx He.
        apply beq_nat_true in Heq; subst.
        set_tac. exfalso. apply H4. eapply LfVars_valid; eauto.
        set_tac.
      * intros H0 H1.
        inversion H1; subst. rewrite Heq in *.
        apply eval_expr_ignore_bind.
        { eapply Lsound; eauto. now constructor. }
        { rewrite freeVars_toREval_toRExp_compat.
          set_tac. intros ?.
          apply H4. eapply LfVars_valid; eauto.
          set_tac. }
      * now extended_ssa.
            (*
  - inversion H; subst.
    + assert (m1 = REAL) by (eapply toRTMap_eval_REAL; eauto).
      inversion ssa_e; subst.
      subst.
      assert (m2 = REAL) by (eapply toRTMap_eval_REAL; eauto).
      subst. eapply Cond_then; eauto.
    + assert (m1 = REAL) by (eapply toRTMap_eval_REAL; eauto).
      inversion ssa_e; subst.
      subst.
      assert (m3 = REAL) by (eapply toRTMap_eval_REAL; eauto).
      subst. eapply Cond_else; eauto.
*)
Qed.

Definition tightenLowerBound (e: expr Q) (lo: Q) (q: SMTLogic) P L :=
  match q with
  | AndQ qP (AndQ (LessQ e' (ConstQ v)) TrueQ) =>
    if checkPre P qP && smt_expr_eq e' (inlineLets L e) then Qmax lo v else lo
  | _ => lo
  end.

Definition tightenUpperBound (e: expr Q) (hi: Q) (q: SMTLogic) P L :=
  match q with
  | AndQ qP (AndQ (LessQ (ConstQ v) e') TrueQ) =>
    if checkPre P qP && smt_expr_eq e' (inlineLets L e) then Qmin hi v else hi
  | _ => hi
  end.

Definition tightenBounds (e: expr Q) (iv: intv) (qMap: usedQueries) P L :=
  match FloverMap.find e qMap with
  | None => iv
  | Some (loQ, hiQ) =>
    (tightenLowerBound e (fst iv) loQ P L, tightenUpperBound e (snd iv) hiQ P L)
  end.

Lemma tightenBounds_low_sound E Gamma e v iv qMap P L :
  eval_precond E P ->
  (forall ql qh, FloverMap.find e qMap = Some (ql, qh) -> ~ eval_smt_logic E ql) ->
  eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
            (toREval (toRExp (inlineLets L e))) v REAL ->
  (Q2R (fst iv) <= v)%R ->
  (Q2R (fst (tightenBounds e iv qMap P L)) <= v)%R.
Proof.
  intros prec_valid unsatQ H Hlo.
  unfold tightenBounds.
  destruct (FloverMap.find e qMap) as [[q ?]|]; auto.
  cbn. unfold tightenLowerBound.
  destruct q; auto.
  destruct q2; auto.
  destruct q2_1; auto.
  destruct e2; auto.
  destruct q2_2; auto.
  remember (checkPre P q1 && smt_expr_eq e1 (inlineLets L e)) as b eqn: Hchk.
  destruct b; auto.
  symmetry in Hchk.
  andb_to_prop Hchk.
  rewrite <- Q2R_max. apply Rmax_lub; auto.
  eapply RangeBound_low_sound; eauto.
  eapply eval_smt_expr_complete in H; eauto.
  rewrite SMTArith2Expr_exact.
  exact H.
Qed.

Lemma tightenBounds_high_sound E Gamma e v iv qMap P L :
  eval_precond E P ->
  (forall ql qh, FloverMap.find e qMap = Some (ql, qh) -> ~ eval_smt_logic E qh) ->
  eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
            (toREval (toRExp (inlineLets L e))) v REAL ->
  (v <= Q2R (snd iv))%R ->
  (v <= Q2R (snd (tightenBounds e iv qMap P L)))%R.
Proof.
  intros prec_valid unsatQ H Hlo.
  unfold tightenBounds.
  destruct (FloverMap.find e qMap) as [[q ?]|]; auto.
  cbn. unfold tightenUpperBound.
  destruct s; auto.
  destruct s2; auto.
  destruct s2_1; auto.
  destruct e1; auto.
  destruct s2_2; auto.
  remember (checkPre P s1 && smt_expr_eq e2 (inlineLets L e)) as b eqn: Hchk.
  destruct b; auto.
  symmetry in Hchk.
  andb_to_prop Hchk.
  rewrite <- Q2R_min. apply Rmin_glb; auto.
  eapply RangeBound_high_sound; eauto.
  eapply eval_smt_expr_complete in H; eauto.
  rewrite SMTArith2Expr_exact.
  exact H.
Qed.

Lemma tightenBounds_sound E Gamma e v iv qMap P L :
  eval_precond E P ->
  (forall ql qh, FloverMap.find e qMap = Some (ql, qh) ->
            ~ eval_smt_logic E ql /\ ~ eval_smt_logic E qh) ->
  eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
              (toREval (toRExp (inlineLets L e))) v REAL ->
  (Q2R (fst iv) <= v <= Q2R (snd iv))%R ->
  (Q2R (fst (tightenBounds e iv qMap P L)) <= v <= Q2R (snd (tightenBounds e iv qMap P L)))%R.
Proof.
  intros H unsatQ ? ?. split.
  - eapply tightenBounds_low_sound; try eassumption; [| lra].
    intros. edestruct unsatQ; eauto.
  - eapply tightenBounds_high_sound; try eassumption; [| lra].
    intros. edestruct unsatQ; eauto.
Qed.

Fixpoint validSMTIntervalbounds (e: expr Q) (A: analysisResult) (P: precond)
         (Q: usedQueries) L (validVars: NatSet.t) : bool :=
  match FloverMap.find e A with
  | None => false
  | Some (intv, _) =>
       match e with
       | Var _ x =>
         if NatSet.mem x validVars
         then true
         else
           match FloverMap.find e (fst P) with
           | None => false
           | Some iv =>
             let new_iv := tightenBounds e iv Q P L in
             isSupersetIntv new_iv intv
           end
       | Const _ n => (Qleb (ivlo intv) n) && (Qleb n (ivhi intv))
       | Unop o f =>
         if validSMTIntervalbounds f A P Q L validVars
         then
           match FloverMap.find f A with
           | None => false
           | Some (iv, _) =>
             match o with
             | Neg =>
               let new_iv := tightenBounds e (negateIntv iv) Q P L in
               isSupersetIntv new_iv intv
             | Inv =>
               if (((Qleb (ivhi iv) 0) && (negb (Qeq_bool (ivhi iv) 0))) ||
                   ((Qleb 0 (ivlo iv)) && (negb (Qeq_bool (ivlo iv) 0))))
               then
                 let new_iv := tightenBounds e (invertIntv iv) Q P L in
                 isSupersetIntv new_iv intv
               else false
             end
           end
         else false
       | Binop op f1 f2 =>
         if ((validSMTIntervalbounds f1 A P Q L validVars)
               && (validSMTIntervalbounds f2 A P Q L validVars))
         then
           match FloverMap.find f1 A, FloverMap.find f2 A with
           | Some (iv1, _), Some (iv2, _) =>
             match op with
             | Plus =>
               let new_iv := tightenBounds e (addIntv iv1 iv2) Q P L in
               isSupersetIntv new_iv intv
             | Sub =>
               let new_iv := tightenBounds e (subtractIntv iv1 iv2) Q P L in
               isSupersetIntv new_iv intv
             | Mult =>
               let new_iv := tightenBounds e (multIntv iv1 iv2) Q P L in
               isSupersetIntv new_iv intv
             | Div =>
               if (((Qleb (ivhi iv2) 0) && (negb (Qeq_bool (ivhi iv2) 0))) ||
                   ((Qleb 0 (ivlo iv2)) && (negb (Qeq_bool (ivlo iv2) 0))))
               then
                 let new_iv := tightenBounds e (divideIntv iv1 iv2) Q P L in
                 isSupersetIntv new_iv intv
               else false
             end
           | _, _ => false
           end
         else false
       | Fma f1 f2 f3 =>
         if ((validSMTIntervalbounds f1 A P Q L validVars)
               && (validSMTIntervalbounds f2 A P Q L validVars)
               && (validSMTIntervalbounds f3 A P Q L validVars))
         then
           match FloverMap.find f1 A, FloverMap.find f2 A, FloverMap.find f3 A with
           | Some (iv1, _), Some (iv2, _), Some (iv3, _) =>
             let mult_iv := tightenBounds (Binop Mult f1 f2) (multIntv iv1 iv2) Q P L in
             let new_iv := tightenBounds e (addIntv mult_iv iv3) Q P L in
             isSupersetIntv new_iv intv
           | _, _, _ => false
           end
         else false
       | Downcast _ f1 =>
         if (validSMTIntervalbounds f1 A P Q L validVars)
         then
           match FloverMap.find f1 A with
           | None => false
           | Some (iv1, _) =>
             isSupersetIntv (tightenBounds e iv1 Q P L) intv
           end
         else
           false
       | Let _ x e1 e2 =>
         if validSMTIntervalbounds e1 A P Q L validVars &&
            validSMTIntervalbounds e2 A P Q (updLets x e1 L) (NatSet.add x validVars)
         then
           match FloverMap.find e1 A, FloverMap.find (Var _ x) A, FloverMap.find e2 A with
           | Some ((e_lo,e_hi), _), Some ((x_lo, x_hi), _), Some (iv2, _) =>
             Qeq_bool (e_lo) (x_lo) &&
             Qeq_bool (e_hi) (x_hi) &&
             isSupersetIntv iv2 intv
           | _, _, _ => false
           end
         else false
       (* | Cond f1 f2 f3 => false *)
       (*
       if ((validSMTIntervalbounds f1 A P validVars) &&
           (validSMTIntervalbounds f2 A P validVars) &&
           (validSMTIntervalbounds f3 A P validVars))
       then
         match FloverMap.find f1 A, FloverMap.find f2 A, FloverMap.find f3 A with
         | Some (iv1, _), Some (iv2, _), Some (iv3, _) =>
           let new_iv := (* TODO *) in
           isSupersetIntv new_iv intv
         | _, _, _ => false
         end
       else false
            *)
       end
  end.

Theorem validSMTIntervalbounds_sound (f: expr Q) (A: analysisResult) (P: precond)
        (Q: usedQueries) L fVars dVars outVars (E: env) Gamma :
  unsat_queries Q ->
  lets_sound L E Gamma ->
  (forall x e, L x = Some e -> NatSet.Subset (NatSet.add x (freeVars e)) (fVars ∪ dVars)) ->
  ssa f (fVars ∪ dVars) outVars ->
  validSMTIntervalbounds f A P Q L dVars = true ->
  dVars_range_valid dVars E A ->
  NatSet.Subset (preVars P) fVars ->
  NatSet.Subset ((freeVars f) -- dVars) fVars ->
  eval_precond E P ->
  validTypes f Gamma ->
  validRanges f A E (toRTMap (toRExpMap Gamma)).
Proof.
  induction f in L, E, dVars |- *;
    intros unsat_qs Lsound Lvars_valid ssa_f valid_bounds valid_definedVars preVars_free usedVars_subset valid_precond types_defined;
    cbn in *.
  - destruct (NatSet.mem n dVars) eqn:?; cbn in *; split; auto.
    + destruct (valid_definedVars n)
        as [vR [iv_n [err_n [env_valid [map_n bounds_valid]]]]];
        try set_tac.
      rewrite map_n in *.
      kill_trivial_exists.
      eexists; split; [auto| split; try eauto ].
      eapply Var_load; cbn; try auto.
      destruct (types_defined) as [m [find_m _]].
      eapply toRExpMap_some in  find_m; cbn; eauto.
      match_simpl; auto.
    + Flover_compute.
      destruct valid_precond as [valid_pre_intv ?].
      destruct (valid_pre_intv n i0) as [vR [env_valid bounds_valid]];
        auto using find_in_precond; set_tac.
      canonize_hyps.
      kill_trivial_exists.
      assert (Heval: eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                               (Var Rdefinitions.R n) vR REAL).
      { constructor; auto.
        destruct types_defined as [m [find_m _]].
        eapply toRExpMap_some in find_m; cbn; eauto.
        match_simpl; auto. }
      eexists; split; [auto | split; eauto].
      eapply Rle_trans2; eauto.
      eapply tightenBounds_sound; eauto.
      * split; eauto.
      * cbn. Flover_compute; eauto.
  - split; [auto |].
    Flover_compute; canonize_hyps; cbn in *.
    kill_trivial_exists.
    exists (perturb (Q2R v) REAL 0).
    split; [eauto| split].
    + econstructor; try eauto.
      cbn. rewrite Rabs_R0; lra.
    + unfold perturb; cbn; lra.
  - Flover_compute; cbn in *; try congruence.
    assert (validRanges f A E (toRTMap (toRExpMap Gamma))) as valid_e.
    { inversion ssa_f. eapply IHf; eauto.
      destruct types_defined as [? [? [[? ?] ?]]]; auto. }
    split; try auto.
    apply validRanges_single in valid_e.
    destruct valid_e as [iv_f [err_f [vF [iveq_f [eval_f valid_bounds_f]]]]].
    rewrite Heqo0 in iveq_f; inversion iveq_f; subst.
    inversion iveq_f; subst.
    destruct u; try andb_to_prop R; cbn in *.
    + kill_trivial_exists.
      pose (v := ((evalUnop Neg vF) )).
      exists v; split; auto.
      assert (Heval: eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                               (Unop Neg (toREval (toRExp f))) v REAL)
        by (econstructor; try eassumption; auto).
      split; auto.
      canonize_hyps.
      eapply Rle_trans2; eauto.
      eapply tightenBounds_sound; eauto.
      eapply inlineLets_sound; eauto.
      cbn. rewrite !Q2R_opp. lra.
    + rename L1 into nodiv0.
      apply le_neq_bool_to_lt_prop in nodiv0.
      kill_trivial_exists.
      pose (v := (perturb (evalUnop Inv vF) REAL 0)).
      exists v.
      assert (Heval: eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                               (Unop Inv (toREval (toRExp f))) v REAL).
      { econstructor; eauto.
        - auto.
        - cbn. rewrite Rabs_R0. lra.
        - hnf. destruct nodiv0 as [nodiv0 | nodiv0]; apply Qlt_Rlt in nodiv0;
               rewrite Q2R0_is_0 in nodiv0; lra. }
       split; [destruct i; auto | split; auto].
      canonize_hyps.
        pose proof (interval_inversion_valid ((Q2R (fst iv_f)),(Q2R (snd iv_f)))
                                             (a :=vF))
          as inv_valid.
         unfold invertInterval in inv_valid; cbn in *.
         eapply Rle_trans2; eauto.
         eapply tightenBounds_sound; eauto using inlineLets_sound.
         cbn. rewrite ?Q2R_inv.
         * apply inv_valid; auto.
           destruct nodiv0; rewrite <- Q2R0_is_0; [left | right]; apply Qlt_Rlt; auto.
         * hnf; intros. destruct nodiv0; try lra.
           assert ((fst iv_f) < 0) by (apply Rlt_Qlt; apply Qlt_Rlt in H0; lra).
           lra.
         * hnf; intros. destruct nodiv0; try lra.
           assert (0 < (snd iv_f)) by (apply Rlt_Qlt; apply Qlt_Rlt in H0; lra).
           lra.
  - destruct types_defined
      as [? [? [[? [? ?]]?]]].
    inversion ssa_f.
    assert (validRanges f1 A E (toRTMap (toRExpMap Gamma))) as valid_f1.
    { Flover_compute; try congruence. eapply IHf1; try eauto.
      - now extended_ssa.
      - set_tac. }
    assert (validRanges f2 A E (toRTMap (toRExpMap Gamma))) as valid_f2.
    { Flover_compute; try congruence. eapply IHf2; try eauto.
      - now extended_ssa.
      - set_tac. }
    repeat split;
      [ intros; subst; Flover_compute; congruence |
                       auto | auto |].
    apply validRanges_single in valid_f1;
      apply validRanges_single in valid_f2.
    destruct valid_f1 as [iv_f1 [err1 [vF1 [env1 [eval_f1 valid_f1]]]]].
    destruct valid_f2 as [iv_f2 [err2 [vF2 [env2 [eval_f2 valid_f2]]]]].
    Flover_compute; try congruence.
    kill_trivial_exists.
    pose (v := perturb (evalBinop b vF1 vF2) REAL 0).
    exists v.
    assert (Heval: eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                             (Binop b (toREval (toRExp f1)) (toREval (toRExp f2))) v REAL).
    { destruct b; cbn in *;
      eapply Binop_dist' with (delta := 0%R); try congruence;
        try rewrite Rabs_R0; cbn; try lra; try reflexivity; eauto.
      andb_to_prop R. intros. rename L0 into nodiv0.
      apply le_neq_bool_to_lt_prop in nodiv0.
      hnf. destruct nodiv0 as [nodiv0 | nodiv0]; apply Qlt_Rlt in nodiv0;
             rewrite Q2R0_is_0 in nodiv0; lra. }
      split; [destruct i; auto | split; auto].
    inversion env1; inversion env2; subst.
    destruct b; cbn in *.
    * andb_to_prop R.
      canonize_hyps.
      pose proof (@interval_addition_valid ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                          (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_add.
      specialize (valid_add vF1 vF2 valid_f1 valid_f2).
      cbn in *.
      rewrite <- ?Q2R_plus, <- ?Q2R_min4, <- ?Q2R_max4 in valid_add.
      eapply Rle_trans2; eauto.
      eapply tightenBounds_sound; eauto using inlineLets_sound.
    * andb_to_prop R.
      canonize_hyps; cbn in *.
      pose proof (interval_subtraction_valid ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                             (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_sub.
      specialize (valid_sub vF1 vF2 valid_f1 valid_f2).
      cbn in *.
      rewrite <- ?Q2R_opp, <- ?Q2R_plus, <- ?Q2R_min4, <- ?Q2R_max4 in valid_sub.
      eapply Rle_trans2; eauto.
      eapply tightenBounds_sound; eauto using inlineLets_sound.
    * andb_to_prop R.
      canonize_hyps; cbn in *.
      pose proof (interval_multiplication_valid ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                                (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_mul.
      specialize (valid_mul vF1 vF2 valid_f1 valid_f2).
      cbn in *.
      rewrite <- ?Q2R_mult, <- ?Q2R_min4, <- ?Q2R_max4 in valid_mul.
      eapply Rle_trans2; eauto.
      eapply tightenBounds_sound; eauto using inlineLets_sound.
    * andb_to_prop R.
      canonize_hyps.
      rename L0 into nodiv0.
      apply le_neq_bool_to_lt_prop in nodiv0.
      pose proof (interval_division_valid (a:=vF1) (b:=vF2)
                                            ((Q2R (fst iv_f1)), Q2R (snd iv_f1))
                                            (Q2R (fst iv_f2), Q2R (snd iv_f2)))
        as valid_div.
        cbn in *.
        destruct valid_div; try auto.
        { destruct nodiv0; rewrite <- Q2R0_is_0; [left | right]; apply Qlt_Rlt; auto. }
          assert (~ (snd iv_f2) == 0).
          { hnf; intros. destruct nodiv0; try lra.
            assert (0 < (snd iv_f2)) by (apply Rlt_Qlt; apply Qlt_Rlt in H8; lra).
            lra. }
          assert (~ (fst iv_f2) == 0).
          { hnf; intros; destruct nodiv0; try lra.
            assert ((fst iv_f2) < 0) by (apply Rlt_Qlt; apply Qlt_Rlt in H9; lra).
            lra. }
          repeat (rewrite <- Q2R_inv in *; try auto).
          rewrite <- ?Q2R_mult, <- ?Q2R_min4, <- ?Q2R_max4 in *.
          eapply Rle_trans2; eauto.
          eapply tightenBounds_sound; eauto using inlineLets_sound.
  - destruct types_defined
      as [mG [find_mg [[validt_f1 [validt_f2 [validt_f3 valid_join]]] valid_exec]]].
    inversion ssa_f.
    assert (validRanges f1 A E (toRTMap (toRExpMap Gamma))) as valid_f1.
    { Flover_compute; try congruence. eapply IHf1; try eauto.
      - now extended_ssa.
      - set_tac. }
    assert (validRanges f2 A E (toRTMap (toRExpMap Gamma))) as valid_f2.
    { Flover_compute; try congruence. eapply IHf2; try eauto.
      - now extended_ssa.
      - set_tac. }
    assert (validRanges f3 A E (toRTMap (toRExpMap Gamma))) as valid_f3.
    { Flover_compute; try congruence. eapply IHf3; try eauto.
      - now extended_ssa.
      - set_tac. }
    repeat split; try auto.
    apply validRanges_single in valid_f1;
      apply validRanges_single in valid_f2;
      apply validRanges_single in valid_f3.
    destruct valid_f1 as [iv_f1 [err1 [vF1 [env1 [eval_f1 valid_f1]]]]].
    destruct valid_f2 as [iv_f2 [err2 [vF2 [env2 [eval_f2 valid_f2]]]]].
    destruct valid_f3 as [iv_f3 [err3 [vF3 [env3 [eval_f3 valid_f3]]]]].
    Flover_compute; try congruence.
    kill_trivial_exists.
    pose (v := perturb (evalFma vF1 vF2 vF3) REAL 0).
    exists v; split; auto.
    assert (Heval: eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                             (Fma (toREval (toRExp f1)) (toREval (toRExp f2))
                                  (toREval (toRExp f3))) v REAL).
    { eapply Fma_dist' with (delta := 0%R); try congruence; cbn;
        try rewrite Rabs_R0; try auto; try lra; eauto. }
    inversion env1; inversion env2; inversion env3; subst.
    split; [auto | ].
    pose proof (interval_multiplication_valid (Q2R (fst iv_f1),Q2R (snd iv_f1)) (Q2R (fst iv_f2), Q2R (snd iv_f2))) as valid_mul.
    specialize (valid_mul vF1 vF2 valid_f1 valid_f2).
    pose (mult_interval := tightenBounds (Binop Mult f1 f2) (multIntv iv_f1 iv_f2) Q P L).
    pose proof (@interval_addition_valid (Q2R (fst mult_interval), Q2R (snd mult_interval))
                                         (Q2R (fst iv_f3), Q2R (snd iv_f3))
                                         (vF1 * vF2) vF3) as valid_add.
    unfold mult_interval in valid_add.
    canonize_hyps.
    eapply Rle_trans2; eauto.
    eapply tightenBounds_sound; eauto using inlineLets_sound.
    cbn in *. rewrite ?Q2R_min4, ?Q2R_max4, ?Q2R_plus.
    apply valid_add; auto.
    assert (Heval_mult: eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                                  (Binop Mult (toREval (toRExp f1)) (toREval (toRExp f2)))
                                  (vF1 * vF2) REAL).
    { eapply Binop_dist' with (delta := 0%R); try congruence;
        try rewrite Rabs_R0; cbn; try lra; try reflexivity; eauto. }
    eapply tightenBounds_sound; eauto.
    eapply inlineLets_sound; eauto.
    + instantiate (1:=(NatSet.union outVars1 outVars2)).
      econstructor; try eauto. set_tac.
    + cbn. now rewrite ?Q2R_min4, ?Q2R_max4, ?Q2R_mult.
  - destruct types_defined
      as [mG [find_mG [[validt_f _] _]]].
    inversion ssa_f.
    assert (validRanges f A E (toRTMap (toRExpMap Gamma))) as valid_f1
        by (Flover_compute; try congruence; eapply IHf; eauto).
    split; try auto.
    apply validRanges_single in valid_f1.
    destruct valid_f1 as [iv_f [err_f [vF [env_f [eval_f valid_f]]]]].
    Flover_compute.
    inversion env_f; subst.
    kill_trivial_exists.
    pose (v := perturb vF REAL 0).
    exists v.
    split; [destruct i; auto |].
    canonize_hyps.
    assert (Heval: eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                             (Downcast REAL (toREval (toRExp f))) v REAL).
    { eapply Downcast_dist'; try eassumption; cbn; auto; try reflexivity.
      rewrite Rabs_R0; lra. }
    split; auto.
    eapply Rle_trans2; eauto.
    eapply tightenBounds_sound; eauto using inlineLets_sound.
  - destruct types_defined
      as [mG [find_mG [[validt_f1 [validt_f2 valid_join]] valid_exec]]].
    Flover_compute; try congruence.
    inversion ssa_f; subst.
    canonize_hyps.
    assert (validRanges f1 A E (toRTMap (toRExpMap Gamma))) as valid_f1.
    { Flover_compute; try congruence. eapply IHf1; try eauto.
      - now extended_ssa.
      - set_tac. }
    pose proof (validRanges_single _ _ _ _ valid_f1) as valid_single_f1.
    destruct valid_single_f1 as [iv_f1 [err_f1 [v [find_v [eval_f1 valid_bounds_f1]]]]].
    rewrite find_v in *; inversion Heqo0; subst.
    assert (NatSet.Subset (freeVars f1) (fVars ∪ dVars))
      by (eapply ssa_subset_freeVars; eauto).
    assert (validRanges f2 A (updEnv n v E) (toRTMap (toRExpMap Gamma))) as valid_f2.
    { eapply (IHf2 (updLets n f1 L)); eauto.
      - intros x e' v'. unfold updLets, updEnv.
        destruct (x =? n) eqn: Heq.
        + intros He. injection He.
          intros <- H0. inversion H0; subst.
          rewrite Heq in *.
          assert (v' = v) by congruence. subst.
          apply eval_expr_ignore_bind; auto.
          rewrite freeVars_toREval_toRExp_compat.
          set_tac.
        + intros H0 H1.
          inversion H1; subst. rewrite Heq in *.
          apply eval_expr_ignore_bind.
          * eapply Lsound; eauto. now constructor.
          * rewrite freeVars_toREval_toRExp_compat.
            set_tac. intros ?.
            assert (n ∈ fVars ∪ dVars) by (eapply Lvars_valid; eauto; set_tac).
            eauto.
      - intros x e'. unfold updLets. destruct (x =? n) eqn: Heq.
        + intros H0. injection H0. intros <-.
          apply beq_nat_true in Heq; subst.
          set_tac. destruct H1 as [? | [_ ?]]; auto.
          apply H in H1. set_tac. tauto.
        + intros H0.
          eapply NatSetProps.subset_trans. eauto. set_tac. tauto.
      - eapply ssa_outVars_extensible with (outVars1:=outVars2); try eauto;
          [ | set_tac].
        eapply ssa_equal_set; eauto.
        intros x. split; set_tac; intros; tauto.
      - intros v0 mem_v0.
        unfold updEnv.
        case_eq (v0 =? n); intros v0_eq.
        + rename L1 into eq_lo;
            rename R into eq_hi.
          rewrite Nat.eqb_eq in v0_eq; subst.
          exists v; eexists; eexists. repeat split; eauto; simpl in *; lra.
        + apply valid_definedVars.
          set_tac.  destruct mem_v0 as [? | [? ?]]; try auto.
          rewrite Nat.eqb_neq in v0_eq.
          congruence.
      - intros x x_contained.
        set_tac.
        repeat split; auto.
        + right. split; auto. hnf; intros. apply H1; set_tac.
        + hnf; intros. apply H1; set_tac.
      - apply eval_precond_updEnv; auto.
        intros ?. set_tac. apply H3. clear H. set_tac. }
    repeat split; auto.
    + repeat eexists; try eauto.
      simpl. apply eqR_Qeq in L2; apply eqR_Qeq in R.
      rewrite <- Qeq_bool_iff in *. intuition.
    + intros vR ?.
      assert (vR = v) by (eapply meps_0_deterministic; eauto); now subst.
    + apply validRanges_single in valid_f2.
      destruct valid_f2 as [? [? [v2 [find_v2 [? ?]]]]].
      rewrite find_v2 in *; inversion Heqo2; subst.
      cbn in *.
      eexists. eexists. exists v2.
      repeat split; eauto; try lra.
Qed.
