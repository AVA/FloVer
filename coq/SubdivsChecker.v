From Flover
     Require Import Infra.RealSimps Infra.RationalSimps Infra.RealRationalProps
     Infra.Ltacs RealRangeArith RealRangeValidator RoundoffErrorValidator
     Environments TypeValidator FPRangeValidator ExpressionAbbrevs ResultChecker
     IntervalArithQ.

Require Import List FunInd Sorting.Permutation Sorting.Mergesort Orders.

Fixpoint resultLeq e (A1 A2: analysisResult) :=
  match e with
  | Unop _ e1 | Downcast _ e1 => resultLeq e1 A1 A2
  | Binop b e1 e2 => resultLeq e1 A1 A2 && resultLeq e2 A1 A2 &&
                              match b with
                              | Div => match FloverMap.find e2 A2 with
                                      | Some (iv2, err) =>
                                        Qleb 0 err &&
                                        (Qleb (snd iv2 + err) 0 && negb (Qeq_bool (snd iv2 + err) 0)
                                        || Qleb 0 (fst iv2 - err) && negb (Qeq_bool (fst iv2 - err) 0))
                                      | _ => false
                                      end
                              | _ => true
                              end
  | Fma e1 e2 e3 => resultLeq e1 A1 A2 && resultLeq e2 A1 A2 && resultLeq e3 A1 A2
  | Let _ x e1 e2 => resultLeq e1 A1 A2 && resultLeq e2 A1 A2 &&
                              match FloverMap.find (Var Q x) A2, FloverMap.find e1 A2 with
                              | Some (ivX, errX), Some (iv1, err1) =>
                                Qeqb (ivlo iv1) (ivlo ivX) && Qeqb (ivhi iv1) (ivhi ivX) && Qeqb err1 errX
                              | _, _ => false
                              end
  | _ => true
  end &&
      match FloverMap.find e A1, FloverMap.find e A2 with
      | Some (ivA1, errA1), Some (ivA2, errA2) => isSupersetIntv ivA1 ivA2 && (Qleb errA1 errA2)
      | _, _ => false
      end.

Lemma resultLeq_sound e A1 A2 :
  resultLeq e A1 A2 = true ->
  exists iv1 err1 iv2 err2,
    FloverMap.find e A1 = Some (iv1, err1) /\
    FloverMap.find e A2 = Some (iv2, err2) /\
    isSupersetIntv iv1 iv2 = true /\
    err1 <= err2.
Proof.
  intros H.
  assert (
      match FloverMap.find e A1, FloverMap.find e A2 with
      | Some (ivA1, errA1), Some (ivA2, errA2) => isSupersetIntv ivA1 ivA2 && (Qleb errA1 errA2)
      | _, _ => false
      end = true).
  { unfold resultLeq in H. destruct e;
    apply andb_prop in H as [_ H]; auto. }
  Flover_compute.
  repeat eexists; auto.
  - unfold isSupersetIntv. now rewrite L0.
  - apply (reflect_iff _ _ (Z.leb_spec0 _ _)). auto.
Qed.

Lemma resultLeq_range_sound e A1 A2 E Gamma :
  resultLeq e A1 A2 = true ->
  validRanges e A1 E Gamma ->
  validRanges e A2 E Gamma.
Proof.
  induction e in E |- *; cbn.
  - intros Hleq (_ & iv & err & vR & Hfind & Heval & Hiv).
    rewrite Hfind in Hleq. Flover_compute.
    split; auto. kill_trivial_exists. exists vR. canonize_hyps; cbn in *.
    repeat split; auto; lra.
  - intros Hleq (_ & iv & err & vR & Hfind & Heval & Hiv).
    rewrite Hfind in Hleq. Flover_compute.
    split; auto. kill_trivial_exists. exists vR. canonize_hyps; cbn in *.
    repeat split; auto; lra.
  - intros H (H1 & iv & err & vR & Hfind & Heval & Hiv).
    apply andb_true_iff in H as [Hleq1 Hleq].
    rewrite Hfind in Hleq. Flover_compute.
    split; auto. kill_trivial_exists. exists vR. canonize_hyps; cbn in *.
    repeat split; auto; lra.
  - intros H ((_ & H1 & H2) & iv & err & vR & Hfind & Heval & Hiv).
    apply andb_true_iff in H as [H Hleq].
    apply andb_true_iff in H as [H Hdiv].
    apply andb_true_iff in H as [Hleq1 Hleq2].
    rewrite Hfind in Hleq.
    (* destruct (FloverMap.find (Binop b e1 e2) A2) eqn: Hfind2; try congruence. *)
    repeat split; auto.
    + intros -> ivA2 errA2 Hfind2.
      edestruct (resultLeq_sound _ _ _ Hleq2)
        as (ivA1 & errA1 & ? & ? & Hfind1 & Hfind2' & Hsub & _).
      rewrite Hfind2' in Hfind2.
      injection Hfind2. intros -> ->.
      (* specialize (Hdiv eq_refl ivA1 errA1 Hfind1). *)
      apply andb_true_iff in Hsub as [Hsub1 Hsub2].
      cbn in *.
      unfold error in *.
      rewrite Hfind2' in Hdiv.
      apply orb_true_iff.
      apply andb_true_iff in Hdiv as [Herr Hdiv].
      apply orb_true_iff in Hdiv as [Hdiv | Hdiv]; [left | right].
      * apply andb_true_iff in Hdiv as [Hle Hneq].
        apply andb_true_iff.
        apply Qle_bool_iff in Herr.
        apply Qle_bool_iff in Hle.
        split; [apply Qle_bool_iff; lra |].
        destruct (Qeq_bool (snd ivA2) 0) eqn: H; auto.
        apply Qeq_bool_iff in H.
        assert (snd ivA2 + errA2 == 0) as Heq by lra.
        apply Qeq_bool_iff in Heq.
        now rewrite Heq in Hneq.
      * apply andb_true_iff in Hdiv as [Hle Hneq].
        apply andb_true_iff.
        apply Qle_bool_iff in Herr.
        apply Qle_bool_iff in Hle.
        split; [apply Qle_bool_iff; lra |].
        destruct (Qeq_bool (fst ivA2) 0) eqn: H; auto.
        apply Qeq_bool_iff in H.
        assert (fst ivA2 - errA2 == 0) as Heq by lra.
        apply Qeq_bool_iff in Heq.
        now rewrite Heq in Hneq.
    + destruct (FloverMap.find (Binop b e1 e2) A2) as [[iv2 err2]|] eqn: Hfind2; try congruence.
      kill_trivial_exists. exists vR.
      apply andb_true_iff in Hleq as [Hsub _].
      apply andb_true_iff in Hsub as [Hsub1 Hsub2].
      canonize_hyps; cbn in *.
      repeat split; auto; lra.
  - intros H ((H1 & H2 & H3) & iv & err & vR & Hfind & Heval & Hiv).
    apply andb_true_iff in H as [H Hleq].
    apply andb_true_iff in H as [H Hleq3].
    apply andb_true_iff in H as [Hleq1 Hleq2].
    rewrite Hfind in Hleq. Flover_compute.
    split; auto. kill_trivial_exists. exists vR. canonize_hyps; cbn in *.
    repeat split; auto; lra.
  - intros H (H1 & iv & err & vR & Hfind & Heval & Hiv).
    apply andb_true_iff in H as [Hleq1 Hleq].
    rewrite Hfind in Hleq. Flover_compute.
    split; auto. kill_trivial_exists. exists vR. canonize_hyps; cbn in *.
    repeat split; auto; lra.
  - intros H ((H1 & Hx & H2) & iv & err & vR & Hfind & Heval & Hiv).
    apply andb_true_iff in H as [H Hleq].
    apply andb_true_iff in H as [Hleq1 Hleq2].
    rewrite Hfind in Hleq. Flover_compute.
    repeat split; auto.
    + repeat eexists. repeat split; eauto.
      unfold Qeqb in *.
      now rewrite L1, R2.
    + kill_trivial_exists. exists vR. canonize_hyps; cbn in *.
      repeat split; auto; lra.
Qed.

Lemma resultLeq_error_sound e A1 A2 E1 E2 Gamma :
  resultLeq e A1 A2 = true ->
  validErrorBounds e E1 E2 A1 Gamma ->
  validErrorBounds e E1 E2 A2 Gamma.
Proof.
  intros Hleq err_sound DeltaMap delta_sound.
  specialize (err_sound _ delta_sound).
  revert E1 E2 err_sound.
  induction e; intros E1 E2 err_sound; cbn.
  - split; auto. intros vR iv err Heval Hfind.
    cbn in Hleq.
    Flover_compute.
    unfold error in *.
    assert (e0 = err) by congruence; subst.
    edestruct err_sound as (_ & H1 & H2); eauto.
    split; auto.
    canonize_hyps.
    intros vFP mFP HevalFP.
    specialize (H2 vFP mFP HevalFP).
    lra.
  - split; auto. intros vR iv err Heval Hfind.
    cbn in Hleq.
    Flover_compute.
    unfold error in *.
    assert (e0 = err) by congruence; subst.
    edestruct err_sound as (_ & H1 & H2); eauto.
    split; auto.
    canonize_hyps.
    intros vFP mFP HevalFP.
    specialize (H2 vFP mFP HevalFP).
    lra.
  - edestruct err_sound as [H1 Herr].
    cbn in Hleq.
    apply andb_true_iff in Hleq as [Hleq1 Hleq].
    split; [destruct u; auto |].
    intros vR iv err Heval Hfind.
    Flover_compute.
    unfold error in *.
    assert (e1 = err) by congruence; subst.
    edestruct Herr as [Hex Hall]; eauto.
    split; auto.
    canonize_hyps.
    intros vFP mFP HevalFP.
    specialize (Hall vFP mFP HevalFP).
    lra.
  - edestruct err_sound as ((_ & H1 & H2) & Herr).
    cbn in Hleq.
    apply andb_true_iff in Hleq as [H Hleq].
    apply andb_true_iff in H as [H Hdiv].
    apply andb_true_iff in H as [Hleq1 Hleq2].
    split.
    + repeat split; auto.
      intros -> iv2 err Hfind2.
      unfold error in *.
      rewrite Hfind2 in Hdiv.
      now apply andb_true_iff in Hdiv as [_ Hdiv].
    + intros vR iv err Heval Hfind.
      clear Hdiv.
      Flover_compute.
      unfold error in *.
      assert (e0 = err) by congruence; subst.
      edestruct Herr as [Hex Hall]; eauto.
      split; auto.
      canonize_hyps.
      intros vFP mFP HevalFP.
      specialize (Hall vFP mFP HevalFP).
      lra.
  - edestruct err_sound as ((H1 & H2 & H3) & Herr).
    cbn in Hleq.
    apply andb_true_iff in Hleq as [H Hleq].
    apply andb_true_iff in H as [H Hleq3].
    apply andb_true_iff in H as [Hleq1 Hleq2].
    split; auto.
    intros vR iv err Heval Hfind.
    Flover_compute.
    unfold error in *.
    assert (e0 = err) by congruence; subst.
    edestruct Herr as [Hex Hall]; eauto.
    split; auto.
    canonize_hyps.
    intros vFP mFP HevalFP.
    specialize (Hall vFP mFP HevalFP).
    lra.
  - edestruct err_sound as [H1 Herr].
    cbn in Hleq.
    apply andb_true_iff in Hleq as [Hleq1 Hleq].
    split; auto.
    intros vR iv err Heval Hfind.
    Flover_compute.
    unfold error in *.
    assert (e1 = err) by congruence; subst.
    edestruct Herr as [Hex Hall]; eauto.
    split; auto.
    canonize_hyps.
    intros vFP mFP HevalFP.
    specialize (Hall vFP mFP HevalFP).
    lra.
  - edestruct err_sound as ((H1 & _ & H2) & Herr).
    cbn in Hleq.
    apply andb_true_iff in Hleq as [H Hleq].
    apply andb_true_iff in H as [H Hx].
    apply andb_true_iff in H as [Hleq1 Hleq2].
    split; auto.
    + split; auto. split; auto.
      intros iv1 err1 ivX errX Hfind1 HfindX.
      unfold error in *.
      rewrite Hfind1, HfindX in Hx.
      apply andb_true_iff in Hx as [Hx Hxerr].
      apply andb_true_iff in Hx as [Hx1 Hx2].
      unfold Qeqb in *.
      canonize_hyps. now repeat split.
    + intros vR iv err Heval Hfind.
      Flover_compute.
      unfold error in *.
      assert (e0 = err) by congruence; subst.
      edestruct Herr as [Hex Hall]; eauto.
      split; auto.
      canonize_hyps.
      intros vFP mFP HevalFP.
      specialize (Hall vFP mFP HevalFP).
      lra.
Qed.

(* TODO: maybe put this somewhere else *)
Lemma approxEnv_empty_dVars A1 A2 Gamma fVars dVars E1 E2 :
  NatSet.Empty dVars ->
  approxEnv E1 Gamma A1 fVars dVars E2 ->
  approxEnv E1 Gamma A2 fVars dVars E2.
Proof.
  intros He H.
  induction H.
  - constructor.
  - econstructor; eauto.
  - exfalso. eapply He. set_tac.
Qed.

Definition checkSubdivsStep e absenv defVars Gamma (PAQ: precond * analysisResult * usedQueries) :=
  match PAQ with
  | (P, A, Q) =>
    RangeErrorChecker e A P Q defVars Gamma
                      && resultLeq e A absenv
  end.

Definition forallbTailrec {X: Type} f (l: list X) :=
  fold_left (fun acc v => acc && f v) l true.

Lemma forallbTailrec_false_fixpoint X f (l: list X) :
  fold_left (fun acc v => acc && f v) l false = false.
Proof. induction l; auto. Qed.

Lemma forallbTailrec_forallb X f (l: list X) :
  forallbTailrec f l = forallb f l.
Proof.
  induction l; auto; cbn.
  destruct (f a); auto using forallbTailrec_false_fixpoint.
Qed.

Definition checkSubdivs e absenv subdivs defVars Gamma :=
  forallbTailrec (checkSubdivsStep e absenv defVars Gamma) subdivs.

Lemma checkSubdivs_sound e absenv subdivs defVars Gamma P A Q :
  checkSubdivs e absenv subdivs defVars Gamma = true ->
  In (P, A, Q) subdivs ->
  RangeErrorChecker e A P Q defVars Gamma = true /\
  resultLeq e A absenv = true.
Proof.
  intros H Hin.
  unfold checkSubdivs in H.
  rewrite forallbTailrec_forallb in H.
  eapply forallb_forall in H; eauto.
  now apply andb_true_iff in H as [H1 H2].
Qed.

Fixpoint coverIntv iv intvs :=
  match intvs with
  | nil => false
  | intv :: nil => isSupersetIntv iv intv
  | intv :: intvs =>
    Qleb (ivlo intv) (ivlo iv) && coverIntv (ivhi intv, ivhi iv) intvs
  end.

Lemma coverIntv_sound iv intvs v :
  coverIntv iv intvs = true ->
  (Q2R (ivlo iv) <= v <= Q2R (ivhi iv))%R ->
  exists intv, In intv intvs /\ (Q2R (ivlo intv) <= v <= Q2R (ivhi intv))%R.
Proof.
  induction intvs as [|intv0 intvs] in iv |- *; [intros H; discriminate H|].
  destruct intvs as [|intv1 intvs]; cbn; intros H Hin.
  - exists intv0. split; auto. andb_to_prop H. canonize_hyps. cbn in *. lra.
  - destruct (Rle_or_lt v (Q2R (ivhi intv0))) as [Hle|Hlt].
    + andb_to_prop H.
      canonize_hyps.
      exists intv0. split; cbn; auto.
      cbn in *. lra.
    + andb_to_prop H.
      destruct (IHintvs (ivhi intv0, ivhi iv)) as [intv [inl Hin1]]; eauto.
      cbn in *. lra.
Qed.

Fixpoint getSlice x iv Ps :=
  match Ps with
  | nil => (nil, nil)
  | P :: Ps' =>
    match P with
    | nil => (nil, Ps) (* fail *)
    | (e, ivE) :: P' =>
      if FloverMapFacts.P.F.eqb (Var Q x) e && isSupersetIntv iv ivE then
           let (sl, rest) := getSlice x iv Ps' in (P' :: sl, rest)
      else (nil, Ps)
    end
  end.

Lemma getSlice_length x iv Ps sl rest :
  getSlice x iv Ps = (sl, rest) ->
  length Ps = length (sl ++ rest).
Proof with try now injection 1; intros; subst.
  induction Ps as [|P Ps] in sl |- *; cbn...
  destruct P as [|[e ivE] P]...
  destruct (FloverMapFacts.P.F.eqb (Var Q x) e) eqn:?...
  destruct (isSupersetIntv iv ivE) eqn:?...
  destruct (getSlice x iv Ps) eqn:?...
  injection 1; intros; subst.
  cbn. now rewrite <- IHPs.
Qed.

Lemma getSlice_sound x iv Ps sl rest :
  getSlice x iv Ps = (sl, rest) ->
  forall P, In P sl ->
       exists ivx, In ((Var Q x, ivx) :: P) Ps /\ isSupersetIntv iv ivx = true.
Proof with try now injection 1; intros; subst.
  induction Ps as [|P1 Ps] in sl |- *; cbn...
  destruct P1 as [|[e ivE] P1]...
  cbn.
  destruct (FloverMapFacts.P.F.eqb (Var Q x) e) eqn:?...
  destruct (isSupersetIntv iv ivE) eqn:?...
  destruct (getSlice x iv Ps) eqn:?...
  injection 1; intros; subst.
  destruct H2 as [-> | Hin].
  - apply eqb_var in Heqb.
    exists ivE. subst. split; auto.
  - edestruct IHPs as [ivx [inPs sub]]; eauto.
Qed.

Lemma getSlice_rest_sound x iv Ps sl rest :
  getSlice x iv Ps = (sl, rest) ->
  forall P, In P rest -> In P Ps.
Proof with try now injection 1; intros; subst.
  induction Ps as [|P Ps] in sl |- *; cbn...
  destruct P as [|[e ivE] P]...
  destruct (FloverMapFacts.P.F.eqb (Var Q x) e) eqn:?...
  destruct (isSupersetIntv iv ivE) eqn:?...
  destruct (getSlice x iv Ps) eqn:?...
  injection 1; intros; subst.
  right. eapply IHPs; eauto.
Qed.

Lemma getSlice_complete x iv Ps sl rest :
  getSlice x iv Ps = (sl, rest) ->
  forall P, In P Ps ->
       In P rest \/ exists P1 ivx, P = (Var Q x, ivx) :: P1 /\ In P1 sl.
Proof with try now injection 1; intros; subst; auto.
  induction Ps as [|P Ps] in sl |- *...
  destruct P as [|[e ivE] P]...
  cbn.
  destruct (FloverMapFacts.P.F.eqb (Var Q x) e) eqn:?...
  destruct (isSupersetIntv iv ivE) eqn:?...
  destruct (getSlice x iv Ps) eqn:?...
  apply eqb_var in Heqb.
  cbn.
  injection 1; intros; subst.
  destruct H2 as [<- | H2].
  - right. exists P, ivE. split; now constructor.
  - edestruct IHPs as [Hin | [P1 [ivx [Heq Hin]]]]; eauto.
    right. exists P1, ivx. split; auto. now constructor 2.
Qed.

(*
Function checkDim cov x iv Ps {measure length Ps} :=
  match Ps with
  | nil => false
  | P :: _ =>
    match FloverMap.find (Var Q x) P with
    | Some ivx =>
      let (b, rest) := getSlice_aux x ivx Ps in
      let covb := cov b in
      covb && match rest with
              | nil => isSupersetIntv iv ivx
              | _ => checkDim cov x (snd ivx, snd iv) rest
              end
    | None => false
    end
  end.
intros cov x iv Ps P Ps' HPs ivx Hfind b rest P' rest' Hrest.
rewrite <- Hrest.
rewrite <- HPs at 2.
cbn.
unfold addToSlice.
rewrite Hfind.
unfold isSupersetIntv, Qleb, Qle_bool.
rewrite !Z.leb_refl.
rewrite <- HPs.
cbn.
destruct (getSlice_aux x ivx Ps') eqn:Heq.
apply getSlice_aux_sound in Heq.
clear Hrest rest'.
injection 1; intros; subst.
cbn.
rewrite Heq, app_length.
apply le_lt_n_Sm, le_plus_r.
Defined.
*)

Function checkDim cov x iv Ps {measure length Ps} :=
  match Ps with
  | nil => false
  | P :: _ =>
    match P with
    | (e, ivx) :: _ =>
      if FloverMapFacts.P.F.eqb (Var Q x) e then
        let (sl, rest) := getSlice x ivx Ps in
        match rest with
        | nil => cov sl && isSupersetIntv iv ivx
        | _ => cov sl && Qleb (fst ivx) (fst iv) && checkDim cov x (snd ivx, snd iv) rest
        end
      else false
    | _ => false
    end
  end.
intros cov x iv Ps P Ps' [e ivE] P' e' ivx H HP HPs Heq sl rest l1 l2 <-.
rewrite <- HPs at 2.
cbn.
rewrite Heq.
unfold isSupersetIntv, Qleb, Qle_bool.
rewrite !Z.leb_refl.
rewrite <- HPs.
destruct (getSlice x ivx Ps') eqn:Hsl.
injection 1; intros; subst.
cbn.
rewrite (getSlice_length _ _ _ Hsl); eauto.
rewrite app_length.
apply le_lt_n_Sm, le_plus_r.
Defined.

Fixpoint covers (P: list (expr Q * intv)) Ps :=
  match P with
  | nil => match Ps with nil::nil => true | _ => false end
  | (Var _ x, iv) :: P =>
    checkDim (covers P) x iv Ps
  | _ :: P => false
  end.

Lemma covers_sound P Ps E :
  covers P Ps = true ->
  eval_preIntv E P ->
  exists P', In P' Ps /\ eval_preIntv E P'.
Proof.
  induction P as [|[e iv] P] in Ps |- *.
  - cbn. destruct Ps as [|[|? ?] [|? ?]] eqn:Heq; try discriminate 1.
    intros _ _.
    exists nil; split; try now constructor.
    intros x iv H. exfalso. apply (in_nil H).
  - destruct e; cbn; intros H; try discriminate H.
    intros HPl.
    assert (exists vR : R, E n = Some vR /\ (Q2R (fst iv) <= vR <= Q2R (snd iv))%R)
      as [v [inE iniv]]
        by (apply HPl; now constructor).
    functional induction checkDim (covers P) n iv Ps; try discriminate H.
    + apply andb_true_iff in H as [H Hsub].
      destruct (IHP _ H) as [P' [inb HP']].
      { hnf. intros. apply HPl. now constructor. }
      destruct (getSlice_sound _ _ _ e3 _ inb) as [iv1 [Hin1 Hsub1]].
      eexists; split; eauto.
      intros x iv' [Heq | inP']; auto.
      injection Heq. intros; subst.
      cbn in *.
      Flover_compute.
      canonize_hyps.
      cbn in *; subst.
      exists v. split; [auto | lra].
    + Flover_compute.
      destruct (Rle_or_lt v (Q2R (snd ivx))) as [Hle|Hlt].
      * destruct (IHP _ L0) as [P' [inb HP']].
        { hnf. intros. apply HPl. now constructor. }
        destruct (getSlice_sound _ _ _ e3 _ inb) as [iv1 [Hin1 Hsub1]].
        eexists; split; eauto.
        intros x iv' [Heq | inP']; auto.
        injection Heq. intros; subst.
        cbn in *.
        Flover_compute.
        canonize_hyps.
        cbn in *; subst.
        exists v. split; [auto | lra].
      * assert (Q2R (snd ivx) <= v <= Q2R (snd iv))%R as Hv.
        { unfold isSupersetIntv in *. cbn in *.
          lra. }
        destruct IHb0 as [P' [inrest H]]; auto.
        { intros x iv' [Heq | inP]; auto.
          injection Heq; intros; subst.
          cbn. exists v. split; auto. apply HPl; cbn; auto. }
        exists P'; split; auto.
        eapply getSlice_rest_sound; eauto.
Qed.

Lemma covers_preVars P Ps :
  covers P Ps = true ->
  forall P1, In P1 Ps -> NatSet.Equal (preIntvVars P1) (preIntvVars P).
Proof.
  induction P as [|[e iv] P] in Ps |- *.
  - cbn. destruct Ps as [|[|? ?] [|? ?]] eqn:Heq; try discriminate 1.
    intros _ P [H|[]].
    rewrite <- H. cbn. apply NatSetProps.equal_refl.
  - cbn. destruct e; try discriminate 1.
    fold (preIntvVars P).
    functional induction (checkDim (covers P) n iv Ps); try discriminate 1.
    + intros H P1 Hin.
      apply andb_true_iff in H as [H _].
      rename e3 into Hsl.
      eapply getSlice_complete in Hsl; eauto.
      destruct Hsl as [[] | [P2 [ivn [-> insl]]]].
      cbn.
      apply NatSetProps.FM.add_m; auto.
      eapply IHP; eauto.
    + intros H P1 Hin.
      Flover_compute.
      rename e3 into Hsl.
      eapply getSlice_complete in Hsl; eauto.
      destruct Hsl as [inrest | [P2 [ivn [-> insl]]]].
      * apply IHb0; auto.
      * cbn.
        apply NatSetProps.FM.add_m; auto.
        eapply IHP; eauto.
Qed.

Module PrecondOrder <: TotalLeBool.

  Definition t := list (expr Q * intv).

  Fixpoint leb (P1 P2: list (expr Q * intv)) :=
    match P1, P2 with
    | nil, _ => true
    | _, nil => false
    | (_, iv1) :: P1, (_, iv2) :: P2 =>
      match (fst iv1) ?= (fst iv2) with
      | Lt => true
      | Eq => leb P1 P2
      | Gt => false
      end
    end.

  Theorem leb_total P1 P2 : leb P1 P2 = true \/ leb P2 P1 = true.
  Proof.
    induction P1 in P2 |- *; destruct P2 as [|[e2 iv2] P2]; auto.
    destruct a as [e1 iv1]; cbn.
    rewrite <- (Qcompare_antisym (fst iv1)).
    destruct (fst iv1 ?= fst iv2); cbn; auto.
  Qed.

End PrecondOrder.

Module Import PrecondSort := Sort PrecondOrder.

Definition checkPreconds (subdivs: list precond) (P: precond) :=
  let Piv := FloverMap.elements (fst P) in
  let Ps := map (fun P => FloverMap.elements (fst P)) subdivs in
  let S_qs := map snd subdivs in
  (* Check that join of the preconditions for the subdivisions
     covers the global precondition *)
  covers Piv (sort Ps) &&
         (* Check that additional constraints encoded by Daisy agree
            for each subdivision *)
         forallbTailrec (fun q => SMTLogic_eqb q (snd P)) S_qs.

Lemma checkPreconds_sound (subdivs: list precond) E P :
  checkPreconds subdivs P = true ->
  eval_precond E P ->
  exists P1, In P1 subdivs /\ eval_precond E P1.
Proof.
  intros H [Pintv Hq].
  apply andb_true_iff in H as [cov Hqs].
  eapply covers_sound in cov; eauto.
  destruct cov as [Pl1 [Hin H]].
  eapply Permutation_in in Hin.
  2: eapply Permutation_sym, Permuted_sort.
  apply in_map_iff in Hin as [P1 [Heq1 Hin]].
  exists P1. split; auto.
  rewrite <- Heq1 in H.
  split; auto.
  rewrite forallbTailrec_forallb in Hqs.
  eapply forallb_forall in Hqs.
  - eapply SMTLogic_eqb_sound; eauto.
  - apply in_map_iff; eauto.
Qed.

Lemma checkPreconds_preVars subdivs P :
  checkPreconds subdivs P = true ->
  forall P1, In P1 subdivs -> NatSet.Equal (preVars P1) (preVars P).
Proof.
  intros chk P1 Hin. apply andb_true_iff in chk as [L R].
  unfold preVars.
  rewrite forallbTailrec_forallb in R.
  eapply forallb_forall in R.
  2: apply in_map_iff; eauto.
  erewrite SMTLogic_eqb_varsLogic; eauto.
  apply NatSetProps.union_equal_1.
  eapply covers_preVars; eauto.
  eapply Permutation_in; eauto using Permuted_sort.
  apply in_map_iff; eauto.
Qed.

(** Interval subdivisions checking function **)
Definition SubdivsChecker (e: expr Q) (absenv: analysisResult)
           (P: precond) subdivs (defVars: FloverMap.t  mType) Gamma :=
  validSSA e (preVars P) &&
           checkPreconds (map (fun S => match S with (P, _, _) => P end) subdivs) P &&
           checkSubdivs e absenv subdivs defVars Gamma.

Definition queriesInSubdivs (subdivs: list (precond * analysisResult * usedQueries)) :=
  map (fun S => match S with (_, _, Q) => Q end) subdivs.

(**
   Soundness proof for the interval subdivs checker.
**)
Theorem subdivs_checking_sound (e: expr Q) (absenv: analysisResult) P subdivs defVars Gamma:
  forall (E1 E2: env) DeltaMap,
    (forall (e': R) (m': mType),
        exists d: R, DeltaMap e' m' = Some d /\ (Rabs d <= mTypeToR m')%R) ->
    approxEnv E1 (toRExpMap Gamma) absenv (freeVars e) NatSet.empty E2 ->
    eval_precond E1 P ->
    (forall Qmap, In Qmap (queriesInSubdivs subdivs) -> unsat_queries Qmap) ->
    getValidMap defVars e (FloverMap.empty mType) = Succes Gamma ->
    SubdivsChecker e absenv P subdivs defVars Gamma = true ->
    (exists outVars, ssa e (freeVars e) outVars) /\
    validRanges e absenv E1 (toRTMap (toRExpMap Gamma)) /\
    validErrorBounds e E1 E2 absenv Gamma /\
    validFPRanges e E2 Gamma DeltaMap.
  (*
    exists Gamma,
      approxEnv E1 (toRExpMap Gamma) absenv (freeVars e) NatSet.empty E2 ->
      exists iv err vR vF m,
        FloverMap.find e absenv = Some (iv, err) /\
        eval_expr E1 (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) vR REAL /\
        eval_expr E2 (toRExpMap Gamma) DeltaMap (toRExp e) vF m /\
        (forall vF m,
            eval_expr E2 (toRExpMap Gamma) DeltaMap (toRExp e) vF m ->
            (Rabs (vR - vF) <= Q2R err))%R.
*)
Proof.
  intros * deltas_matched approxEnv P_valid unsat_qs valid_typeMap chk.
  apply andb_prop in chk as [chk valid_subdivs].
  apply andb_prop in chk as [valid_ssa valid_cover].
  eapply (checkPreconds_sound) in P_valid as [P1 [in_subdivs P_valid]]; eauto.
  assert (validSSA e (preVars P1) = true) as valid_ssa'.
  { eapply validSSA_eq_set; eauto. eapply checkPreconds_preVars; eauto. }
  apply in_map_iff in in_subdivs as [[[P2 A] Qmap] [Heq in_subdivs]].
  cbn in Heq; subst.
  pose proof (checkSubdivs_sound  e _ _ _ _ _ _ _ valid_subdivs in_subdivs) as [range_err_check A_leq].
  assert (ResultChecker e A P1 Qmap defVars Gamma = true) as res_check
    by (unfold ResultChecker; now rewrite valid_ssa', range_err_check).
  eapply approxEnv_empty_dVars in approxEnv; eauto.
  edestruct result_checking_sound as (Hssa & validR & validErr & validFPR); eauto.
  { apply unsat_qs. apply in_map_iff. now exists (P1, A, Qmap). }
  repeat split; eauto using resultLeq_range_sound, resultLeq_error_sound.
Qed.

(*
  exists Gamma; intros approxE1E2.
  assert (approxEnv E1 (toRExpMap Gamma) A (freeVars e) NatSet.empty E2) as approxE1E2'
      by (eapply approxEnv_empty_dVars; eauto).
  assert (validRanges e A E1 (toRTMap (toRExpMap Gamma))) as validRange.
  { edestruct result_checking_sound; eauto; [| intuition].
    apply unsat_qs. apply in_map_iff. now exists (P1, A, Qmap). }
  assert (validErrorBounds e E1 E2 A Gamma) as Hsound.
  { edestruct result_checking_sound; eauto; [| intuition].
    apply unsat_qs. apply in_map_iff. now exists (P1, A, Qmap). }
  eapply validRanges_single in validRange.
  destruct validRange as [iv [err [vR [Hfind [eval_real validRange]]]]].
  eapply validErrorBoundsRec_single in Hsound; eauto.
  destruct Hsound as [[vF [mF eval_float]] err_bounded]; auto.
  destruct (resultLeq_sound _ _ _ A_leq) as [iv1 [err1 [iv2 [err2 H]]]].
  destruct H as [F1 [F2 [sub err_leq]]].
  exists iv2, err2, vR, vF, mF; repeat split; auto.
  assert (err = err1) by congruence; subst.
  apply Qle_Rle in err_leq.
  intros vF0 m Heval.
  specialize (err_bounded vF0 m Heval).
  lra.
Qed.
*)
