From Coq
     Require Import Reals.Reals QArith.Qreals.

From Flover
     Require Import Infra.RealSimps Infra.RationalSimps Infra.RealRationalProps
     Infra.Ltacs AffineForm TypeValidator.

From Coq Require Export QArith.QArith.

From Flover
     Require Export IntervalValidation SMTArith SMTValidation
     RealRangeArith Infra.ExpressionAbbrevs.

Definition RangeValidator e A (P: precond) (Qmap: usedQueries) dVars :=
  if
    validIntervalbounds e A (fst P) dVars
  then true
  else validSMTIntervalbounds e A P Qmap (fun _ => None) dVars.
         (*
  else match validAffineBounds e A (fst P) dVars (FloverMap.empty (affine_form Q)) 1 with
       | Some _ => true
       | None => validSMTIntervalbounds e A P Qmap (fun _ => None) dVars
       end.
*)

Theorem RangeValidator_sound (e : expr Q) (A : analysisResult) (P : precond)
        Qmap fVars dVars outVars (E : env) (Gamma : FloverMap.t mType):
  RangeValidator e A P Qmap dVars = true ->
  ssa e (NatSet.union fVars dVars) outVars ->
  dVars_range_valid dVars E A ->
  (* affine_dVars_range_valid dVars E A 1 (FloverMap.empty (affine_form Q)) (fun _: nat => None) -> *)
  validTypes e Gamma ->
  eval_precond E P ->
  NatSet.Subset (preVars P) fVars ->
  NatSet.Subset (freeVars e -- dVars) fVars ->
  unsat_queries Qmap ->
  validRanges e A E (toRTMap (toRExpMap Gamma)).
Proof.
  intros range_valid ssa_e dVars_valid types_valid pre_valid preVars_free ? unsat_qs.
  unfold RangeValidator in *.
  destruct P as [Piv C].
  destruct (validIntervalbounds e A Piv dVars) eqn: Hivcheck.
  - destruct pre_valid. eapply validIntervalbounds_sound; eauto.
    revert preVars_free. apply NatSetProps.subset_trans.
    unfold preVars. apply NatSetProps.union_subset_1.
  - cbn in *. rewrite Hivcheck in range_valid.
    eapply validSMTIntervalbounds_sound; eauto; congruence.
    (*
  intros range_valid dVars_valid affine_dVars_valid types_valid pre_valid unsat_qs.
  unfold RangeValidator in *.
  destruct P as [Piv C].
  destruct (validIntervalbounds e A Piv dVars) eqn: Hivcheck.
  - eapply validIntervalbounds_sound; set_tac; eauto.
    now destruct pre_valid.
  - pose (iexpmap := (FloverMap.empty (affine_form Q))).
    pose (inoise := 1%nat).
    epose (imap := (fun _ : nat => None)).
    fold iexpmap inoise imap in range_valid, affine_dVars_valid.
    cbn in range_valid. rewrite Hivcheck in range_valid.
    destruct (validAffineBounds e A Piv dVars iexpmap inoise) eqn: Hafcheck.
    2: now eapply validSMTIntervalbounds_sound; set_tac; eauto.
    destruct p as [exprAfs noise].
    pose proof (validAffineBounds_sound) as sound_affine.
    assert ((forall e' : FloverMap.key,
                (exists af : affine_form Q, FloverMap.find (elt:=affine_form Q) e' iexpmap = Some af) ->
                checked_expressions A E Gamma (usedVars e) dVars e' iexpmap inoise imap)) as Hchecked
        by (intros e' Hein; destruct Hein as [af Hein]; unfold iexpmap in Hein;
            rewrite FloverMapFacts.P.F.empty_o in Hein;
            congruence).
    assert (1 > 0)%nat as Hinoise by omega.
    eassert (forall n : nat, (n >= 1)%nat -> imap n = None) as Himap by trivial.
    assert (NatSet.Subset (usedVars e -- dVars) (usedVars e)) as Hsubset by set_tac.
    destruct pre_valid as [Hpre _].
    specialize (sound_affine e A Piv (usedVars e) dVars E Gamma exprAfs noise iexpmap
                             inoise imap Hchecked Hinoise Himap Hafcheck
                             affine_dVars_valid Hsubset Hpre)
      as [map2 [af [vR [aiv [aerr sound_affine]]]]]; intuition.
*)
Qed.

(*
Theorem RangeValidatorCmd_sound (f : cmd Q) (A : analysisResult) (P : precond)
        (Qmap: usedQueries) dVars
        (E : env) Gamma fVars outVars:
  RangeValidatorCmd f A P Qmap dVars = true ->
  ssa f (NatSet.union fVars dVars) outVars ->
  dVars_range_valid dVars E A ->
  affine_dVars_range_valid dVars E A 1 (FloverMap.empty (affine_form Q)) (fun _: nat => None) ->
  eval_precond E P ->
  NatSet.Subset (preVars P) fVars ->
  NatSet.Subset (freeVars f -- dVars) fVars ->
  validTypesCmd f Gamma ->
  unsat_queries Qmap ->
  validRangesCmd f A E (toRTMap (toRExpMap Gamma)).
Proof.
  intros ranges_valid ssa_valid dVars_valid affine_dVars_valid pre_valid preVars_free ? ? ?.
  unfold RangeValidatorCmd in ranges_valid.
  destruct P as [Piv C].
  destruct (validIntervalboundsCmd f A Piv dVars) eqn:iv_valid.
  - destruct pre_valid. eapply validIntervalboundsCmd_sound; eauto.
    revert preVars_free. apply NatSetProps.subset_trans.
    unfold preVars. apply NatSetProps.union_subset_1.
  - pose (iexpmap := (FloverMap.empty (affine_form Q))).
    pose (inoise := 1%nat).
    epose (imap := (fun _ : nat => None)).
    fold iexpmap inoise imap in ranges_valid, H1.
    cbn in ranges_valid. rewrite iv_valid in ranges_valid.
    destruct (validAffineBoundsCmd f A Piv dVars iexpmap inoise) eqn: Hafcheck.
    2: now (eapply validSMTIntervalboundsCmd_sound; eauto).
    destruct p as [exprAfs noise].
    destruct pre_valid as [pre_valid _].
    pose proof (validAffineBoundsCmd_sound) as sound_affine.
    assert ((forall e' : FloverMap.key,
                (exists af : affine_form Q, FloverMap.find (elt:=affine_form Q) e' iexpmap = Some af) ->
                checked_expressions A E Gamma fVars dVars e' iexpmap inoise imap)) as Hchecked
        by (intros e' Hein; destruct Hein as [af Hein]; unfold iexpmap in Hein;
            rewrite FloverMapFacts.P.F.empty_o in Hein;
            congruence).
    assert (1 > 0)%nat as Hinoise by omega.
    eassert (forall n : nat, (n >= 1)%nat -> imap n = None) as Himap by trivial.
    specialize (sound_affine f A Piv fVars dVars outVars E Gamma exprAfs noise iexpmap
                             inoise imap Hchecked Hinoise Himap Hafcheck ssa_valid
                             affine_dVars_valid)
      as [map2 [af [vR [aiv [aerr sound_affine]]]]]; intuition.
    revert preVars_free. eapply NatSetProps.subset_trans.
    apply NatSetProps.union_subset_1.
Qed.
*)
