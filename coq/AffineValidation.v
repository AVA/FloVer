From Coq
     Require Import QArith.QArith QArith.Qreals QArith.Qminmax Lists.List
     micromega.Psatz Recdef.

From Flover.Infra
     Require Import Abbrevs RationalSimps RealRationalProps Ltacs RealSimps.

From Flover
     Require Import TypeValidator ssaPrgs AffineForm AffineArithQ AffineArith
     IntervalValidation RealRangeArith.

Definition updateExpMapIncr e new_af noise (emap: expressionsAffine) intv incr :=
  let new_iv := toIntv new_af in
  if isSupersetIntv new_iv intv then
    Some (FloverMap.add e new_af emap, (noise + incr)%nat)
  else None.

Definition updateExpMap e af noise emap intv :=
  updateExpMapIncr e af noise emap intv 0.

Definition updateExpMapSucc e af noise emap intv :=
  updateExpMapIncr e af noise emap intv 1.

Definition nozeroiv iv :=
  ((Qleb (ivhi iv) 0) && (negb (Qeq_bool (ivhi iv) 0))) ||
            ((Qleb 0 (ivlo iv)) && (negb (Qeq_bool (ivlo iv) 0))).

Fixpoint validAffineBounds (e: expr Q) (A: analysisResult) P (validVars: NatSet.t)
           (exprsAf: expressionsAffine) (currentMaxNoise: nat): option (expressionsAffine * nat) :=
  match FloverMap.find e exprsAf with
  | Some _ =>
    (* expression has already been checked; we do not want to introduce *)
    (*      a new affine polynomial for the same expression *)
    Some (exprsAf, currentMaxNoise)
  | None =>
    (* We see it for the first time; update the expressions map *)
    olet ares := FloverMap.find e A in
    let (intv, _) := ares in
    match e with
    | Var _ v =>
      if NatSet.mem v validVars then
        Some (exprsAf, currentMaxNoise)
      else
        match FloverMap.find e P with
        | Some iv =>
          let af := fromIntv iv currentMaxNoise in
          if isSupersetIntv (toIntv af) intv then
            Some (FloverMap.add e af exprsAf, (currentMaxNoise + 1)%nat)
          else None
        | None => None
        end
    | Const _ c => if isSupersetIntv (c, c) intv then
                    let af := fromIntv (c,c) currentMaxNoise in
                    Some (FloverMap.add e af exprsAf, currentMaxNoise)
                  else None
    | Unop o e' =>
      olet valid := validAffineBounds e' A P validVars exprsAf currentMaxNoise in
      let (exprsAf', n') := valid in
      olet af := FloverMap.find e' exprsAf' in
      match o with
      | Neg =>
        updateExpMap e (AffineArithQ.negate_aff af) n' exprsAf' intv
      | Inv =>
        let iv := toIntv af in
        if nozeroiv iv
        then
          updateExpMapSucc e (AffineArithQ.inverse_aff af n') n' exprsAf' intv
        else None
      end
    | Binop o e1 e2 =>
      olet valid1 := validAffineBounds e1 A P validVars exprsAf currentMaxNoise in
      let (exprsAf1, n1) := valid1 in
      olet af1 := FloverMap.find e1 exprsAf1 in
      olet valid2 := validAffineBounds e2 A P validVars exprsAf1 n1 in
      let (exprsAf2, n2) := valid2 in
      olet af2 := FloverMap.find e2 exprsAf2 in
      match o with
      | Plus =>
        updateExpMap e (AffineArithQ.plus_aff af1 af2) n2 exprsAf2 intv
      | Sub =>
        updateExpMap e (AffineArithQ.subtract_aff af1 af2) n2 exprsAf2 intv
      | Mult =>
        updateExpMapSucc e (AffineArithQ.mult_aff af1 af2 n2) n2 exprsAf2 intv
      | Div =>
          olet ares2 := FloverMap.find e2 A in
          let (aiv2, _) := ares2 in
          let iv2 := toIntv af2 in
          if nozeroiv iv2 && nozeroiv aiv2
          then
            updateExpMapIncr e (AffineArithQ.divide_aff af1 af2 n2) n2 exprsAf2 intv 2
          else None
      end
    | Fma e1 e2 e3 =>
      olet valid1 := validAffineBounds e1 A P validVars exprsAf currentMaxNoise in
      let (exprsAf1, n1) := valid1 in
      olet af1 := FloverMap.find e1 exprsAf1 in
      olet valid2 := validAffineBounds e2 A P validVars exprsAf1 n1 in
      let (exprsAf2, n2) := valid2 in
      olet af2 := FloverMap.find e2 exprsAf2 in
      olet valid3 := validAffineBounds e3 A P validVars exprsAf2 n2 in
      let (exprsAf3, n3) := valid3 in
      olet af3 := FloverMap.find e3 exprsAf3 in
        updateExpMapSucc e (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 n3) af3) n3 exprsAf3 intv
    | Downcast _ e' =>
      olet valid' := validAffineBounds e' A P validVars exprsAf currentMaxNoise in
      let (exprsAf', n') := valid' in
      olet asubres := FloverMap.find e' A in
      let (iv, _) := asubres in
      olet af' := FloverMap.find e' exprsAf' in
      if (isSupersetIntv intv iv) && (isSupersetIntv iv intv) then
        Some (FloverMap.add e af' exprsAf', n')
      else None
    | Let _ _ _ _ => None
    end
  end.

Fixpoint afQ2R (af: affine_form Q): affine_form R := match af with
| AffineForm.Const c => AffineForm.Const (Q2R c)
| Noise n v af' => Noise n (Q2R v) (afQ2R af')
end.

Lemma afQ2R_const v:
  afQ2R (AffineForm.Const v) = AffineForm.Const (Q2R v).
Proof.
  trivial.
Qed.

Lemma afQ2R_get_const a:
  get_const (afQ2R a) = Q2R (get_const a).
Proof.
  induction a; auto.
Qed.

Lemma afQ2R_radius a:
  radius (afQ2R a) = Q2R (AffineArithQ.radius a).
Proof.
  induction a; try (simpl; lra).
  simpl.
  rewrite Q2R_plus.
  rewrite Rabs_eq_Qabs.
  now f_equal.
Qed.

Lemma afQ2R_get_max_index a:
  get_max_index (afQ2R a) = get_max_index a.
Proof.
  unfold get_max_index.
  functional induction (get_max_index_aux 0 a); try auto;simpl; rewrite e0; auto.
Qed.

Lemma to_interval_to_intv a:
  (Q2R (fst (toIntv a)), Q2R (snd (toIntv a))) = toInterval (afQ2R a).
Proof.
  unfold toIntv, toInterval.
  unfold mkInterval, mkIntv.
  simpl fst; simpl snd.
  rewrite afQ2R_get_const.
  rewrite afQ2R_radius.
  f_equal; try apply Q2R_minus; try apply Q2R_plus.
Qed.

Lemma to_interval_to_intv_iv a:
  (Q2RP (toIntv a)) = toInterval (afQ2R a).
Proof.
  rewrite <- to_interval_to_intv.
  reflexivity.
Qed.

Lemma afQ2R_plus_aff af1 af2:
  afQ2R (AffineArithQ.plus_aff af1 af2) = plus_aff (afQ2R af1) (afQ2R af2).
Proof.
  unfold AffineArithQ.plus_aff, plus_aff.
  remember (af1, af2) as a12.
  assert (fst a12 = af1 /\ snd a12 = af2) as Havals by now rewrite Heqa12.
  destruct Havals as [Heqa1 Heqa2].
  rewrite <- Heqa1, <- Heqa2.
  clear Heqa1 Heqa2 Heqa12 af1 af2.
  functional induction (AffineArithQ.plus_aff_tuple a12); simpl; rewrite plus_aff_tuple_equation.
  - f_equal; apply Q2R_plus.
  - f_equal.
    assumption.
  - f_equal.
    assumption.
  - rewrite e2.
    f_equal; try apply Q2R_plus.
    assumption.
  - rewrite e2.
    rewrite e3.
    f_equal; try apply Q2R_plus.
    assumption.
  - rewrite e2.
    rewrite e3.
    f_equal; try apply Q2R_plus.
    assumption.
Qed.

Lemma afQ2R_mult_aff_aux af1 af2:
  afQ2R (AffineArithQ.mult_aff_aux (af1, af2)) = mult_aff_aux (afQ2R af1, afQ2R af2).
Proof.
  unfold AffineArithQ.mult_aff, mult_aff.
  remember (af1, af2) as a12.
  assert (fst a12 = af1 /\ snd a12 = af2) as Havals by now rewrite Heqa12.
  destruct Havals as [Heqa1 Heqa2].
  rewrite <- Heqa1, <- Heqa2.
  clear Heqa1 Heqa2 Heqa12 af1 af2.
  functional induction (AffineArithQ.mult_aff_aux a12); simpl in *;
    rewrite mult_aff_aux_equation;
    try (f_equal; try apply Q2R_mult; assumption).
  {
    rewrite e2.
    f_equal; try assumption.
    simpl.
    rewrite Q2R_plus; do 2 rewrite Q2R_mult.
    do 2 rewrite afQ2R_get_const.
    reflexivity.
  }
  all: rewrite e2; rewrite e3.
  all: f_equal; try assumption.
  all: simpl.
  all: rewrite Q2R_mult.
  all: rewrite afQ2R_get_const.
  all: reflexivity.
Qed.

Lemma afQ2R_mult_aff af1 af2 n:
  afQ2R (AffineArithQ.mult_aff af1 af2 n) = mult_aff (afQ2R af1) (afQ2R af2) n.
Proof.
  unfold AffineArithQ.mult_aff, mult_aff.
  destruct (Qeq_bool (AffineArithQ.radius af1) 0) eqn: Heq.
  - rewrite orb_true_l.
    rewrite Qeq_bool_iff in Heq.
    apply Qeq_eqR in Heq.
    rewrite <- afQ2R_radius in Heq.
    rewrite Q2R0_is_0 in Heq.
    destruct Req_dec_sum as [Heq' | Heq']; rewrite Heq in Heq'; try lra.
    apply afQ2R_mult_aff_aux.
  - rename Heq into Heq1.
    destruct (Qeq_bool (AffineArithQ.radius af2) 0) eqn: Heq2.
    + rewrite orb_true_r.
      rewrite Qeq_bool_iff in Heq2.
      apply Qeq_eqR in Heq2.
      rewrite <- afQ2R_radius in Heq2.
      rewrite Q2R0_is_0 in Heq2.
      destruct Req_dec_sum as [Heq' | Heq']; rewrite Heq2 in Heq'; try lra.
      apply afQ2R_mult_aff_aux.
    + apply RMicromega.Qeq_false in Heq1.
      apply RMicromega.Qeq_false in Heq2.
      rewrite Q2R0_is_0 in Heq1.
      rewrite Q2R0_is_0 in Heq2.
      rewrite <- afQ2R_radius in Heq1.
      rewrite <- afQ2R_radius in Heq2.
      simpl.
      destruct Req_dec_sum as [Heq' | Heq'];
        try (apply Rmult_integral in Heq'; destruct Heq'; try lra).
      f_equal.
      * rewrite Q2R_mult.
        do 2 rewrite afQ2R_radius.
        reflexivity.
      * apply afQ2R_mult_aff_aux.
Qed.

Lemma afQ2R_negate_aff af:
  afQ2R (AffineArithQ.negate_aff af) = negate_aff (afQ2R af).
Proof.
  unfold AffineArithQ.negate_aff.
  unfold AffineArithQ.mult_aff_const.
  rewrite afQ2R_mult_aff.
  simpl.
  unfold negate_aff.
  unfold mult_aff_const.
  f_equal.
  - f_equal.
    rewrite Q2R_opp.
    lra.
  - f_equal.
    unfold get_max_index.
    functional induction (get_max_index_aux 0 af); try auto; simpl; now rewrite e0.
Qed.

Lemma afQ2R_subtract_aff af1 af2:
  afQ2R (AffineArithQ.subtract_aff af1 af2) = subtract_aff (afQ2R af1) (afQ2R af2).
Proof.
  unfold AffineArithQ.subtract_aff.
  rewrite afQ2R_plus_aff.
  rewrite afQ2R_negate_aff.
  reflexivity.
Qed.

Lemma afQ2R_inverse_aff af n:
  above_zero (afQ2R af) \/ below_zero (afQ2R af) ->
  afQ2R (AffineArithQ.inverse_aff af n) = inverse_aff (afQ2R af) n.
Proof.
  intros above_below.
  unfold AffineArithQ.inverse_aff.
  unfold inverse_aff.
  unfold above_zero, below_zero in above_below.
  unfold ivhi, IVhi, ivlo, IVlo in *.
  rewrite <- to_interval_to_intv in above_below.
  rewrite <- to_interval_to_intv.
  simpl fst in above_below |-*.
  simpl snd in above_below |-*.
  assert (AffineArithQ.radius af >= 0) by eauto using AffineArithQ.radius_nonneg.
  assert (get_const af - AffineArithQ.radius af <= get_const af + AffineArithQ.radius af) as Hrel by lra.
  replace 0%R with (Q2R 0) in above_below by lra.
  destruct above_below as [Heq | Heq]; apply Rlt_Qlt in Heq.
  - assert (get_const af + AffineArithQ.radius af > 0) as Hposhi by lra.
    destruct (Qlt_bool (get_const af + AffineArithQ.radius af) 0) eqn: H'';
      try rewrite Qlt_bool_iff in H''; try lra.
    apply Qlt_Rlt in Hposhi.
    destruct (Rlt_dec (Q2R (get_const af + AffineArithQ.radius af)) 0); try lra.
    apply Rlt_Qlt in Hposhi.
    assert (~ get_const af - AffineArithQ.radius af == 0) as minus_nonzero by lra.
    assert (~ get_const af + AffineArithQ.radius af == 0) as plus_nonzero by lra.
    assert (~ -(get_const af - AffineArithQ.radius af) == 0) as minus_nonzero' by lra.
    assert (~ -(get_const af + AffineArithQ.radius af) == 0) as plus_nonzero' by lra.
    assert (~ maxAbs (get_const af - AffineArithQ.radius af, get_const af + AffineArithQ.radius af) *
            maxAbs (get_const af - AffineArithQ.radius af, get_const af + AffineArithQ.radius af) == 0) as ?.
    {
      unfold maxAbs.
      unfold minAbs.
      simpl fst.
      simpl snd.
      try rewrite Qabs.Qabs_pos; try lra.
      try rewrite Qabs.Qabs_pos; try lra.
      try rewrite Q.max_r; try lra.
      try rewrite Q.min_l; try lra.
      unfold not.
      intros H'.
      apply Qmult_integral in H'; lra.
    }
    assert (~ maxAbs (get_const af - AffineArithQ.radius af, get_const af + AffineArithQ.radius af) == 0) as ?.
    {
      unfold maxAbs.
      unfold minAbs.
      simpl fst.
      simpl snd.
      try rewrite Qabs.Qabs_pos; try lra.
      try rewrite Qabs.Qabs_pos; try lra.
      try rewrite Q.max_r; try lra.
    }
    assert (~ minAbs (get_const af - AffineArithQ.radius af, get_const af + AffineArithQ.radius af) == 0) as ?.
    {
      unfold maxAbs.
      unfold minAbs.
      simpl fst.
      simpl snd.
      try rewrite Qabs.Qabs_pos; try lra.
      try rewrite Qabs.Qabs_pos; try lra.
      try rewrite Q.max_r; try lra.
      try rewrite Q.min_l; try lra.
    }
    simpl.
    f_equal; unfold toIntv; simpl;
      repeat rewrite minAbs_impl_RminAbs; repeat rewrite maxAbs_impl_RmaxAbs;
      replace 1%R with (Q2R (1%Q)) by lra;
      replace 2%R with (Q2R (2#1)) by lra;
      repeat (repeat rewrite <- Q2R_plus; repeat rewrite <- Q2R_mult; repeat rewrite <- Q2R_div;
              repeat rewrite <- Q2R_minus; repeat rewrite <- Q2R_opp); try lra.
      unfold mult_aff_const, plus_aff_const.
      unfold AffineArithQ.mult_aff_const, AffineArithQ.plus_aff_const.
      repeat rewrite <- afQ2R_const.
      rewrite <- afQ2R_mult_aff.
      rewrite <- afQ2R_plus_aff.
      repeat f_equal.
      now rewrite afQ2R_get_max_index.
  - rewrite <- Qlt_bool_iff in Heq.
    rewrite Heq.
    rewrite Qlt_bool_iff in Heq.
    apply Qlt_Rlt in Heq.
    destruct (Rlt_dec (Q2R (get_const af + AffineArithQ.radius af)) 0); try lra.
    apply Rlt_Qlt in Heq.
    assert (get_const af - AffineArithQ.radius af < 0) as minus_neg by lra.
    assert (~ get_const af - AffineArithQ.radius af == 0) as minus_nonzero by lra.
    assert (~ get_const af + AffineArithQ.radius af == 0) as plus_nonzero by lra.
    assert (~ -(get_const af - AffineArithQ.radius af) == 0) as minus_nonzero' by lra.
    assert (~ -(get_const af + AffineArithQ.radius af) == 0) as plus_nonzero' by lra.
    assert (~ maxAbs (get_const af - AffineArithQ.radius af, get_const af + AffineArithQ.radius af) *
            maxAbs (get_const af - AffineArithQ.radius af, get_const af + AffineArithQ.radius af) == 0) as ?.
    {
      unfold maxAbs.
      unfold minAbs.
      simpl fst.
      simpl snd.
      try rewrite Qabs.Qabs_neg; try lra.
      try rewrite Qabs.Qabs_neg; try lra.
      try rewrite Q.max_l; try lra.
      try rewrite Q.min_r; try lra.
      unfold not.
      intros H'.
      apply Qmult_integral in H'; lra.
    }
    assert (~ maxAbs (get_const af - AffineArithQ.radius af, get_const af + AffineArithQ.radius af) == 0) as ?.
    {
      unfold maxAbs.
      unfold minAbs.
      simpl fst.
      simpl snd.
      try rewrite Qabs.Qabs_neg; try lra.
      try rewrite Qabs.Qabs_neg; try lra.
      rewrite Q.max_l; try lra.
    }
    assert (~ minAbs (get_const af - AffineArithQ.radius af, get_const af + AffineArithQ.radius af) == 0) as ?.
    {
      unfold maxAbs.
      unfold minAbs.
      simpl fst.
      simpl snd.
      try rewrite Qabs.Qabs_neg; try lra.
      try rewrite Qabs.Qabs_neg; try lra.
      try rewrite Q.max_l; try lra.
      try rewrite Q.min_r; try lra.
    }
    simpl.
    f_equal; unfold toIntv; simpl;
      repeat rewrite minAbs_impl_RminAbs; repeat rewrite maxAbs_impl_RmaxAbs;
      replace 1%R with (Q2R (1%Q)) by lra;
      replace 2%R with (Q2R (2#1)) by lra;
      repeat (repeat rewrite <- Q2R_plus; repeat rewrite <- Q2R_mult; repeat rewrite <- Q2R_div;
              repeat rewrite <- Q2R_minus; repeat rewrite <- Q2R_opp); try lra.
      unfold mult_aff_const, plus_aff_const.
      unfold AffineArithQ.mult_aff_const, AffineArithQ.plus_aff_const.
      repeat rewrite <- afQ2R_const.
      rewrite <- afQ2R_mult_aff.
      rewrite <- afQ2R_plus_aff.
      repeat f_equal.
      now rewrite afQ2R_get_max_index.
Qed.

Lemma afQ2R_divide_aff af1 af2 n:
  above_zero (afQ2R af2) \/ below_zero (afQ2R af2) ->
  afQ2R (AffineArithQ.divide_aff af1 af2 n) = divide_aff (afQ2R af1) (afQ2R af2) n.
Proof.
  intros.
  unfold AffineArithQ.divide_aff.
  rewrite afQ2R_mult_aff.
  rewrite afQ2R_inverse_aff; auto.
Qed.

Lemma afQ2R_fresh n a:
  fresh n a <-> fresh n (afQ2R a).
Proof.
  split; induction a; intros *.
  all: try (unfold fresh, get_max_index; rewrite get_max_index_aux_equation; now simpl).
  all: intros A.
  all: remember A as A' eqn:tmp; clear tmp.
  all: apply fresh_noise_gt in A.
  all: apply fresh_noise_compat in A'.
  all: specialize (IHa A').
  all: apply fresh_noise; assumption.
Qed.

Lemma nozero_above_below af:
  nozeroiv (toIntv af) = true ->
  above_zero (afQ2R af) \/ below_zero (afQ2R af).
Proof.
  intros noz % orb_prop.
  destruct noz as [below | above].
  - right.
    unfold below_zero.
    apply andb_prop in below as [below notz].
    rewrite negb_true_iff in notz.
    apply Qeq_bool_neq in notz.
    rewrite Qle_bool_iff in below.
    assert (IVhi (toInterval (afQ2R af)) = Q2R (ivhi (toIntv af))) as Heq
        by (rewrite <- to_interval_to_intv; trivial).
    rewrite Heq.
    assert (ivhi (toIntv af) < 0) as Hlt by lra.
    apply Qlt_Rlt in Hlt.
    lra.
  - left.
    unfold above_zero.
    apply andb_prop in above as [above notz].
    rewrite negb_true_iff in notz.
    apply Qeq_bool_neq in notz.
    rewrite Qle_bool_iff in above.
    assert (IVlo (toInterval (afQ2R af)) = Q2R (ivlo (toIntv af))) as Heq
        by (rewrite <- to_interval_to_intv; trivial).
    rewrite Heq.
    assert (ivlo (toIntv af) > 0) as Hlt by lra.
    apply Qlt_Rlt in Hlt.
    lra.
Qed.

Definition affine_dVars_range_valid (dVars: NatSet.t) (E: env) (A: analysisResult) noise exprAfs map1: Prop :=
  forall v, NatSet.In v dVars ->
       exists af vR iv err,
         isSupersetIntv (toIntv af) iv = true /\
         FloverMap.find (elt:=affine_form Q) (Var Q v) exprAfs = Some af /\
         fresh noise af /\
         (forall n, (n >= noise)%nat -> map1 n = None) /\
         FloverMap.find (Var Q v) A = Some (iv, err) /\
         E v = Some vR /\
         af_evals (afQ2R af) vR map1.

Lemma validAffineBounds_validRanges e (A: analysisResult) E Gamma:
  (exists map af vR aiv aerr,
      FloverMap.find e A = Some (aiv, aerr) /\
      isSupersetIntv (toIntv af) aiv = true /\
      eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) vR REAL /\
      af_evals (afQ2R af) vR map) ->
  exists iv err vR,
    FloverMap.find e A = Some (iv, err) /\
    eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) vR REAL /\
    (Q2R (fst iv) <= vR <= Q2R (snd iv))%R.
Proof.
  intros sound_affine.
  destruct sound_affine as [map [af [vR [aiv [aerr [Haiv [Hsup [Hee Heval]]]]]]]].
  exists aiv, aerr, vR.
  split; try split; try auto.
  apply AffineArith.to_interval_containment in Heval.
  unfold isSupersetIntv in Hsup.
  apply andb_prop in Hsup as [Hsupl Hsupr].
  apply Qle_bool_iff in Hsupl.
  apply Qle_bool_iff in Hsupr.
  apply Qle_Rle in Hsupl.
  apply Qle_Rle in Hsupr.
  rewrite <- to_interval_to_intv in Heval.
  simpl in Heval.
  destruct Heval as [Heval1 Heval2].
  split; eauto using Rle_trans.
Qed.

Definition checked_expressions (A: analysisResult) E Gamma fVars dVars e iexpmap
           inoise map1 :=
  exists af vR aiv aerr,
    (* WAS: usedVars e *)
    NatSet.Subset (freeVars e) (NatSet.union fVars dVars) /\
    FloverMap.find e A = Some (aiv, aerr) /\
    isSupersetIntv (toIntv af) aiv = true /\
    FloverMap.find e iexpmap = Some af /\
    fresh inoise af /\
    (forall n, (n >= inoise)%nat -> map1 n = None) /\
    eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) vR REAL /\
    validRanges e A E (toRTMap (toRExpMap Gamma)) /\
    af_evals (afQ2R af) vR map1.

Lemma checked_expressions_contained A E Gamma fVars dVars e expmap1 expmap2 map1
      map2 noise1 noise2:
  contained_map map1 map2 ->
  contained_flover_map expmap1 expmap2 ->
  (noise2 >= noise1)%nat ->
  (forall n : nat, (n >= noise2)%nat -> map2 n = None) ->
  checked_expressions A E Gamma fVars dVars e expmap1 noise1 map1 ->
  checked_expressions A E Gamma fVars dVars e expmap2 noise2 map2.
Proof.
  intros cont contf Hnoise Hvalidmap checked1.
  unfold checked_expressions in checked1 |-*.
  destruct checked1 as [af [vR [aiv [aerr checked1]]]].
  exists af, vR, aiv, aerr.
  intuition; eauto using fresh_monotonic, af_evals_map_extension.
Qed.

Lemma checked_expressions_flover_map_add_compat A E Gamma fVars dVars e e' af
      expmap noise map:
  Q_orderedExps.exprCompare e e' <> Eq ->
  checked_expressions A E Gamma fVars dVars e' expmap noise map ->
  checked_expressions A E Gamma fVars dVars e' (FloverMap.add e af expmap) noise map.
Proof.
  intros Hneq checked1.
  unfold checked_expressions in checked1 |-*.
  destruct checked1 as [af' [vR [aiv [aerr checked1]]]].
  exists af', vR, aiv, aerr.
  intuition.
  rewrite FloverMapFacts.P.F.add_neq_o; auto.
Qed.

Lemma af_evals_afQ2R_from_intv_updMap iv noise map vR:
  (Q2R (fst iv) <= vR <= Q2R (snd iv))%R ->
  exists q : noise_type,
    af_evals (afQ2R (fromIntv iv noise)) vR (updMap map noise q).
Proof.
  intros interval_containment.
  destruct (Qeq_bool (ivlo iv) (ivhi iv)) eqn: Heq.
  {
    exists noise_zero.
    unfold af_evals, fromIntv.
    rewrite Heq.
    cbn.
    rewrite Qeq_bool_iff in Heq.
    apply Qeq_eqR in Heq.
    rewrite Q2R_plus.
    rewrite Q2R_div; [|lra].
    rewrite Q2R_div; [|lra].
    field_rewrite ((Q2R (2 # 1)) = 2%R).
    cbn in Heq.
    rewrite Heq.
    rewrite Heq in interval_containment.
    lra.
  }
  unfold af_evals, fromIntv.
  rewrite Heq.
  apply Qeq_bool_neq in Heq.
  simpl in Heq.
  simpl.
  setoid_rewrite upd_sound.
  simpl.
  apply Q.max_case_strong.
  - intros x y Hxy.
    apply Qeq_eqR in Hxy.
    rewrite Hxy.
    auto.
  - intros Hmax.
    apply Qle_Rle in Hmax.
    repeat rewrite Q2R_minus.
    repeat rewrite Q2R_plus.
    repeat rewrite Q2R_div; try lra.
    replace (Q2R (2#1)) with 2%R by lra.
    repeat rewrite Q2R_minus in Hmax.
    repeat rewrite Q2R_plus in Hmax.
    rewrite Q2R_div in Hmax; try lra.
    rewrite Q2R_div in Hmax; try lra.
    replace (Q2R (2#1)) with 2%R in Hmax by lra.
    pose (l := (Q2R (fst iv))).
    pose (h := (Q2R (snd iv))).
    fold l h in Hmax, interval_containment |-*.
    pose (noise_expression := ((vR - h / 2 - l / 2) / (h / 2 + l / 2 - l))%R).
    assert (-(1) <= noise_expression <= 1)%R as Hnoise.
    {
      unfold noise_expression.
      apply Rabs_Rle_condition.
      destruct (Rle_lt_dec (h / 2 + l / 2 - l) 0)%R as [Hle0 | Hle0].
      - apply Rle_lt_or_eq_dec in Hle0; destruct Hle0 as [Hlt | Hlt];
          try (field_simplify in Hlt; assert (h = l) as Hz by lra; apply eqR_Qeq in Hz; lra).
      - rewrite Rdiv_abs_le_bounds; try lra.
        assert (0 < h - l)%R as H1 by lra.
        field_rewrite (vR - h / 2 - l /2 = vR - (h + l) / 2)%R.
        field_rewrite (1 * (h / 2 + l / 2 - l) = (h - l) / 2)%R.
        apply Rabs_Rle_condition; lra.
    }
    rename noise into inoise.
    pose (noise := exist (fun x => -(1) <= x <= 1)%R noise_expression Hnoise).
    exists noise.
    unfold noise, noise_expression.
    simpl.
    field.
    intros Hnotz.
    field_simplify in Hnotz.
    assert (h = l) as Hz by lra.
    apply eqR_Qeq in Hz.
    lra.
  - intros Hmax.
    apply Qle_Rle in Hmax.
    repeat rewrite Q2R_minus.
    repeat rewrite Q2R_plus.
    repeat rewrite Q2R_div; try lra.
    replace (Q2R (2#1)) with 2%R by lra.
    repeat rewrite Q2R_minus in Hmax.
    repeat rewrite Q2R_plus in Hmax.
    rewrite Q2R_div in Hmax; try lra.
    rewrite Q2R_div in Hmax; try lra.
    replace (Q2R (2#1)) with 2%R in Hmax by lra.
    pose (l := (Q2R (fst iv))).
    pose (h := (Q2R (snd iv))).
    fold l h in Hmax, interval_containment |-*.
    pose (noise_expression := ((vR - h / 2 - l / 2) / (h / 2 + l / 2 - l))%R).
    assert (-(1) <= noise_expression <= 1)%R as Hnoise.
    {
      unfold noise_expression.
      apply Rabs_Rle_condition.
      destruct (Rle_lt_dec (h / 2 + l / 2 - l) 0)%R as [Hle0 | Hle0].
      - apply Rle_lt_or_eq_dec in Hle0; destruct Hle0 as [Hlt | Hlt];
          try (field_simplify in Hlt; assert (h = l) as Hz by lra; apply eqR_Qeq in Hz; lra).
      - rewrite Rdiv_abs_le_bounds; try lra.
        assert (0 < h - l)%R as H1 by lra.
        field_rewrite (vR - h / 2 - l /2 = vR - (h + l) / 2)%R.
        field_rewrite (1 * (h / 2 + l / 2 - l) = (h - l) / 2)%R.
        apply Rabs_Rle_condition; lra.
    }
    rename noise into inoise.
    pose (noise := exist (fun x => -(1) <= x <= 1)%R noise_expression Hnoise).
    exists noise.
    unfold noise, noise_expression.
    simpl.
    field.
    intros Hnotz.
    field_simplify in Hnotz.
    assert (h = l) as Hz by lra.
    apply eqR_Qeq in Hz.
    lra.
Qed.

(*
Lemma validAffineBounds_sound_var A P E Gamma fVars dVars n:
  forall (noise : nat) (exprAfs : expressionsAffine) (inoise : nat)
    (iexpmap : FloverMap.t (affine_form Q)) (map1 : nat -> option noise_type),
    (forall e : FloverMap.key,
        (exists af : affine_form Q, FloverMap.find (elt:=affine_form Q) e iexpmap = Some af) ->
        checked_expressions A E Gamma fVars dVars e iexpmap inoise map1) ->
    (inoise > 0)%nat ->
    (forall n0 : nat, (n0 >= inoise)%nat -> map1 n0 = None) ->
    validAffineBounds (Var Q n) A P dVars iexpmap inoise = Some (exprAfs, noise) ->
    affine_dVars_range_valid dVars E A inoise iexpmap map1 ->
    NatSet.Subset (usedVars (Var Q n) -- dVars) fVars ->
    P_intv_sound E P ->
    validTypes (Var Q n) Gamma ->
    exists (map2 : noise_mapping) (af : affine_form Q) (vR : R) (aiv : intv)
      (aerr : error),
      contained_map map1 map2 /\
      contained_flover_map iexpmap exprAfs /\
      FloverMap.find (elt:=intv * error) (Var Q n) A = Some (aiv, aerr) /\
      isSupersetIntv (toIntv af) aiv = true /\
      FloverMap.find (elt:=affine_form Q) (Var Q n) exprAfs = Some af /\
      fresh noise af /\
      (forall n0 : nat, (n0 >= noise)%nat -> map2 n0 = None) /\
      (noise >= inoise)%nat /\
      eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (Var Q n))) vR REAL /\
      validRanges (Var Q n) A E (toRTMap (toRExpMap Gamma)) /\
      af_evals (afQ2R af) vR map2 /\
      (forall e : FloverMap.key,
          FloverMap.find (elt:=affine_form Q) e iexpmap = None ->
          (exists af0 : affine_form Q, FloverMap.find (elt:=affine_form Q) e exprAfs = Some af0) ->
          checked_expressions A E Gamma fVars dVars e exprAfs noise map2).
Proof.
  intros * visitedExpr inoisegtz validmap1 validBounds dVarsValid varsDisjoint fVarsSound varsTyped;
    simpl in validBounds.
  specialize (dVarsValid n).
  specialize (fVarsSound n).
  destruct (varsTyped) as [mV [find_mv [_ valid_exec]]].
  pose proof visitedExpr as visitedExpr'.
  destruct (FloverMap.find (elt:=affine_form Q) (Var Q n) iexpmap) eqn: Hvisited.
  {
    inversion validBounds; subst; clear validBounds.
    specialize (visitedExpr (Var Q n)).
    assert (NatSet.Subset (usedVars (Var Q n)) (fVars ∪ dVars)).
    {
      set_tac. set_tac. subst.
      hnf in varsDisjoint.
      specialize (varsDisjoint a0).
      destruct (NatSet.mem a0 dVars) eqn:?.
      + right. now apply NatSet.mem_spec.
      + left. apply varsDisjoint. set_tac.
    }
    destruct visitedExpr as [af [vR [aiv [aerr visitedExpr]]]]; eauto.
    exists map1, af, vR, aiv, aerr.
    intuition.
  }
  destruct (FloverMap.find (elt:=intv * error) (Var Q n) A) as [p |] eqn: Hares; simpl in validBounds; try congruence.
  destruct p as [aiv aerr].
  destruct (n mem dVars) eqn: Hmem.
  - rewrite NatSet.mem_spec in Hmem.
    specialize (dVarsValid Hmem).
    assert (n ∈ fVars ∪ dVars) as H by intuition.
    destruct dVarsValid as [af [vR [iv [err dVarsValid]]]]; try reflexivity.
    inversion validBounds; subst; clear validBounds.
    exists map1, af, vR, iv, err.
    intuition; try congruence.
  - assert (exists iv, FloverMap.find (Var Q n) P = Some iv) as [iv var_P]
        by (Flover_compute; eauto).
    destruct (isSupersetIntv (toIntv (fromIntv iv inoise)) aiv) eqn: Hsup;
      (rewrite var_P, Hsup in validBounds; try discriminate).
    assert (exprAfs = FloverMap.add (Var Q n) (fromIntv iv inoise) iexpmap) as HexprAfs
        by congruence.
    assert (noise = (inoise + 1)%nat) as Hnoise by congruence.
    apply not_in_not_mem in Hmem.
    assert (n ∈ fVars ∪ dVars) as H by intuition.
    (* assert (n ∈ fVars) as H' by intuition. *)
    destruct (fVarsSound iv) as [vR [eMap interval_containment]].
    { now apply find_in_precond. }
    assert (FloverMap.find (Var Q n) (FloverMap.add (Var Q n) (fromIntv iv inoise) iexpmap) = Some (fromIntv iv inoise)) as Hfind
        by (rewrite FloverMapFacts.P.F.add_eq_o; try auto; apply Q_orderedExps.exprCompare_refl).
    assert (eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                      (toREval (toRExp (Var Q n))) vR REAL) as Heeval.
    { eapply Var_load; try eauto.
      unfold toRTMap.
      assert (exists m, toRExpMap Gamma (Var R n) = Some m) as t_var.
      { eexists; eapply toRExpMap_some with (e:= Var Q n); eauto. }
      destruct t_var as(? & t_var).
      rewrite t_var; auto. }
    destruct (Qeq_bool (ivlo iv) (ivhi iv)) eqn: Heq.
    + assert (af_evals (afQ2R (fromIntv iv inoise)) vR map1) as Hevals.
      {
        assert (fromIntv iv inoise = (AffineForm.Const (ivhi iv / (2 # 1) + ivlo iv / (2 # 1))%Q)) as HfromIntv
            by (unfold fromIntv; now rewrite Heq).
        pose proof Heq as Heq'.
        apply Qeq_bool_iff in Heq'.
        simpl in Heq'.
        apply Qeq_eqR in Heq'.
        rewrite Heq' in interval_containment.
        assert (vR = Q2R (snd iv)) as HvR by lra.
        rewrite HfromIntv.
        unfold af_evals.
        simpl.
        rewrite Q2R_plus.
        repeat rewrite Q2R_div by lra.
        rewrite Heq'.
        rewrite HvR.
        lra.
      }
      assert (fresh (inoise + 1) (fromIntv iv inoise)) as Hfresh
          by (unfold fresh, fromIntv, get_max_index; rewrite Heq; simpl; lia).
      rewrite HexprAfs, Hnoise.
      exists map1, (fromIntv iv inoise), vR, aiv, aerr.
      repeat split; auto.
      * reflexivity.
      * apply contained_flover_map_extension; assumption.
      * intros n' Hn'.
        apply validmap1.
        lia.
      * lia.
      * apply validAffineBounds_validRanges.
        exists map1, (fromIntv iv inoise), vR, aiv, aerr.
        repeat split; auto.
      * intros e Hnone Hsome.
        destruct Hsome as [afS Hsome].
        {
          destruct (FloverMapFacts.O.MO.eq_dec (Var Q n) e).
          - assert (Q_orderedExps.exprCompare e (Var Q n) = Eq)
              by (now rewrite Q_orderedExps.exprCompare_eq_sym).
            rewrite FloverMapFacts.P.F.add_eq_o in Hsome; auto.
            inversion Hsome; subst; clear Hsome.
            unfold checked_expressions.
            exists (fromIntv iv inoise), vR, aiv, aerr.
            intuition.
            + rewrite usedVars_eq_compat; eauto.
              set_tac.
              left; set_tac; split; auto; subst; set_tac.
            + erewrite FloverMapFacts.P.F.find_o; eauto.
            + rewrite FloverMapFacts.P.F.add_eq_o; auto.
            + erewrite expr_compare_eq_eval_compat; eauto.
            + eapply validRanges_eq_compat; eauto.
              simpl; split; auto.
              apply validAffineBounds_validRanges.
              exists map1, (fromIntv iv inoise), vR, aiv, aerr.
              repeat split; auto.
          - rewrite FloverMapFacts.P.F.add_neq_o in Hsome; auto.
            congruence.
        }
    + assert (exists q, af_evals (afQ2R (fromIntv iv inoise)) vR (updMap map1 inoise q))
        as [q Hevals] by (apply af_evals_afQ2R_from_intv_updMap; auto).
      assert (forall n0 : nat, (n0 >= inoise + 1)%nat -> updMap map1 inoise q n0 = None).
      {
        intros n' Hn'.
        unfold updMap.
        destruct (n' =? inoise) eqn: Hneq.
        - apply beq_nat_true in Hneq.
          lia.
        - apply validmap1.
          lia.
      }
      assert (fresh (inoise + 1) (fromIntv iv inoise)) as Hfresh
          by (unfold fresh, fromIntv, get_max_index; rewrite Heq; simpl; lia).
      rewrite HexprAfs, Hnoise.
      exists (updMap map1 inoise q), (fromIntv iv inoise), vR, aiv, aerr.
      repeat split; auto.
      * apply contained_map_extension.
        apply validmap1; lia.
      * apply contained_flover_map_extension.
        assumption.
      * lia.
      * apply validAffineBounds_validRanges.
        exists (updMap map1 inoise q), (fromIntv iv inoise), vR, aiv, aerr.
        repeat split; auto.
      * intros e Hnone Hsome.
        destruct Hsome as [afS Hsome].
        {
          destruct (FloverMapFacts.O.MO.eq_dec (Var Q n) e).
          - assert (Q_orderedExps.exprCompare e (Var Q n) = Eq)
              by (now rewrite Q_orderedExps.exprCompare_eq_sym).
            rewrite FloverMapFacts.P.F.add_eq_o in Hsome; auto.
            inversion Hsome; subst; clear Hsome.
            unfold checked_expressions.
            exists (fromIntv iv inoise), vR, aiv, aerr.
            intuition.
            + rewrite usedVars_eq_compat; eauto.
              set_tac.
              left; set_tac; split; auto; subst; set_tac.
            + erewrite FloverMapFacts.P.F.find_o; eauto.
            + rewrite FloverMapFacts.P.F.add_eq_o; auto.
            + erewrite expr_compare_eq_eval_compat; eauto.
            + eapply validRanges_eq_compat; eauto.
              simpl; split; auto.
              apply validAffineBounds_validRanges.
              exists (updMap map1 inoise q), (fromIntv iv inoise), vR, aiv, aerr.
              repeat split; auto.
          - rewrite FloverMapFacts.P.F.add_neq_o in Hsome; auto.
            congruence.
        }
Qed.

Lemma validAffineBounds_sound_const A P E Gamma fVars dVars m v:
  forall (noise : nat) (exprAfs : expressionsAffine) (inoise : nat)
    (iexpmap : FloverMap.t (affine_form Q)) (map1 : nat -> option noise_type),
    (forall e : FloverMap.key,
        (exists af : affine_form Q, FloverMap.find (elt:=affine_form Q) e iexpmap = Some af) ->
        checked_expressions A E Gamma fVars dVars e iexpmap inoise map1) ->
    (inoise > 0)%nat ->
    (forall n : nat, (n >= inoise)%nat -> map1 n = None) ->
    validAffineBounds (Const m v) A P dVars iexpmap inoise = Some (exprAfs, noise) ->
    affine_dVars_range_valid dVars E A inoise iexpmap map1 ->
    NatSet.Subset (usedVars (Const m v) -- dVars) fVars ->
    P_intv_sound E P ->
    validTypes (Const m v) Gamma ->
    exists (map2 : noise_mapping) (af : affine_form Q) (vR : R) (aiv : intv)
      (aerr : error),
      contained_map map1 map2 /\
      contained_flover_map iexpmap exprAfs /\
      FloverMap.find (elt:=intv * error) (Const m v) A = Some (aiv, aerr) /\
      isSupersetIntv (toIntv af) aiv = true /\
      FloverMap.find (elt:=affine_form Q) (Const m v) exprAfs = Some af /\
      fresh noise af /\
      (forall n : nat, (n >= noise)%nat -> map2 n = None) /\
      (noise >= inoise)%nat /\
      eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (Const m v))) vR REAL /\
      validRanges (Const m v) A E (toRTMap (toRExpMap Gamma)) /\
      af_evals (afQ2R af) vR map2 /\
      (forall e : FloverMap.key,
          FloverMap.find (elt:=affine_form Q) e iexpmap = None ->
          (exists af0 : affine_form Q, FloverMap.find (elt:=affine_form Q) e exprAfs = Some af0) ->
          checked_expressions A E Gamma fVars dVars e exprAfs noise map2).
Proof.
  intros * visitedExpr inoisegtz validmap1 validBounds dVarsValid varsDisjoint fVarsSound varsTyped;
    simpl in validBounds.
  pose proof visitedExpr as visitedExpr'.
  unfold checked_expressions in visitedExpr.
  destruct (FloverMap.find (elt:=affine_form Q) (Const m v) iexpmap) eqn: Hvisited.
  {
    inversion validBounds; subst; clear validBounds.
    specialize (visitedExpr (Const m v)).
    destruct visitedExpr as [af [vR [aiv [aerr visitedExpr]]]].
    - eexists; eauto.
    - exists map1, af, vR, aiv, aerr.
      intuition.
  }
  destruct varsTyped as (mt & find_t & t_eq & valid_exec); subst.
  rename mt into m.
  destruct (FloverMap.find (elt:=intv * error) (Const m v) A) eqn: Hares;
    simpl in validBounds; try congruence.
  destruct p as [i e].
  destruct (isSupersetIntv (v, v) i) eqn: Hsup; try congruence.
  assert (isSupersetIntv (v, v) i = true) as Hsup' by assumption.
  apply andb_prop in Hsup' as [L R].
  rewrite Qle_bool_iff in L, R.
  simpl ivlo in L, R.
  simpl ivhi in L, R.
  assert (fst i <= v) as L' by assumption.
  assert (v <= snd i) as R' by assumption.
  apply Qle_Rle in L.
  apply Qle_Rle in R.
  inversion validBounds; subst; clear validBounds.
  assert (isSupersetIntv (toIntv (fromIntv (v, v) noise)) i = true).
  {
    unfold fromIntv, toIntv.
    simpl.
    rewrite Qeq_bool_refl.
    apply andb_true_intro.
    split; rewrite Qle_bool_iff; simpl; field_simplify; field_rewrite ((2#1) * v * / (2#1) == v).
    * now field_rewrite (fst i / 1 == fst i).
    * now field_rewrite (snd i / 1 == snd i).
  }
  assert (fresh noise (fromIntv (v, v) noise))
    by (unfold fromIntv; simpl ivlo; simpl ivhi; rewrite Qeq_bool_refl;
        unfold fresh, get_max_index; rewrite get_max_index_aux_equation; lia).
  assert (af_evals (afQ2R (fromIntv (v, v) noise)) (perturb (Q2R v) REAL 0) map1).
  {
    unfold perturb.
    unfold fromIntv.
    simpl.
    rewrite Qeq_bool_refl.
    simpl.
    rewrite Q2R_plus.
    rewrite Q2R_div; try lra.
    unfold af_evals; simpl.
    lra.
  }
  assert (FloverMap.find (elt:=affine_form Q) (Const m v) (FloverMap.add (Const m v) (fromIntv (v, v) noise) iexpmap) = Some (fromIntv (v, v) noise))
    by (rewrite FloverMapFacts.P.F.add_eq_o; try auto;
        apply Q_orderedExps.exprCompare_refl).
  assert (eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (Const m v)))
                    (perturb (Q2R v) REAL 0) REAL)
    by (unfold DeltaMapR; constructor; simpl; auto; rewrite Rabs_R0; lra).
  exists map1, (fromIntv (v, v) noise), (perturb (Q2R v) REAL 0), i, e.
  repeat split; auto.
  - reflexivity.
  - apply contained_flover_map_extension.
    assumption.
  - apply validAffineBounds_validRanges.
    exists map1, (fromIntv (v, v) noise), (perturb (Q2R v) REAL 0), i, e.
    repeat split; auto.
  - intros e' Hnone Hsome.
    destruct Hsome as [afS Hsome].
    destruct (FloverMapFacts.O.MO.eq_dec (Const m v) e').
    + assert (Q_orderedExps.exprCompare e' (Const m v) = Eq)
        by (now rewrite Q_orderedExps.exprCompare_eq_sym).
      rewrite FloverMapFacts.P.F.add_eq_o in Hsome; auto.
      inversion Hsome; subst; clear Hsome.
      unfold checked_expressions.
      exists (fromIntv (v, v) noise), (perturb (Q2R v) REAL 0), i, e.
      intuition.
      * rewrite usedVars_eq_compat; eauto.
        simpl.
        set_tac.
      * erewrite FloverMapFacts.P.F.find_o; eauto.
      * rewrite FloverMapFacts.P.F.add_eq_o; auto.
      * erewrite expr_compare_eq_eval_compat; eauto.
      * eapply validRanges_eq_compat; eauto.
        simpl; split; auto.
        apply validAffineBounds_validRanges.
        exists map1, (fromIntv (v, v) noise), (perturb (Q2R v) REAL 0), i, e.
        repeat split; auto.
    + rewrite FloverMapFacts.P.F.add_neq_o in Hsome; auto.
      congruence.
Qed.

Definition validAffineBounds_IH_e A P E Gamma fVars dVars e :=
  forall (noise : nat) (exprAfs : expressionsAffine) (inoise : nat)
    (iexpmap : FloverMap.t (affine_form Q)) (map1 : nat -> option noise_type),
    (forall e : FloverMap.key,
        (exists af : affine_form Q, FloverMap.find (elt:=affine_form Q) e iexpmap = Some af) ->
        checked_expressions A E Gamma fVars dVars e iexpmap inoise map1) ->
    (inoise > 0)%nat ->
    (forall n : nat, (n >= inoise)%nat -> map1 n = None) ->
    validAffineBounds e A P dVars iexpmap inoise = Some (exprAfs, noise) ->
    affine_dVars_range_valid dVars E A inoise iexpmap map1 ->
    NatSet.Subset (usedVars e -- dVars) fVars ->
    P_intv_sound E P ->
    validTypes e Gamma ->
    exists (map2 : noise_mapping) (af : affine_form Q) (vR : R) (aiv : intv)
      (aerr : error),
      contained_map map1 map2 /\
      contained_flover_map iexpmap exprAfs /\
      FloverMap.find (elt:=intv * error) e A = Some (aiv, aerr) /\
      isSupersetIntv (toIntv af) aiv = true /\
      FloverMap.find (elt:=affine_form Q) e exprAfs = Some af /\
      fresh noise af /\
      (forall n : nat, (n >= noise)%nat -> map2 n = None) /\
      (noise >= inoise)%nat /\
      eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) vR REAL /\
      validRanges e A E (toRTMap (toRExpMap Gamma)) /\
      af_evals (afQ2R af) vR map2 /\
      (forall e : FloverMap.key,
          FloverMap.find (elt:=affine_form Q) e iexpmap = None ->
          (exists af0 : affine_form Q, FloverMap.find (elt:=affine_form Q) e exprAfs = Some af0) ->
          checked_expressions A E Gamma fVars dVars e exprAfs noise map2).

Lemma validAffineBounds_sound_unop A P E Gamma fVars dVars u e:
  validAffineBounds_IH_e A P E Gamma fVars dVars e ->
  forall (noise : nat) (exprAfs : expressionsAffine) (inoise : nat)
    (iexpmap : FloverMap.t (affine_form Q)) (map1 : nat -> option noise_type),
  (forall e0 : FloverMap.key,
   (exists af : affine_form Q, FloverMap.find (elt:=affine_form Q) e0 iexpmap = Some af) ->
   checked_expressions A E Gamma fVars dVars e0 iexpmap inoise map1) ->
  (inoise > 0)%nat ->
  (forall n : nat, (n >= inoise)%nat -> map1 n = None) ->
  validAffineBounds (Unop u e) A P dVars iexpmap inoise = Some (exprAfs, noise) ->
  affine_dVars_range_valid dVars E A inoise iexpmap map1 ->
  NatSet.Subset (usedVars (Unop u e) -- dVars) fVars ->
  P_intv_sound E P ->
  validTypes (Unop u e) Gamma ->
  exists (map2 : noise_mapping) (af : affine_form Q) (vR : R) (aiv : intv)
  (aerr : error),
    contained_map map1 map2 /\
    contained_flover_map iexpmap exprAfs /\
    FloverMap.find (elt:=intv * error) (Unop u e) A = Some (aiv, aerr) /\
    isSupersetIntv (toIntv af) aiv = true /\
    FloverMap.find (elt:=affine_form Q) (Unop u e) exprAfs = Some af /\
    fresh noise af /\
    (forall n : nat, (n >= noise)%nat -> map2 n = None) /\
    (noise >= inoise)%nat /\
    eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (Unop u e))) vR REAL /\
    validRanges (Unop u e) A E (toRTMap (toRExpMap Gamma)) /\
    af_evals (afQ2R af) vR map2 /\
    (forall e0 : FloverMap.key,
     FloverMap.find (elt:=affine_form Q) e0 iexpmap = None ->
     (exists af0 : affine_form Q, FloverMap.find (elt:=affine_form Q) e0 exprAfs = Some af0) ->
     checked_expressions A E Gamma fVars dVars e0 exprAfs noise map2).
Proof.
  intros IHe * visitedExpr inoisegtz validmap1 validBounds dVarsValid varsDisjoint fVarsSound varsTyped;
    simpl in validBounds.
  destruct (FloverMap.find (elt:=affine_form Q) (Unop u e) iexpmap) eqn: Hvisited.
  {
    pose proof visitedExpr as visitedExpr'.
    inversion validBounds; subst; clear validBounds.
    specialize (visitedExpr (Unop u e)).
    destruct visitedExpr as [af [vR [aiv [aerr visitedExpr]]]].
    - eexists; eauto.
    - exists map1, af, vR, aiv, aerr.
      intuition.
  }
  unfold updateExpMap, updateExpMapSucc, updateExpMapIncr in validBounds.
  destruct (FloverMap.find (elt:=intv * error) (Unop u e) A) as [p |] eqn: Hares; simpl in validBounds; try congruence.
  destruct p as [aiv aerr].
  destruct (validAffineBounds e A P dVars iexpmap inoise) eqn: Hsubvalid; simpl in validBounds; try congruence.
  destruct p as [subexprAff subnoise].
  destruct (FloverMap.find (elt:=affine_form Q) e subexprAff) as [af |] eqn: He; simpl in validBounds; try congruence.
  validTypes_split.
  destruct (IHe subnoise subexprAff inoise iexpmap map1)
    as [ihmap [af' [vR [subaiv [subaerr [Hcont [Hcontf [Hsubares [Hsubsup [Haf [subfresh [Hsubvalidmap [Hsubnoise [subeval [subranges [subaff HvisitedExpr]]]]]]]]]]]]]]]];
    auto; clear IHe.
  assert (af' = af) by congruence; subst.
  destruct u.
  - destruct (isSupersetIntv (toIntv (AffineArithQ.negate_aff af)) aiv) eqn: Hsup; try congruence.
    exists ihmap, (AffineArithQ.negate_aff af), (-vR)%R, aiv, aerr.
    assert (af_evals (afQ2R (AffineArithQ.negate_aff af)) (- vR) ihmap) as ?
        by (rewrite afQ2R_negate_aff; now apply negate_aff_sound).
    inversion validBounds; subst; clear validBounds.
    rewrite plus_0_r.
    assert (fresh subnoise (AffineArithQ.negate_aff af)) by
        (unfold AffineArithQ.negate_aff; now apply AffineArithQ.mult_aff_const_fresh_compat).
    assert (eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                      (toREval (toRExp (Unop Neg e))) (- vR) REAL)
      as eval_real.
    {eapply Unop_neg'; try eauto.
      destruct varsTyped as (mt & find_t & ? & valid_exec).
      - eapply toRExpMap_some in find_t; eauto.
      - auto. }
    repeat split; auto.
    + pose proof contained_flover_map_extension as H'.
      specialize (H' _ iexpmap _ (AffineArithQ.negate_aff af) Hvisited).
      etransitivity; try eassumption.
      apply contained_flover_map_add_compat.
      assumption.
    + rewrite FloverMapFacts.P.F.add_eq_o; try auto.
      apply Q_orderedExps.exprCompare_refl.
    + apply validAffineBounds_validRanges.
      exists ihmap, (AffineArithQ.negate_aff af), (-vR)%R, aiv, aerr.
      repeat split; auto.
    + intros e' Hnone Hsome.
      destruct Hsome as [afS Hsome].
      {
        destruct (FloverMapFacts.O.MO.eq_dec (Unop Neg e) e').
        - assert (Q_orderedExps.exprCompare e' (Unop Neg e) = Eq)
            by (now rewrite Q_orderedExps.exprCompare_eq_sym).
          rewrite FloverMapFacts.P.F.add_eq_o in Hsome; auto.
          inversion Hsome; subst; clear Hsome.
          unfold checked_expressions.
          exists (AffineArithQ.negate_aff af), (-vR)%R, aiv, aerr.
          intuition.
          + rewrite usedVars_eq_compat; eauto.
            simpl in varsDisjoint |-*; auto.
            now apply subset_diff_to_subset_union.
          + erewrite FloverMapFacts.P.F.find_o; eauto.
          + rewrite FloverMapFacts.P.F.add_eq_o; auto.
          + erewrite expr_compare_eq_eval_compat; eauto.
          + eapply validRanges_eq_compat; eauto.
            simpl; split; auto.
            apply validAffineBounds_validRanges.
            exists ihmap, (AffineArithQ.negate_aff af), (-vR)%R, aiv, aerr.
            repeat split; auto.
        - rewrite FloverMapFacts.P.F.add_neq_o in Hsome; auto.
          apply checked_expressions_flover_map_add_compat; auto.
          apply HvisitedExpr; eauto.
      }
  - destruct (nozeroiv (toIntv af)) eqn: noZeroAf; try congruence.
    destruct (isSupersetIntv (toIntv (AffineArithQ.inverse_aff af subnoise)) aiv) eqn: Hsup; try congruence.
    inversion validBounds; subst; clear validBounds.
    pose proof (inverse_aff_sound) as invsound.
    assert (fresh subnoise (afQ2R af)) as H' by now apply afQ2R_fresh.
    assert (above_zero (afQ2R af) \/ below_zero (afQ2R af)) as H'' by eauto using nozero_above_below.
    specialize (invsound (afQ2R af) vR ihmap subnoise H' H'' subaff) as [qInv inveval].
    assert (fresh (subnoise + 1) (AffineArithQ.inverse_aff af subnoise))
      by now apply AffineArithQ.inverse_aff_fresh_compat.
    assert (forall n : nat, (n >= subnoise + 1)%nat -> updMap ihmap subnoise qInv n = None)
      by (intros n ?; unfold updMap; assert (n <> subnoise) as Hneq by lia;
          rewrite <- Nat.eqb_neq in Hneq; rewrite Hneq; apply Hsubvalidmap; lia).
    assert (af_evals (afQ2R (AffineArithQ.inverse_aff af subnoise)) (perturb (evalUnop Inv vR) REAL 0) (updMap ihmap subnoise qInv)).
    {
      unfold perturb.
      simpl evalUnop.
      replace (/ vR)%R with (1 / vR)%R by lra.
      now rewrite afQ2R_inverse_aff.
    }
    assert (contained_map map1 (updMap ihmap subnoise qInv))
      by (eapply contained_map_trans; try exact Hcont;
          apply contained_map_extension; apply Hsubvalidmap; lia).
    assert (contained_flover_map iexpmap (FloverMap.add (Unop Inv e) (AffineArithQ.inverse_aff af subnoise) subexprAff))
      by (pose proof contained_flover_map_extension as Hcont';
          specialize (Hcont' _ iexpmap _ (AffineArithQ.inverse_aff af subnoise) Hvisited);
          etransitivity; try eassumption; apply contained_flover_map_add_compat;
          assumption).
    assert (FloverMap.find (Unop Inv e) (FloverMap.add (Unop Inv e) (AffineArithQ.inverse_aff af subnoise) subexprAff) = Some (AffineArithQ.inverse_aff af subnoise))
      by (rewrite FloverMapFacts.P.F.add_eq_o; try auto; apply Q_orderedExps.exprCompare_refl).
    exists (updMap ihmap subnoise qInv), (AffineArithQ.inverse_aff af subnoise),
    (perturb (evalUnop Inv vR) REAL 0)%R, aiv, aerr.
    assert (eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                      (toREval (toRExp (Unop Inv e))) (/ vR) REAL)
      as eval_real.
    { eapply Unop_inv' with (delta:=0%R); try eauto.
      - rewrite Rabs_R0; cbn; lra.
      - eauto using above_below_nonzero.
      - eauto.
      - auto.
      - auto. }
    repeat split; try auto.
    + lia.
    + apply validAffineBounds_validRanges.
      exists (updMap ihmap subnoise qInv), (AffineArithQ.inverse_aff af subnoise),
      (perturb (evalUnop Inv vR) REAL 0)%R, aiv, aerr.
      repeat split; auto.
    + intros e' Hnone Hsome.
      destruct Hsome as [afS Hsome].
      {
        destruct (FloverMapFacts.O.MO.eq_dec (Unop Inv e) e').
        - assert (Q_orderedExps.exprCompare e' (Unop Inv e) = Eq)
            by (now rewrite Q_orderedExps.exprCompare_eq_sym).
          rewrite FloverMapFacts.P.F.add_eq_o in Hsome; auto.
          inversion Hsome; subst; clear Hsome.
          unfold checked_expressions.
          assert (FloverMap.find (elt:=intv * error) e' A = Some (aiv, aerr))
            by (erewrite FloverMapFacts.P.F.find_o; eauto).
          exists (AffineArithQ.inverse_aff af subnoise),
          (perturb (evalUnop Inv vR) REAL 0)%R, aiv, aerr.
          repeat split; auto.
          + rewrite usedVars_eq_compat; eauto.
            simpl.
            now apply subset_diff_to_subset_union.
          + rewrite FloverMapFacts.P.F.add_eq_o; auto.
          + erewrite expr_compare_eq_eval_compat; eauto.
          + eapply validRanges_eq_compat; eauto.
            split; auto.
            apply validAffineBounds_validRanges.
            exists (updMap ihmap subnoise qInv), (AffineArithQ.inverse_aff af subnoise),
            (perturb (evalUnop Inv vR) REAL 0)%R, aiv, aerr.
            repeat split; auto.
        - rewrite FloverMapFacts.P.F.add_neq_o in Hsome; auto.
          apply checked_expressions_flover_map_add_compat; auto.
          eapply checked_expressions_contained with (noise1 := subnoise) (map1 := ihmap)
                                                    (expmap1 := subexprAff); eauto.
          + apply contained_map_extension.
            apply Hsubvalidmap; lia.
          + reflexivity.
          + lia.
      }
Qed.

Lemma validAffineBounds_sound_binop A P E Gamma fVars dVars b e1 e2:
  validAffineBounds_IH_e A P E Gamma fVars dVars e1 ->
  validAffineBounds_IH_e A P E Gamma fVars dVars e2 ->
  forall (noise : nat) (exprAfs : expressionsAffine) (inoise : nat)
    (iexpmap : FloverMap.t (affine_form Q)) (map1 : nat -> option noise_type),
    (forall e : FloverMap.key,
        (exists af : affine_form Q, FloverMap.find (elt:=affine_form Q) e iexpmap = Some af) ->
        checked_expressions A E Gamma fVars dVars e iexpmap inoise map1) ->
    (inoise > 0)%nat ->
    (forall n : nat, (n >= inoise)%nat -> map1 n = None) ->
    validAffineBounds (Binop b e1 e2) A P dVars iexpmap inoise = Some (exprAfs, noise) ->
    affine_dVars_range_valid dVars E A inoise iexpmap map1 ->
    NatSet.Subset (usedVars (Binop b e1 e2) -- dVars) fVars ->
    P_intv_sound E P ->
    validTypes (Binop b e1 e2) Gamma ->
    exists (map2 : noise_mapping) (af : affine_form Q) (vR : R) (aiv : intv)
      (aerr : error),
      contained_map map1 map2 /\
      contained_flover_map iexpmap exprAfs /\
      FloverMap.find (elt:=intv * error) (Binop b e1 e2) A = Some (aiv, aerr) /\
      isSupersetIntv (toIntv af) aiv = true /\
      FloverMap.find (elt:=affine_form Q) (Binop b e1 e2) exprAfs = Some af /\
      fresh noise af /\
      (forall n : nat, (n >= noise)%nat -> map2 n = None) /\
      (noise >= inoise)%nat /\
      eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (Binop b e1 e2))) vR REAL /\
      validRanges (Binop b e1 e2) A E (toRTMap (toRExpMap Gamma)) /\
      af_evals (afQ2R af) vR map2 /\
      (forall e : FloverMap.key,
          FloverMap.find (elt:=affine_form Q) e iexpmap = None ->
          (exists af0 : affine_form Q, FloverMap.find (elt:=affine_form Q) e exprAfs = Some af0) ->
          checked_expressions A E Gamma fVars dVars e exprAfs noise map2).
Proof.
  intros IHe1 IHe2 * visitedExpr inoisegtz validmap1 validBounds dVarsValid varsDisjoint fVarsSound varsTyped;
    simpl in validBounds.
  destruct (FloverMap.find (elt:=affine_form Q) (Binop b e1 e2) iexpmap) eqn: Hvisited.
  {
    inversion validBounds; subst; clear validBounds.
    pose proof visitedExpr as visitedExpr'.
    specialize (visitedExpr (Binop b e1 e2)).
    destruct visitedExpr as [af [vR [aiv [aerr visitedExpr]]]].
    - eexists; eauto.
    - exists map1, af, vR, aiv, aerr.
      intuition.
  }
  unfold updateExpMap, updateExpMapSucc, updateExpMapIncr in validBounds.
  destruct (FloverMap.find (elt:=intv * error) (Binop b e1 e2) A)
    as [p |] eqn: Hares;
    simpl in validBounds; try congruence.
  destruct p as [aiv aerr].
  destruct (validAffineBounds e1 A P dVars iexpmap inoise) eqn: Hsubvalid1;
    simpl in validBounds; try congruence.
  destruct p as [subexprAff1 subnoise1].
  destruct (FloverMap.find (elt:=affine_form Q) e1 subexprAff1)
    as [af1 |] eqn: He1;
    simpl in validBounds; try congruence.
  destruct (validAffineBounds e2 A P dVars subexprAff1 subnoise1)
           eqn: Hsubvalid2;
    simpl in validBounds; try congruence.
  destruct p as [subexprAff2 subnoise2].
  destruct (FloverMap.find (elt:=affine_form Q) e2 subexprAff2)
    as [af2 |] eqn: He2;
    simpl in validBounds; try congruence.
  validTypes_split.
  destruct (IHe1 subnoise1 subexprAff1 inoise iexpmap map1)
    as (ihmap1 & af1' & vR1 & ihaiv1 & ihaerr1 & ihcont1 & ihcontf1 & ihares1 &
        ihsup1 & Haf1 & subfresh1 & Hsubvalidmap1 & Hsubnoise1 & subeval1 &
        subranges1 & subaff1 & visitedExpr1);
    try auto; clear IHe1.
  { repeat set_tac. }
  assert (af1' = af1) by congruence; subst.
  destruct (IHe2 subnoise2 subexprAff2 subnoise1 subexprAff1 ihmap1)
    as (ihmap2 & af2' & vR2 & ihaiv2 & ihaerr2 & ihcont2 & ihcontf2 & ihares2 &
        ihsup2 & Haf2 & subfresh2 & Hsubvalidmap2 & Hsubnoise2 & subeval2 &
        subranges2 & subaff2 & visitedExpr2);
    try auto; try lra; clear IHe2; eauto using fresh_n_gt_O.
  {
    clear - ihcont1 ihcontf1 Hsubnoise1 Hsubvalidmap1 visitedExpr1 visitedExpr.
    intros e' Hsome.
    specialize (visitedExpr e').
    specialize (visitedExpr1 e').
    destruct (FloverMap.find (elt:=affine_form Q) e' iexpmap) eqn:?;
             eauto using checked_expressions_contained.
  }
  {
    unfold affine_dVars_range_valid in dVarsValid |-*.
    intros v vin.
    specialize (dVarsValid v vin).
    destruct dVarsValid as [af [vR [iv [err dVarsValid]]]].
    exists af, vR, iv, err; intuition.
    - eapply fresh_monotonic; try eassumption.
    - eapply af_evals_map_extension; try eassumption.
  }
  { repeat set_tac. }
  assert (af2' = af2) by congruence; subst.
  destruct b.
  - destruct (isSupersetIntv (toIntv (AffineArithQ.plus_aff af1 af2)) aiv)
             eqn: Hsup; try congruence.
    inversion validBounds; subst; clear validBounds.
    assert (af_evals (afQ2R (AffineArithQ.plus_aff af1 af2))
                     (perturb (evalBinop Plus vR1 vR2) REAL 0) ihmap2).
    {
      unfold perturb.
      simpl evalBinop.
      rewrite afQ2R_plus_aff.
      apply plus_aff_sound; auto.
      eauto using af_evals_map_extension.
    }
    assert (eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (Binop Plus e1 e2)))
                      (perturb (evalBinop Plus vR1 vR2) REAL 0) REAL).
    { eapply Binop_dist' with (delta := 0%R); eauto; try congruence.
      - rewrite Rabs_R0; cbn; lra.
      - auto.
      - auto. }
    exists ihmap2, (AffineArithQ.plus_aff af1 af2), (perturb (evalBinop Plus vR1 vR2) REAL 0)%R, aiv, aerr.
    rewrite plus_0_r.
    repeat split; eauto using AffineArithQ.plus_aff_fresh_compat, fresh_monotonic.
    + etransitivity; try exact ihcont1.
      etransitivity; try exact ihcont2.
      reflexivity.
    + pose proof contained_flover_map_extension as H'.
      specialize (H' _ iexpmap _ (AffineArithQ.plus_aff af1 af2) Hvisited).
      etransitivity; try eassumption.
      apply contained_flover_map_add_compat.
      clear H'.
      etransitivity; try eassumption.
    + rewrite FloverMapFacts.P.F.add_eq_o; try auto.
      apply Q_orderedExps.exprCompare_refl.
    + lia.
    + congruence.
    + apply validAffineBounds_validRanges.
      exists ihmap2, (AffineArithQ.plus_aff af1 af2), (perturb (evalBinop Plus vR1 vR2) REAL 0)%R, aiv, aerr.
      repeat split; auto.
    + intros e Hnone Hsome.
      specialize (visitedExpr1 e Hnone).
      specialize (visitedExpr2 e).
      {
        destruct (FloverMapFacts.O.MO.eq_dec (Binop Plus e1 e2) e).
        - assert (Q_orderedExps.exprCompare e (Binop Plus e1 e2) = Eq)
            by (now rewrite Q_orderedExps.exprCompare_eq_sym).
          exists (AffineArithQ.plus_aff af1 af2), (perturb (evalBinop Plus vR1 vR2) REAL 0)%R, aiv, aerr.
          intuition; eauto using fresh_monotonic, af_evals_map_extension, AffineArithQ.plus_aff_fresh_compat.
          + rewrite usedVars_eq_compat; eauto; simpl.
            now apply subset_diff_to_subset_union.
          + erewrite FloverMapFacts.P.F.find_o; eauto.
          + rewrite FloverMapFacts.P.F.add_eq_o; auto.
          + erewrite expr_compare_eq_eval_compat; eauto.
          + eapply validRanges_eq_compat; eauto.
            repeat split; try congruence; auto.
            eapply validAffineBounds_validRanges.
            exists ihmap2, (AffineArithQ.plus_aff af1 af2), (perturb (evalBinop Plus vR1 vR2) REAL 0)%R, aiv, aerr.
            repeat split; auto.
        - destruct (FloverMap.find e subexprAff1).
          + destruct (FloverMap.find (Binop Plus e1 e2) subexprAff1) eqn: Hecont.
            * specialize (visitedExpr1 ltac:(eauto)).
              unfold checked_expressions in visitedExpr1.
              destruct visitedExpr1 as [af' [vR' [aiv' [aerr' visitedExpr1]]]].
              exists af', vR', aiv', aerr'.
              intuition; eauto using fresh_monotonic, af_evals_map_extension.
              rewrite FloverMapFacts.P.F.add_neq_o; try auto.
            * eapply checked_expressions_contained with (map1 := ihmap1)
                                                        (expmap1 := subexprAff1); eauto.
              pose proof contained_flover_map_extension as H'.
              specialize (H' _ subexprAff1 _ (AffineArithQ.plus_aff af1 af2) Hecont).
              etransitivity; try eassumption.
              apply contained_flover_map_add_compat.
              clear H'.
              etransitivity; try eassumption.
              reflexivity.
          + eapply checked_expressions_flover_map_add_compat; auto.
            apply visitedExpr2; eauto.
            destruct Hsome as [af' Hsome].
            exists af'.
            rewrite FloverMapFacts.P.F.add_neq_o in Hsome; try auto.
      }
  - destruct (isSupersetIntv (toIntv (AffineArithQ.subtract_aff af1 af2)) aiv) eqn: Hsup; try congruence.
    inversion validBounds; subst; clear validBounds.
    rewrite plus_0_r.
    assert (af_evals (afQ2R (AffineArithQ.subtract_aff af1 af2)) (perturb (evalBinop Sub vR1 vR2) REAL 0) ihmap2).
    {
      unfold perturb.
      simpl evalBinop.
      rewrite afQ2R_subtract_aff.
      apply subtract_aff_sound; auto.
      eauto using af_evals_map_extension.
    }
    assert (fresh subnoise2 (AffineArithQ.subtract_aff af1 af2)).
    {
      unfold AffineArithQ.subtract_aff.
      apply AffineArithQ.plus_aff_fresh_compat.
      {
        eapply fresh_monotonic.
        exact Hsubnoise2.
        assumption.
      }
      unfold AffineArithQ.negate_aff.
      now apply AffineArithQ.mult_aff_const_fresh_compat.
    }
    assert (eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                      (toREval (toRExp (Binop Sub e1 e2)))
                      (perturb (evalBinop Sub vR1 vR2) REAL 0) REAL).
    { eapply Binop_dist' with (delta := 0%R); eauto; try congruence.
      - rewrite Rabs_R0; cbn; lra.
      - auto.
      - auto. }
    exists ihmap2, (AffineArithQ.subtract_aff af1 af2),
    (perturb (evalBinop Sub vR1 vR2) REAL 0)%R, aiv, aerr.
    repeat split; auto.
    + etransitivity; try exact ihcont1.
      etransitivity; try exact ihcont2.
      reflexivity.
    + pose proof contained_flover_map_extension as H'.
      specialize (H' _ iexpmap _ (AffineArithQ.subtract_aff af1 af2) Hvisited).
      etransitivity; try eassumption.
      apply contained_flover_map_add_compat.
      clear H'.
      etransitivity; try eassumption.
    + rewrite FloverMapFacts.P.F.add_eq_o; try auto.
      apply Q_orderedExps.exprCompare_refl.
    + lia.
    + congruence.
    + eapply validAffineBounds_validRanges.
      exists ihmap2, (AffineArithQ.subtract_aff af1 af2), (perturb (evalBinop Sub vR1 vR2) REAL 0)%R, aiv, aerr.
      repeat split; auto.
    + intros e Hnone Hsome.
      specialize (visitedExpr1 e Hnone).
      specialize (visitedExpr2 e).
      {
        destruct (FloverMapFacts.O.MO.eq_dec (Binop Sub e1 e2) e).
        - assert (Q_orderedExps.exprCompare e (Binop Sub e1 e2) = Eq)
            by (now rewrite Q_orderedExps.exprCompare_eq_sym).
          exists (AffineArithQ.subtract_aff af1 af2), (perturb (evalBinop Sub vR1 vR2) REAL 0)%R, aiv, aerr.
          intuition; eauto using fresh_monotonic, af_evals_map_extension, AffineArithQ.plus_aff_fresh_compat.
          + rewrite usedVars_eq_compat; eauto; simpl.
            now apply subset_diff_to_subset_union.
          + erewrite FloverMapFacts.P.F.find_o; eauto.
          + rewrite FloverMapFacts.P.F.add_eq_o; auto.
          + erewrite expr_compare_eq_eval_compat; eauto.
          + eapply validRanges_eq_compat; eauto.
            repeat split; try congruence; auto.
            eapply validAffineBounds_validRanges.
            exists ihmap2, (AffineArithQ.subtract_aff af1 af2), (perturb (evalBinop Sub vR1 vR2) REAL 0)%R, aiv, aerr.
            repeat split; auto.
        - destruct (FloverMap.find e subexprAff1).
          + destruct (FloverMap.find (Binop Sub e1 e2) subexprAff1) eqn: Hecont.
            * specialize (visitedExpr1 ltac:(eauto)).
              unfold checked_expressions in visitedExpr1.
              destruct visitedExpr1 as [af' [vR' [aiv' [aerr' visitedExpr1]]]].
              exists af', vR', aiv', aerr'.
              intuition; eauto using fresh_monotonic, af_evals_map_extension.
              rewrite FloverMapFacts.P.F.add_neq_o; try auto.
            * eapply checked_expressions_contained with (map1 := ihmap1)
                                                        (expmap1 := subexprAff1)
                                                        (noise1 := subnoise1); eauto.
              pose proof contained_flover_map_extension as H'.
              specialize (H' _ subexprAff1 _ (AffineArithQ.subtract_aff af1 af2) Hecont).
              etransitivity; try eassumption.
              apply contained_flover_map_add_compat; auto.
          + eapply checked_expressions_flover_map_add_compat; auto.
            apply visitedExpr2; eauto.
            destruct Hsome as [af' Hsome].
            exists af'.
            rewrite FloverMapFacts.P.F.add_neq_o in Hsome; try auto.
      }
  - destruct (isSupersetIntv (toIntv (AffineArithQ.mult_aff af1 af2 subnoise2)) aiv) eqn: Hsup; try congruence.
    inversion validBounds; subst; clear validBounds.
    pose proof mult_aff_sound as multsound.
    assert (fresh subnoise2 (afQ2R af1)) as fresh1
        by (apply afQ2R_fresh; eauto using fresh_monotonic).
    assert (fresh subnoise2 (afQ2R af2)) as fresh2 by now apply afQ2R_fresh.
    assert (af_evals (afQ2R af1) vR1 ihmap2) as subaff1' by eauto using af_evals_map_extension.
    specialize (multsound (afQ2R af1) (afQ2R af2) vR1 vR2 ihmap2 subnoise2 fresh1 fresh2 subaff1' subaff2) as [qMult multsound].
    assert (contained_flover_map iexpmap (FloverMap.add (Binop Mult e1 e2) (AffineArithQ.mult_aff af1 af2 subnoise2) subexprAff2))
      by (pose proof contained_flover_map_extension as H;
          specialize (H _ iexpmap _ (AffineArithQ.mult_aff af1 af2 subnoise2) Hvisited);
          etransitivity; try eassumption;
          apply contained_flover_map_add_compat;
          clear H;
          etransitivity; try eassumption).
    assert (FloverMap.find (elt:=affine_form Q) (Binop Mult e1 e2) (FloverMap.add (Binop Mult e1 e2) (AffineArithQ.mult_aff af1 af2 subnoise2) subexprAff2) = Some (AffineArithQ.mult_aff af1 af2 subnoise2))
      by (rewrite FloverMapFacts.P.F.add_eq_o; try auto;
          apply Q_orderedExps.exprCompare_refl).
    assert (fresh (subnoise2 + 1) (AffineArithQ.mult_aff af1 af2 subnoise2)).
    {
      unfold AffineArithQ.mult_aff.
      destruct Qeq_bool.
      - rewrite orb_true_l.
        apply AffineArithQ.mult_aff_aux_fresh_compat;
          apply fresh_inc; now rewrite afQ2R_fresh.
      - destruct Qeq_bool.
        + rewrite orb_true_r.
          apply AffineArithQ.mult_aff_aux_fresh_compat;
            apply fresh_inc; now rewrite afQ2R_fresh.
        + simpl.
          apply fresh_noise; try lia.
          apply AffineArithQ.mult_aff_aux_fresh_compat;
            apply fresh_inc; now rewrite afQ2R_fresh.
    }
    assert (eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                      (toREval (toRExp (Binop Mult e1 e2)))
                      (perturb (evalBinop Mult vR1 vR2) REAL 0) REAL).
    { eapply Binop_dist' with (delta := 0%R); eauto; try congruence.
      - rewrite Rabs_R0; cbn; lra.
      - auto.
      - auto. }
    assert (af_evals (afQ2R (AffineArithQ.mult_aff af1 af2 subnoise2)) (perturb (evalBinop Mult vR1 vR2) REAL 0) (updMap ihmap2 subnoise2 qMult)) by
        (unfold perturb; simpl evalBinop; rewrite afQ2R_mult_aff; assumption).
    assert (forall n : nat, (n >= subnoise2 + 1)%nat -> updMap ihmap2 subnoise2 qMult n = None).
    {
      intros n ?.
      assert (n <> subnoise2) as Hneq by lia.
      rewrite <- Nat.eqb_neq in Hneq.
      unfold updMap.
      rewrite Hneq.
      apply Hsubvalidmap2.
      lia.
    }
    assert (contained_map map1 (updMap ihmap2 subnoise2 qMult))
      by (etransitivity; try exact ihcont1;
          etransitivity; try exact ihcont2; apply contained_map_extension; auto).
    exists (updMap ihmap2 subnoise2 qMult), (AffineArithQ.mult_aff af1 af2 subnoise2),
    (perturb (evalBinop Mult vR1 vR2) REAL 0)%R, aiv, aerr.
    repeat split; auto.
    + lia.
    + congruence.
    + apply validAffineBounds_validRanges.
      exists (updMap ihmap2 subnoise2 qMult), (AffineArithQ.mult_aff af1 af2 subnoise2),
      (perturb (evalBinop Mult vR1 vR2) REAL 0)%R, aiv, aerr.
      repeat split; auto.
    + intros e Hnone Hsome.
      specialize (visitedExpr1 e Hnone).
      specialize (visitedExpr2 e).
      {
        destruct (FloverMapFacts.O.MO.eq_dec (Binop Mult e1 e2) e).
        - assert (Q_orderedExps.exprCompare e (Binop Mult e1 e2) = Eq)
            by (now rewrite Q_orderedExps.exprCompare_eq_sym).
          exists (AffineArithQ.mult_aff af1 af2 subnoise2),
          (perturb (evalBinop Mult vR1 vR2) REAL 0)%R, aiv, aerr.
          intuition; eauto using fresh_monotonic, af_evals_map_extension.
          + rewrite usedVars_eq_compat; eauto; simpl.
            now apply subset_diff_to_subset_union.
          + erewrite FloverMapFacts.P.F.find_o; eauto.
          + rewrite FloverMapFacts.P.F.add_eq_o; auto.
          + erewrite expr_compare_eq_eval_compat; eauto.
          + eapply validRanges_eq_compat; eauto.
            repeat split; try congruence; auto.
            eapply validAffineBounds_validRanges.
            exists (updMap ihmap2 subnoise2 qMult), (AffineArithQ.mult_aff af1 af2 subnoise2),
            (perturb (evalBinop Mult vR1 vR2) REAL 0)%R, aiv, aerr.
            repeat split; auto.
        - destruct (FloverMap.find e subexprAff1).
          + destruct (FloverMap.find (Binop Mult e1 e2) subexprAff1) eqn: Hecont.
            * specialize (visitedExpr1 ltac:(eauto)).
              unfold checked_expressions in visitedExpr1.
              destruct visitedExpr1 as [af' [vR' [aiv' [aerr' visitedExpr1]]]].
              {
                exists af', vR', aiv', aerr'.
                intuition; eauto using fresh_monotonic, af_evals_map_extension.
                - rewrite FloverMapFacts.P.F.add_neq_o; try auto.
                - apply fresh_monotonic with (n := subnoise1); eauto; lia.
                - apply af_evals_map_extension with (map1 := ihmap1); auto.
                  etransitivity; try exact ihcont2.
                  apply contained_map_extension; auto.
              }
            * {
                pose proof contained_flover_map_extension as H'.
                specialize (H' _ subexprAff1 _ (AffineArithQ.mult_aff af1 af2 subnoise2) Hecont).
                eapply checked_expressions_contained with (map1 := ihmap1)
                                                          (expmap1 := subexprAff1)
                                                          (noise1 := subnoise1); eauto.
                - etransitivity; try eassumption.
                  apply contained_map_extension; auto.
                - etransitivity; try eassumption.
                  now apply contained_flover_map_add_compat.
                - lia.
              }
          + eapply checked_expressions_flover_map_add_compat; auto.
            apply checked_expressions_contained with (expmap1 := subexprAff2) (map1 := ihmap2)
                                                     (noise1 := subnoise2).
            * apply contained_map_extension; auto.
            * reflexivity.
            * lia.
            * intros n' ?.
              assert (n' <> subnoise2) as Hneq by lia.
              rewrite <- Nat.eqb_neq in Hneq.
              unfold updMap.
              rewrite Hneq.
              apply Hsubvalidmap2.
              lia.
            * apply visitedExpr2; eauto.
              destruct Hsome as [af' Hsome].
              exists af'.
              rewrite FloverMapFacts.P.F.add_neq_o in Hsome; try auto.
      }
  - destruct (FloverMap.find (elt:=intv * error) e2 A) eqn: Hfind2; simpl in validBounds; try congruence.
    destruct p as [iv2 err2].
    destruct (nozeroiv (toIntv af2) && nozeroiv iv2) eqn: Hnozero'; try congruence.
    apply andb_prop in Hnozero'.
    destruct Hnozero' as [Hnozero Hnozero'].
    destruct (isSupersetIntv (toIntv (AffineArithQ.divide_aff af1 af2 subnoise2)) aiv) eqn: Hsup; try congruence.
    inversion validBounds; subst; clear validBounds.
    pose proof divide_aff_sound as divsound.
    assert (fresh subnoise2 (afQ2R af1)) as fresh1
        by now apply afQ2R_fresh; eauto using fresh_monotonic.
    assert (fresh subnoise2 (afQ2R af2)) as fresh2 by now apply afQ2R_fresh.
    assert (af_evals (afQ2R af1) vR1 ihmap2) as subaff1' by eauto using af_evals_map_extension.
    assert (above_zero (afQ2R af2) \/ below_zero (afQ2R af2)) as nozero' by eauto using nozero_above_below.
    specialize (divsound (afQ2R af1) (afQ2R af2) vR1 vR2 subnoise2 ihmap2 fresh1 fresh2 nozero' subaff1' subaff2) as [qInv [qMult multsound]].
    assert (fresh (subnoise2 + 2) (AffineArithQ.divide_aff af1 af2 subnoise2)).
    {
      unfold AffineArithQ.divide_aff, AffineArithQ.mult_aff.
      destruct Qeq_bool.
      - rewrite orb_true_l.
        apply AffineArithQ.mult_aff_aux_fresh_compat.
        {
          apply fresh_monotonic with (n := subnoise2);
            try lia; try apply afQ2R_fresh; auto.
        }
        apply fresh_monotonic with (n := (subnoise2 + 1)%nat); try lia.
        now apply AffineArithQ.inverse_aff_fresh_compat.
      - destruct Qeq_bool.
        + rewrite orb_true_r.
          apply AffineArithQ.mult_aff_aux_fresh_compat.
          {
            apply fresh_monotonic with (n := subnoise2);
              try lia; try apply afQ2R_fresh; auto.
          }
          apply fresh_monotonic with (n := (subnoise2 + 1)%nat); try lia.
          now apply AffineArithQ.inverse_aff_fresh_compat.
        + simpl.
          apply fresh_noise; try lia.
          apply AffineArithQ.mult_aff_aux_fresh_compat.
          {
            apply fresh_monotonic with (n := subnoise2);
              try lia; try apply afQ2R_fresh; auto.
          }
          apply fresh_monotonic with (n := (subnoise2 + 1)%nat); try lia.
          now apply AffineArithQ.inverse_aff_fresh_compat.
    }
    assert (contained_flover_map iexpmap (FloverMap.add (Binop Div e1 e2) (AffineArithQ.divide_aff af1 af2 subnoise2) subexprAff2))
      by (pose proof contained_flover_map_extension as H';
          specialize (H' _ iexpmap _ (AffineArithQ.divide_aff af1 af2 subnoise2) Hvisited);
          etransitivity; try eassumption;
          apply contained_flover_map_add_compat;
          clear H';
          etransitivity; try eassumption).
    assert (forall n : nat, (n >= subnoise2 + 2)%nat -> updMap (updMap ihmap2 subnoise2 qInv) (subnoise2 + 1) qMult n = None).
    {
      intros n ?.
      assert (n <> (subnoise2 + 1)%nat) as Hneq by lia.
      assert (n <> subnoise2) as Hneq' by lia.
      rewrite <- Nat.eqb_neq in Hneq.
      rewrite <- Nat.eqb_neq in Hneq'.
      unfold updMap.
      rewrite Hneq', Hneq.
      apply Hsubvalidmap2.
      lia.
    }
    assert (eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                      (toREval (toRExp (Binop Div e1 e2)))
                      (perturb (evalBinop Div vR1 vR2) REAL 0) REAL).
    { eapply Binop_dist' with (delta := 0%R); eauto; try congruence.
      - rewrite Rabs_R0; cbn; lra.
      - intros _; eauto using above_below_nonzero.
      - auto.
      - auto. }
    assert (af_evals (afQ2R (AffineArithQ.divide_aff af1 af2 subnoise2))
                     (perturb (evalBinop Div vR1 vR2) REAL 0)
                     (updMap (updMap ihmap2 subnoise2 qInv) (subnoise2 + 1) qMult))
      by (unfold perturb; simpl evalBinop; rewrite afQ2R_divide_aff; auto).
    exists (updMap (updMap ihmap2 subnoise2 qInv) (subnoise2 + 1) qMult),
    (AffineArithQ.divide_aff af1 af2 subnoise2),
    (perturb (evalBinop Div vR1 vR2) REAL 0)%R, aiv, aerr.
    repeat split; auto.
    + etransitivity; try exact ihcont1.
      etransitivity; try exact ihcont2.
      apply contained_map_extension2; apply Hsubvalidmap2; lia.
    + rewrite FloverMapFacts.P.F.add_eq_o; try auto.
      apply Q_orderedExps.exprCompare_refl.
    + lia.
    + intros _ iv' err' H'.
      assert ((iv', err') = (ihaiv2, ihaerr2)) as Hiveq by congruence.
      inversion Hiveq; subst; clear Hiveq.
      assert (ihaiv2 = iv2) by congruence; subst.
      apply Hnozero'.
    + apply validAffineBounds_validRanges.
      exists (updMap (updMap ihmap2 subnoise2 qInv) (subnoise2 + 1) qMult),
      (AffineArithQ.divide_aff af1 af2 subnoise2), (perturb (evalBinop Div vR1 vR2) REAL 0)%R, aiv, aerr.
      repeat split; auto.
    + intros e Hnone Hsome.
      specialize (visitedExpr1 e Hnone).
      specialize (visitedExpr2 e).
      {
        destruct (FloverMapFacts.O.MO.eq_dec (Binop Div e1 e2) e).
        - assert (Q_orderedExps.exprCompare e (Binop Div e1 e2) = Eq)
            by (now rewrite Q_orderedExps.exprCompare_eq_sym).
          exists (AffineArithQ.divide_aff af1 af2 subnoise2),
          (perturb (evalBinop Div vR1 vR2) REAL 0)%R, aiv, aerr.
          repeat split; eauto using fresh_monotonic, af_evals_map_extension.
          + rewrite usedVars_eq_compat; eauto; simpl.
            now apply subset_diff_to_subset_union.
          + erewrite FloverMapFacts.P.F.find_o; eauto.
          + rewrite FloverMapFacts.P.F.add_eq_o; auto.
          + erewrite expr_compare_eq_eval_compat; eauto.
          + eapply validRanges_eq_compat; eauto.
            repeat split; auto.
            {
              intros _ iv' err' H'.
              assert ((iv', err') = (ihaiv2, ihaerr2)) as Hiveq by congruence.
              inversion Hiveq; subst; clear Hiveq.
              assert (ihaiv2 = iv2) by congruence; subst.
              apply Hnozero'.
            }
            eapply validAffineBounds_validRanges.
            exists (updMap (updMap ihmap2 subnoise2 qInv) (subnoise2 + 1) qMult),
            (AffineArithQ.divide_aff af1 af2 subnoise2), (perturb (evalBinop Div vR1 vR2) REAL 0)%R, aiv, aerr.
            repeat split; auto.
        - destruct (FloverMap.find e subexprAff1).
          + destruct (FloverMap.find (Binop Div e1 e2) subexprAff1) eqn: Hecont.
            * specialize (visitedExpr1 ltac:(eauto)).
              unfold checked_expressions in visitedExpr1.
              destruct visitedExpr1 as [af' [vR' [aiv' [aerr' visitedExpr1]]]].
              {
                exists af', vR', aiv', aerr'.
                intuition; eauto using fresh_monotonic, af_evals_map_extension.
                - rewrite FloverMapFacts.P.F.add_neq_o; try auto.
                - apply fresh_monotonic with (n := subnoise1); eauto; lia.
                - apply af_evals_map_extension with (map1 := ihmap1); auto.
                  etransitivity; try exact ihcont2.
                  apply contained_map_extension2; auto.
                  apply Hsubvalidmap2; lia.
                - rewrite FloverMapFacts.P.F.add_neq_o; try auto.
                - apply fresh_monotonic with (n := subnoise1); eauto; lia.
                - apply af_evals_map_extension with (map1 := ihmap1); auto.
                  etransitivity; try exact ihcont2.
                  apply contained_map_extension2; auto.
                  apply Hsubvalidmap2; lia.
              }
            * {
                pose proof contained_flover_map_extension as H'.
                specialize (H' _ subexprAff1 _ (AffineArithQ.divide_aff af1 af2 subnoise2) Hecont).
                eapply checked_expressions_contained with (map1 := ihmap1)
                                                          (expmap1 := subexprAff1)
                                                          (noise1 := subnoise1); eauto.
                - etransitivity; try eassumption.
                  apply contained_map_extension2; auto.
                  apply Hsubvalidmap2; lia.
                - etransitivity; try eassumption.
                  now apply contained_flover_map_add_compat.
                - lia.
              }
          + eapply checked_expressions_flover_map_add_compat; auto.
            apply checked_expressions_contained with (expmap1 := subexprAff2) (map1 := ihmap2)
                                                     (noise1 := subnoise2).
            * apply contained_map_extension2; auto.
              apply Hsubvalidmap2; lia.
            * reflexivity.
            * lia.
            * intros n' ?.
              assert (n' <> subnoise2) as Hneq by lia.
              rewrite <- Nat.eqb_neq in Hneq.
              assert (n' <> subnoise2 + 1)%nat as Hneq' by lia.
              rewrite <- Nat.eqb_neq in Hneq'.
              unfold updMap.
              rewrite Hneq.
              rewrite Hneq'.
              apply Hsubvalidmap2.
              lia.
            * apply visitedExpr2; eauto.
              destruct Hsome as [af' Hsome].
              exists af'.
              rewrite FloverMapFacts.P.F.add_neq_o in Hsome; try auto.
      }
Qed.

Lemma validAffineBounds_sound_fma A P E Gamma fVars dVars e1 e2 e3:
  validAffineBounds_IH_e A P E Gamma fVars dVars e1 ->
  validAffineBounds_IH_e A P E Gamma fVars dVars e2 ->
  validAffineBounds_IH_e A P E Gamma fVars dVars e3 ->
  forall (noise : nat) (exprAfs : expressionsAffine) (inoise : nat)
    (iexpmap : FloverMap.t (affine_form Q)) (map1 : nat -> option noise_type),
    (forall e : FloverMap.key,
        (exists af : affine_form Q, FloverMap.find (elt:=affine_form Q) e iexpmap = Some af) ->
        checked_expressions A E Gamma fVars dVars e iexpmap inoise map1) ->
    (inoise > 0)%nat ->
    (forall n : nat, (n >= inoise)%nat -> map1 n = None) ->
    validAffineBounds (Fma e1 e2 e3) A P dVars iexpmap inoise = Some (exprAfs, noise) ->
    affine_dVars_range_valid dVars E A inoise iexpmap map1 ->
    NatSet.Subset (usedVars (Fma e1 e2 e3) -- dVars) fVars ->
    P_intv_sound E P ->
    validTypes (Fma e1 e2 e3) Gamma ->
    exists (map2 : noise_mapping) (af : affine_form Q) (vR : R) (aiv : intv)
      (aerr : error),
      contained_map map1 map2 /\
      contained_flover_map iexpmap exprAfs /\
      FloverMap.find (elt:=intv * error) (Fma e1 e2 e3) A = Some (aiv, aerr) /\
      isSupersetIntv (toIntv af) aiv = true /\
      FloverMap.find (elt:=affine_form Q) (Fma e1 e2 e3) exprAfs = Some af /\
      fresh noise af /\
      (forall n : nat, (n >= noise)%nat -> map2 n = None) /\
      (noise >= inoise)%nat /\
      eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (Fma e1 e2 e3))) vR REAL /\
      validRanges (Fma e1 e2 e3) A E (toRTMap (toRExpMap Gamma)) /\
      af_evals (afQ2R af) vR map2 /\
      (forall e : FloverMap.key,
          FloverMap.find (elt:=affine_form Q) e iexpmap = None ->
          (exists af0 : affine_form Q, FloverMap.find (elt:=affine_form Q) e exprAfs = Some af0) ->
          checked_expressions A E Gamma fVars dVars e exprAfs noise map2).
Proof.
  intros IHe1 IHe2 IHe3 * visitedExpr inoisegtz validmap1 validBounds dVarsValid
                                      varsDisjoint fVarsSound varsTyped;
    simpl in validBounds.
  destruct (FloverMap.find (elt:=affine_form Q) (Fma e1 e2 e3) iexpmap) eqn: Hvisited.
  {
    inversion validBounds; subst; clear validBounds.
    destruct (visitedExpr (Fma e1 e2 e3)) as [af [vR [aiv [aerr visitedExpr']]]].
    - eexists; eauto.
    - exists map1, af, vR, aiv, aerr.
      intuition.
  }
  unfold updateExpMap, updateExpMapSucc, updateExpMapIncr in validBounds.
  destruct (FloverMap.find (elt:=intv * error) (Fma e1 e2 e3) A)
    as [p |] eqn: Hares; simpl in validBounds; try congruence.
  destruct p as [aiv aerr].
  destruct (validAffineBounds e1 A P dVars iexpmap inoise) eqn: Hsubvalid1;
    simpl in validBounds; try congruence.
  destruct p as [subexprAff1 subnoise1].
  destruct (FloverMap.find (elt:=affine_form Q) e1 subexprAff1)
    as [af1 |] eqn: He1; simpl in validBounds; try congruence.
  destruct (validAffineBounds e2 A P dVars subexprAff1 subnoise1) eqn: Hsubvalid2; simpl in validBounds; try congruence.
  destruct p as [subexprAff2 subnoise2].
  destruct (FloverMap.find (elt:=affine_form Q) e2 subexprAff2) as [af2 |] eqn: He2; simpl in validBounds; try congruence.
  destruct (validAffineBounds e3 A P dVars subexprAff2 subnoise2) eqn: Hsubvalid3; simpl in validBounds; try congruence.
  destruct p as [subexprAff3 subnoise3].
  destruct (FloverMap.find (elt:=affine_form Q) e3 subexprAff3) as [af3 |] eqn: He3; simpl in validBounds; try congruence.
  validTypes_split.
  destruct (IHe1 subnoise1 subexprAff1 inoise iexpmap map1) as [ihmap1 [af1' [vR1 [_ [_ [ihcont1 [ihcontf1 [_ [_ [Haf1 [subfresh1 [Hsubmapvalid1 [Hsubnoise1 [subeval1 [subranges1 [subaff1 visitedExpr1]]]]]]]]]]]]]]]];
    try auto; clear IHe1.
  { repeat set_tac. }
  assert (af1' = af1) by congruence; subst.
  destruct (IHe2 subnoise2 subexprAff2 subnoise1 subexprAff1 ihmap1) as [ihmap2 [af2' [vR2 [_ [_ [ihcont2 [ihcontf2 [_ [_ [Haf2 [subfresh2 [Hsubmapvalid2 [Hsubnoise2 [subeval2 [subranges2 [subaff2 visitedExpr2]]]]]]]]]]]]]]]];
    try auto; clear IHe2; try lia.
  {
    intros e' Hsome.
    specialize (visitedExpr e').
    specialize (visitedExpr1 e').
    destruct (FloverMap.find (elt:=affine_form Q) e' iexpmap) eqn: ?;
                                                                     eauto using checked_expressions_contained.
  }
  {
    unfold affine_dVars_range_valid in dVarsValid |-*.
    intros v vin.
    specialize (dVarsValid v vin).
    destruct dVarsValid as [af [vR [iv [err dVarsValid]]]].
    exists af, vR, iv, err; intuition.
    - eapply fresh_monotonic; try eassumption.
    - eapply af_evals_map_extension; try eassumption.
  }
  { repeat set_tac. }
  assert (af2' = af2) by congruence; subst.
  destruct (IHe3 subnoise3 subexprAff3 subnoise2 subexprAff2 ihmap2) as [ihmap3 [af3' [vR3 [_ [_ [ihcont3 [ihcontf3 [_ [_ [Haf3 [subfresh3 [Hsubmapvalid3 [Hsubnoise3 [subeval3 [subranges3 [subaff3 visitedExpr3]]]]]]]]]]]]]]]];
    try auto; clear IHe3; try lia.
  {
    intros e' Hsome.
    specialize (visitedExpr e').
    specialize (visitedExpr1 e').
    specialize (visitedExpr2 e').
    destruct (FloverMap.find (elt:=affine_form Q) e' iexpmap) eqn: ?; eauto using checked_expressions_contained.
    destruct (FloverMap.find (elt:=affine_form Q) e' subexprAff1) eqn: ?; eauto using checked_expressions_contained.
  }
  {
    unfold affine_dVars_range_valid in dVarsValid |-*.
    intros v vin.
    specialize (dVarsValid v vin).
    destruct dVarsValid as [af [vR [iv [err dVarsValid]]]].
    exists af, vR, iv, err; intuition.
    - eapply fresh_monotonic; try eassumption.
      eapply fresh_monotonic; try eassumption.
    - eapply af_evals_map_extension; try eassumption.
      eapply af_evals_map_extension; try eassumption.
  }
  { repeat set_tac. }
  assert (af3' = af3) by congruence; subst.
  destruct (isSupersetIntv (toIntv (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3)) aiv) eqn: Hsup; try congruence.
  inversion validBounds; subst; clear validBounds.
  pose proof mult_aff_sound as multsound.
  assert (fresh subnoise3 (afQ2R af1)) as fresh1 by now apply afQ2R_fresh;
    eauto using fresh_monotonic.
  assert (fresh subnoise3 (afQ2R af2)) as fresh2 by now apply afQ2R_fresh;
    eauto using fresh_monotonic.
  assert (fresh subnoise3 (afQ2R af3)) as fresh3 by now apply afQ2R_fresh.
  assert (af_evals (afQ2R af1) vR1 ihmap3) as subaff1' by eauto using af_evals_map_extension.
  assert (af_evals (afQ2R af2) vR2 ihmap3) as subaff2' by eauto using af_evals_map_extension.
  assert (af_evals (afQ2R af3) vR3 ihmap3) as subaff3' by eauto using af_evals_map_extension.
  specialize (multsound (afQ2R af1) (afQ2R af2) vR1 vR2 ihmap3 subnoise3 fresh1 fresh2 subaff1' subaff2') as [qMult multsound].
  assert (contained_map map1 (updMap ihmap3 subnoise3 qMult)).
  {
    etransitivity; try exact ihcont1.
    etransitivity; try exact ihcont2.
    etransitivity; try exact ihcont3.
    apply contained_map_extension.
    apply Hsubmapvalid3; lia.
  }
  assert (contained_flover_map iexpmap (FloverMap.add (Fma e1 e2 e3) (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3) subexprAff3)).
  {
    pose proof contained_flover_map_extension as H'.
    specialize (H' _ iexpmap _ (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3) Hvisited).
    etransitivity; try eassumption.
    apply contained_flover_map_add_compat.
    clear H'.
    etransitivity; try eassumption.
    etransitivity; try eassumption.
  }
  assert (FloverMap.find (elt:=affine_form Q) (Fma e1 e2 e3) (FloverMap.add (Fma e1 e2 e3) (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3) subexprAff3) = Some (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3))
    by (rewrite FloverMapFacts.P.F.add_eq_o; try auto;
        apply Q_orderedExps.exprCompare_refl).
  assert (fresh (subnoise3 + 1) (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3)).
  {
    rewrite <- afQ2R_fresh in fresh3.
    apply AffineArithQ.plus_aff_fresh_compat.
    - unfold AffineArithQ.mult_aff.
      destruct Qeq_bool.
      + rewrite orb_true_l.
        apply AffineArithQ.mult_aff_aux_fresh_compat;
          apply fresh_inc; now rewrite afQ2R_fresh.
      + destruct Qeq_bool.
        * rewrite orb_true_r.
          apply AffineArithQ.mult_aff_aux_fresh_compat;
            apply fresh_inc; now rewrite afQ2R_fresh.
        * simpl.
          apply fresh_noise; try lia.
          apply AffineArithQ.mult_aff_aux_fresh_compat;
            apply fresh_inc; now rewrite afQ2R_fresh.
    - apply fresh_monotonic with (n := subnoise3); try lia; auto.
  }
  assert (forall n : nat, (n >= subnoise3 + 1)%nat -> updMap ihmap3 subnoise3 qMult n = None).
  {
    intros n ?.
    assert (n <> subnoise3) as Hneq by lia.
    rewrite <- Nat.eqb_neq in Hneq.
    unfold updMap.
    rewrite Hneq.
    apply Hsubmapvalid3.
    lia.
  }
  assert (eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR
                    (toREval (toRExp (Fma e1 e2 e3)))
                    (perturb (evalFma vR1 vR2 vR3)REAL 0) REAL).
  { eapply Fma_dist'; eauto; try congruence.
    - rewrite Rabs_R0; cbn;  lra.
    - auto.
    - auto.
    - auto. }
  assert (af_evals (afQ2R (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3)) (perturb (evalFma vR1 vR2 vR3) REAL 0) (updMap ihmap3 subnoise3 qMult)).
  {
    unfold perturb.
    unfold evalFma.
    simpl evalBinop.
    rewrite afQ2R_plus_aff.
    rewrite afQ2R_mult_aff.
    apply plus_aff_sound; try assumption.
    eapply af_evals_map_extension; try apply contained_map_extension; auto.
  }
  exists (updMap ihmap3 subnoise3 qMult),
  (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3),
  (perturb (evalFma vR1 vR2 vR3) REAL 0)%R, aiv, aerr.
  repeat split; auto.
  + lia.
  + apply validAffineBounds_validRanges.
    exists (updMap ihmap3 subnoise3 qMult),
    (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3),
    (perturb (evalFma vR1 vR2 vR3) REAL 0)%R, aiv, aerr.
    repeat split; auto.
  + intros e Hnone Hsome.
    specialize (visitedExpr1 e Hnone).
    specialize (visitedExpr2 e).
    specialize (visitedExpr3 e).
    {
      destruct (FloverMapFacts.O.MO.eq_dec (Fma e1 e2 e3) e).
      - assert (Q_orderedExps.exprCompare e (Fma e1 e2 e3) = Eq)
          by (now rewrite Q_orderedExps.exprCompare_eq_sym).
        exists (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3),
        (perturb (evalFma vR1 vR2 vR3) REAL 0)%R, aiv, aerr.
        repeat split; eauto using fresh_monotonic, af_evals_map_extension.
        + rewrite usedVars_eq_compat; eauto; simpl.
          now apply subset_diff_to_subset_union.
        + erewrite FloverMapFacts.P.F.find_o; eauto.
        + rewrite FloverMapFacts.P.F.add_eq_o; auto.
        + erewrite expr_compare_eq_eval_compat; eauto.
        + eapply validRanges_eq_compat; eauto.
          repeat split; auto.
          eapply validAffineBounds_validRanges.
          exists (updMap ihmap3 subnoise3 qMult),
          (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3),
          (perturb (evalFma vR1 vR2 vR3) REAL 0)%R, aiv, aerr.
          repeat split; auto.
      - destruct (FloverMap.find e subexprAff1).
        + destruct (FloverMap.find (Fma e1 e2 e3) subexprAff1) eqn: Hecont.
          * specialize (visitedExpr1 ltac:(eauto)).
            unfold checked_expressions in visitedExpr1.
            destruct visitedExpr1 as [af' [vR' [aiv' [aerr' visitedExpr1]]]].
            {
              exists af', vR', aiv', aerr'.
              intuition; eauto using fresh_monotonic, af_evals_map_extension.
              - rewrite FloverMapFacts.P.F.add_neq_o; try auto.
              - apply fresh_monotonic with (n := subnoise1); eauto; lia.
              - apply af_evals_map_extension with (map1 := ihmap1); auto.
                etransitivity; try exact ihcont2.
                etransitivity; try exact ihcont3.
                apply contained_map_extension; auto.
            }
          * {
              pose proof contained_flover_map_extension as H'.
              specialize (H' _ subexprAff1 _ (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3) Hecont).
              eapply checked_expressions_contained with (map1 := ihmap1)
                                                        (expmap1 := subexprAff1)
                                                        (noise1 := subnoise1); eauto.
              - etransitivity; try eassumption.
                etransitivity; try eassumption.
                apply contained_map_extension; auto.
              - etransitivity; try eassumption.
                apply contained_flover_map_add_compat.
                etransitivity; try exact ihcontf2; eauto.
              - lia.
            }
        + destruct (FloverMap.find e subexprAff2).
          * {
              destruct (FloverMap.find (Fma e1 e2 e3) subexprAff2) eqn: Hecont.
              - specialize (visitedExpr2 ltac:(eauto) ltac:(eauto)).
                unfold checked_expressions in visitedExpr2.
                destruct visitedExpr2 as [af' [vR' [aiv' [aerr' visitedExpr2]]]].
                exists af', vR', aiv', aerr'.
                intuition; eauto using fresh_monotonic, af_evals_map_extension.
                + rewrite FloverMapFacts.P.F.add_neq_o; try auto.
                + apply fresh_monotonic with (n := subnoise2); eauto; lia.
                + apply af_evals_map_extension with (map1 := ihmap2); auto.
                  etransitivity; try exact ihcont3.
                  apply contained_map_extension; auto.
              - pose proof contained_flover_map_extension as H'.
                specialize (H' _ subexprAff2 _ (AffineArithQ.plus_aff (AffineArithQ.mult_aff af1 af2 subnoise3) af3) Hecont).
                eapply checked_expressions_contained with (map1 := ihmap2)
                                                          (expmap1 := subexprAff2)
                                                          (noise1 := subnoise2); eauto.
                + etransitivity; try eassumption.
                  apply contained_map_extension; auto.
                + etransitivity; try eassumption.
                  now apply contained_flover_map_add_compat.
                + lia.
            }
          * {
              eapply checked_expressions_flover_map_add_compat; auto.
              apply checked_expressions_contained with (expmap1 := subexprAff3) (map1 := ihmap3)
                                                       (noise1 := subnoise3); auto.
              - apply contained_map_extension; auto.
              - reflexivity.
              - lia.
              - apply visitedExpr3; eauto.
                destruct Hsome as [af' Hsome].
                exists af'.
                rewrite FloverMapFacts.P.F.add_neq_o in Hsome; try auto.
            }
    }
Qed.

Lemma validAffineBounds_sound_downcast A P E Gamma fVars dVars m e:
  validAffineBounds_IH_e A P E Gamma fVars dVars e ->
  forall (noise : nat) (exprAfs : expressionsAffine) (inoise : nat)
    (iexpmap : FloverMap.t (affine_form Q)) (map1 : nat -> option noise_type),
    (forall e0 : FloverMap.key,
        (exists af : affine_form Q, FloverMap.find (elt:=affine_form Q) e0 iexpmap = Some af) ->
        checked_expressions A E Gamma fVars dVars e0 iexpmap inoise map1) ->
    (inoise > 0)%nat ->
    (forall n : nat, (n >= inoise)%nat -> map1 n = None) ->
    validAffineBounds (Downcast m e) A P dVars iexpmap inoise = Some (exprAfs, noise) ->
    affine_dVars_range_valid dVars E A inoise iexpmap map1 ->
    NatSet.Subset (usedVars (Downcast m e) -- dVars) fVars ->
    P_intv_sound E P ->
    validTypes (Downcast m e) Gamma ->
    exists (map2 : noise_mapping) (af : affine_form Q) (vR : R) (aiv : intv)
      (aerr : error),
      contained_map map1 map2 /\
      contained_flover_map iexpmap exprAfs /\
      FloverMap.find (elt:=intv * error) (Downcast m e) A = Some (aiv, aerr) /\
      isSupersetIntv (toIntv af) aiv = true /\
      FloverMap.find (elt:=affine_form Q) (Downcast m e) exprAfs = Some af /\
      fresh noise af /\
      (forall n : nat, (n >= noise)%nat -> map2 n = None) /\
      (noise >= inoise)%nat /\
      eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp (Downcast m e))) vR REAL /\
      validRanges (Downcast m e) A E (toRTMap (toRExpMap Gamma)) /\
      af_evals (afQ2R af) vR map2 /\
      (forall e0 : FloverMap.key,
          FloverMap.find (elt:=affine_form Q) e0 iexpmap = None ->
          (exists af0 : affine_form Q, FloverMap.find (elt:=affine_form Q) e0 exprAfs = Some af0) ->
          checked_expressions A E Gamma fVars dVars e0 exprAfs noise map2).
Proof.
  intros IHe * visitedExpr inoisegtz validmap1 validBounds dVarsValid varsDisjoint fVarsSound varsTyped;
    simpl in validBounds.
  destruct (FloverMap.find (elt:=affine_form Q) (Downcast m e) iexpmap) eqn: Hvisited.
  {
    inversion validBounds; subst; clear validBounds.
    destruct (visitedExpr (Downcast m e)) as [af [vR [aiv [aerr visitedExpr']]]].
    - eexists; eauto.
    - exists map1, af, vR, aiv, aerr.
      intuition.
  }
  unfold updateExpMap, updateExpMapSucc, updateExpMapIncr in validBounds.
  destruct (FloverMap.find (elt:=intv * error) (Downcast m e) A) as [p |] eqn: Hares; simpl in validBounds; try congruence.
  destruct p as [aiv aerr].
  destruct (validAffineBounds e A P dVars iexpmap inoise) eqn: Hsubvalid; simpl in validBounds; try congruence.
  destruct p as [subexprAff subnoise].
  destruct (FloverMap.find e A) as [p |] eqn: Hasub; simpl in validBounds; try congruence.
  destruct p as [asubiv asuberr].
  destruct (FloverMap.find (elt:=affine_form Q) e subexprAff) as [af |] eqn: He; simpl in validBounds; try congruence.
  validTypes_split.
  destruct (IHe subnoise subexprAff inoise iexpmap map1)
    as [ihmap [af' [vR [subaiv [subaerr [ihcont [ihcontf [Hsubares [Hsubsup [Haf [subfresh [Hsubmapvalid [Hsubnoise [subeval [subranges [subaff visitedSubexpr]]]]]]]]]]]]]]]];
    auto; clear IHe.
  assert (asubiv = subaiv) by congruence; subst.
  assert (asuberr = subaerr) by congruence; subst; clear Hsubares.
  assert (af' = af) by congruence; subst.
  destruct (isSupersetIntv aiv subaiv && isSupersetIntv subaiv aiv) eqn: Hsup; try congruence.
  inversion validBounds; subst; clear validBounds.
  apply andb_prop in Hsup as [Hsub__l Hsub__r].
  assert (isSupersetIntv (toIntv af) aiv = true).
  {
    unfold isSupersetIntv in *.
    pose proof (supIntvAntisym _ _ Hsub__l Hsub__r) as Heq.
    unfold isEqIntv in Heq.
    destruct Heq as [Heq1 Heq2].
    apply andb_prop in Hsubsup as [Hsubsup1 Hsubsup2].
    rewrite Qle_bool_iff in Hsubsup1, Hsubsup2.
    rewrite andb_true_intro; auto.
    split; rewrite Qle_bool_iff; lra.
  }
  exists ihmap, af, (perturb vR REAL 0)%R, aiv, aerr.
  repeat split; auto.
  - pose proof contained_flover_map_extension as Hc.
    specialize (Hc _ iexpmap _ af Hvisited).
    etransitivity; try eassumption.
    apply contained_flover_map_add_compat.
    assumption.
  - rewrite FloverMapFacts.P.F.add_eq_o; try auto.
    apply Q_orderedExps.exprCompare_refl.
  - apply Downcast_dist with (m1 := REAL); try rewrite Rabs_R0; auto; simpl; lra.
  - apply validAffineBounds_validRanges.
    exists ihmap, af, (perturb vR REAL 0)%R, aiv, aerr.
    repeat split; auto.
    apply Downcast_dist with (m1 := REAL); try rewrite Rabs_R0; auto; simpl; lra.
  - intros e' Hnone Hsome.
    destruct Hsome as [afS Hsome].
    destruct (FloverMapFacts.O.MO.eq_dec (Downcast m e) e').
    + assert (Q_orderedExps.exprCompare e' (Downcast m e) = Eq)
        by (now rewrite Q_orderedExps.exprCompare_eq_sym).
      rewrite FloverMapFacts.P.F.add_eq_o in Hsome; auto.
      inversion Hsome; subst; clear Hsome.
      unfold checked_expressions.
      exists afS, (perturb vR REAL 0)%R, aiv, aerr.
      intuition.
      * rewrite usedVars_eq_compat; eauto; simpl.
        now apply subset_diff_to_subset_union.
      * erewrite FloverMapFacts.P.F.find_o; eauto.
      * rewrite FloverMapFacts.P.F.add_eq_o; auto.
      * erewrite expr_compare_eq_eval_compat; try exact H0.
        apply Downcast_dist with (m1 := REAL); try rewrite Rabs_R0; auto; simpl; lra.
      * eapply validRanges_eq_compat; eauto.
        repeat split; auto.
        eapply validAffineBounds_validRanges.
        exists ihmap, afS, (perturb vR REAL 0)%R, aiv, aerr.
        repeat split; auto.
        apply Downcast_dist with (m1 := REAL); try rewrite Rabs_R0; auto; simpl; lra.
    + rewrite FloverMapFacts.P.F.add_neq_o in Hsome; auto.
      apply checked_expressions_flover_map_add_compat; auto.
      apply visitedSubexpr; eauto.
Qed.

Lemma validAffineBounds_sound (e: expr Q) (A: analysisResult) P
      fVars dVars (E: env) Gamma exprAfs noise iexpmap inoise map1:
  (forall e, (exists af, FloverMap.find e iexpmap = Some af) ->
        checked_expressions A E Gamma fVars dVars e iexpmap inoise map1) ->
  (inoise > 0)%nat ->
  (forall n, (n >= inoise)%nat -> map1 n = None) ->
  validAffineBounds e A P dVars iexpmap inoise = Some (exprAfs, noise) ->
  affine_dVars_range_valid dVars E A inoise iexpmap map1 ->
  NatSet.Subset (NatSet.diff (Expressions.usedVars e) dVars) fVars ->
  P_intv_sound E P ->
  validTypes e Gamma ->
  exists map2 af vR aiv aerr,
    contained_map map1 map2 /\
    contained_flover_map iexpmap exprAfs /\
    FloverMap.find e A = Some (aiv, aerr) /\
    isSupersetIntv (toIntv af) aiv = true /\
    FloverMap.find e exprAfs = Some af /\
    fresh noise af /\
    (forall n, (n >= noise)%nat -> map2 n = None) /\
    (noise >= inoise)%nat /\
    eval_expr E (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) vR REAL /\
    validRanges e A E (toRTMap (toRExpMap Gamma)) /\
    af_evals (afQ2R af) vR map2 /\
    (forall e, FloverMap.find e iexpmap = None ->
          (exists af, FloverMap.find e exprAfs = Some af) ->
          checked_expressions A E Gamma fVars dVars e exprAfs noise map2).
Proof.
  revert noise exprAfs inoise iexpmap map1.
  induction e; eauto using validAffineBounds_sound_var, validAffineBounds_sound_const,
               validAffineBounds_sound_unop, validAffineBounds_sound_binop,
               validAffineBounds_sound_fma, validAffineBounds_sound_downcast.
Qed.

Fixpoint validAffineBoundsCmd (c: cmd Q) (A: analysisResult) P (validVars: NatSet.t)
           (exprsAf: expressionsAffine) (currentMaxNoise: nat): option (expressionsAffine * nat) :=
  match c with
  | Let m x e c' => match FloverMap.find e A, FloverMap.find (Var Q x) A with
                  | Some ((elo, ehi), _), Some ((xlo, xhi), _) =>
                    if Qeq_bool elo xlo && Qeq_bool ehi xhi then
                      olet res__e := validAffineBounds e A P validVars exprsAf currentMaxNoise in
                      let (exprsAf', noise') := res__e in
                      olet eaf := FloverMap.find e exprsAf' in
                      match FloverMap.find (Var Q x) exprsAf' with
                      | Some _ => None
                      | None => validAffineBoundsCmd c' A P (NatSet.add x validVars)
                                                    (FloverMap.add (Var Q x) eaf exprsAf') noise'
                      end
                    else None
                  | _, _ => None
                  end
  | Ret e => validAffineBounds e A P validVars exprsAf currentMaxNoise
  end.

Lemma validAffineBoundsCmd_sound (c: cmd Q) (A: analysisResult) P
      fVars dVars outVars (E: env) Gamma exprAfs noise iexpmap inoise map1:
  (forall e, (exists af, FloverMap.find e iexpmap = Some af) ->
        checked_expressions A E Gamma fVars dVars e iexpmap inoise map1) ->
  (inoise > 0)%nat ->
  (forall n, (n >= inoise)%nat -> map1 n = None) ->
  validAffineBoundsCmd c A P dVars iexpmap inoise = Some (exprAfs, noise) ->
  ssa c (NatSet.union fVars dVars) outVars ->
  affine_dVars_range_valid dVars E A inoise iexpmap map1 ->
  NatSet.Subset (NatSet.diff (Commands.freeVars c) dVars) fVars ->
  NatSet.Subset (preIntvVars P) fVars ->
  P_intv_sound E P ->
  validTypesCmd c Gamma ->
  exists map2 af vR aiv aerr,
    contained_map map1 map2 /\
    contained_flover_map iexpmap exprAfs /\
    FloverMap.find (getRetExp c) A = Some (aiv, aerr) /\
    isSupersetIntv (toIntv af) aiv = true /\
    FloverMap.find (getRetExp c) exprAfs = Some af /\
    fresh noise af /\
    (forall n, (n >= noise)%nat -> map2 n = None) /\
    (noise >= inoise)%nat /\
    bstep (toREvalCmd (toRCmd c)) E (toRTMap (toRExpMap Gamma)) DeltaMapR vR REAL /\
    validRangesCmd c A E (toRTMap (toRExpMap Gamma)) /\
    af_evals (afQ2R af) vR map2 /\
    (forall e, FloverMap.find e iexpmap = None ->
          (exists af, FloverMap.find e exprAfs = Some af) ->
          exists E' Gamma' dVars, checked_expressions A E' Gamma' fVars dVars e exprAfs noise map2).
Proof.
  revert E Gamma dVars iexpmap inoise exprAfs noise map1.
  induction c; intros * visitedExpr Hnoise Hmapvalid valid_bounds_cmd
                        Hssa dvars_valid Hsubset varsP_free fvars_valid vtyped; simpl in valid_bounds_cmd.
  - destruct (FloverMap.find e A) eqn: Hares__e; try congruence.
    destruct p as (aiv__e, aerr__e).
    destruct aiv__e as (elo, ehi).
    destruct (FloverMap.find (Var Q n) A) eqn: Hares__n; try congruence.
    destruct p as (aiv__n, aerr__n).
    destruct aiv__n as (xlo, xhi).
    destruct (Qeq_bool elo xlo && Qeq_bool ehi xhi) eqn: Heq; try congruence.
    destruct (validAffineBounds e A P dVars iexpmap inoise) eqn: Hvalidab;
      simpl in valid_bounds_cmd; try congruence.
    destruct p as (exprAfs', noise').
    destruct (FloverMap.find e exprAfs') eqn: Heaf; simpl in valid_bounds_cmd; try congruence.
    rename a into eaf.
    destruct (FloverMap.find (elt:=affine_form Q) (Var Q n) exprAfs') eqn: Hvar; try congruence.
    inversion Hssa; subst.
    pose proof validAffineBounds_sound as validab_sound.
    simpl in Hsubset.
    assert (NatSet.Subset (usedVars e -- dVars) fVars) as Hsubs.
    { clear varsP_free.
      set_tac. repeat split; auto.
      hnf; intros; subst.
      set_tac. }
    destruct vtyped as [(? & ? & ? & ? & ? & ?) ?].
    edestruct validab_sound as
        [map2 [af [vR [aiv [aerr [Hcont [Hcontf [Hares [Hsup [Hsubaf [Hfresh [Hvalidmap [Hnoise' [Heval [Hranges [evals visitedNewexpr]]]]]]]]]]]]]]]]; eauto; clear validab_sound.
    rewrite Hares in Hares__e.
    inversion Hares__e; subst; clear Hares__e.
    assert (noise' > 0)%nat as H' by omega.
    assert (NatSet.Equal (NatSet.add n (NatSet.union fVars dVars)) (NatSet.union fVars (NatSet.add n dVars))) as sets_eq.
    {
      hnf. intros a; split; intros in_set; set_tac.
      * destruct in_set as [ ? | [? ?]]; try auto; set_tac.
        destruct H9; auto.
      * destruct in_set as [? | ?]; try auto; set_tac.
        destruct H8 as [? | [? ?]]; auto.
    }
    assert (ssa c (fVars ∪ (NatSet.add n dVars)) outVars) as H'''.
    {
      eapply ssa_equal_set; symmetry in sets_eq.
      exact sets_eq.
      assumption.
    }
    assert (affine_dVars_range_valid (NatSet.add n dVars) (updEnv n vR E)
                                     A noise' (FloverMap.add (Var Q n) eaf exprAfs') map2) as ranges_valid.
    {
      unfold affine_dVars_range_valid.
      intros *.
      intros mem_v.
      unfold updEnv.
      case_eq (v =? n); intros v_eq.
      {
        apply andb_prop in Heq.
        destruct Heq as [eq_lo eq_hi].
        apply Qeq_bool_iff in eq_lo.
        apply Qeq_bool_iff in eq_hi.
        rewrite Nat.eqb_eq in v_eq; subst.
        exists af, vR, (xlo, xhi), aerr__n.
        intuition.
        {
          unfold isSupersetIntv in Hsup |-*.
          apply andb_true_intro.
          apply andb_prop in Hsup.
          destruct Hsup as [Hsup__l Hsup__r].
          rewrite Qle_bool_iff in Hsup__l, Hsup__r.
          simpl in Hsup__l, Hsup__r |-*.
          split; rewrite Qle_bool_iff; lra.
        }
        rewrite FloverMapFacts.P.F.add_eq_o; try auto.
        congruence.
        apply Q_orderedExps.exprCompare_refl.
      }
      {
        unfold affine_dVars_range_valid in dvars_valid.
        rewrite add_spec_strong in mem_v.
        apply beq_nat_false in v_eq.
        destruct mem_v as [? | [? indVars]]; try congruence.
        specialize (dvars_valid v indVars).
        destruct dvars_valid as [caf [cvR [civ [cerr [cHsup [cHaf [cHfresh [cHvalidmap [cHares [cHemap cHeval]]]]]]]]]].
        exists caf, cvR, civ, cerr.
        intuition.
        - rewrite FloverMapFacts.P.F.add_neq_o; try (apply Hcontf; assumption).
          simpl.
          intros Heqn.
          apply nat_compare_eq in Heqn.
          omega.
        - eapply fresh_monotonic; try exact Hnoise'.
          assumption.
        - eapply af_evals_map_extension; eassumption.
      }
    }
    assert (NatSet.Subset (freeVars c -- NatSet.add n dVars) fVars) as H4'.
    {
      hnf. intros a a_freeVar.
      rewrite NatSet.diff_spec in a_freeVar.
      destruct a_freeVar as [a_freeVar a_no_dVar].
      apply Hsubset.
      simpl.
      rewrite NatSet.diff_spec, NatSet.remove_spec, NatSet.union_spec.
      repeat split; try auto.
      { hnf; intros; subst.
        apply a_no_dVar.
        rewrite NatSet.add_spec; auto. }
      { hnf; intros a_dVar.
        apply a_no_dVar.
        rewrite NatSet.add_spec; auto. }
    }
    assert (P_intv_sound (updEnv n vR E) P) as H4'''.
    {
      intros v0 iv inP.
      unfold updEnv.
      case_eq (v0 =? n); intros case_v0; auto.
      rewrite Nat.eqb_eq in case_v0; subst.
      exfalso; set_tac. apply H6.
      apply NatSetProps.union_subset_1. apply varsP_free.
      exact (preIntvVars_sound _ _ _ inP).
    }
    edestruct IHc with (E := updEnv n vR E) (Gamma := Gamma)
                       (dVars := NatSet.add n dVars)
                       (iexpmap := FloverMap.add (Var Q n) eaf exprAfs')
      as [ihmap [ihaf [ihvR [ihaiv [ihaerr [ihcont [ihcontf [ihares [ihsup [ihsubaf [ihfresh [ihvalidmap [ihnoise [ihbstep [ihranges [ihevals ihchecked]]]]]]]]]]]]]]]]; eauto; clear IHc.
    {
      intros e' Hsome.
      specialize (visitedNewexpr e').
      destruct (FloverMap.find e' iexpmap) eqn: ?.
      - specialize (visitedExpr e' ltac:(eauto)).
        apply checked_expressions_contained with (expmap1 := iexpmap) (map1 := map1)
                                                 (noise1 := inoise); eauto.
        + pose proof contained_flover_map_extension as Hcont'.
          eapply contained_flover_map_none in Hvar; try eassumption.
          specialize (Hcont' _ iexpmap _ eaf Hvar).
          etransitivity; try eassumption.
          apply contained_flover_map_add_compat.
          clear Hcont'.
          now etransitivity; try eassumption.
        + destruct visitedExpr as [af' [vR' [aiv' [aerr' visitedExpr]]]].
          exists af', vR', aiv', aerr'.
          intuition.
          * rewrite <- sets_eq. rewrite <- H8; set_tac.
          * eapply eval_expr_ssa_extension; try eassumption.
            rewrite usedVars_toREval_toRExp_compat; try auto.
            set_tac. hnf; intros [? | ?];
                       apply H6; rewrite NatSet.union_spec; auto.
          * eapply validRanges_ssa_extension; eauto.
            now apply not_in_not_mem in H6.
      - destruct (FloverMap.find e' exprAfs') eqn: ?.
        + specialize (visitedNewexpr ltac:(eauto) ltac:(eauto)).
          unfold checked_expressions in visitedNewexpr |-*.
          destruct visitedNewexpr as [af' [vR' [aiv' [aerr' visitedNewexpr]]]].
          destruct (FloverMapFacts.O.MO.eq_dec (Var Q n) e');
            try (erewrite FloverMapFacts.P.F.find_o in Heqo0;
                 try eapply Q_orderedExps.exprCompare_eq_sym; eauto; congruence).
          exists af', vR', aiv', aerr'.
          intuition.
          * rewrite <- sets_eq; rewrite <- H8; set_tac.
          * rewrite FloverMapFacts.P.F.add_neq_o; eauto.
          * eapply eval_expr_ssa_extension; try eassumption.
            rewrite usedVars_toREval_toRExp_compat; try auto.
            set_tac. hnf; intros [? | ?];
                       apply H6; rewrite NatSet.union_spec; auto.
          * eapply validRanges_ssa_extension; eauto.
            now apply not_in_not_mem in H6.
        + destruct (FloverMapFacts.O.MO.eq_dec (Var Q n) e') as [Hvare' | Hvare'];
            try (rewrite FloverMapFacts.P.F.add_neq_o in Hsome; eauto;
                 rewrite Heqo0 in Hsome; destruct Hsome as [? Hsome]; congruence).
          assert (Q_orderedExps.exprCompare e' (Var Q n) = Eq)
            by (now rewrite Q_orderedExps.exprCompare_eq_sym).
          hnf.
          do 2 (erewrite FloverMapFacts.P.F.find_o with (x := e'); eauto).
          exists af, vR, (xlo, xhi), aerr__n.
          repeat split; auto.
          * erewrite usedVars_eq_compat; eauto; rewrite <- sets_eq; set_tac; left; set_tac.
          * apply andb_prop in Heq.
            destruct Heq as [eq_lo eq_hi].
            apply Qeq_bool_iff in eq_lo.
            apply Qeq_bool_iff in eq_hi.
            unfold isSupersetIntv in Hsup |-*.
            apply andb_true_intro.
            apply andb_prop in Hsup.
            destruct Hsup as [Hsup__l Hsup__r].
            rewrite Qle_bool_iff in Hsup__l, Hsup__r.
            simpl in Hsup__l, Hsup__r |-*.
            split; rewrite Qle_bool_iff; lra.
          * assert (af = eaf) by congruence; subst.
            rewrite FloverMapFacts.P.F.add_eq_o; eauto.
            apply Q_orderedExps.exprCompare_refl.
          * rewrite expr_compare_eq_eval_compat with (e2 := (Var Q n)); auto.
            eapply Var_load; eauto.
            { cbn. erewrite toRExpMap_some with (e:=Var Q n); eauto. }
            { unfold updEnv; simpl; now rewrite Nat.eqb_refl. }
          * eapply validRanges_eq_compat; eauto.
            split; try auto.
            apply validAffineBounds_validRanges.
            exists map2, af, vR, (xlo, xhi), aerr__n.
            repeat split; try auto;
            apply andb_prop in Heq.
            { destruct Heq as [eq_lo eq_hi].
              apply Qeq_bool_iff in eq_lo.
              apply Qeq_bool_iff in eq_hi.
              unfold isSupersetIntv in Hsup |-*.
              apply andb_true_intro.
              apply andb_prop in Hsup.
              destruct Hsup as [Hsup__l Hsup__r].
              rewrite Qle_bool_iff in Hsup__l, Hsup__r.
              simpl in Hsup__l, Hsup__r |-*.
              split; rewrite Qle_bool_iff; lra. }
            { eapply Var_load; eauto.
              - cbn. erewrite toRExpMap_some with (e:=Var Q n); eauto.
              - unfold updEnv; simpl; now rewrite Nat.eqb_refl. }
    }
    assert (contained_map map1 ihmap) as ?
      by (etransitivity; eassumption).
    assert (contained_flover_map exprAfs' (FloverMap.add (Var Q n) eaf exprAfs')) as ?
     by (apply contained_flover_map_extension; assumption).
    assert (contained_flover_map iexpmap exprAfs) as ?
        by (etransitivity; try eassumption; etransitivity; try eassumption).
    assert (noise >= inoise)%nat by lia.
    exists ihmap, ihaf, ihvR, ihaiv, ihaerr.
    repeat split; try auto.
    + econstructor; eauto.
    + intros vR' ?.
      assert (vR' = vR) by (eapply meps_0_deterministic; eauto); subst.
      auto.
    + exists ihaiv, ihaerr, ihvR.
      split; auto.
      split.
      * econstructor; try eauto.
      * apply AffineArith.to_interval_containment in ihevals.
        unfold isSupersetIntv in ihsup.
        apply andb_prop in ihsup as [Hsupl Hsupr].
        apply Qle_bool_iff in Hsupl.
        apply Qle_bool_iff in Hsupr.
        apply Qle_Rle in Hsupl.
        apply Qle_Rle in Hsupr.
        rewrite <- to_interval_to_intv in ihevals.
        destruct ihevals as [Heval1 Heval2].
        split; eauto using Rle_trans.
    + intros e' Hnone Hsome.
      specialize (ihchecked e').
      specialize (visitedNewexpr e').
      destruct (FloverMapFacts.O.MO.eq_dec (Var Q n) e').
      * exists (updEnv n vR E), Gamma, (NatSet.add n dVars),
        af, vR, (xlo, xhi), aerr__n.
        assert (Q_orderedExps.exprCompare e' (Var Q n) = Eq)
              by (now rewrite Q_orderedExps.exprCompare_eq_sym).
        assert (isSupersetIntv (toIntv af) (xlo, xhi) = true).
        {
          apply andb_prop in Heq.
          destruct Heq as [eq_lo eq_hi].
          apply Qeq_bool_iff in eq_lo.
          apply Qeq_bool_iff in eq_hi.
          unfold isSupersetIntv in Hsup |-*.
          apply andb_true_intro.
          apply andb_prop in Hsup.
          destruct Hsup as [Hsup__l Hsup__r].
          rewrite Qle_bool_iff in Hsup__l, Hsup__r.
          simpl in Hsup__l, Hsup__r |-*.
          split; rewrite Qle_bool_iff; lra.
        }
        do 2 (erewrite FloverMapFacts.P.F.find_o with (x := e'); eauto).
        {
          repeat split; auto.
          - rewrite usedVars_eq_compat with (e2 := Var Q n); eauto.
            rewrite <- sets_eq.
            simpl.
            set_tac.
          - assert (af = eaf) by congruence; subst.
            apply ihcontf.
            rewrite FloverMapFacts.P.F.add_eq_o; eauto.
            apply Q_orderedExps.exprCompare_refl.
          - apply fresh_monotonic with (n := noise'); try lia; auto.
          - rewrite expr_compare_eq_eval_compat with (e2 := (Var Q n)); auto.
            eapply Var_load; eauto.
              { cbn. erewrite toRExpMap_some with (e:=Var Q n); eauto. }
              { unfold updEnv; simpl; now rewrite Nat.eqb_refl. }
          - eapply validRanges_eq_compat; eauto.
            split; auto.
            apply validAffineBounds_validRanges.
            exists map2, af, vR, (xlo, xhi), aerr__n.
            intuition.
            eapply Var_load; eauto.
              { cbn. erewrite toRExpMap_some with (e:=Var Q n); eauto. }
              { unfold updEnv; simpl; now rewrite Nat.eqb_refl. }
          - eapply af_evals_map_extension with (map1 := map2); eauto.
        }
      * rewrite FloverMapFacts.P.F.add_neq_o in ihchecked; eauto.
        {
          destruct (FloverMap.find (elt:=affine_form Q) e' exprAfs').
          - specialize (visitedNewexpr Hnone ltac:(eauto)).
            exists E, Gamma, dVars.
            apply checked_expressions_contained with (expmap1 := exprAfs') (noise1 := noise')
                                                     (map1 := map2); eauto.
            etransitivity; eauto.
          - specialize (ihchecked ltac:(eauto) Hsome).
            eapply ihchecked.
        }
  - simpl.
    eapply validAffineBounds_sound in valid_bounds_cmd; auto.
    destruct valid_bounds_cmd as [map2 [af [vR [aiv [aerr valid_bounds_cmd]]]]].
    exists map2, af, vR, aiv, aerr.
    intuition; eauto.
    + constructor.
      eauto.
    + exists aiv, aerr, vR.
      split; auto.
      split.
      * constructor; eauto.
      * apply AffineArith.to_interval_containment in H9.
        unfold isSupersetIntv in H2.
        apply andb_prop in H2 as [Hsupl Hsupr].
        apply Qle_bool_iff in Hsupl.
        apply Qle_bool_iff in Hsupr.
        apply Qle_Rle in Hsupl.
        apply Qle_Rle in Hsupr.
        rewrite <- to_interval_to_intv in H9.
        destruct H9 as [Heval1 Heval2].
        split; eauto using Rle_trans.
    + destruct vtyped; auto.
Qed.
*)