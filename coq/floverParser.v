Require Import Coq.Strings.Ascii Coq.Strings.String Coq.Lists.List
        Coq.Reals.Reals Coq.Bool.Bool Coq.QArith.QArith.

Require Import CertificateChecker Infra.MachineType.

Import Coq.Lists.List.ListNotations.

(*
Inductive Token:Type :=
| DLET
| DRET
| DPRE
| DABS
| DCOND
| DGAMMA
| DTYPE:mType -> Token
| DFIXED
| DVAR
| DCONST: N -> Token
| DNEG
| DPLUS
| DSUB
| DMUL
| DDIV
| DFRAC
| DCAST.

Open Scope string_scope.

Definition getChar (input:string):=
  match input with
  |String c s => c
  | _ => ascii_of_N 0
  end.

Definition getConst (c:ascii) :=
  ((N_of_ascii c) - 48)%N.

Definition suffix (s:string) :=
match s with
|String c s' => s'
| EmptyString => EmptyString
end.

Definition isDigit (c:ascii) :=
  (48 <=? (N_of_ascii c))%N && (N_of_ascii c <=? 57)%N.

Definition isAlpha (c:ascii) :bool :=
  (65 <=? N_of_ascii c)%N && (N_of_ascii c <=? 90)%N ||
    (97 <=? N_of_ascii c)%N && (N_of_ascii c <=? 122)%N.

Definition isAlphaNum (c :ascii) :bool :=
  isDigit c || isAlpha c.

Fixpoint lexConst (input:string) (akk:N) :=
  match input with
  |String c input' =>
   if (isDigit c)
   then lexConst input' (akk * 10 + getConst c)
   else (akk, input)
  |EmptyString => (akk, input)
  end.

Fixpoint lexName (input:string) :=
    match input with
     | String char input' =>
       if (isAlphaNum char)
       then
         let (name, input') := lexName input' in
         (String char name, input')
       else ("", input)
     | "" => ("",input)
     end.

Fixpoint strSize str :nat :=
    match str with
    | String _ str' => 1 + strSize str'
    | "" => 0
    end.

Fixpoint lex input fuel :=
match fuel with
  |S fuel' =>
   match input with
       | String char input' =>
         if isDigit char
         then
           let (num, input'') := lexConst input 0 in
           DCONST num :: lex input'' fuel'
         else
           if isAlpha char
           then
             let (name, input'') := lexName input in
             match name with
                 | "Ret" => DRET :: lex input'' fuel'
                 | "Let" => DLET :: lex input'' fuel'
                 | "PRE" => DPRE :: lex input'' fuel'
                 | "ABS" => DABS :: lex input'' fuel'
                 | "GAMMA" => DGAMMA :: lex input'' fuel'
                 | "Var" => DVAR :: lex input'' fuel'
                 | "Cast" => DCAST :: lex input'' fuel'
                 | "F" => DFIXED :: lex input'' fuel'
                 | "MTYPE" => let ts := lex input'' fuel' in
                              (match ts with
                               |DCONST 16 :: ts' => DTYPE M16 :: ts'
                               |DCONST 32 :: ts' => DTYPE M32 :: ts'
                               |DCONST 64 :: ts' => DTYPE M64 :: ts'
                               |DFIXED :: DCONST w :: DCONST f :: ts' =>
                                DTYPE (F ((N.succ_pos w)-1) f) :: ts'
                            (* | DCONST 128 :: ts' => DTYPE M128 :: ts' *)
                            (* | DCONST 256 :: ts' => DTYPE M256 :: ts' *)
                               | _ => []
                               end)
                 | _ => []
             end
           else
             match char with
                  | "+"%char => DPLUS :: lex input' fuel'
                  | "-"%char => DSUB  :: lex input' fuel'
                  | "*"%char => DMUL :: lex input' fuel'
                  | "/"%char => DDIV :: lex input' fuel'
                  | "035"%char => DFRAC :: lex input' fuel'
                  | "?"%char => DCOND :: lex input' fuel'
                  | "~"%char => DNEG :: lex input' fuel'
                  | " "%char => lex input' fuel'
                  | "010"%char => lex input' fuel'
                  | _ => []
              end
       |  _  => []
   end
  |_ => []
end.

Fixpoint str_join s1 s2 :=
  match s1 with
  | String c s1' => String c (str_join s1' s2)
  | "" => s2
  end.

Fixpoint str_of_num (n:N) (m:nat):=
  match m with
    |S m' =>
     if (n <? 10)%N then String (ascii_of_N (n + 48)%N) ""
     else str_join (str_of_num (n/10) m') (String (ascii_of_N ((n mod 10) + 48)) "")
    |_ => ""
  end .

Fixpoint type_to_string (m:mType) :=
match m with
|M32 => "MTYPE 32"
|M64 => "MTYPE 64"
| _ => "" (* FIXME *)
end.

Definition pp_token (token:Token) :=
  match token with
  | DLET => "Let"
  | DRET => "Ret"
  | DPRE => "PRE"
  | DABS => "ABS"
  | DCOND => "?"
  | DVAR => "Var"
  | DCONST num => str_of_num num (N.to_nat num)
  | DGAMMA => "Gamma"
  | DTYPE m => str_join "MTYPE " (type_to_string m)
  | DFIXED => ""
  | DNEG => "~"
  | DPLUS => "+"
  | DSUB => "-"
  | DMUL => "*"
  | DDIV => "/"
  | DFRAC => "#"
  | DCAST => "Cast"
  end .

(* Pretty Printer for Tokens *)
Fixpoint pp (tokList:list Token) :=
  match tokList with
  | token :: tokRest => str_join (pp_token token) ( str_join " " (pp tokRest))
  | [] => ""
  end.

(** Prefix form parser for exprressions **)
Fixpoint parseExp (tokList:list Token) (fuel:nat):option (expr Q * list Token) :=
  match fuel with
    |S fuel' =>
     match tokList with
     | DCONST n :: DFRAC :: DCONST m :: DTYPE t :: tokRest =>
       match m with
       | 0%N => None
       | Npos p => Some (Const t (Z.of_N n # p), tokRest)
       end
     | DVAR :: DCONST n :: tokRest => Some (Var Q (N.to_nat n), tokRest)
     | DNEG :: tokRest =>
       match (parseExp tokRest fuel') with
       | Some (Const m c, tokRest) => Some (Const m (- c), tokRest)
       | Some (e1,tokRest') => Some (Unop Neg e1, tokRest')
       | None => None
       end
     | DPLUS :: tokRest =>
       match parseExp tokRest fuel' with
       | Some (e1,tokRest') =>
         match (parseExp tokRest' fuel') with
         | Some (e2, tokRest'') => Some (Binop Plus e1 e2, tokRest'')
         | None => None
         end
       | None => None
       end
     | DSUB :: tokRest =>
       match parseExp tokRest fuel' with
       | Some (e1,tokRest') =>
         match (parseExp tokRest' fuel') with
         | Some (e2, tokRest'') => Some (Binop Sub e1 e2, tokRest'')
         | None => None
         end
       | None => None
       end
     | DMUL :: tokRest =>
       match parseExp tokRest fuel' with
       | Some (e1,tokRest') =>
         match (parseExp tokRest' fuel') with
         | Some (e2, tokRest'') => Some (Binop Mult e1 e2, tokRest'')
         | None => None
         end
       | None => None
       end
     | DDIV :: tokRest =>
       match parseExp tokRest fuel' with
       | Some (e1,tokRest') =>
         match (parseExp tokRest' fuel') with
         | Some (e2, tokRest'') => Some (Binop Div e1 e2, tokRest'')
         | None => None
         end
       | None => None
       end
     | DCAST :: DTYPE m :: tokRest =>
       match parseExp tokRest fuel' with
       |Some (e1, tokRest') =>
        Some (Downcast  m e1, tokRest')
       |_ => None
       end
     | _ => None
     end
    |_ => None
  end.

Definition parseRet input fuel :option (cmd Q * list Token):=
    match parseExp input fuel with
     | Some (e, tokRest) => Some (Ret e, tokRest)
     | None => None
    end .

Fixpoint parseLet input fuel:option (cmd Q * list Token) :=
  match fuel with
    |S fuel' =>
     match input with
     (* We already have a valid let binding *)
     | DVAR ::DCONST n :: DTYPE m :: exprLetRest =>
       (* so we parse an exprression *)
       match parseExp exprLetRest fuel with
       | Some (e, letRest) =>
         match letRest with
             (* If we continue with a let *)
             | DLET :: letBodyRest =>
               (* Parse it *)
               match (parseLet letBodyRest fuel') with
               (* And construct a result from it *)
               | Some (letCmd, arbRest) => Some (Let m (N.to_nat n) e letCmd, arbRest)
               | _ => None
               end
             (* But if we have a return *)
             | DRET :: retBodyRest =>
               (* parse only this *)
               match (parseRet retBodyRest fuel) with
               (* and construct the result *)
               | Some (retCmd, arbRest) => Some (Let m (N.to_nat n) e retCmd, arbRest)
               | _ => None
               end
             | _ => None (* fail if there is no continuation for the let *)
       end
     | None => None (* fail if we do not have an exprression to bind *)
     end
    | _ => None (* fail if we cannot find a variable *)
  end
| _ => None
end.

Definition parseFrac (input:list Token) :option (Q * list Token) :=
  match input with
  | DNEG :: DCONST n :: DFRAC :: DCONST m :: tokRest =>
    match m with
    |0%N => None
    |Npos p => Some ((- Z.of_N n # p),tokRest)
    end
  | DCONST n :: DFRAC :: DCONST m :: tokRest =>
    match m with
    |0%N => None
    |Npos p => Some ((Z.of_N n # p),tokRest)
    end
  | _ => None
  end.

Definition parseIV (input:list Token) :option (intv * list Token) :=
  match (parseFrac input) with
  |Some (iv_lo, tokRest) =>
   match (parseFrac tokRest) with
   | Some (iv_hi, tokList) => Some ((iv_lo, iv_hi), tokList)
   | _ => None
   end
  | _ => None
  end.

Definition defaultPreIntv : precondIntv := FloverMap.empty intv.

Definition updPre (n:nat) (iv:intv) (P:precondIntv) : precondIntv :=
  FloverMap.add (Var Q n) iv P.

(** Precondition parser:
  The precondition should be encoded in the following format:
  PRECOND ::= DCOND DVAR DCONST FRACTION FRACTION PRECOND | EPSILON
  The beginning of the precondition is marked by the DPRE token
**)
Fixpoint parsePrecondRec (input:list Token) (fuel:nat) :option (precondIntv * list Token) :=
  match fuel with
    |S fuel' =>
     match input with
     | DCOND :: DVAR :: DCONST n :: fracRest =>
       match parseIV fracRest with
       | Some (iv, precondRest) =>
         match parsePrecondRec precondRest fuel' with
         | Some (P, tokRest) => Some (updPre (N.to_nat n) iv P, tokRest)
         | None => Some (updPre (N.to_nat n) iv defaultPreIntv, precondRest)
         end
       | _ => None
       end
     | _ => None
     end
    |_ => None
  end.

Definition parsePrecond (input :list Token) fuel : option (precond * list Token) :=
  match input with
  | DPRE :: tokRest =>
    match parsePrecondRec tokRest fuel with
      (* TODO parse addition constraints *)
    | Some (Piv, rest) => Some ((Piv, TrueQ), rest)
    | _ => None
    end
  | _ => None
  end.

Definition defaultAbsenv:analysisResult := FloverMap.empty (intv * error).

Definition updAbsenv (e:expr Q) (iv:intv) (err:Q) (A:analysisResult):=
  FloverMap.add e  (iv, err) A.

(** Abstract environment parser:
  The abstract environment should be encoded in the following format:
  ABSENV ::= ? EXPRESSION FRACTION FRACTION FRACTION ABSENV | EPSILON
  The beginning of the abstract environment is marked by the DABS token
**)
Fixpoint parseAbsEnvRec (input:list Token) fuel :option (analysisResult * list Token):=
  match fuel with
    |S fuel' =>
     match input with
     | DCOND :: exprRest =>
       match parseExp exprRest fuel with
       | Some (e,fracRest) =>
         match parseIV fracRest with
         | Some (iv, errRest) =>
           match parseFrac errRest with
           | Some (err, absenvRest) =>
             match parseAbsEnvRec absenvRest fuel' with
             | Some (A, tokRest) => Some (updAbsenv e iv err A, tokRest)
             | None => Some (updAbsenv e iv err defaultAbsenv, absenvRest)
             end
           | None => None
           end
         | _ => None
         end
       | None => None
       end
     | _ => Some (defaultAbsenv, input)
     end
    |_ => None
  end.

Definition parseAbsEnv (input:list Token) fuel :=
  match input with
  | DABS :: tokRest => parseAbsEnvRec tokRest fuel
  | _ => None
  end.

Definition defaultGamma := FloverMap.empty mType.

Fixpoint parseGammaRec (input: list Token) (fuel:nat): option ((FloverMap.t mType) * list Token) :=
  match fuel with
  |0%nat => None
  |S fuel' =>
   match parseExp input fuel with
   |None => Some (defaultGamma, input)
   | Some (e, rest) =>
     match rest with
     |DTYPE m :: inputRest =>
      match parseGammaRec inputRest fuel' with
      |Some (Gamma, rest) => Some (FloverMap.add e m Gamma, rest)
      |None => None
      end
     | _ => None
     end
   end
  end.

Definition parseGamma (input:list Token) :=
  match input with
  | DGAMMA :: tokenRest => parseGammaRec tokenRest (length tokenRest)
  | _ => None
  end.

Definition defaultQuerymap := FloverMap.empty (SMTLogic * SMTLogic).

Definition updQuerymap e (ql qr: SMTLogic) Qmap :=
  FloverMap.add e (ql, qr) Qmap.

(* TODO: parse the queries properly *)
Definition parseQuerymap (input: list Token) :=
  Some (defaultQuerymap, input).

Definition dParse  (input:list Token) fuel :=
  let cmdParse :=
      match input with
      | DLET :: tokRest => parseLet tokRest fuel
      | DRET :: tokRest => parseRet tokRest fuel
      | _ => None
      end
  in
  match cmdParse with
  | None => None
  | Some (dCmd, tokRest) =>
    match parseGamma tokRest with
    | None => None
    | Some (Gamma, residual) =>
      match parsePrecond residual fuel with
      | None => None
      | Some (P, absenvRest) =>
        match parseAbsEnv absenvRest fuel with
        | None => None
        | Some (A, querymapRest) =>
          match parseQuerymap querymapRest with
          | None => None
          | Some (Qmap, residual) => Some ((dCmd, P, A, Qmap, Gamma), residual)
          end
        end
      end
    end
  end.

Fixpoint str_length s :=
  match s with
  |String c s' => S (str_length s')
  |"" => O
  end.

Fixpoint check (f:cmd Q) (P:precond) (A:analysisResult) Qmap Gamma (n:nat) :=
  match n with
  |S n' => CertificateCheckerCmd f A P Qmap Gamma && (check f P A Qmap Gamma n')
  |_ => true
  end.

Fixpoint check_all (num_fun:nat) (iters:nat) (input:list Token) fuel:=
  match num_fun with
  |S nf =>
   match (dParse input fuel) with
   |Some ((f,P,A,Qmap,Gamma), residual) =>
    if (check f P A Qmap Gamma iters)
    then
      match residual with
      |a :: b => check_all nf iters residual fuel
      |[] => "True"
      end
    else
      "False"
   |None => "Failure: Parse"
   end
  |_ => "Failure: Number of Functions in certificate\n"
  end.

Definition runChecker (input:string) :=
  let tokList := lex input (str_length input) in
  match tokList with
  | DCONST n :: DCONST m :: tokRest => check_all (N.to_nat m) (N.to_nat n) tokRest (List.length tokList * 100)
  | _ => "failure no num of functions"
  end.
*)