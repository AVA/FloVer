From Flover
     Require Import Expressions Environments ssaPrgs TypeValidator
     IntervalValidation RoundoffErrorValidator Infra.Ltacs Infra.RealRationalProps.

Fixpoint FPRangeValidator (e:expr Q) (A:analysisResult) typeMap dVars {struct e}
  : bool :=
  match FloverMap.find e typeMap, FloverMap.find e A with
  |Some m, Some (iv_e, err_e) =>
   let iv_e_float := widenIntv iv_e err_e in
   let recRes :=
       match e with
       | Binop b e1 e2 =>
         FPRangeValidator e1 A typeMap dVars &&
         FPRangeValidator e2 A typeMap dVars
       | Fma e1 e2 e3 =>
         FPRangeValidator e1 A typeMap dVars &&
         FPRangeValidator e2 A typeMap dVars &&
         FPRangeValidator e3 A typeMap dVars
       | Unop u e =>
         FPRangeValidator e A typeMap dVars
       | Downcast m e => FPRangeValidator e A typeMap dVars
       | Let _ x e1 e2 =>
         FPRangeValidator e1 A typeMap dVars &&
         FPRangeValidator e2 A typeMap (NatSet.add x dVars)
         (*
       | Cond e1 e2 e3 =>
         FPRangeValidator e1 A typeMap dVars &&
         FPRangeValidator e2 A typeMap dVars &&
         FPRangeValidator e3 A typeMap dVars
*)
       | _ =>  true
       end
   in
   match e with
   | Var _ v =>
     if NatSet.mem v dVars
     then true
     else
       if (validValue (ivhi iv_e_float) m &&
           validValue (ivlo iv_e_float) m)
       then ((normal (ivlo iv_e_float) m) || (Qeq_bool (ivlo iv_e_float) 0)) &&
             (normal (ivhi iv_e_float) m || (Qeq_bool (ivhi iv_e_float) 0)) &&
              recRes
       else
         false
   | _ => if (validValue (ivhi iv_e_float) m &&
              validValue (ivlo iv_e_float) m)
         then ((normal (ivlo iv_e_float) m) || (Qeq_bool (ivlo iv_e_float) 0)) &&
               (normal (ivhi iv_e_float) m || (Qeq_bool (ivhi iv_e_float) 0)) && recRes
         else
           false
   end
  | _, _ => false
  end.

(*
Fixpoint FPRangeValidatorCmd (f:cmd Q) (A:analysisResult) typeMap dVars :=
  match f with
  | Let m n e g =>
    if FPRangeValidator e A typeMap dVars
    then FPRangeValidatorCmd g A typeMap (NatSet.add n dVars)
     else false
| Ret e => FPRangeValidator e A typeMap dVars
  end.
*)

Ltac prove_fprangeval m v :=
  destruct m eqn:?; try auto;
  unfold normal, Normal, validValue, Denormal in *; canonize_hyps;
  repeat (match goal with
          |H: _ && _ = true |- _ => andb_to_prop H
          |H: _ || _ = true |- _ => rewrite orb_true_iff in H; destruct H
          end);
  canonize_hyps;
  try rewrite <- Rabs_eq_Qabs in *;
  Q2R_to_head;
  rewrite <- Q2R_minus, <- Q2R_plus in *;
  destruct (Req_dec v 0); try auto ; (* v = 0 by auto *)
  destruct (Rlt_le_dec (Rabs v) (Q2R (minValue_pos m)))%R (* denormal case *);
  try (subst; auto; fail);
  destruct (Rle_lt_dec (Rabs v) (Q2R (maxValue m)))%R;
  try (subst; auto; fail);
  lra.

(**
   Global correctness predicate for FPRangeValidator.
   Given expression e and its evaluation, the result is a valid value for
   its assigned type
**)
Fixpoint validFPRanges (e:expr Q) (E2:env) Gamma DeltaMap :Prop :=
  (match e with
  | Var _ x => True
  | Const m v => True
  | Unop uop e1 => validFPRanges e1 E2 Gamma DeltaMap
  | Binop bop e1 e2 => validFPRanges e1 E2 Gamma DeltaMap /\
                      validFPRanges e2 E2 Gamma DeltaMap
  | Fma e1 e2 e3 => validFPRanges e1 E2 Gamma DeltaMap /\
                   validFPRanges e2 E2 Gamma DeltaMap /\
                   validFPRanges e3 E2 Gamma DeltaMap
  | Downcast m e => validFPRanges e E2 Gamma DeltaMap
  | Let m x e1 e2=> validFPRanges e1 E2 Gamma DeltaMap /\
                  (forall v m,
                      eval_expr E2 (toRExpMap Gamma) DeltaMap (toRExp e1) v m ->
                      validFPRanges e2 (updEnv x v E2) Gamma DeltaMap)
   end) /\
  forall v m,
  eval_expr E2 (toRExpMap Gamma) DeltaMap (toRExp e) v m ->
  validFloatValue v m.

Lemma validFPRanges_single e E2 Gamma DeltaMap:
  validFPRanges e E2 Gamma DeltaMap ->
  forall v m ,
  eval_expr E2 (toRExpMap Gamma) DeltaMap (toRExp e) v m ->
  validFloatValue v m.
Proof.
  intros * valid_fp; destruct e; simpl in *; apply valid_fp.
Qed.

Theorem FPRangeValidator_sound e:
  forall E1 E2 A Gamma dVars fVars outVars DeltaMap,
  (forall (v : R) (m' : mType),
      exists d : R, DeltaMap v m' = Some d /\ (Rabs d <= mTypeToR m')%R) ->
  approxEnv E1 (toRExpMap Gamma) A fVars dVars E2 ->
  validRanges e A E1 (toRTMap (toRExpMap Gamma)) ->
  validErrorBoundsRec e E1 E2 A Gamma DeltaMap ->
  ssa e (NatSet.union fVars dVars) outVars ->
  FPRangeValidator e A Gamma dVars = true ->
  validTypes e Gamma ->
  NatSet.Subset (NatSet.diff (freeVars e) dVars) fVars ->
  (forall v, NatSet.In v dVars ->
        exists vF m,
          E2 v = Some vF /\
          FloverMap.find (Var Q v) Gamma = Some m /\
          validFloatValue vF m) ->
  validFPRanges e E2 Gamma DeltaMap.
Proof.
  induction e;
    intros * DeltaMap_valid approxEnv_E1_E2 validRanges_e validErrors_e
                            ssa_e FPRanges_checked validTypes_e
                            usedVars_in_fVars dVars_valid;
    try (split; [ cbn; auto | ]).
  - intros * eval_float.
    apply validTypes_single in validTypes_e.
    destruct validTypes_e as [ mE [ type_e valid_exec]].
    assert (m = mE) by (eapply valid_exec; eauto); subst.
    rename mE into m.
    unfold validFloatValue.
    apply validRanges_single in validRanges_e.
    destruct validRanges_e as [iv_e [err_e [vR [map_e [eval_real vR_bounded]]]]].
    destruct iv_e as  [e_lo e_hi].
    assert (Rabs (vR - v) <= Q2R (err_e))%R.
    { eapply validErrorBoundsRec_single; eauto. }
    destruct (distance_gives_iv (a:=vR) v (e:=Q2R err_e) (Q2R e_lo, Q2R e_hi))
      as [v_in_errIv];
      try auto.
    simpl in *.
    assert (Rabs v <= Rabs (Q2R e_hi + Q2R err_e) \/
            Rabs v <= Rabs (Q2R e_lo - Q2R err_e))%R
      as abs_bounded
        by (apply bounded_inAbs; auto).
    unfold FPRangeValidator in *.
    Flover_compute.
    destruct (n mem dVars) eqn:?.
    + set_tac. edestruct dVars_valid as [? [? [? [? ?]]]]; eauto.
      unfold Q_orderedExps.V in type_e.
      rewrite type_e in *. inversion H2; subst.
      inversion eval_float; subst.
      rewrite H6 in H1; inversion H1; subst.
      destruct x0; auto.
    + prove_fprangeval m v.
  - intros * eval_float.
    apply validTypes_single in validTypes_e.
    destruct validTypes_e as [ mE [ type_e valid_exec]].
    assert (m0 = mE) by (eapply valid_exec; eauto); subst.
    rename mE into m0.
    unfold validFloatValue.
    apply validRanges_single in validRanges_e.
    destruct validRanges_e as [iv_e [err_e [vR [map_e [eval_real vR_bounded]]]]].
    destruct iv_e as  [e_lo e_hi].
    assert (Rabs (vR - v0) <= Q2R (err_e))%R.
    { eapply validErrorBoundsRec_single; eauto. }
    destruct (distance_gives_iv (a:=vR) v0 (e:=Q2R err_e) (Q2R e_lo, Q2R e_hi))
      as [v_in_errIv];
      try auto.
    simpl in *.
    assert (Rabs v0 <= Rabs (Q2R e_hi + Q2R err_e) \/
            Rabs v0 <= Rabs (Q2R e_lo - Q2R err_e))%R
      as abs_bounded
        by (apply bounded_inAbs; auto).
    unfold FPRangeValidator in *.
    Flover_compute.
    prove_fprangeval m0 v0.
  - cbn in FPRanges_checked; Flover_compute; auto.
    eapply IHe; try eauto.
    destruct validRanges_e; auto.
    destruct validErrors_e; destruct u; try contradiction; auto.
    { inversion ssa_e; subst. eauto. }
    validTypes_split; auto.
  - intros * eval_float.
    apply validTypes_single in validTypes_e.
    destruct validTypes_e as [ mE [ type_e valid_exec]].
    assert (m = mE) by (eapply valid_exec; eauto); subst.
    rename mE into m.
    unfold validFloatValue.
    apply validRanges_single in validRanges_e.
    destruct validRanges_e as [iv_e [err_e [vR [map_e [eval_real vR_bounded]]]]].
    destruct iv_e as  [e_lo e_hi].
    assert (Rabs (vR - v) <= Q2R (err_e))%R.
    { eapply validErrorBoundsRec_single; eauto. }
    destruct (distance_gives_iv (a:=vR) v (e:=Q2R err_e) (Q2R e_lo, Q2R e_hi))
      as [v_in_errIv];
      try auto.
    simpl in *.
    assert (Rabs v <= Rabs (Q2R e_hi + Q2R err_e) \/
            Rabs v <= Rabs (Q2R e_lo - Q2R err_e))%R
      as abs_bounded
        by (apply bounded_inAbs; auto).
    unfold FPRangeValidator in *.
    Flover_compute.
    prove_fprangeval m v.
  - cbn in FPRanges_checked; Flover_compute.
    validTypes_split.
    assert (NatSet.Subset ((freeVars e1) -- dVars) fVars).
    { set_tac. split; set_tac. }
    assert (NatSet.Subset ((freeVars e2) -- dVars) fVars).
    { intros * ? in_set. apply usedVars_in_fVars. set_tac. split; set_tac. }
    destruct validRanges_e as [ [? [? ?]] ?].
    destruct validErrors_e as [ [? [? ?]] ?].
    inversion ssa_e; subst.
    split; [ eapply IHe1; try eauto | eapply IHe2; try eauto ].
  - intros * eval_float.
    apply validTypes_single in validTypes_e.
    destruct validTypes_e as [ mE [ type_e valid_exec]].
    assert (m = mE) by (eapply valid_exec; eauto); subst.
    rename mE into m.
    unfold validFloatValue.
    apply validRanges_single in validRanges_e.
    destruct validRanges_e as [iv_e [err_e [vR [map_e [eval_real vR_bounded]]]]].
    destruct iv_e as  [e_lo e_hi].
    assert (Rabs (vR - v) <= Q2R (err_e))%R.
    { eapply validErrorBoundsRec_single; eauto. }
    destruct (distance_gives_iv (a:=vR) v (e:=Q2R err_e) (Q2R e_lo, Q2R e_hi))
      as [v_in_errIv];
      try auto.
    simpl in *.
    assert (Rabs v <= Rabs (Q2R e_hi + Q2R err_e) \/
            Rabs v <= Rabs (Q2R e_lo - Q2R err_e))%R
      as abs_bounded
        by (apply bounded_inAbs; auto).
    unfold FPRangeValidator in *.
    Flover_compute.
    prove_fprangeval m v.
  - cbn in FPRanges_checked; Flover_compute.
    validTypes_split.
    assert (NatSet.Subset ((freeVars e1) -- dVars) fVars).
    { set_tac. split; set_tac. }
    assert (NatSet.Subset ((freeVars e2) -- dVars) fVars).
    { intros * ? in_set. apply usedVars_in_fVars. set_tac. split; set_tac. }
    assert (NatSet.Subset ((freeVars e3) -- dVars) fVars).
    { intros * ? in_set. apply usedVars_in_fVars. set_tac. split; set_tac. }
    destruct validRanges_e as [[? [? ?]] ?].
    destruct validErrors_e as [[? [? ?]] ?].
    inversion ssa_e; subst.
    repeat split;
      [ eapply IHe1; eauto | eapply IHe2; eauto | eapply IHe3; eauto].
  - intros * eval_float.
    apply validTypes_single in validTypes_e.
    destruct validTypes_e as [ mE [ type_e valid_exec]].
    assert (m = mE) by (eapply valid_exec; eauto); subst.
    rename mE into m.
    unfold validFloatValue.
    apply validRanges_single in validRanges_e.
    destruct validRanges_e as [iv_e [err_e [vR [map_e [eval_real vR_bounded]]]]].
    destruct iv_e as  [e_lo e_hi].
    assert (Rabs (vR - v) <= Q2R (err_e))%R.
    { eapply validErrorBoundsRec_single; eauto. }
    destruct (distance_gives_iv (a:=vR) v (e:=Q2R err_e) (Q2R e_lo, Q2R e_hi))
      as [v_in_errIv];
      try auto.
    simpl in *.
    assert (Rabs v <= Rabs (Q2R e_hi + Q2R err_e) \/
            Rabs v <= Rabs (Q2R e_lo - Q2R err_e))%R
      as abs_bounded
        by (apply bounded_inAbs; auto).
    unfold FPRangeValidator in *.
    Flover_compute.
    prove_fprangeval m v.
  - eapply IHe; eauto.
    destruct validRanges_e; auto.
    destruct validErrors_e; auto.
    { inversion ssa_e; subst. eauto. }
    cbn in FPRanges_checked; Flover_compute; auto.
    validTypes_split; auto.
  - intros * eval_float.
    apply validTypes_single in validTypes_e.
    destruct validTypes_e as [ mE [ type_e valid_exec]].
    assert (m0 = mE) by (eapply valid_exec; eauto); subst.
    rename mE into m0.
    unfold validFloatValue.
    apply validRanges_single in validRanges_e.
    destruct validRanges_e as [iv_e [err_e [vR [map_e [eval_real vR_bounded]]]]].
    destruct iv_e as  [e_lo e_hi].
    assert (Rabs (vR - v) <= Q2R (err_e))%R.
    { eapply validErrorBoundsRec_single; eauto. }
    destruct (distance_gives_iv (a:=vR) v (e:=Q2R err_e) (Q2R e_lo, Q2R e_hi))
      as [v_in_errIv];
      try auto.
    simpl in *.
    assert (Rabs v <= Rabs (Q2R e_hi + Q2R err_e) \/
            Rabs v <= Rabs (Q2R e_lo - Q2R err_e))%R
      as abs_bounded
        by (apply bounded_inAbs; auto).
    unfold FPRangeValidator in *.
    Flover_compute.
    prove_fprangeval m0 v.
  - simpl in FPRanges_checked. Flover_compute.
    validTypes_split.
    destruct validRanges_e as [[validRanges_e1 [validRanges_x validRanges_e2]] validRanges_top].
    destruct validRanges_top as (iv_let & err_let & vR & find_let & eval_real_let & ?).
    destruct validRanges_x as (? & ? & ? & ? & find_A_e1 & find_x & ?).
    inversion eval_real_let; subst.
    destruct validErrors_e as [[validErrors_e1 [validError_x validErrors_e2]] validErrors_top].
    specialize (validError_x _ _ _ _ find_A_e1 find_x).
    inversion ssa_e; subst.
    rename H5 into n_not_def; rename H11 into ssa_e1; rename H12 into ssa_e2;
      rename H13 into outvars_up.
    assert (validFPRanges e1 E2 Gamma DeltaMap).
    { eapply IHe1; try eauto.
      set_tac. split. set_tac. auto. }
    split; try auto.
    intros * eval_e1.
    destruct validTypes_e as [m_l [find_m [[? [? [ me [ find_e1 [ find_var [? ?]]]]]] ?]]].
    destruct (validErrors_top vR iv_let err_let)
      as [ [vF [mF eval_let_float]] errors_valid];
      eauto.
    specialize (errors_valid _ _ eval_let_float).
    eapply validRanges_single in validRanges_e1.
    destruct validRanges_e1 as [iv_e1 [err_e1 [vR1 [find_e1' [eval_real_e1 ?]]]]].
    eapply validErrorBoundsRec_single in validErrors_e1; eauto.
    destruct validErrors_e1 as [ [ vF1 [ mF1 eval_e1_fl]] err1_bounded].
    specialize (err1_bounded _ _ eval_e1_fl).
    eapply validTypes_single in valid_arg.
    destruct valid_arg as [? [find_e1_Gamma ?]].
    rewrite find_e1 in find_e1_Gamma. inversion find_e1_Gamma; subst.
    assert (mF1 = x3).
    { eapply H11; eauto. }
    assert (m1 = x3).
    { eapply H11; eauto. }
    subst.
    assert (vF1 = v).
    { eapply eval_expr_functional; eauto. }
    subst.
    eapply IHe2; try eauto.
    + instantiate (1:=fVars).
      eapply approxUpdBound; try eauto.
      * eapply toRExpMap_some with (e:=Var Q n); try eauto.
      * intuition. rewrite <- H16. auto.
    + eapply ssa_equal_set with (inVars:=NatSet.add n (NatSet.union fVars dVars)); eauto.
      split; intros; set_tac.
      * destruct H5; try auto.
        set_tac. destruct H5; try auto.
        destruct H5; auto.
      * destruct H5; try auto.
        destruct H5; try auto.
        set_tac. destruct H12; auto.
    + set_tac. split; try set_tac.
      * right; split; [ auto | ].
        hnf; intros; subst. apply H12. set_tac.
      * hnf; intros; apply H12; set_tac.
    + intros * is_dVar.
      unfold updEnv.
      destruct (v0 =? n) eqn:?.
      * exists v, x3. rewrite Nat.eqb_eq in Heqb; subst.
        repeat split; try auto.
        eapply validFPRanges_single in H1; eauto.
      * eapply dVars_valid.
        set_tac.
        destruct is_dVar as [? | [? ?]]; try auto.
        subst; simpl in *. rewrite Nat.eqb_refl in Heqb. inversion Heqb.
  - intros* eval_let_float. simpl in FPRanges_checked. Flover_compute.
    validTypes_split.
    destruct validRanges_e as [[validRanges_e1 [validRanges_x validRanges_e2]] validRanges_top].
    destruct validRanges_top as (iv_let & err_let & vR & find_let & eval_real_let & ?).
    destruct validRanges_x as (? & ? & ? & ? & find_A_e1 & find_x & ?).
    inversion eval_real_let; subst.
    destruct validErrors_e as [[validErrors_e1 [validError_x validErrors_e2]] validErrors_top].
    specialize (validError_x _ _ _ _ find_A_e1 find_x).
    inversion ssa_e; subst.
    rename H5 into n_not_def; rename H11 into ssa_e1; rename H12 into ssa_e2;
      rename H13 into outvars_up.
    assert (validFPRanges e1 E2 Gamma DeltaMap).
    { eapply IHe1; try eauto.
      set_tac. split. set_tac. auto. }
    destruct validTypes_e as [m_l [find_m [[? [? [ me [ find_e1 [ find_var [? ?]]]]]] ?]]].
    eapply validRanges_single in validRanges_e1.
    destruct validRanges_e1 as [iv_e1 [err_e1 [vR1 [find_e1' [eval_real_e1 ?]]]]].
    eapply validErrorBoundsRec_single in validErrors_e1; eauto.
    destruct validErrors_e1 as [ [ vF1 [ mF1 eval_e1_fl]] err1_bounded].
    specialize (err1_bounded _ _ eval_e1_fl).
    eapply validTypes_single in valid_arg.
    destruct valid_arg as [? [find_e1_Gamma ?]].
    rewrite find_e1 in find_e1_Gamma. inversion find_e1_Gamma; subst.
    assert (mF1 = x3).
    { eapply H11; eauto. }
    subst.
    assert (validFPRanges e2 (updEnv n vF1 E2) Gamma DeltaMap).
    { eapply IHe2; try eauto.
      - eapply approxUpdBound; try eauto.
        + eapply toRExpMap_some with (e:=Var Q n); eauto.
        + intuition. rewrite <- H16; auto.
      - eapply ssa_equal_set with (inVars:=NatSet.add n (NatSet.union fVars dVars)); eauto.
        split; intros; set_tac.
        + destruct H5; try auto.
          set_tac. destruct H5; try auto.
          destruct H5; auto.
        + destruct H5; try auto.
          destruct H5; try auto.
          set_tac. destruct H12; auto.
      - set_tac. split; try set_tac.
        + right; split; [ auto | ].
          hnf; intros; subst. apply H12. set_tac.
        + hnf; intros; apply H12; set_tac.
      - intros * is_dVar.
        unfold updEnv.
        destruct (v0 =? n) eqn:?.
        + exists vF1, x3. rewrite Nat.eqb_eq in Heqb; subst.
          repeat split; try auto.
          eapply validFPRanges_single in H1; eauto.
        + eapply dVars_valid.
          set_tac.
          destruct is_dVar as [? | [? ?]]; try auto.
          subst; simpl in *. rewrite Nat.eqb_refl in Heqb. inversion Heqb. }
    eapply validFPRanges_single; try eauto.
    inversion eval_let_float; subst.
    assert (v0 = vF1).
    { eapply eval_expr_functional; eauto. }
    subst; auto.
Qed.