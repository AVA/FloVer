#!/bin/bash
function display_help {
  echo "configure.sh - Generate a file 'Make' which can be used to"
  echo "               generate Makefile.coq for this directory"
  echo ""
  echo -e "  --help\t Show this text"
  echo -e "  --timing\t Use timing scripts"
}

TIMING=no
while [ $# != 0 ]; do
   case "$1" in
   --help)  display_help;;
   --timing) TIMING=yes;;
   *)
   esac
   shift
 done

if [[ "$TIMING" = "yes" ]]; then
    echo "Created .timing to indicate usage of timing script"
    touch .timing
fi

coq_ver=$(${COQBIN}coqc -v 2>/dev/null | sed -n -e 's/The Coq Proof Assistant, version \([^ ]*\).*$/\1/p')
case "$coq_ver" in
    8.7.2)
    ;;
    8.8.0)
    ;;
    8.8.1)
    ;;
    8.8.2)
    ;;
    *)
        echo "Error: Need one of Coq 8.7.2, 8.8.0 8.8.1 or 8.8.2"
        exit 1
esac

echo "Found Coq version $coq_ver."

echo "-R . Flover" > _CoqProject

find . -path ./attic -prune -o -path ./output -prune -o -name "*.v" -print >> _CoqProject

coq_makefile -f _CoqProject -o Makefile

echo 'ifeq ($(wildcard time.rb)$(wildcard .timing),time.rb.timing)
export TIMECMD=@./time.rb $(if $(findstring j,$(MAKEFLAGS)),--parallel,)
endif' >>Makefile
