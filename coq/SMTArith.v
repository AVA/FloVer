(*
  Formalization of SMT Arithmetic for FloVer. Original author: Joachim Bard.
*)

Require Import Coq.Reals.Reals Coq.QArith.Qreals.
Require Import Flover.Expressions.
Require Import Flover.Infra.ExpressionAbbrevs.
Require Import Flover.ExpressionSemantics.
Require Import Flover.Infra.Ltacs.
Require Import Flover.RealRangeArith.

Open Scope R.

Inductive SMTArith :=
| ConstQ (r: Q) : SMTArith
| VarQ (x: nat) : SMTArith
| UMinusQ (e: SMTArith) : SMTArith
| PlusQ (e1 e2: SMTArith) : SMTArith
| MinusQ (e1 e2: SMTArith) : SMTArith
| TimesQ (e1 e2: SMTArith) : SMTArith
| DivQ (e1 e2: SMTArith) : SMTArith.


Inductive SMTLogic :=
| LessQ (e1 e2: SMTArith) : SMTLogic
| LessEqQ (e1 e2: SMTArith) : SMTLogic
| EqualsQ (e1 e2: SMTArith) : SMTLogic
| TrueQ : SMTLogic
| NotQ (q: SMTLogic) : SMTLogic
| AndQ (q1 q2: SMTLogic) : SMTLogic
| OrQ (q1 q2: SMTLogic) : SMTLogic.

Definition FalseQ := NotQ TrueQ.

Definition getPrecond q :=
  match q with
  | AndQ p _ => Some p
  | _ => None
  end.

Definition getBound q :=
  match q with
  | AndQ _ (AndQ b _) => Some b
  | _ => None
  end.

Inductive eval_smt (E: env) : SMTArith -> R -> Prop :=
| VarQ_load x v : E x = Some v -> eval_smt E (VarQ x) v
| ConstQ_eval r : eval_smt E (ConstQ r) (Q2R r)
| UMinusQ_eval e v : eval_smt E e v -> eval_smt E (UMinusQ e) (- v)
| PlusQ_eval e1 e2 v1 v2 :
    eval_smt E e1 v1 -> eval_smt E e2 v2 -> eval_smt E (PlusQ e1 e2) (v1 + v2)
| MinusQ_eval e1 e2 v1 v2 :
    eval_smt E e1 v1 -> eval_smt E e2 v2 -> eval_smt E (MinusQ e1 e2) (v1 - v2)
| TimesQ_eval e1 e2 v1 v2 :
    eval_smt E e1 v1 -> eval_smt E e2 v2 -> eval_smt E (TimesQ e1 e2) (v1 * v2)
| DivQ_eval e1 e2 v1 v2 :
    v2 <> 0 -> eval_smt E e1 v1 -> eval_smt E e2 v2 -> eval_smt E (DivQ e1 e2) (v1 / v2).

Fixpoint eval_smt_logic (E: env) (q: SMTLogic) : Prop :=
  match q with
  | LessQ e1 e2 => exists v1 v2, eval_smt E e1 v1 /\ eval_smt E e2 v2 /\ v1 < v2
  | LessEqQ e1 e2 => exists v1 v2, eval_smt E e1 v1 /\ eval_smt E e2 v2 /\ v1 <= v2
  | EqualsQ e1 e2 => exists v, eval_smt E e1 v /\ eval_smt E e2 v
  | TrueQ => True
  | NotQ q => ~ (eval_smt_logic E q)
  | AndQ q1 q2 => eval_smt_logic E q1 /\ eval_smt_logic E q2
  | OrQ q1 q2 => eval_smt_logic E q1 \/ eval_smt_logic E q2
  end.

Lemma andq_assoc E q1 q2 q3 :
  eval_smt_logic E (AndQ q1 (AndQ q2 q3)) <-> eval_smt_logic E (AndQ (AndQ q1 q2) q3).
Proof. cbn. tauto. Qed.

Lemma andq_comm E q1 q2 :
  eval_smt_logic E (AndQ q1 q2) <-> eval_smt_logic E (AndQ q2 q1).
Proof. cbn. tauto. Qed.

Lemma not_eval_falseq E : ~ eval_smt_logic E FalseQ.
Proof. auto. Qed.

Fixpoint SMTArith_eqb e e' : bool :=
  match e, e' with
  | ConstQ r, ConstQ r' => Qeq_bool r r'
  | VarQ x, VarQ x' => beq_nat x x'
  | UMinusQ e, UMinusQ e' => SMTArith_eqb e e'
  | PlusQ e1 e2, PlusQ e1' e2' => SMTArith_eqb e1 e1' && SMTArith_eqb e2 e2'
  | MinusQ e1 e2, MinusQ e1' e2' => SMTArith_eqb e1 e1' && SMTArith_eqb e2 e2'
  | TimesQ e1 e2, TimesQ e1' e2' => SMTArith_eqb e1 e1' && SMTArith_eqb e2 e2'
  | DivQ e1 e2, DivQ e1' e2' => SMTArith_eqb e1 e1' && SMTArith_eqb e2 e2'
  | _, _ => false
  end.

Fixpoint SMTLogic_eqb q q' : bool :=
  match q, q' with
  | LessQ q1 q2, LessQ q1' q2' => SMTArith_eqb q1 q1' && SMTArith_eqb q2 q2'
  | LessEqQ q1 q2, LessEqQ q1' q2' => SMTArith_eqb q1 q1' && SMTArith_eqb q2 q2'
  | EqualsQ q1 q2, EqualsQ q1' q2' => SMTArith_eqb q1 q1' && SMTArith_eqb q2 q2'
  | TrueQ, TrueQ => true
  | NotQ q, NotQ q' => SMTLogic_eqb q q'
  | AndQ q1 q2, AndQ q1' q2' => SMTLogic_eqb q1 q1' && SMTLogic_eqb q2 q2'
  | OrQ q1 q2, OrQ q1' q2' => SMTLogic_eqb q1 q1' && SMTLogic_eqb q2 q2'
  | _, _ => false
  end.

Fixpoint SMTArith2Expr (e: SMTArith) : expr Q :=
  match e with
  | ConstQ r => Expressions.Const REAL r
  | VarQ x => Var Q x
  | UMinusQ e0 => Unop Neg (SMTArith2Expr e0)
  | PlusQ e1 e2 => Binop Plus (SMTArith2Expr e1) (SMTArith2Expr e2)
  | MinusQ e1 e2 => Binop Sub (SMTArith2Expr e1) (SMTArith2Expr e2)
  | TimesQ e1 e2 => Binop Mult (SMTArith2Expr e1) (SMTArith2Expr e2)
  | DivQ e1 e2 => Binop Div (SMTArith2Expr e1) (SMTArith2Expr e2)
  end.

Lemma SMTArith_eqb_refl e : SMTArith_eqb e e = true.
Proof.
  induction e; cbn; try rewrite IHe1, IHe2; auto using Qeq_bool_refl, Nat.eqb_refl.
Qed.

Lemma SMTLogic_eqb_refl q : SMTLogic_eqb q q = true.
Proof.
  induction q; cbn; try rewrite IHq1, IHq2; auto; now rewrite ?SMTArith_eqb_refl.
Qed.

Lemma SMTArith_eqb_sym e e' : SMTArith_eqb e e' = SMTArith_eqb e' e.
Proof.
  induction e in e' |- *; destruct e'; cbn; auto using Qeq_bool_comm, Nat.eqb_sym;
    try (rewrite IHe1, IHe2; reflexivity).
Qed.

Lemma SMTArith_eqb_sound E e e' v :
  SMTArith_eqb e e' = true ->
  eval_smt E e v ->
  eval_smt E e' v.
Proof.
  induction e in e', v |- *; destruct e'; cbn; try discriminate.
  - intros Heq H. inversion H; subst. apply Qeq_bool_eq, Qeq_eqR in Heq.
    rewrite Heq. constructor.
  - intros Heq H. apply beq_nat_true in Heq. inversion H; subst.
    now constructor.
  - intros Heq H. inversion H; subst. constructor. auto.
  - intros Heq H. andb_to_prop Heq. inversion H; subst.
    constructor; auto.
  - intros Heq H. andb_to_prop Heq. inversion H; subst.
    constructor; auto.
  - intros Heq H. andb_to_prop Heq. inversion H; subst.
    constructor; auto.
  - intros Heq H. andb_to_prop Heq. inversion H; subst.
    constructor; auto.
Qed.

Lemma SMTLogic_eqb_sound E q q' :
  SMTLogic_eqb q q' = true ->
  eval_smt_logic E q <-> eval_smt_logic E q'.
Proof.
  induction q in q' |- *; destruct q'; cbn; try discriminate.
  - intros H. andb_to_prop H.
    split; intros [v1 [v2 [H1 [H2 H3]]]]; exists v1, v2; repeat split;
      eauto using SMTArith_eqb_sound.
    all: rewrite SMTArith_eqb_sym in L, R; eauto using SMTArith_eqb_sound.
  - intros H. andb_to_prop H.
    split; intros [v1 [v2 [H1 [H2 H3]]]]; exists v1, v2; repeat split;
      eauto using SMTArith_eqb_sound.
    all: rewrite SMTArith_eqb_sym in L, R; eauto using SMTArith_eqb_sound.
  - intros H. andb_to_prop H.
    split; intros [v [H1 H2]]; exists v; repeat split;
      eauto using SMTArith_eqb_sound.
    all: rewrite SMTArith_eqb_sym in L, R; eauto using SMTArith_eqb_sound.
  - tauto.
  - intros Heq. split; intros nH H; now apply nH, (IHq q').
  - intros H. andb_to_prop H. split; intros [H1 H2]; split.
    all: try now apply (IHq1 _ L).
    all: now apply (IHq2 _ R).
  - intros H. andb_to_prop H. split; intros [H1 | H2].
    all: try (left; now apply (IHq1 _ L)).
    all: right; now apply (IHq2 _ R).
Qed.

Lemma SMTArith2Expr_exact e : toREval (toRExp (SMTArith2Expr e)) = toRExp (SMTArith2Expr e).
Proof.
  induction e; cbn; auto; now rewrite ?IHe, ?IHe1, ?IHe2.
Qed.

(* TODO handle Let and Cond *)
Fixpoint smt_expr_eq (e: SMTArith) (e': expr Q) : bool :=
  match e', e with
  | Expressions.Const _ r', ConstQ r => Qeq_bool r r'
  | Var _ x', VarQ x => beq_nat x x'
  | Unop Neg e', UMinusQ e => smt_expr_eq e e'
  | Unop Inv e', DivQ (ConstQ (1#1)) e => smt_expr_eq e e'
  | Binop Plus e1' e2', PlusQ e1 e2 => (smt_expr_eq e1 e1') && (smt_expr_eq e2 e2')
  | Binop Sub e1' e2', MinusQ e1 e2 => (smt_expr_eq e1 e1') && (smt_expr_eq e2 e2')
  | Binop Mult e1' e2', TimesQ e1 e2 => (smt_expr_eq e1 e1') && (smt_expr_eq e2 e2')
  | Binop Div e1' e2', DivQ e1 e2 => (smt_expr_eq e1 e1') && (smt_expr_eq e2 e2')
  | Fma e1' e2' e3', PlusQ (TimesQ e1 e2) e3 =>
    smt_expr_eq e1 e1' && smt_expr_eq e2 e2' && smt_expr_eq e3 e3'
  | Downcast _ e', _ => smt_expr_eq e e'
  | _, _ => false
  end.

Lemma smt_expr_eq_sound e : smt_expr_eq e (SMTArith2Expr e) = true.
Proof.
  induction e; cbn; auto using Qeq_bool_refl, beq_nat_refl; intuition.
Qed.

Lemma eval_smt_expr_complete E Gamma v e e' :
  smt_expr_eq e e' = true ->
  eval_expr E (toRTMap Gamma) DeltaMapR (toREval (toRExp e')) v REAL ->
  eval_expr E (toRTMap Gamma) DeltaMapR (toRExp (SMTArith2Expr e)) v REAL.
Proof.
  induction e' in e, v |- *.
  - destruct e; try discriminate. intros H. apply beq_nat_true in H. now rewrite H.
  - destruct e; try discriminate. intros H. apply Qeq_bool_eq in H.
    apply Qeq_eqR in H. cbn. now rewrite H.
  - destruct e, u; try discriminate.
    + intros Heq H.
      inversion H; subst.
      econstructor; eauto.
      destruct m; try discriminate.
      eapply IHe'; auto.
    + destruct e1; try discriminate.
      destruct r as [n d].
      destruct n; try discriminate.
      destruct p; try discriminate.
      destruct d; try discriminate.
      cbn. intros Heq H.
      inversion H; subst.
      destruct m; try discriminate.
      eapply Binop_dist'; eauto.
      cbn.
      rewrite RMicromega.IQR_1.
      apply RealSimps.inv_eq_1_div.
  - intros Heq H.
    inversion H; subst.
    assert (m1 = REAL) by (eapply toRTMap_eval_REAL; eauto).
    assert (m2 = REAL) by (eapply toRTMap_eval_REAL; eauto).
    subst.
    destruct e, b; cbn; try discriminate; andb_to_prop Heq.
    all: eapply (Binop_dist' (v1:=v1) (v2:=v2)); eauto.
  - destruct e; try discriminate.
    destruct e1; try discriminate.
    intros Heq H. andb_to_prop Heq.
    fold smt_expr_eq in *.
    inversion H; subst.
    assert (m1 = REAL) by (eapply toRTMap_eval_REAL; eauto).
    assert (m2 = REAL) by (eapply toRTMap_eval_REAL; eauto).
    assert (m3 = REAL) by (eapply toRTMap_eval_REAL; eauto).
    subst. unfold evalFma. cbn.
    eapply (Binop_dist' (m1:=REAL) (m2:=REAL) (v1:=v1 * v2) (v2:= v3));
      try discriminate; eauto.
    eapply (Binop_dist' (m1:=REAL) (m2:=REAL) (v1:=v1) (v2:=v2)); eauto.
    discriminate.
  - cbn. intros Heq H.
    inversion H; subst.
    destruct m1; try discriminate.
    now apply IHe'.
  - cbn. congruence.
  (* - cbn. congruence. *)
Qed.

Lemma eval_smt_expr E e v :
  eval_smt E e v ->
  eval_expr E (fun _ => Some REAL) DeltaMapR (toRExp (SMTArith2Expr e)) v REAL.
Proof with try (right; apply Rabs_R0).
  induction 1.
  - now constructor.
  - rewrite <- (delta_0_deterministic (Q2R r) REAL 0)... constructor... auto.
  - apply (Unop_neg (m:= REAL)); auto.
  - rewrite <- (delta_0_deterministic _ REAL 0)...
    apply (Binop_dist (m1:= REAL) (m2:=REAL)); auto... discriminate.
  - rewrite <- (delta_0_deterministic _ REAL 0)...
    apply (Binop_dist (m1:= REAL) (m2:=REAL)); auto... discriminate.
  - rewrite <- (delta_0_deterministic _ REAL 0)...
    apply (Binop_dist (m1:= REAL) (m2:=REAL)); auto... discriminate.
  - rewrite <- (delta_0_deterministic _ REAL 0)...
    apply (Binop_dist (m1:= REAL) (m2:=REAL)); auto...
Qed.

Lemma eval_expr_smt E Gamma e v :
  eval_expr E (toRTMap Gamma) DeltaMapR (toREval (toRExp (SMTArith2Expr e))) v REAL
  -> eval_smt E e v.
Proof.
  induction e in v |- *; cbn; intros H.
  - inversion H. cbn. constructor.
  - inversion H. cbn. constructor; auto.
  - inversion H. cbn. constructor. destruct m; try discriminate. auto.
  - inversion H. cbn. constructor.
    + apply IHe1.
      match_pat (eval_expr _ _ _ (toREval (toRExp (SMTArith2Expr e1))) _ _)
                (fun H => rewrite <- (toRTMap_eval_REAL _ H)). assumption.
    + apply IHe2.
      match_pat (eval_expr _ _ _ (toREval (toRExp (SMTArith2Expr e2))) _ _)
                (fun H => rewrite <- (toRTMap_eval_REAL _ H)). assumption.
  - inversion H. cbn. constructor.
    + apply IHe1.
      match_pat (eval_expr _ _ _ (toREval (toRExp (SMTArith2Expr e1))) _ _)
                (fun H => rewrite <- (toRTMap_eval_REAL _ H)). assumption.
    + apply IHe2.
      match_pat (eval_expr _ _ _ (toREval (toRExp (SMTArith2Expr e2))) _ _)
                (fun H => rewrite <- (toRTMap_eval_REAL _ H)). assumption.
  - inversion H. cbn. constructor.
    + apply IHe1.
      match_pat (eval_expr _ _ _ (toREval (toRExp (SMTArith2Expr e1))) _ _)
                (fun H => rewrite <- (toRTMap_eval_REAL _ H)). assumption.
    + apply IHe2.
      match_pat (eval_expr _ _ _ (toREval (toRExp (SMTArith2Expr e2))) _ _)
                (fun H => rewrite <- (toRTMap_eval_REAL _ H)). assumption.
  - inversion H. cbn. constructor; auto.
    + apply IHe1.
      match_pat (eval_expr _ _ _ (toREval (toRExp (SMTArith2Expr e1))) _ _)
                (fun H => rewrite <- (toRTMap_eval_REAL _ H)). assumption.
    + apply IHe2.
      match_pat (eval_expr _ _ _ (toREval (toRExp (SMTArith2Expr e2))) _ _)
                (fun H => rewrite <- (toRTMap_eval_REAL _ H)). assumption.
Qed.

Fixpoint varsSMT (e: SMTArith) :=
  match e with
  | VarQ x => {{x}}
  | ConstQ _ => NatSet.empty
  | UMinusQ e => varsSMT e
  | PlusQ e1 e2 => varsSMT e1 ∪ varsSMT e2
  | MinusQ e1 e2 => varsSMT e1 ∪ varsSMT e2
  | TimesQ e1 e2 => varsSMT e1 ∪ varsSMT e2
  | DivQ e1 e2 => varsSMT e1 ∪ varsSMT e2
  end.

Fixpoint varsLogic (q: SMTLogic) :=
  match q with
  | LessQ e1 e2 => varsSMT e1 ∪ varsSMT e2
  | LessEqQ e1 e2 => varsSMT e1 ∪ varsSMT e2
  | EqualsQ e1 e2 => varsSMT e1 ∪ varsSMT e2
  | TrueQ => NatSet.empty
  | NotQ q => varsLogic q
  | AndQ q1 q2 => varsLogic q1 ∪ varsLogic q2
  | OrQ q1 q2 => varsLogic q1 ∪ varsLogic q2
  end.

Lemma SMTArith_eqb_varsSMT e1 e2 :
  SMTArith_eqb e1 e2 = true ->
  varsSMT e1 = varsSMT e2.
Proof.
  induction e1 in e2 |- *; destruct e2; try discriminate 1; cbn; auto; intros H.
  - apply beq_nat_true in H. now subst.
  - Flover_compute. erewrite IHe1_1; eauto. erewrite IHe1_2; eauto.
  - Flover_compute. erewrite IHe1_1; eauto. erewrite IHe1_2; eauto.
  - Flover_compute. erewrite IHe1_1; eauto. erewrite IHe1_2; eauto.
  - Flover_compute. erewrite IHe1_1; eauto. erewrite IHe1_2; eauto.
Qed.

Lemma SMTLogic_eqb_varsLogic q1 q2 :
  SMTLogic_eqb q1 q2 = true ->
  varsLogic q1 = varsLogic q2.
Proof.
  induction q1 in q2 |- *; destruct q2; try discriminate 1; cbn; auto; intros H.
  - Flover_compute. erewrite SMTArith_eqb_varsSMT; eauto.
    erewrite (SMTArith_eqb_varsSMT e2); eauto.
  - Flover_compute. erewrite SMTArith_eqb_varsSMT; eauto.
    erewrite (SMTArith_eqb_varsSMT e2); eauto.
  - Flover_compute. erewrite SMTArith_eqb_varsSMT; eauto.
    erewrite (SMTArith_eqb_varsSMT e2); eauto.
  - Flover_compute. erewrite IHq1_1; eauto. erewrite IHq1_2; eauto.
  - Flover_compute. erewrite IHq1_1; eauto. erewrite IHq1_2; eauto.
Qed.

Lemma eval_smt_updEnv x v r E e :
  ~ (x ∈ varsSMT e) ->
  eval_smt E e v <-> eval_smt (updEnv x r E) e v.
Proof.
  induction e in v |- *; intros Hx.
  - split; intros H; inversion H; subst; constructor.
  - split; intros H; inversion H; subst; constructor.
    + unfold updEnv. cbn in Hx. case_eq (x0 =? x); auto.
      intros Heq. exfalso. apply Hx. set_tac. symmetry. now apply beq_nat_true.
    + unfold updEnv in *. cbn in Hx. case_eq (x0 =? x); intros Heq; rewrite Heq in *; auto.
      exfalso. apply Hx. set_tac. symmetry. now apply beq_nat_true.
  - split; intros H; inversion H; subst; constructor; apply IHe; auto.
  - split; intros H; inversion H; subst; constructor.
    all: try apply IHe1; auto.
    all: try apply IHe2; auto.
    all: intros ?; apply Hx; cbn; set_tac.
  - split; intros H; inversion H; subst; constructor.
    all: try apply IHe1; auto.
    all: try apply IHe2; auto.
    all: intros ?; apply Hx; cbn; set_tac.
  - split; intros H; inversion H; subst; constructor.
    all: try apply IHe1; auto.
    all: try apply IHe2; auto.
    all: intros ?; apply Hx; cbn; set_tac.
  - split; intros H; inversion H; subst; constructor.
    all: try apply IHe1; auto.
    all: try apply IHe2; auto.
    all: intros ?; apply Hx; cbn; set_tac.
Qed.

Lemma eval_smt_logic_updEnv x v E q :
  ~ (x ∈ varsLogic q) ->
  eval_smt_logic E q <-> eval_smt_logic (updEnv x v E) q.
Proof.
  intros H. induction q; cbn.
  - split; intros [v1 [v2 [H1 [H2 H3]]]]; exists v1, v2; repeat split; auto.
    all: try apply eval_smt_updEnv; auto.
    all: try eapply eval_smt_updEnv; eauto.
    all: try (intros ?; apply H; cbn; set_tac).
  - split; intros [v1 [v2 [H1 [H2 H3]]]]; exists v1, v2; repeat split; auto.
    all: try apply eval_smt_updEnv; auto.
    all: try eapply eval_smt_updEnv; eauto.
    all: try (intros ?; apply H; cbn; set_tac).
  - split; intros [v' [H1 H2]]; exists v'; repeat split; auto.
    all: try apply eval_smt_updEnv; auto.
    all: try eapply eval_smt_updEnv; eauto.
    all: try (intros ?; apply H; cbn; set_tac).
  - tauto.
  - split; intros H1 H2; apply H1; apply IHq; auto.
  - split; intros [H1 H2]; split.
    all: try apply IHq1; auto.
    all: try apply IHq2; auto.
    all: intros Hx; apply H; cbn; set_tac.
  - split; (intros [H1 | H2]; [left | right]).
    all: try apply IHq1; auto.
    all: try apply IHq2; auto.
    all: intros Hx; apply H; cbn; set_tac.
Qed.

Definition precond : Type := precondIntv * SMTLogic.

Definition preVars (P: precond) :=
  preIntvVars (FloverMap.elements (fst P)) ∪ varsLogic (snd P).

Definition eval_precond E (P: precond) :=
  eval_preIntv E (FloverMap.elements (fst P)) /\ eval_smt_logic E (snd P).

Lemma eval_precond_updEnv E x v P :
  eval_precond E P ->
  ~ (x ∈ preVars P) ->
  eval_precond (updEnv x v E) P.
Proof.
  unfold preVars. destruct P as [Piv C]. cbn. intros [HPiv HC] H. split.
  - unfold eval_preIntv in *.
    intros y iv inl. destruct (HPiv y iv) as [r [H1 H2]]; auto.
    exists r. split; auto. unfold updEnv. case_eq (y =? x); intros Heq; auto.
    exfalso. apply H. set_tac. left. apply beq_nat_true in Heq. subst.
    eapply preIntvVars_sound. eauto.
  - apply eval_smt_logic_updEnv; auto. intros ?. apply H. set_tac.
Qed.

Definition usedQueries : Type := FloverMap.t (SMTLogic * SMTLogic).

Definition unsat_queries qMap :=
  forall E e ql qh, FloverMap.find e qMap = Some (ql, qh) ->
               ~ eval_smt_logic E ql /\ ~ eval_smt_logic E qh.

Definition addVarConstraint (el: expr Q * intv) q :=
  match el with
  | (Var _ x, (lo, hi)) => AndQ (LessEqQ (ConstQ lo) (VarQ x)) (AndQ (LessEqQ (VarQ x) (ConstQ hi)) q)
  | _ => q
  end.

Definition precond2SMT (P: precond) :=
  List.fold_right addVarConstraint (snd P) (FloverMap.elements (fst P)).

Lemma pre_to_smt_correct E P :
  eval_precond E P -> eval_smt_logic E (precond2SMT P).
Proof.
  unfold eval_precond, eval_preIntv, precond2SMT.
  destruct P as [intvs q]. cbn.
  induction (FloverMap.elements intvs) as [|[e [lo hi]] l IHl].
  - intros. now cbn.
  - intros [H Hq]. cbn.
    destruct e; try (apply IHl; split; auto; intros x iv inl; apply H; cbn; auto).
    repeat split.
    + destruct (H n (lo, hi)) as [v [? ?]]; cbn; auto.
      exists (Q2R lo), v. repeat split; try now constructor. tauto.
    + destruct (H n (lo, hi)) as [v [? ?]]; cbn; auto.
      exists v, (Q2R hi). repeat split; try now constructor. tauto.
    + apply IHl. split; auto. intros x iv inl. apply H; cbn; auto.
Qed.

Lemma smt_to_pre_correct E P :
  eval_smt_logic E (precond2SMT P) -> eval_precond E P.
Proof.
  unfold precond2SMT, eval_precond, eval_preIntv.
  destruct P as [intvs q]. cbn.
  induction (FloverMap.elements intvs) as [|p l IHl]; try (cbn; tauto).
  cbn. intros H. split.
  - intros x [lo hi] [hd | inl].
    + subst. cbn in H. destruct H as [H1 [H2 H3]].
      destruct H1 as [v1 [v2 H1]].
      exists v2. repeat split; destruct H1 as [lov1 [xv2 leq]].
      * inversion xv2. now subst.
      * inversion lov1. now subst.
      * destruct H2 as [v1' [v2' [xv1' [hiv2' leq']]]].
        inversion xv1'. inversion hiv2'. inversion xv2. subst.
        assert (eq: v2 = v1') by congruence. now rewrite eq.
    + apply IHl; auto. destruct p as [e [lo' hi']].
      destruct e; cbn in H; auto.
      apply H.
  - destruct p as [e [lo hi]]. destruct e; try now apply IHl.
      cbn in H. tauto.
Qed.

(* checker for precondition *)
Fixpoint checkPrelist (lP: list (FloverMap.key * intv)) C q : bool :=
  match lP, q with
  | List.nil, _ => SMTLogic_eqb C q
  | List.cons (Var _ x, (lo, hi)) lP', AndQ (LessEqQ (ConstQ lo') (VarQ y)) (AndQ (LessEqQ (VarQ z) (ConstQ hi')) q') => (x =? y) && (x =? z) && Qeq_bool lo lo' && Qeq_bool hi hi' && checkPrelist lP' C q'
  | List.cons (Var _ x, _) _, _ => false
  | List.cons el lP', _ => checkPrelist lP' C q
  end.

(*
Lemma checkPrelist_LessQ lP C e1 e2 : checkPrelist lP C (LessQ e1 e2) = false.
Proof.
  induction lP as [|[e [lo hi]] l IHl]; auto.
  destruct e; auto.
Qed.

Lemma checkPrelist_LessEqQ lP e1 e2 : checkPrelist lP (LessEqQ e1 e2) = false.
Proof.
  induction lP as [|[e [lo hi]] l IHl]; auto.
  destruct e; auto.
Qed.

Lemma checkPrelist_EqualsQ lP e1 e2 : checkPrelist lP (EqualsQ e1 e2) = false.
Proof.
  induction lP as [|[e [lo hi]] l IHl]; auto.
  destruct e; auto.
Qed.

Lemma checkPrelist_NotQ lP q : checkPrelist lP (NotQ q) = false.
Proof.
  induction lP as [|[e [lo hi]] l IHl]; auto.
  destruct e; auto.
Qed.

Lemma checkPrelist_OrQ lP q1 q2 : checkPrelist lP (OrQ q1 q2) = false.
Proof.
  induction lP as [|[e [lo hi]] l IHl]; auto.
  destruct e; auto.
Qed.
*)

Definition checkPre (P: precond) q := checkPrelist (FloverMap.elements (fst P)) (snd P) q.

Lemma checkPre_precond P : checkPre P (precond2SMT P) = true.
Proof.
  unfold precond2SMT, checkPre.
  destruct P as [Piv C]. cbn.
  induction (FloverMap.elements Piv) as [|[e [lo hi]] l IHl]; cbn. 
  - apply SMTLogic_eqb_refl.
  - destruct e; cbn; auto.
    now rewrite Nat.eqb_refl, !Qeq_bool_refl, IHl.
Qed.

Lemma checkPre_pre_smt E P q :
  checkPre P q = true ->
  eval_precond E P ->
  eval_smt_logic E q.
Proof with try discriminate.
  unfold checkPre, eval_precond, eval_preIntv.
  destruct P as [Piv C]. cbn.
  induction (FloverMap.elements Piv) as [|[e [lo hi]] l IHl] in q |- *.
  - cbn. intros Heq [_ H]. now apply (SMTLogic_eqb_sound _ C).
  - destruct e as [x| | | | | |].
    all: try now (cbn; intros Heq [H1 H2]; apply IHl; auto).
    cbn.
    destruct q as [? ?|? ?|? ?| |?|q1 q2|? ?]...
    destruct q1 as [? ?|e1 e2|? ?| |?|? ?|? ?]...
    destruct e1 as [r| | | | | |]... destruct e2 as [|y| | | | |]...
    destruct q2 as [? ?|? ?|? ?| |?|q1 q2|? ?]...
    destruct q1 as [? ?|e1 e2|? ?| |?|? ?|? ?]...
    destruct e1 as [|z| | | | |]... destruct e2 as [r'| | | | | |]...
    intros chk [H HC].
    apply andb_prop in chk as [chk chk0].
    apply andb_prop in chk as [chk chk1].
    apply andb_prop in chk as [chk chk2].
    apply andb_prop in chk as [chk4 chk3].
    apply beq_nat_true in chk4. apply beq_nat_true in chk3.
    apply Qeq_bool_eq in chk2. apply Qeq_bool_eq in chk1.
    rewrite <- chk3, <- chk4.
    apply Qeq_sym in chk2. apply Qeq_sym in chk1.
    (* assert (x ∈ fVars) as fx by (subst; set_tac; set_tac). *)
    repeat split.
    + destruct (H x (lo, hi)) as [v [H0 [H1 H2]]]; cbn; auto.
      exists (Q2R r), v. repeat split; try now constructor. erewrite Qeq_eqR; now eauto.
    + destruct (H x (lo, hi)) as [v [H0 [H1 H2]]]; cbn; auto.
      exists v, (Q2R r'). repeat split; try now constructor. erewrite Qeq_eqR; now eauto.
    + apply IHl; auto.
Qed.

(*
Lemma checkPre_smt_pre E P q :
  checkPre P q = true -> eval_smt_logic E q -> eval_precond E P.
Proof with try discriminate.
  unfold P_intv_sound, checkPre.
  induction (FloverMap.elements P) as [|[e [lo hi]] l IHl] in q |- *.
  - destruct q; cbn; intros chk; try discriminate. tauto.
  - destruct e as [x| | | | |];
      try (intros chk H ? ? [?|?]; [discriminate| apply (IHl _ chk); auto]).
    destruct q as [? ?|? ?|? ?| |?|q1 q2|? ?]...
    destruct q1 as [? ?|e1 e2|? ?| |?|? ?|? ?]...
    destruct e1 as [r| | | | | |]... destruct e2 as [|y| | | | |]...
    destruct q2 as [? ?|? ?|? ?| |?|q1 q2|? ?]...
    destruct q1 as [? ?|e1 e2|? ?| |?|? ?|? ?]...
    destruct e1 as [|z| | | | |]... destruct e2 as [r'| | | | | |]...
    cbn. intros chk H x' [lo' hi'] [eq|inl].
    + apply andb_prop in chk. destruct chk as [chk chk0].
      apply andb_prop in chk. destruct chk as [chk chk1].
      apply andb_prop in chk. destruct chk as [chk chk2].
      apply andb_prop in chk. destruct chk as [chk4 chk3].
      apply beq_nat_true in chk4. apply beq_nat_true in chk3.
      apply Qeq_bool_eq in chk2. apply Qeq_bool_eq in chk1.
      destruct H as [[v1 [v2 [H1 [H2 H3]]]] [[v1' [v2' [H1' [H2' H3']]]] H]].
      symmetry in chk4. symmetry in chk3.
      assert (x' = x) by congruence.
      assert (lo' = lo) by congruence.
      assert (hi' = hi) by congruence.
      subst.
      inversion H1. inversion H2. inversion H1'. inversion H2'.
      assert (v2 = v1') by congruence.
      subst. cbn. rewrite (Qeq_eqR _ _ chk2). rewrite (Qeq_eqR _ _ chk1).
      exists v1'. repeat split; auto.
    + apply andb_prop in chk. destruct chk as [chk chk0].
      apply (IHl _ chk0); tauto.
Qed.
*)

Lemma precond_bound_correct E P preQ bound :
  eval_precond E P ->
  checkPre P preQ = true ->
  eval_smt_logic E bound ->
  eval_smt_logic E (AndQ preQ bound).
Proof.
  intros.
  split; auto.
  eapply checkPre_pre_smt; eauto.
Qed.

Lemma RangeBound_low_sound E P preQ e r Gamma v :
  eval_precond E P ->
  checkPre P preQ = true ->
  ~ eval_smt_logic E (AndQ preQ (AndQ (LessQ e (ConstQ r)) TrueQ)) ->
  eval_expr E (toRTMap Gamma) DeltaMapR (toREval (toRExp (SMTArith2Expr e))) v REAL ->
  Q2R r <= v.
Proof.
  intros H0 H1 H2 H3.
  apply eval_expr_smt in H3.
  apply Rnot_lt_le. intros B.
  apply H2. eapply precond_bound_correct; eauto.
  split; cbn; auto.
  do 2 eexists. repeat split; first [eassumption | constructor].
Qed.

Lemma RangeBound_high_sound E P preQ e r Gamma v :
  eval_precond E P ->
  checkPre P preQ = true ->
  ~ eval_smt_logic E (AndQ preQ (AndQ (LessQ (ConstQ r) e) TrueQ)) ->
  eval_expr E (toRTMap Gamma) DeltaMapR (toREval (toRExp (SMTArith2Expr e))) v REAL ->
  v <= Q2R r.
Proof.
  intros H0 H1 H2 H3.
  apply eval_expr_smt in H3.
  apply Rnot_lt_le. intros B.
  apply H2. eapply precond_bound_correct; eauto.
  split; cbn; auto.
  do 2 eexists. repeat split; first [eassumption | constructor].
Qed.
