open Format;;
open CoqChecker;;
open List;;
open String;;
open Char;;
open Big_int;;

 let explode s = let rec exp i l = if i < 0 then l else exp (i - 1) ((s.[i]) :: l) in exp (String.length s - 1) [];;

let rec implode sl =
  match sl with
  |c :: sl' -> (String.make 1 c)^(implode sl')
  |[] -> "";;

(*
Based on:
https://stackoverflow.com/questions/5774934/how-do-i-read-in-lines-from-a-text-file-in-ocaml
*)

let read_file filename =
  let text = ref "" in
  let chan = open_in filename in
  try
    while true; do
      text := (!text) ^ (input_line chan) ^ "\n";
    done; !text
  with End_of_file ->
    close_in chan;
    !text;;

let input = read_file (Sys.argv.(1));;
let res = implode (CoqChecker.runChecker (explode (input)));;
let () = printf "%s\n" res;;
