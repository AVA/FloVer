From Flover
     Require Import Infra.RealSimps Infra.RationalSimps Infra.RealRationalProps
     Infra.Ltacs RealRangeArith RealRangeValidator RoundoffErrorValidator
     Environments TypeValidator FPRangeValidator ExpressionAbbrevs.

Definition RangeErrorChecker (e: expr Q) (absenv: analysisResult)
           (P: precond) Qmap (defVars: FloverMap.t  mType) Gamma :=
  RangeValidator e absenv P Qmap NatSet.empty &&
                 FPRangeValidator e absenv Gamma NatSet.empty &&
                 RoundoffErrorValidator e Gamma absenv NatSet.empty.

(** Result checking function **)
Definition ResultChecker (e: expr Q) (absenv: analysisResult)
           (P: precond) Qmap (defVars: FloverMap.t  mType) Gamma :=
  validSSA e (preVars P) && RangeErrorChecker e absenv P Qmap defVars Gamma.

(**
   Soundness proof for the result checker.
**)
Theorem result_checking_sound (e: expr Q) (absenv: analysisResult) P Qmap defVars Gamma:
  forall (E1 E2: env) DeltaMap,
    (forall (v : R) (m' : mType),
        exists d : R, DeltaMap v m' = Some d /\ (Rabs d <= mTypeToR m')%R) ->
    approxEnv E1 (toRExpMap Gamma) absenv (freeVars e) NatSet.empty E2 ->
    eval_precond E1 P ->
    unsat_queries Qmap ->
    getValidMap defVars e (FloverMap.empty mType) = Succes Gamma ->
    ResultChecker e absenv P Qmap defVars Gamma = true ->
    (exists outVars, ssa e (freeVars e) outVars) /\
    validRanges e absenv E1 (toRTMap (toRExpMap Gamma)) /\
    validErrorBounds e E1 E2 absenv Gamma /\
    validFPRanges e E2 Gamma DeltaMap.
(**
   The proofs is a simple composition of the soundness proofs for the range
   validator and the error bound validator.
**)
Proof.
  intros * deltaMap_valid approxE1E2 P_valid unsat_qs valid_typeMap results_valid.
  unfold ResultChecker in results_valid.
  assert (validTypes e Gamma).
  { eapply getValidMap_top_correct; eauto.
    intros. cbn in *; congruence. }
  andb_to_prop results_valid.
  pose proof (NatSetProps.empty_union_2 (freeVars e) NatSet.empty_spec) as union_empty.
  hnf in union_empty.
  assert (dVars_range_valid NatSet.empty E1 absenv).
  { unfold dVars_range_valid.
    intros; set_tac. }
  (*
  assert (affine_dVars_range_valid NatSet.empty E1 absenv 1 (FloverMap.empty _)
                                   (fun _ => None)) as affine_dvars_valid.
  { unfold affine_dVars_range_valid.
    intros; set_tac. }
*)
  assert (NatSet.Subset (freeVars e -- NatSet.empty) (freeVars e)).
  { hnf; intros a in_empty.
    set_tac. }
  rename R into validFPR.
  rename R0 into validRoundoffErr.
  pose proof (validSSA_checks_freeVars _ _ L) as sub.
  destruct (validSSA_sound e (preVars P)) as [outVars ssa_e]; auto.
  assert (validSSA e (freeVars e) = true) as ssa_valid
      by (eapply validSSA_downward_closed; eauto using NatSetProps.subset_refl).
  destruct (validSSA_sound e (freeVars e)) as [outVars2 ssa_e2]; eauto.
  assert (validRanges e absenv E1 (toRTMap (toRExpMap Gamma))) as valid_e.
  { clear ssa_e2 outVars2 ssa_valid.
    eapply (RangeValidator_sound (dVars:=NatSet.empty) (A:=absenv) (P:=P) (E:=E1) (fVars:=preVars P));
      eauto using NatSetProps.subset_refl; set_tac.
    eapply ssa_equal_set; eauto.
    intros n. split; set_tac. intros [?|?]; auto. set_tac. }
  pose proof (validRanges_single _ _ _ _ valid_e) as valid_single;
    destruct valid_single as [iv_e [ err_e [vR [ map_e [eval_real real_bounds_e]]]]].
  destruct iv_e as [elo ehi].
  (*
  assert (dVars_contained NatSet.empty (FloverMap.empty (affine_form Q))) as Hdvars
      by (unfold dVars_contained; intros * Hset; clear - Hset; set_tac).
*)
  assert (ssa e (NatSet.union (freeVars e) (NatSet.empty)) outVars2).
  { eapply ssa_equal_set; eauto. }
  eexists; repeat split; try eauto.
  - eapply RoundoffErrorValidator_sound; eauto.
  - eapply FPRangeValidator_sound; eauto.
    + eapply RoundoffErrorValidator_sound; eauto.
    + intros; set_tac.
Qed.

(**
   Soundness proof for the result checker.
**)
Corollary result_checking_sound_single (e: expr Q) (absenv: analysisResult) P Qmap defVars Gamma:
  forall (E1 E2: env) DeltaMap,
    (forall (e': R) (m': mType),
        exists d: R, DeltaMap e' m' = Some d /\ (Rabs d <= mTypeToR m')%R) ->
    eval_precond E1 P ->
    unsat_queries Qmap ->
    getValidMap defVars e (FloverMap.empty mType) = Succes Gamma ->
    ResultChecker e absenv P Qmap defVars Gamma = true ->
    exists Gamma,
      approxEnv E1 (toRExpMap Gamma) absenv (freeVars e) NatSet.empty E2 ->
      exists iv err vR vF m,
        FloverMap.find e absenv = Some (iv, err) /\
        eval_expr E1 (toRTMap (toRExpMap Gamma)) DeltaMapR (toREval (toRExp e)) vR REAL /\
        eval_expr E2 (toRExpMap Gamma) DeltaMap (toRExp e) vF m /\
        (forall vF m,
            eval_expr E2 (toRExpMap Gamma) DeltaMap (toRExp e) vF m ->
            (Rabs (vR - vF) <= Q2R err))%R.
Proof.
  intros * deltas_matched P_valid unsat_qs valid_typeMap results_valid.
  exists Gamma; intros approxE1E2.
  edestruct result_checking_sound as (_ & validRange & Hsound & _); try eauto.
  eapply validRanges_single in validRange.
  destruct validRange as [iv [err [vR [Hfind [eval_real validRange]]]]].
  eapply validErrorBoundsRec_single in Hsound; eauto.
  destruct Hsound as [[vF [mF eval_float]] err_bounded]; auto.
  exists iv, err, vR, vF, mF; repeat split; auto.
Qed.
