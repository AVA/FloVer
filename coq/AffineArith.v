(*
  Formalization of Affine Arithmetic for FloVer. Original author: Raphael Monat.
*)

Require Import Coq.Reals.Reals Coq.QArith.Qreals Coq.Lists.List Coq.micromega.Psatz.
Require Import Recdef.
Require Import Flover.AffineForm Flover.Infra.Abbrevs Flover.Infra.RealSimps Flover.Infra.Ltacs Flover.IntervalArith.

Open Scope R.

Fixpoint radius (a: affine_form R): R := match a with
| Const v => 0
| Noise _ v a' => (Rabs v) + radius a'
end.

Lemma radius_nonneg (a: affine_form R):
  0 <= radius a.
Proof.
  induction a; simpl in *; try lra.
  assert (0 = 0 + 0) as tmp by lra; rewrite tmp; clear tmp.
  eapply Rplus_le_compat; try assumption.
  apply Rabs_pos.
Qed.

Definition toInterval (a: affine_form R): interval := mkInterval (get_const a - radius a) (get_const a + radius a).

Lemma valid_to_interval (af: affine_form R):
  valid (toInterval af).
Proof.
  unfold valid.
  unfold toInterval.
  simpl.
  pose proof (radius_nonneg af).
  apply Rplus_le_compat; try lra.
Qed.

Definition fromInterval (iv: interval) (noise: nat): affine_form R :=
  let ivMid := (IVhi iv / 2) + (IVlo iv / 2) in
  let ivRad := Rmax (ivMid - (IVlo iv)) ((IVhi iv) - ivMid) in
  if Req_dec_sum (IVlo iv) (IVhi iv) then Const ivMid
  else Noise noise ivRad (Const (ivMid)).

Lemma to_from_interval_eq (iv: interval) (n: nat):
  valid iv ->
  (toInterval (fromInterval iv n)) = iv.
Proof.
  intros vld.
  unfold valid, fromInterval, toInterval in *.
  unfold IVlo, IVhi in vld.
  destruct iv as (l, r).
  simpl in *.
  destruct (Req_dec_sum l r) as [H | H]; simpl in *.
  - subst; f_equal; lra.
  - apply Rmax_case_strong; intros; rewrite Rabs_pos_eq; try lra; f_equal; lra.
Qed.

Lemma to_interval_functional a x1 x2:
  toInterval a = x1 -> toInterval a = x2 -> x1 = x2.
Proof.
  congruence.
Qed.

Lemma fresh_from_interval n m iv:
  (n < m)%nat -> fresh m (fromInterval iv n). 
Proof.
  intros Hle.
  unfold fresh, get_max_index, fromInterval.
  destruct Req_dec_sum; cbn; lia.
Qed.

Definition above_zero a := 0 < (IVlo (toInterval a)).
Definition below_zero a := (IVhi (toInterval a)) < 0.

Lemma above_zero_monotone a n v :
  above_zero (Noise n v a) -> above_zero a.
Proof.
  unfold above_zero.
  simpl.
  intros noise.
  unfold Qminus in *.
  pose proof (Rabs_pos v) as nnabs.
  pose proof (radius_nonneg a) as nnrad.
  lra.
Qed.

Lemma above_IVhi a:
  above_zero a -> IVhi (toInterval a) > 0.
Proof.
  unfold above_zero.
  pose proof (valid_to_interval a) as validiv.
  unfold valid in validiv.
  lra.
Qed.

Lemma below_zero_monotone a n v :
  below_zero (Noise n v a) -> below_zero a.
Proof.
  unfold below_zero.
  simpl.
  intros noise.
  pose proof (Rabs_pos v) as nnabs.
  pose proof (radius_nonneg a) as nnrad.
  lra.
Qed.

Lemma below_IVlo a:
  below_zero a -> IVlo (toInterval a) < 0.
Proof.
  unfold below_zero.
  pose proof (valid_to_interval a) as validiv.
  unfold valid in validiv.
  lra.
Qed.

Definition noise_type := {q: R | -(1) <= q /\ q <= 1}.
Definition noise_val (q: noise_type): R := proj1_sig q.

Coercion noise_val : noise_type >-> R.

Lemma zero_is_valid_noise:
  -1 <= 0 <= 1.
Proof.
  lra.
Qed.

Lemma one_is_valid_noise:
  -1 <= 1 <= 1.
Proof.
  lra.
Qed.

Definition noise_zero := exist (fun (x: R) => -1 <= x /\ x <= 1) 0 zero_is_valid_noise.
Definition noise_one := exist (fun (x: R) => -1 <= x /\ x <= 1) 1 one_is_valid_noise.

Definition noise_mapping := nat -> option noise_type.

Definition contained_map (map1 map2: noise_mapping) := forall k v, map1 k = Some v -> map2 k = Some v.

Instance contained_map_preorder : PreOrder contained_map.
Proof.
  constructor.
  - unfold Reflexive, contained_map; auto.
  - unfold Transitive, contained_map; auto.
Qed.

Lemma contained_map_refl map:
  contained_map map map.
Proof.
  intros k v; auto.
Qed.

Lemma contained_map_trans map1 map2 map3:
  contained_map map1 map2 -> contained_map map2 map3 -> contained_map map1 map3.
Proof.
  unfold contained_map; auto.
Qed.

Definition updMap (m: noise_mapping) (n: nat) (q: noise_type): noise_mapping :=
  fun x => if x =? n then Some q else m x.

Lemma upd_sound map n q:
  updMap map n q n = Some q.
Proof.
  unfold updMap.
  assert (n =? n = true) as H by apply Nat.eqb_refl.
  now rewrite H.
Qed.

Lemma contained_map_extension map n q:
  map n = None -> contained_map map (updMap map n q).
Proof.
  intros A.
  intros k v B.
  unfold updMap.
  destruct (k =? n) eqn: Heq; try assumption.
  apply beq_nat_true in Heq.
  congruence.
Qed.

Lemma contained_map_extension2 map n1 q1 n2 q2:
  map n1 = None -> map n2 = None -> contained_map map (updMap (updMap map n2 q2) n1 q1).
Proof.
  intros A B.
  intros k v C.
  unfold updMap.
  destruct (k =? n1) eqn: Heq.
  - apply beq_nat_true in Heq; congruence.
  - destruct (k =? n2) eqn: Heq'; try assumption.
    apply beq_nat_true in Heq'; congruence.
Qed.

Fixpoint eval_aff (a:  affine_form R) (map: noise_mapping): option R :=
  match a with
  | Const v => Some v
  | Noise n v a' => match (map n), eval_aff a' map with
                       | None, _ => None
                       | _, None => None
                       | Some res, Some res_a' =>
                         Some ((res * v) + res_a')
                       end
  end.

Definition af_evals (a:  affine_form R) (v: R) (map: noise_mapping): Prop :=
  plet v_aff := eval_aff a map in v_aff = v.

Lemma constant_evals_unchanged v1 v2 map:
  af_evals (Const v1) v2 map -> v1 = v2.
Proof.
  intros A.
  unfold af_evals in A.
  now simpl in A.
Qed.

Lemma af_evals_noise_compat a v1 v2 n map:
  af_evals (Noise n v1 a) v2 map ->
  exists q, map n = Some q /\ af_evals a (v2 - q * v1) map.
Proof.
  intros A.
  unfold af_evals in *.
  simpl in A.
  destruct (map n); try contradiction.
  destruct (eval_aff a map); try contradiction.
  simpl in A |-*.
  exists n0.
  rewrite <- A.
  split; auto; ring.
Qed.

Lemma af_evals_map_extension a v map1 map2:
  contained_map map1 map2 ->
  af_evals a v map1 ->
  af_evals a v map2.
Proof.
  unfold contained_map.
  intros cont.
  revert v.
  induction a.
  - now simpl in *.
  - intros v__eval eval1.
    apply af_evals_noise_compat in eval1 as [q1 [Hmap1 eval1]].
    specialize (IHa _ eval1).
    specialize (cont _ _ Hmap1).
    clear eval1 Hmap1.
    unfold af_evals in *.
    simpl in *.
    rewrite cont.
    destruct (eval_aff a map2); try contradiction.
    simpl in IHa |-*.
    rewrite IHa; lra.
Qed.

Lemma bounded_by_abs_pos v (q: noise_type):
  0 < v -> - Rabs v <= q * v <= Rabs v.
Proof.
  intros v_pos % Rlt_le; destruct q as [q [q_l q_h]]; simpl; rewrite Rabs_pos_eq; try lra; split.
  - field_rewrite (q * v = - (- q * v)).
    apply Ropp_le_contravar.
    pose proof (Rmult_le_compat_r _ _ _ v_pos q_l); lra.
  - pose proof (Rmult_le_compat_r _ _ _ v_pos q_h); lra.
Qed.

Lemma bounded_by_abs v (q: noise_type):
  - Rabs v <= q * v <= Rabs v.
Proof.
  destruct (Req_dec_sum v 0) as [H | H].
  - subst.
    rewrite Rabs_pos_eq; lra.
  - destruct (Rlt_dec v 0) as [H' | H']. 
    + destruct (bounded_by_abs_pos (v:=-v) q) as [v_l v_h]; try lra.
      rewrite Rabs_Ropp in v_h, v_l.
      assert (q * v = - (q * -v)) as H0 by lra.
      assert (Rabs v = - - Rabs v) as H1 by lra.
      rewrite H0.
      rewrite H1 at 2.
      split; apply Ropp_le_contravar; auto.
    + apply bounded_by_abs_pos; lra.
Qed.

(* Interval containment property *)
Lemma bounded_aff a map v:
  (eval_aff a map) = (Some v) ->
  (get_const a) - (radius a) <= v /\ v <= (get_const a) + (radius a).
Proof.
  revert v; induction a; intros v0 eval; simpl in *; subst.
  - inversion eval; subst; lra.
  - case_eq (map n); intros; rewrite H in eval; try congruence.
    case_eq (eval_aff a map); intros; rewrite H0 in eval; inversion eval.
    subst.
    specialize (IHa _ H0); destruct IHa as [IHal IHar].
    assert (- Rabs v <= n0 * v <= Rabs v) as [v_l v_h] by eauto using bounded_by_abs.
    split.
    + pose proof (Rplus_le_compat _ _ _ _ IHal v_l); lra.
    + pose proof (Rplus_le_compat _ _ _ _ IHar v_h); lra.
Qed.

Lemma bounded_aff_abs a map v:
  (eval_aff a map) = Some v ->
  Rabs (v - get_const a) <= radius a.
Proof.
  intros.
  apply Rabs_le.
  pose proof (bounded_aff a map H).
  lra.
Qed.

Lemma bounded_af_evals_abs a map v:
  af_evals a v map ->
  Rabs (v - get_const a) <= radius a.
Proof.
  unfold af_evals.
  destruct (eval_aff a map) eqn: Hv.
  - intros A.
    simpl in A. 
    rewrite <- A.
    eauto using bounded_aff_abs.
  - simpl; intros; contradiction.
Qed.

Lemma to_interval_containment a v map:
  af_evals a v map ->
  IVlo (toInterval a) <= v <= IVhi (toInterval a).
Proof.
  intros evals.
  simpl.
  unfold af_evals in evals.
  destruct (eval_aff a map) eqn: Hq; simpl in evals; try contradiction.
  rewrite <- evals.
  now apply bounded_aff with (map := map).
Qed.

Lemma radius_0_const (a:  affine_form R) v map:
  af_evals a v map ->
  radius a = 0 ->
  v = get_const a.
Proof.
  unfold af_evals.
  intros evals rad.
  destruct (eval_aff a map) eqn: Hev; try contradiction.
  apply bounded_aff in Hev.
  rewrite evals in *.
  rewrite rad in Hev.
  lra.
Qed.

(***********************************************)
(******** Addition of two affine forms *********)
(***********************************************)

Function plus_aff_tuple (a:  affine_form R *  affine_form R) { measure aff_length_tuple a }:  affine_form R :=
  let (a1, a2) := a in
  match a1, a2 with
  | Const v1, Const v2 => Const (v1 + v2)
  | Const v1, Noise n2 v2 a2' => Noise n2 v2 (plus_aff_tuple (a1, a2'))
  | Noise n1 v1 a1', Const v2 => Noise n1 v1 (plus_aff_tuple (a1', a2))
  | Noise n1 v1 a1', Noise n2 v2 a2' =>
   if (n1 =? n2)%nat then
     Noise n1 (v1+v2) (plus_aff_tuple (a1', a2'))
   else if n1 <? n2%nat then
          Noise n2 v2 (plus_aff_tuple (a1, a2'))
        else
          Noise n1 v1 (plus_aff_tuple (a1', a2))
  end.
Proof.
  - intros. unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
Defined.

Definition plus_aff (a1 a2:  affine_form R):  affine_form R := plus_aff_tuple (a1, a2).

Lemma plus_aff_tuple_sound (a:  affine_form R *  affine_form R) (v1 v2: R) map:
  af_evals (fst a) v1 map ->
  af_evals (snd a) v2 map ->
  af_evals (plus_aff_tuple a) (v1 + v2) map.
Proof.
  revert v1 v2; unfold af_evals.
  functional induction (plus_aff_tuple a); simpl in *; intros v3 v4 G1 G2.
  - lra.
  - case_eq (map n2); intros; rewrite H in *; [ | inversion G2 ].
    destruct (eval_aff a2' map); [ | inversion G2].
    specialize (IHa0 _ _ G1 (eq_refl r)).
    case_eq (eval_aff (plus_aff_tuple (Const v1, a2')) map); intros; rewrite H0 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
  - case_eq (map n1); intros; rewrite H in *; [ | inversion G1 ].
    destruct (eval_aff a1' map); [ | inversion G1 ].
    specialize (IHa0 _ _ (eq_refl r) G2).
    case_eq (eval_aff (plus_aff_tuple (a1', Const v2)) map); intros; rewrite H0 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
  - apply beq_nat_true in e2; subst.
    case_eq (map n2); intros; rewrite H in *; [ | inversion G1 ].
    destruct (eval_aff a1' map); [ | inversion G1 ].
    destruct (eval_aff a2' map); [ | inversion G2 ].
    specialize (IHa0 _ _ (eq_refl r) (eq_refl r0)).
    case_eq (eval_aff (plus_aff_tuple (a1', a2')) map); intros; rewrite H0 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
  - case_eq (map n1); intros; rewrite H in *; [ | inversion G1 ].
    case_eq (map n2); intros; rewrite H0 in *; [ | inversion G2 ].
    destruct (eval_aff a1' map); [ | inversion G1 ].
    destruct (eval_aff a2' map); [ | inversion G2 ].
    specialize (IHa0 _ _ (eq_refl (n * v1 + r)) (eq_refl r0)).
    case_eq (eval_aff (plus_aff_tuple (Noise n1 v1 a1', a2')) map); intros; rewrite H1 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
  - case_eq (map n1); intros; rewrite H in *; [ | inversion G1 ].
    case_eq (map n2); intros; rewrite H0 in *; [ | inversion G2 ].
    destruct (eval_aff a1' map); [ | inversion G1 ].
    destruct (eval_aff a2' map); [ | inversion G2 ].
    specialize (IHa0 _ _ (eq_refl r) (eq_refl (n0 * v2 + r0))).
    case_eq (eval_aff (plus_aff_tuple (a1', Noise n2 v2 a2')) map); intros; rewrite H1 in *; [ | inversion IHa0 ].
    simpl in *.
    lra.
Qed.

Lemma plus_aff_sound (a1:  affine_form R) (a2:  affine_form R) (v1 v2: R) map:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  af_evals (plus_aff a1 a2) (v1 + v2) map.
Proof.
  eauto using plus_aff_tuple_sound.
Qed.

Lemma plus_aff_comm (a1 a2:  affine_form R) v1 v2 map:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  af_evals (plus_aff a1 a2) (v1 + v2) map <-> af_evals (plus_aff a2 a1) (v2 + v1) map.
Proof.
  intros evals1 evals2.
  pose proof (plus_aff_sound _ _ _ _ _ evals1 evals2) as evalspl.
  pose proof (plus_aff_sound _ _ _ _ _ evals2 evals1) as evalspr.
  unfold af_evals in *.
  split; Flover_compute.
Qed.

Lemma plus_aff_fresh_compat a1 a2 n:
  fresh n a1 ->
  fresh n a2 ->
  fresh n (plus_aff a1 a2).
Proof.
  unfold plus_aff.
  remember (a1, a2) as a12.
  assert (fst a12 = a1 /\ snd a12 = a2) as Havals by now rewrite Heqa12.
  destruct Havals as [Heqa1 Heqa2].
  rewrite <- Heqa1, <- Heqa2.
  clear Heqa1 Heqa2 Heqa12 a1 a2.
  intros fresh1 fresh2.
  functional induction (plus_aff_tuple a12); simpl in *; eauto using fresh_noise_gt, fresh_noise_compat, fresh_noise.
  pose proof (fresh_noise_gt fresh1) as freshn1.
  apply fresh_noise_compat in fresh1.
  pose proof (fresh_noise_gt fresh2) as freshn2.
  apply fresh_noise_compat in fresh2.
  specialize (IHa fresh1 fresh2).
  now apply (fresh_noise _ freshn1).
Qed.

(***********************************************)
(***** Multiplication of two affine forms ******)
(***********************************************)

Function mult_aff_aux (a:  affine_form R *  affine_form R) { measure aff_length_tuple a }:  affine_form R :=
  let (a1, a2) := a in
  let c_a1 := get_const a1 in
  let c_a2 := get_const a2 in
  match a1, a2 with
  | Const v1, Const v2 => Const (v1 * v2)
  | Const v1, Noise n2 v2 a2' => Noise n2 (v2 * v1) (mult_aff_aux (a1, a2'))
  | Noise n1 v1 a1', Const v2 => Noise n1 (v1 * v2) (mult_aff_aux (a1', a2))
  | Noise n1 v1 a1', Noise n2 v2 a2' =>
   if (n1 =? n2) then
     Noise n1 (v1 * c_a2 + v2 * c_a1) (mult_aff_aux (a1', a2'))
   else if (n1 <? n2) then
          Noise n2 (v2 * c_a1) (mult_aff_aux (a1, a2'))
        else
          Noise n1 (v1 * c_a2) (mult_aff_aux (a1', a2))
  end.
Proof.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
  - intros; unfold aff_length_tuple; simpl; auto; lia.
Defined.

Definition mult_aff (a1 a2:  affine_form R) (next_noise: nat):  affine_form R :=
  if (Req_dec_sum (radius a1 * radius a2) 0) then
    mult_aff_aux (a1, a2)
  else 
    Noise next_noise (radius a1 * radius a2) (mult_aff_aux (a1, a2)).

Lemma mult_aff_aux_sound_helper1 (a1 a2:  affine_form R) (val1 val2 v1 v2 q: R) (q1 qIHa: noise_type):
  q = (val1 - q1 * v1) * (val2 - q1 * v2) - qIHa * radius a1 * radius a2 ->
  Rabs (val1 - q1 * v1 - get_const a1) <= radius a1 ->
  Rabs (val2 - get_const a2) <= Rabs v2 + radius a2 ->
  Rabs (val1 * val2 - q1 * (v1 * get_const a2 + v2 * get_const a1) - q) <=
  (Rabs v1 + radius a1) * (Rabs v2 + radius a2).
Proof.
  intros Hq bounds1 bounds2.
  rewrite Hq.
  field_rewrite (val1 * val2 - q1 * (v1 * get_const a2 + v2 * get_const a1) - ((val1 - q1 * v1) * (val2 - q1 * v2) - qIHa * radius a1 * radius a2) = - q1 * v1 * q1 * v2 + q1 * v2 * (val1 - get_const a1) + q1 * v1 * (val2 - get_const a2) + qIHa * (radius a1 * radius a2)).
  field_rewrite ((Rabs v1 + radius a1) * (Rabs v2 + radius a2) = Rabs v1 * Rabs v2 + radius a1 * Rabs v2 + Rabs v1 * radius a2 + radius a1 * radius a2).
  eapply Rle_trans; try eapply Rabs_triang.
  eapply Rplus_le_compat.
  - field_rewrite (- q1 * v1 * q1 * v2 + q1 * v2 * (val1 - get_const a1) + q1 * v1 * (val2 - get_const a2) = q1 * v2 * (val1 - q1 * v1 - get_const a1) + q1 * v1 * (val2 - get_const a2)).
    field_rewrite (Rabs v1 * Rabs v2 + radius a1 * Rabs v2 + Rabs v1 * radius a2 = Rabs v2 * radius a1 + Rabs v1 * (Rabs v2 + radius a2)).
    eapply Rle_trans; try eapply Rabs_triang.
    pose proof (Rabs_bounded _ (v2 * (val1 - q1 * v1 - get_const a1)) (proj2_sig q1)) as H.
    pose proof (Rabs_bounded _ (v1 * (val2 - get_const a2)) (proj2_sig q1)) as H'.
    eapply Rplus_le_compat; eapply Rle_trans; try (rewrite Rmult_assoc; eauto);
      rewrite Rabs_mult; eauto using Rabs_pos, Rmult_le_compat_l.
  - eapply Rle_trans; try apply (Rabs_bounded _ (radius a1 * radius a2) (proj2_sig qIHa)).
    rewrite Rabs_mult.
    pose proof (radius_nonneg a1) as H1.
    pose proof (radius_nonneg a2) as H2.
    repeat rewrite Rabs_pos_eq; try lra.
Qed.

Lemma mult_aff_aux_sound_helper2 (a1 a2:  affine_form R) (val1 val2 v1 v2 q: R) (q1 qIHa: noise_type):
  q = val1 * (val2 - q1 * v2) - qIHa * (Rabs v1 + radius a1) * radius a2 ->
  Rabs (val1 - get_const a1) <= Rabs v1 + radius a1 ->
  Rabs (val1 * val2 - q1 * (v2 * get_const a1) - q) <=
  (Rabs v1 + radius a1) * (Rabs v2 + radius a2).
Proof.
  intros Hq bounds1.
  rewrite Hq.
  field_rewrite (val1 * val2 - q1 * (v2 * get_const a1) - (val1 * (val2 - q1 * v2) - qIHa * (Rabs v1 + radius a1) * radius a2) = q1 * v2 * (val1 - get_const a1) + qIHa * (Rabs v1 * radius a2) + qIHa * (radius a1 * radius a2)).
  field_rewrite ((Rabs v1 + radius a1) * (Rabs v2 + radius a2) = Rabs v2 * (Rabs v1 + radius a1) + Rabs v1 * radius a2 + radius a1 * radius a2).
  eapply Rle_trans; try eapply Rabs_triang.
  pose proof (Rabs_bounded _ (v2 * (val1 - get_const a1)) (proj2_sig q1)) as H.
  epose proof (Rabs_bounded _ _ (proj2_sig qIHa)).
  eapply Rplus_le_compat.
  - eapply Rle_trans; try eapply Rabs_triang.
    eapply Rplus_le_compat.
    + eapply Rle_trans.
      * rewrite Rmult_assoc.
        eauto.
      * rewrite Rabs_mult.
        apply Rmult_le_compat_l; eauto using Rabs_pos.
    + eapply Rle_trans; try eauto.
      rewrite Rabs_mult.
      rewrite (Rabs_pos_eq (radius a2) (radius_nonneg a2)).
      rewrite (Rabs_pos_eq (Rabs v1) (Rabs_pos v1)).
      lra.
  - eapply Rle_trans.
    apply (Rabs_bounded qIHa (radius a1 * radius a2) (proj2_sig qIHa)).
    rewrite Rabs_mult.
    pose proof (radius_nonneg a1) as H1.
    pose proof (radius_nonneg a2) as H2.
    repeat rewrite Rabs_pos_eq; lra.
Qed.

Lemma mult_aff_aux_sound_helper3 (a1 a2:  affine_form R) (val1 val2 v1 v2 q: R) (q1 qIHa: noise_type):
  q = (val1 - q1 * v1) * val2 - qIHa * radius a1 * (Rabs v2 + radius a2) ->
  Rabs (val2 - get_const a2) <= Rabs v2 + radius a2 ->
  Rabs (val1 * val2 - q1 * (v1 * get_const a2) - q) <=
  (Rabs v1 + radius a1) * (Rabs v2 + radius a2).
Proof.
  field_rewrite ((val1 - q1 * v1) * val2 - qIHa * radius a1 * (Rabs v2 + radius a2) = val2 * (val1 - q1 * v1) - qIHa * (Rabs v2 + radius a2) * radius a1).
  field_rewrite (val1 * val2 - q1 * (v1 * get_const a2) - q = val2 * val1 - q1 * (v1 * get_const a2) - q).
  field_rewrite ((Rabs v1 + radius a1) * (Rabs v2 + radius a2) = (Rabs v2 + radius a2) * (Rabs v1 + radius a1)).
  eauto using mult_aff_aux_sound_helper2.
Qed.

Lemma mult_aff_aux_sound (a1:  affine_form R) (a2:  affine_form R) (v1 v2: R) map:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  exists (q: noise_type), af_evals (mult_aff_aux (a1, a2)) (v1 * v2 - q * (radius a1) * (radius a2)) map.
Proof.
  revert v1 v2.
  remember (a1, a2) as a12.
  assert (fst a12 = a1 /\ snd a12 = a2) as Havals by now rewrite Heqa12.
  destruct Havals as [Heqa1 Heqa2].
  rewrite <- Heqa1, <- Heqa2.
  clear Heqa1 Heqa2 Heqa12 a1 a2.
  functional induction (mult_aff_aux a12); intros val1 val2 evals1 evals2; simpl in *.
  - unfold af_evals in *; simpl in *.
    exists noise_zero; rewrite evals1, evals2; ring.
  - apply af_evals_noise_compat in evals2 as [q2 [Hq2 evals2]].
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    apply constant_evals_unchanged in evals1.
    exists qIHa.
    unfold af_evals in *; simpl; rewrite Hq2.
    destruct (eval_aff (mult_aff_aux (Const v1, a2')) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    rewrite IHa; ring_simplify; rewrite evals1; ring.
  - (* Same as the previous case *)
    apply af_evals_noise_compat in evals1 as [q1 [Hq1 evals1]].
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    apply constant_evals_unchanged in evals2.
    exists qIHa.
    unfold af_evals in *; simpl; rewrite Hq1.
    destruct (eval_aff (mult_aff_aux (a1', Const v2)) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    rewrite IHa; ring_simplify; rewrite evals2; ring.
  - apply beq_nat_true in e2.
    rewrite <- e2 in *.
    pose proof (bounded_af_evals_abs _ _ _ evals2) as bounds2;
      simpl get_const in bounds2; simpl radius in bounds2.
    apply af_evals_noise_compat in evals1 as [q1 [Hq1 evals1]].
    apply af_evals_noise_compat in evals2 as [q2 [Hq2 evals2]].
    pose proof (bounded_af_evals_abs _ _ _ evals1) as bounds1;
      simpl get_const in bounds1; simpl radius in bounds1.
    assert (q1 = q2) as Heqq by congruence; rewrite <- Heqq in *.
    clear Heqq e2 q2.
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    unfold af_evals in IHa |-*; simpl; rewrite Hq1.
    destruct (eval_aff (mult_aff_aux (a1', a2')) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    pose proof (radius_nonneg a1') as nnrad1; pose proof (radius_nonneg a2') as nnrad2.
    pose proof (Rabs_pos v1) as nnabsv1; pose proof (Rabs_pos v2) as nnabsv2.
    destruct (Req_dec_sum (radius a1') 0) as [rad1 | rad1];
      destruct (Req_dec_sum (radius a2') 0) as [rad2 | rad2];
      destruct (Req_dec_sum v1 0) as [v1zero | v1zero];
      destruct (Req_dec_sum v2 0) as [v2zero | v2zero].
    all: try pose proof (radius_0_const _ _ _ evals1 rad1) as const1.
    all: try pose proof (radius_0_const _ _ _ evals2 rad2) as const2.
    all: try (exists noise_zero;
      simpl;
      rewrite IHa;
      try rewrite <- const1; try rewrite <- const2;
      try rewrite rad1; try rewrite rad2; try rewrite v1zero; try rewrite v2zero; simpl;
      ring).
    all: pose (needed_noise_expr := (val1 * val2 - q1 * (v1 * get_const a2' + v2 * get_const a1') - r) / ((Rabs v1 + radius a1') * (Rabs v2 + radius a2'))).
    all: pose (nominator := val1 * val2 - q1 * (v1 * get_const a2' + v2 * get_const a1') - r).
    all: fold nominator in needed_noise_expr.
    all: pose (denominator := (Rabs v1 + radius a1') * (Rabs v2 + radius a2')).
    all: fold denominator in needed_noise_expr.
    all: unfold needed_noise_expr.
    all: try pose proof (Rabs_pos_lt v1 v1zero) as posv1.
    all: try pose proof (Rabs_pos_lt v2 v2zero) as posv2.
    all: try assert (Rabs v1 + radius a1' > 0) as possum1 by lra.
    all: try assert (Rabs v2 + radius a2' > 0) as possum2 by lra.
    all: pose proof mult_aff_aux_sound_helper1 as noise_bounds.
    all: specialize (noise_bounds a1' a2' val1 val2 v1 v2 r q1 qIHa IHa bounds1 bounds2).
    all: apply Rabs_Rle_condition in noise_bounds.
    all: fold nominator denominator in noise_bounds.
    all: assert (0 < denominator) as denominator_pos by eauto using Rmult_0_lt_preserving.
    all: assert (1 * denominator = denominator) as tmp by lra; rewrite <- tmp in noise_bounds; clear tmp.
    all: rewrite <- (Rdiv_le_bounds nominator _ 1 denominator_pos) in noise_bounds.
    all: fold needed_noise_expr in noise_bounds.
    all: pose (needed_noise := exist (fun x => -1 <= x <= 1) needed_noise_expr noise_bounds).
    all: exists needed_noise; simpl.
    all: unfold needed_noise_expr, nominator, denominator.
    all: field; lra.
  - pose proof (bounded_af_evals_abs _ _ _ evals1) as bounds1;
      simpl get_const in bounds1; simpl radius in bounds1.
    pose proof (bounded_af_evals_abs _ _ _ evals2) as bounds2;
      simpl get_const in bounds2; simpl radius in bounds2.
    apply af_evals_noise_compat in evals2 as [q2 [Hq2 evals2]].
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    apply af_evals_noise_compat in evals1 as [q1 [Hq1 evals1]].
    unfold af_evals in IHa |-*; simpl; rewrite Hq2.
    pose proof (radius_nonneg a1') as nnrad1; pose proof (radius_nonneg a2') as nnrad2.
    pose proof (Rabs_pos v1) as nnabsv1; pose proof (Rabs_pos v2) as nnabsv2.
    destruct (eval_aff (mult_aff_aux (Noise n1 v1 a1', a2')) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    destruct (Req_dec_sum (radius a1') 0) as [rad1 | rad1];
      destruct (Req_dec_sum (radius a2') 0) as [rad2 | rad2];
      destruct (Req_dec_sum v1 0) as [v1zero | v1zero];
      destruct (Req_dec_sum v2 0) as [v2zero | v2zero].
    all: try pose proof (radius_0_const _ _ _ evals1 rad1) as const1.
    all: try pose proof (radius_0_const _ _ _ evals2 rad2) as const2.
    all: try (exists noise_zero;
      simpl;
      rewrite IHa;
      try rewrite <- const1; try rewrite <- const2;
      try rewrite rad1; try rewrite rad2; try rewrite v1zero; try rewrite v2zero; simpl;
      try ring_simplify;
      try rewrite Rabs_R0;
      ring).
    all: pose (needed_noise_expr := (val1 * val2 - q2 * (v2 * get_const a1') - r) / ((Rabs v1 + radius a1') * (Rabs v2 + radius a2'))).
    all: pose (nominator := val1 * val2 - q2 * (v2 * get_const a1') - r).
    all: fold nominator in needed_noise_expr.
    all: pose (denominator := (Rabs v1 + radius a1') * (Rabs v2 + radius a2')).
    all: fold denominator in needed_noise_expr.
    all: unfold needed_noise_expr.
    all: try pose proof (Rabs_pos_lt v1 v1zero) as posv1.
    all: try pose proof (Rabs_pos_lt v2 v2zero) as posv2.
    all: try assert (Rabs v1 + radius a1' > 0) as possum1 by lra.
    all: try assert (Rabs v2 + radius a2' > 0) as possum2 by lra.
    all: pose proof mult_aff_aux_sound_helper2 as noise_bounds.
    all: specialize (noise_bounds a1' a2' val1 val2 v1 v2 r q2 qIHa IHa bounds1).
    all: rewrite Rabs_Rle_condition in noise_bounds.
    all: fold nominator denominator in noise_bounds.
    all: assert (0 < denominator) as denominator_pos by eauto using Rmult_0_lt_preserving.
    all: assert (1 * denominator = denominator) as tmp by lra; rewrite <- tmp in noise_bounds; clear tmp.
    all: rewrite <- (Rdiv_le_bounds nominator _ 1 denominator_pos) in noise_bounds.
    all: fold needed_noise_expr in noise_bounds.
    all: pose (needed_noise := exist (fun x => -1 <= x <= 1) needed_noise_expr noise_bounds).
    all: exists needed_noise; simpl.
    all: unfold needed_noise_expr, nominator, denominator.
    all: field; lra.
  - (* Same as the previous case *)
    pose proof (bounded_af_evals_abs _ _ _ evals1) as bounds1;
      simpl get_const in bounds1; simpl radius in bounds1.
    pose proof (bounded_af_evals_abs _ _ _ evals2) as bounds2;
      simpl get_const in bounds2; simpl radius in bounds2.
    apply af_evals_noise_compat in evals1 as [q1 [Hq1 evals1]].
    specialize (IHa _ _ evals1 evals2) as [qIHa IHa].
    apply af_evals_noise_compat in evals2 as [q2 [Hq2 evals2]].
    unfold af_evals in IHa |-*; simpl; rewrite Hq1.
    pose proof (radius_nonneg a1') as nnrad1; pose proof (radius_nonneg a2') as nnrad2.
    pose proof (Rabs_pos v1) as nnabsv1; pose proof (Rabs_pos v2) as nnabsv2.
    destruct (eval_aff (mult_aff_aux (a1', Noise n2 v2 a2')) map) eqn: Hm; try contradiction.
    simpl in IHa |-*.
    destruct (Req_dec_sum (radius a1') 0) as [rad1 | rad1];
      destruct (Req_dec_sum (radius a2') 0) as [rad2 | rad2];
      destruct (Req_dec_sum v1 0) as [v1zero | v1zero];
      destruct (Req_dec_sum v2 0) as [v2zero | v2zero].
    all: try pose proof (radius_0_const _ _ _ evals1 rad1) as const1.
    all: try pose proof (radius_0_const _ _ _ evals2 rad2) as const2.
    all: try (exists noise_zero;
      simpl;
      rewrite IHa;
      try rewrite <- const1; try rewrite <- const2;
      try rewrite rad1; try rewrite rad2; try rewrite v1zero; try rewrite v2zero; simpl;
      try ring_simplify;
      try rewrite Rabs_R0;
      ring).
    all: pose (needed_noise_expr := (val1 * val2 - q1 * (v1 * get_const a2') - r) / ((Rabs v1 + radius a1') * (Rabs v2 + radius a2'))).
    all: pose (nominator := val1 * val2 - q1 * (v1 * get_const a2') - r).
    all: fold nominator in needed_noise_expr.
    all: pose (denominator := (Rabs v1 + radius a1') * (Rabs v2 + radius a2')).
    all: fold denominator in needed_noise_expr.
    all: unfold needed_noise_expr.
    all: try pose proof (Rabs_pos_lt v1 v1zero) as posv1.
    all: try pose proof (Rabs_pos_lt v2 v2zero) as posv2.
    all: try assert (Rabs v1 + radius a1' > 0) as possum1 by lra.
    all: try assert (Rabs v2 + radius a2' > 0) as possum2 by lra.
    all: pose proof mult_aff_aux_sound_helper3 as noise_bounds.
    all: specialize (noise_bounds a1' a2' val1 val2 v1 v2 r q1 qIHa IHa bounds2).
    all: rewrite Rabs_Rle_condition in noise_bounds.
    all: fold nominator denominator in noise_bounds.
    all: assert (denominator > 0) as denominator_pos by eauto using Rmult_nonneg.
    all: assert (1 * denominator = denominator) as tmp by lra; rewrite <- tmp in noise_bounds; clear tmp.
    all: rewrite <- (Rdiv_le_bounds nominator _ 1 denominator_pos) in noise_bounds.
    all: fold needed_noise_expr in noise_bounds.
    all: pose (needed_noise := exist (fun x => -1 <= x <= 1) needed_noise_expr noise_bounds).
    all: exists needed_noise; simpl.
    all: unfold needed_noise_expr, nominator, denominator.
    all: field; lra.
Qed.

Lemma bounded_value_mult_aff_aux (a1:  affine_form R) (a2:  affine_form R) (v1 v2: R) map v_aux:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  eval_aff (mult_aff_aux (a1, a2)) map = Some v_aux ->
  Rabs (v1 * v2 - v_aux) <= (radius a1 * radius a2).
Proof.
  intros evals1 evals2 evalsm.
  pose proof (mult_aff_aux_sound a1 a2 v1 v2 map evals1 evals2) as boundm.
  destruct boundm as [q boundm].
  unfold af_evals in boundm; rewrite evalsm in boundm; rewrite boundm.
  field_rewrite (v1 * v2 - (v1 * v2 - q * radius a1 * radius a2) = q * (radius a1 * radius a2)).
  eapply Rle_trans; try apply (Rabs_bounded _ (radius a1 * radius a2) (proj2_sig q)).
  rewrite Rabs_mult.
  repeat rewrite Rabs_pos_eq; eauto using radius_nonneg; try lra.
Qed.

Lemma eval_updMap_compat a map n q:
  fresh n a ->
  eval_aff a map = eval_aff a (updMap map n q).
Proof.
  intros freshn.
  induction a.
  - trivial.
  - pose proof (fresh_noise_gt freshn) as ngt.
    apply fresh_noise_compat in freshn.
    specialize (IHa freshn).
    simpl.
    rewrite IHa.
    unfold updMap.
    assert (~ n0 = n) as neqn by lia.
    apply Nat.eqb_neq in neqn.
    now rewrite neqn.
Qed.

Lemma mult_aff_aux_fresh_compat a1 a2 n:
  fresh n a1 ->
  fresh n a2 ->
  fresh n (mult_aff_aux (a1, a2)).
Proof.
  remember (a1, a2) as a12.
  assert (fst a12 = a1 /\ snd a12 = a2) as Havals by now rewrite Heqa12.
  destruct Havals as [Heqa1 Heqa2].
  rewrite <- Heqa1, <- Heqa2.
  clear Heqa1 Heqa2 Heqa12 a1 a2.
  intros fresh1 fresh2.
  functional induction (mult_aff_aux a12); simpl in *; eauto using fresh_noise_gt, fresh_noise_compat, fresh_noise.
  pose proof (fresh_noise_gt fresh1) as freshn1.
  apply fresh_noise_compat in fresh1.
  pose proof (fresh_noise_gt fresh2) as freshn2.
  apply fresh_noise_compat in fresh2.
  specialize (IHa fresh1 fresh2).
  now apply (fresh_noise _ freshn1).
Qed.

Lemma mult_aff_sound (a1:  affine_form R) (a2:  affine_form R) (v1 v2: R) map (n: nat):
  fresh n a1 ->
  fresh n a2 ->
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  exists q, af_evals (mult_aff a1 a2 n) (v1 * v2) (updMap map n q).
Proof.
  intros fresh1 fresh2 evals1 evals2.
  unfold mult_aff.
  pose proof (mult_aff_aux_sound a1 a2 v1 v2 map evals1 evals2) as [q bounds].
  exists q.
  unfold af_evals in *.
  assert (af_evals (mult_aff_aux (a1, a2)) (v1 * v2 - q * radius a1 * radius a2) (updMap map n q))
    as Heq.
  { pose proof (mult_aff_aux_fresh_compat fresh1 fresh2) as freshmult.
    pose proof (eval_updMap_compat map q freshmult) as Heq'.
    rewrite Heq' in bounds.
    unfold af_evals.
    Flover_compute.
  }
  destruct (Req_dec_sum (radius a1 * radius a2) 0) as [H | H]; simpl.
  all: try rewrite upd_sound.
  all: unfold af_evals in Heq.
  all: (destruct (eval_aff (mult_aff_aux (a1, a2)) (updMap map n q)) eqn: Hn; try contradiction).
  all: simpl in *.
  all: try replace (q * radius a1 * radius a2) with 0 in Heq by (rewrite Rmult_assoc; rewrite H; lra).
  all: rewrite Heq.
  all: try rewrite H2.
  all: lra.
Qed.

(********************************************)
(*************** Subtraction ****************)
(********************************************)

Definition mult_aff_const (x:  affine_form R) (a: R):  affine_form R :=
  mult_aff x (Const a) (get_max_index x + 1).

Lemma mult_aff_const_sound (af:  affine_form R) map v c:
  af_evals af v map ->
  af_evals (mult_aff_const af c) (v * c) map.
Proof.
  intros evals1.
  assert (af_evals (Const c) c map) as evals2.
  { unfold af_evals; now simpl. }
  unfold mult_aff_const, mult_aff.
  assert (radius (Const c) = 0) as rad2 by now simpl.
  assert (radius af * radius (Const c) = 0) as rad by (rewrite rad2; lra).
  destruct (Req_dec_sum (radius af * radius (Const c)) 0) as [H | H] eqn: Hr'; try contradiction.
  assert (fresh (get_max_index af + 1) af) as fresh1 by (unfold fresh; omega).
  assert (fresh (get_max_index af + 1) (Const c)) as fresh2
      by (unfold fresh, get_max_index; simpl; omega).
  pose proof (mult_aff_sound _ _ _ fresh1 fresh2 evals1 evals2) as [q evalsm].
  unfold af_evals in evalsm |-*.
  pose proof (mult_aff_aux_fresh_compat fresh1 fresh2) as freshm.
  pose proof (eval_updMap_compat map q freshm) as mapcompat.
  unfold mult_aff in evalsm.
  rewrite Hr' in evalsm.
  Flover_compute.
Qed.

Definition plus_aff_const (x:  affine_form R) (a: R):  affine_form R :=
  plus_aff x (Const a).

Lemma plus_aff_const_sound (af:  affine_form R) map v c:
  af_evals af v map ->
  af_evals (plus_aff_const af c) (v + c) map.
Proof.
  intros evals1.
  assert (af_evals (Const c) c map) as evals2.
  { unfold af_evals; now simpl. }
  unfold plus_aff_const.
  apply (plus_aff_sound _ _ _ _ _ evals1 evals2).
Qed.

Definition negate_aff a:  affine_form R :=
  mult_aff_const a (-1).

Lemma negate_aff_sound a v map:
  af_evals a v map ->
  af_evals (negate_aff a) (-v) map.
Proof.
  intros evals.
  pose proof (mult_aff_const_sound _ _ _ (-1) evals) as evalsm.
  unfold af_evals, negate_aff in evalsm |-*.
  Flover_compute.
  replace r0 with r by congruence; lra.
Qed.

Definition subtract_aff a1 a2:  affine_form R :=
  plus_aff a1 (negate_aff a2).

Lemma subtract_aff_sound a1 a2 v1 v2 map:
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  af_evals (subtract_aff a1 a2) (v1 - v2) map.
Proof.
  intros evals1 evals2.
  unfold subtract_aff.
  pose proof (negate_aff_sound _ _ _ evals2) as evalsm.
  pose proof (plus_aff_sound _ _ _ _ _ evals1 evalsm) as evalsp. 
  unfold af_evals in evalsp |-*.
  Flover_compute.
  replace r0 with r by congruence; lra.
Qed.

(********************************************)
(******** Inverse of an affine form *********)
(********************************************)

(* Concerning the inverse of an affine form x lying in [a; b], it *)
(* should be alpha x + beta +- delta, with (explanation should be in the *)
(* annex of my report): *)

(*   alpha = -1/b^2 *)
(*   beta = 0.5(1/a+1/b) - 0.5 alpha (a + b) *)
(*   delta = 0.5(1/a-1/b - alpha (a - b) *)

(* *This is the case when 0 < a < b* *)

(* Then, if v \in x, to prove that 1/v in 1/x, the new noise term *)
(* should be 1/delta ( 1/v - alpha * v - beta ) *)

Definition inverse_aff (x:  affine_form R) (n: nat):  affine_form R :=
  let ivx := toInterval x in
  let a := RminAbsFun ivx in
  let b := RmaxAbsFun ivx in
  let alpha := - (1/(b*b)) in
  let beta := (1/a + 1/b - alpha * (a+b)) / 2 in
  let delta := (1/a - 1/b - alpha * (a-b)) / 2 in
  let alphax := mult_aff_const x alpha in
  if (Rlt_dec (IVhi ivx) 0) then
    Noise n delta (plus_aff_const alphax (-beta))
  else
    Noise n delta (plus_aff_const alphax beta).

Lemma inverse_aff_sound (a:  affine_form R) (v: R) map n:
  fresh n a ->
  above_zero a \/ below_zero a ->
  af_evals a v map ->
  exists q, af_evals (inverse_aff a n) (1/v) (updMap map n q).
Proof.
  intros freshn bounds evals.
  pose proof (valid_to_interval a) as validiv; unfold valid in validiv.
  pose proof (to_interval_containment _ _ _ evals) as containv.
  unfold inverse_aff, af_evals, toInterval; simpl.
  destruct bounds as [above | below]; unfold above_zero, below_zero in *.
  - pose proof (above_IVhi above) as above'.
    apply Rgt_lt in above'.
    simpl in above, above', containv.
    assert (~ ((get_const a + radius a)) < 0) as not_below by lra.
    destruct Rlt_dec as [H | _]; try contradiction.
    simpl.
    pose (alpha := (- (1 / (RmaxAbsFun (get_const a - radius a, get_const a + radius a) *
                       RmaxAbsFun (get_const a - radius a, get_const a + radius a))))).
    fold alpha.
    pose (beta := ((1 / RminAbsFun (get_const a - radius a, get_const a + radius a) +
                   1 / RmaxAbsFun (get_const a - radius a, get_const a + radius a) -
                   alpha *
                   (RminAbsFun (get_const a - radius a, get_const a + radius a) +
                    RmaxAbsFun (get_const a - radius a, get_const a + radius a))) / 
                  2)).
    fold beta.
    pose (delta := ((1 / RminAbsFun (get_const a - radius a, get_const a + radius a) -
                   1 / RmaxAbsFun (get_const a - radius a, get_const a + radius a) -
                   alpha *
                   (RminAbsFun (get_const a - radius a, get_const a + radius a) -
                    RmaxAbsFun (get_const a - radius a, get_const a + radius a))) / 
                  2)).
    fold delta.
    destruct (Req_dec (radius a) 0) as [Hrad | Hrad].
    + exists noise_zero.
      assert (af_evals a v map) as constev by assumption.
      apply radius_0_const in constev; try assumption.
      rewrite upd_sound.
      pose proof (eval_updMap_compat map noise_zero freshn) as Heq'.
      unfold af_evals in evals.
      rewrite Heq' in evals.
      fold af_evals in evals.
      pose proof (mult_aff_const_sound a _ v alpha evals) as evalsm.
      pose proof (plus_aff_const_sound (mult_aff_const a alpha) _ _ beta evalsm) as evalsp.
      unfold af_evals in evalsp.
      destruct (eval_aff (plus_aff_const (mult_aff_const a alpha) beta) (updMap map n noise_zero)); try contradiction; simpl in evalsp.
      simpl.
      rewrite evalsp; clear evalsm evalsp Heq'.
      ring_simplify.
      unfold beta, alpha.
      unfold RminAbsFun, RmaxAbsFun.
      simpl fst; simpl snd.
      rewrite Rabs_pos_eq; try (simpl; lra).
      rewrite Rmin_left, Rmax_right; try (rewrite Rabs_pos_eq; lra).
      rewrite Rabs_pos_eq; try lra.
      rewrite Hrad, <- constev.
      field_simplify; try lra.
      field_simplify; try lra.
    + pose (needed_expr := (1/v - alpha * v - beta) / delta).
      assert (0 < radius a) as radgtzero by lra.
      unfold RminAbsFun, RmaxAbsFun in *.
      simpl fst in *; simpl snd in *.
      assert (get_const a - radius a < get_const a + radius a) as intervalnotpoint by lra.
      assert (0 < v) as vpos by lra.
      enough (-1 <= needed_expr <= 1) as noise_bounds.
      * pose (needed_noise := exist (fun x => -1 <= x <= 1) needed_expr noise_bounds).
        exists needed_noise.
        rewrite upd_sound.
        pose proof (eval_updMap_compat map needed_noise freshn) as Heq'.
        unfold af_evals in evals.
        rewrite Heq' in evals.
        fold af_evals in evals.
        pose proof (mult_aff_const_sound a _ v alpha evals) as evalsm.
        pose proof (plus_aff_const_sound (mult_aff_const a alpha) _ _ beta evalsm) as evalsp.
        unfold af_evals in evalsp.
        destruct (eval_aff (plus_aff_const (mult_aff_const a alpha) beta) (updMap map n needed_noise)); try contradiction; simpl in evalsp.
        simpl.
        rewrite evalsp; clear evalsm evalsp Heq'.
        unfold needed_expr.
        field_simplify; try lra.
        split; try lra.
        unfold delta, alpha.
        rewrite Rabs_pos_eq; try (simpl; lra).
        rewrite Rmin_left, Rmax_right; try (rewrite Rabs_pos_eq; lra).
        rewrite Rabs_pos_eq; try lra.
        field_rewrite (get_const a - radius a - (get_const a + radius a) = (-2) * radius a).
        assert (get_const a > radius a) as constrad by lra.
        pose proof (Rpos_1_div_pos _ above) as frac1.
        pose proof (Rpos_1_div_pos _ above') as frac2.
        pose proof (Rlt_inverse_lt _ _ above' above intervalnotpoint) as inversebounds.
        assert (1 / (get_const a - radius a) - 1 / (get_const a + radius a) > 0) as bounds1 by lra.
        field_rewrite (- (1 / ((get_const a + radius a) * (get_const a + radius a))) * (- 2 * radius a) = (2 * radius a) / ((get_const a + radius a) * (get_const a + radius a))).
        field_rewrite (1 / (get_const a - radius a) - 1 / (get_const a + radius a) = (2 * radius a) / ((get_const a - radius a) * (get_const a + radius a))).
        field_rewrite ((2 * radius a / ((get_const a - radius a) * (get_const a + radius a)) - 2 * radius a / ((get_const a + radius a) * (get_const a + radius a))) / 2 = (radius a / ((get_const a - radius a) * (get_const a + radius a)) - radius a / ((get_const a + radius a) * (get_const a + radius a)))).
        intros zeroeq.
        field_simplify_eq in zeroeq; try field_simplify; try lra.
        apply Rmult_integral in zeroeq.
        destruct zeroeq as [ zeroeq | zeroeq]; try lra.
        apply Rmult_integral in zeroeq; destruct zeroeq; lra.
      * (* Next line is technical *)
        replace (IZR (Zneg xH)) with (Ropp (1)) by lra.
        rewrite <- Rabs_Rle_condition.
        unfold needed_expr, delta, beta, alpha.
        rewrite (Rabs_pos_eq _ (Rlt_le _ _ above)); try (simpl; lra).
        rewrite (Rabs_pos_eq _ (Rlt_le _ _ above')); try (simpl; lra).
        rewrite Rmin_left, Rmax_right; try (apply Rlt_le; lra).
        pose (c := get_const a).
        pose (r := radius a).
        fold c r in above', above, intervalnotpoint, containv, not_below, Hrad, radgtzero |-*.
        field_rewrite (c - r - (c + r) = (-2 * r)).
        field_rewrite (- (1 / ((c + r) * (c + r))) * (- 2 * r) = ((2 * r) / ((c + r) * (c + r)))).
        field_rewrite (1 / (c - r) - 1 / (c + r) = (2 * r) / ((c - r) * (c + r))).
        field_rewrite ((2 * r / ((c - r) * (c + r)) - 2 * r / ((c + r) * (c + r))) / 2 = (r / ((c - r) * (c + r)) - r / ((c + r) * (c + r)))).
        field_rewrite (c - r + (c + r) = 2 * c).
        field_rewrite (1 / (c - r) + 1 / (c + r) = (2 * c) / ((c - r) * (c + r))).
        field_rewrite (1 / v - - (1 / ((c + r) * (c + r))) * v = 1 / v + (v / ((c + r) * (c + r)))).
        field_rewrite ((2 * c / ((c - r) * (c + r)) -
       - (1 / ((c + r) * (c + r))) * (2 * c)) = (2 * c / ((c - r) * (c + r)) + ((2 * c) / ((c + r) * (c + r))))).
        field_rewrite ((2 * c / ((c - r) * (c + r)) + 2 * c / ((c + r) * (c + r))) / 2 = (c / ((c - r) * (c + r)) + c / ((c + r) * (c + r)))).
        field_rewrite ((c / ((c - r) * (c + r)) + c / ((c + r) * (c + r))) = (2 * c * c / ((c - r) * (c + r) * (c + r)))).
        field_rewrite (1 / v + v / ((c + r) * (c + r)) = ((c + r) * (c + r) + v * v) / (v * (c + r) * (c + r))).
        field_rewrite (((c + r) * (c + r) + v * v) / (v * (c + r) * (c + r)) = ((c + r) * (c + r) * (c - r) + v * v * (c - r)) / (v * (c + r) * (c + r) * (c - r))).
        field_rewrite (2 * c * c / ((c - r) * (c + r) * (c + r)) = 2 * v * c * c / (v * (c - r) * (c + r) * (c + r))).
        field_rewrite ((((c + r) * (c + r) * (c - r) + v * v * (c - r)) / (v * (c + r) * (c + r) * (c - r)) - 2 * v * c * c / (v * (c - r) * (c + r) * (c + r))) = (((c + r) * (c + r) * (c - r) + v * v * (c - r) - 2 * v * c * c) / (v * (c - r) * (c + r) * (c + r)))).
        field_rewrite ((r / ((c - r) * (c + r)) - r / ((c + r) * (c + r))) = (2 * r * r / ((c - r) * (c + r) * (c + r)))).
        assert ((c - r) * (c + r) * (c + r) > 0) as prodnonzero1
            by (field_rewrite (0 = 0 * (c + r) * (c + r)); eapply Rmult_lt_compat_r; try assumption;
                eapply Rmult_lt_compat_r; try assumption).
        assert (2 * r * r > 0) as prodnonzero2
            by (field_rewrite (0 = 0 * r * r); eapply Rmult_lt_compat_r; try lra;
                eapply Rmult_lt_compat_r; try lra).
        assert ((2 * r * r / ((c - r) * (c + r) * (c + r))) > 0) as divnonzero
          by (apply Rdiv_pos; lra).
        rewrite (Rdiv_abs_le_bounds _ _ _ divnonzero).
        assert (v * (c - r) * (c + r) * (c + r) > 0) as prodnonzero3
            by (field_rewrite (0 = 0 * (c - r) * (c + r) * (c + r)); eapply Rmult_lt_compat_r; try lra;
                eapply Rmult_lt_compat_r; try lra; eapply Rmult_lt_compat_r; try lra).
        rewrite (Rdiv_abs_le_bounds _ _ _ prodnonzero3).
        rewrite Rmult_1_l.
        field_rewrite ((2 * r * r / ((c - r) * (c + r) * (c + r))) * (v * (c - r) * (c + r) * (c + r)) = (2 * r * r * v)).
        assert (2 * r * r * v > 0) as prodnonzero4
            by (field_rewrite (0 = 0 * r * r * v); eapply Rmult_lt_compat_r; try lra;
                eapply Rmult_lt_compat_r; try lra; eapply Rmult_lt_compat_r; try lra).
        field_rewrite ((c + r) * (c + r) * (c - r) + v * v * (c - r) = ((c + r) * (c + r) + v * v) * (c - r)).
        rewrite Rabs_Rle_condition.
        split.
        -- rewrite Rle_minus_iff.
           rewrite Ropp_involutive.
           field_rewrite (((c + r) * (c + r) + v * v) * (c - r) - 2 * v * c * c + 2 * r * r * v = ((c + r) * (c + r) + v * v - 2 * v * (c + r)) * (c - r)).
           apply Rmult_le_pos; try lra.
           field_rewrite ((c + r) * (c + r) + v * v - 2 * v * (c + r) = (c + r - v) * (c + r - v)).
           apply Rmult_le_pos; try lra.
        -- rewrite Rminus_le_iff.
           field_rewrite (((c + r) * (c + r) + v * v) * (c - r) - 2 * v * c * c + - (2 * r * r * v) = ((c + r - v) * (c + r - v)) * (c - r) + - (((2 * r) * (2 * r)) * v)).
           rewrite <- Rminus_le_iff.
           destruct (Req_dec (c + r) v) as [vbound | vbound].
           ** rewrite <- vbound.
              field_rewrite ((c + r - (c + r)) * (c + r - (c + r)) * (c - r) = 0).
              field_rewrite (2 * r * (2 * r) * (c + r) = (4 * r * r) * (c + r)).
              apply Rmult_le_pos; lra.
           ** apply Rmult_piecewise_le; try apply Rmult_nonneg; try lra.
              apply Rmult_piecewise_le; try lra.
  - pose proof (below_IVlo below) as below'.
    simpl in below, below', containv.
    assert (get_const a + radius a < 0) as below_bool by lra.
    destruct Rlt_dec as [_ | H]; try contradiction.
    simpl.
    pose (alpha := (- (1 / (RmaxAbsFun (get_const a - radius a, get_const a + radius a) *
                       RmaxAbsFun (get_const a - radius a, get_const a + radius a))))).
    fold alpha.
    pose (beta := ((1 / RminAbsFun (get_const a - radius a, get_const a + radius a) +
                   1 / RmaxAbsFun (get_const a - radius a, get_const a + radius a) -
                   alpha *
                   (RminAbsFun (get_const a - radius a, get_const a + radius a) +
                    RmaxAbsFun (get_const a - radius a, get_const a + radius a))) / 
                  2)).
    fold beta.
    pose (delta := ((1 / RminAbsFun (get_const a - radius a, get_const a + radius a) -
                   1 / RmaxAbsFun (get_const a - radius a, get_const a + radius a) -
                   alpha *
                   (RminAbsFun (get_const a - radius a, get_const a + radius a) -
                    RmaxAbsFun (get_const a - radius a, get_const a + radius a))) / 
                  2)).
    fold delta.
    destruct (Req_dec (radius a) 0) as [Hrad | Hrad].
    + exists noise_zero.
      assert (af_evals a v map) as constev by assumption.
      apply radius_0_const in constev; try assumption.
      rewrite upd_sound.
      pose proof (eval_updMap_compat map noise_zero freshn) as Heq'.
      unfold af_evals in evals.
      rewrite Heq' in evals.
      fold af_evals in evals.
      pose proof (mult_aff_const_sound a _ v alpha evals) as evalsm.
      pose proof (plus_aff_const_sound (mult_aff_const a alpha) _ _ (- beta) evalsm) as evalsp.
      unfold af_evals in evalsp.
      destruct (eval_aff (plus_aff_const (mult_aff_const a alpha) (- beta)) (updMap map n noise_zero)); try contradiction; simpl in evalsp.
      simpl.
      rewrite evalsp; clear evalsm evalsp Heq'.
      ring_simplify.
      unfold beta, alpha.
      unfold RminAbsFun, RmaxAbsFun.
      simpl fst; simpl snd.
      rewrite Rabs_left; try (simpl; lra).
      rewrite Rmin_left, Rmax_right; try (rewrite Rabs_left; lra).
      rewrite Rabs_left; try lra.
      rewrite Hrad, <- constev.
      field_simplify; try lra.
      field_simplify; try lra.
    + pose (needed_expr := (1/v - alpha * v + beta) / delta).
      assert (radius a > 0) as radgtzero by lra.
      unfold RminAbsFun, RmaxAbsFun in *.
      simpl fst in *; simpl snd in *.
      assert (get_const a - radius a < get_const a + radius a) as intervalnotpoint by lra.
      assert (v < 0) as vpos by lra.
      enough (-1 <= needed_expr <= 1) as noise_bounds.
      * pose (needed_noise := exist (fun x => -1 <= x <= 1) needed_expr noise_bounds).
        exists needed_noise.
        rewrite upd_sound.
        pose proof (eval_updMap_compat map needed_noise freshn) as Heq'.
        unfold af_evals in evals.
        rewrite Heq' in evals.
        fold af_evals in evals.
        pose proof (mult_aff_const_sound a _ v alpha evals) as evalsm.
        pose proof (plus_aff_const_sound (mult_aff_const a alpha) _ _ (-beta) evalsm) as evalsp.
        unfold af_evals in evalsp.
        destruct (eval_aff (plus_aff_const (mult_aff_const a alpha) (-beta)) (updMap map n needed_noise)); try contradiction; simpl in evalsp.
        simpl.
        rewrite evalsp; clear evalsm evalsp Heq'.
        unfold needed_expr.
        field_simplify; try lra.
        split; try lra.
        unfold delta, alpha.
        rewrite Rabs_left; try (simpl; lra).
        rewrite Rabs_left; try (simpl; lra).
        rewrite Rmin_right, Rmax_left; try lra.
        field_rewrite (- (get_const a + radius a) - - (get_const a - radius a) = (-2) * radius a).
        assert (get_const a < radius a) as constrad by lra.
        assert (-(get_const a + radius a) > 0) as neg_below by lra.
        assert (-(get_const a - radius a) > 0) as neg_below' by lra.
        pose proof (Rpos_1_div_pos _ neg_below) as frac1.
        pose proof (Rpos_1_div_pos _ neg_below') as frac2.
        assert (-(get_const a - radius a) > -(get_const a + radius a)) as intervalnotpoint' by lra.
        pose proof (Rlt_inverse_lt _ _ neg_below' neg_below intervalnotpoint') as inversebounds.
        assert (1 / - (get_const a - radius a) - 1 / - (get_const a + radius a) < 0) as bounds1 by lra.
        field_rewrite (- (1 / (- (get_const a - radius a) * - (get_const a - radius a))) * (- 2 * radius a) = ((2 * radius a) / ((get_const a - radius a) * (get_const a - radius a)))).
        field_rewrite (1 / - (get_const a + radius a) - 1 / - (get_const a - radius a) = (2 * radius a) / ((get_const a - radius a) * (get_const a + radius a))).
        field_rewrite ((2 * radius a / ((get_const a - radius a) * (get_const a + radius a)) -
   2 * radius a / ((get_const a - radius a) * (get_const a - radius a))) / 
  2 = (radius a / ((get_const a - radius a) * (get_const a + radius a)) -
   radius a / ((get_const a - radius a) * (get_const a - radius a)))).
        intros zeroeq.
        field_simplify_eq in zeroeq; try field_simplify; try lra.
        apply Rmult_integral in zeroeq.
        destruct zeroeq as [ zeroeq | zeroeq]; try lra.
        unfold Qpower, Qpower_positive, pow_pos in zeroeq.
        apply Rmult_integral in zeroeq; destruct zeroeq; lra.
      * (* Next line is technical *)
        replace (IZR (Zneg xH)) with (Ropp (1)) by lra.
        rewrite <- Rabs_Rle_condition.
        unfold needed_expr, delta, beta, alpha.
        rewrite (Rabs_left _ below); try (simpl; lra).
        rewrite (Rabs_left _ below'); try (simpl; lra).
        rewrite Rmin_right, Rmax_left; try (apply Rlt_le; lra).
        pose (c := get_const a).
        pose (r := radius a).
        fold c r in below', below, intervalnotpoint, containv, Hrad, radgtzero |-*.
        field_rewrite (-(c + r) + - (c - r) = (-2) * c).
        field_rewrite (- (1 / (- (c - r) * - (c - r))) * (- 2 * c) = ((2 * c) / ((c - r) * (c - r)))).
        field_rewrite (1 / - (c + r) + 1 / - (c - r) = - (2 * c) / ((c - r) * (c + r))).
        field_rewrite ((- (2 * c) / ((c - r) * (c + r)) - 2 * c / ((c - r) * (c - r))) / 2 = (-c / ((c - r) * (c + r)) - c / ((c - r) * (c - r)))).
        field_rewrite (- (c + r) - - (c - r) = -2 * r).
        field_rewrite (1 / - (c + r) - 1 / - (c - r) = (2 * r) / ((c - r) * (c + r))).
        field_rewrite (1 / v - - (1 / (- (c - r) * - (c - r))) * v = 1 / v + (v / ((c - r) * (c - r)))).
        field_rewrite ((2 * r / ((c - r) * (c + r)) - - (1 / (- (c - r) * - (c - r))) * (- 2 * r)) = (2 * r / ((c - r) * (c + r)) - ((2 * r) / ((c - r) * (c - r))))).
        field_rewrite ((2 * r / ((c - r) * (c + r)) - 2 * r / ((c - r) * (c - r))) / 2 = (r / ((c - r) * (c + r)) - r / ((c - r) * (c - r)))).
        field_rewrite ((- c / ((c - r) * (c + r)) - c / ((c - r) * (c - r))) = (-2 * c * c / ((c - r) * (c - r) * (c + r)))).
        field_rewrite (1 / v + v / ((c - r) * (c - r)) = ((c - r) * (c - r) + v * v) / (v * (c - r) * (c - r))).
        field_rewrite (((c - r) * (c - r) + v * v) / (v * (c - r) * (c - r)) = ((c - r) * (c - r) * (c + r) + v * v * (c + r)) / (v * (c - r) * (c - r) * (c + r))).
        field_rewrite (- 2 * c * c / ((c - r) * (c - r) * (c + r)) = - 2 * v * c * c / (v * (c - r) * (c - r) * (c + r))).
        field_rewrite ((((c - r) * (c - r) * (c + r) + v * v * (c + r)) / (v * (c - r) * (c - r) * (c + r)) + - 2 * v * c * c / (v * (c - r) * (c - r) * (c + r))) = (((c - r) * (c - r) * (c + r) + v * v * (c + r) - 2 * v * c * c) / (v * (c - r) * (c - r) * (c + r)))).
        field_rewrite ((r / ((c - r) * (c + r)) - r / ((c - r) * (c - r))) = (- 2 * r * r / ((c - r) * (c - r) * (c + r)))).
        field_rewrite (- 2 * r * r / ((c - r) * (c - r) * (c + r)) = - 2 * v * r * r / (v * (c - r) * (c - r) * (c + r))).
        assert (v * (c - r) * ((c - r) * (c + r)) > 0) as prodnonzero1
            by (apply Rmult_nonneg with (x := v * (c - r)); apply Rmult_neg_nonneg; assumption). 
        assert (- 2 * v * r * r > 0) as prodnonzero2
            by (field_rewrite (0 = 0 * r * r); eapply Rmult_lt_compat_r; try lra;
                eapply Rmult_lt_compat_r; try lra).
        assert ((- 2 * v * r * r / (v * (c - r) * (c - r) * (c + r))) > 0) as divnonzero
          by (apply Rdiv_pos; lra).
        rewrite (Rdiv_abs_le_bounds _ _ _ divnonzero).
        assert (v * (c - r) * (c - r) * (c + r) > 0) as prodnonzero3 by lra.
        rewrite (Rdiv_abs_le_bounds _ _ _ prodnonzero3).
        rewrite Rmult_1_l.
        field_rewrite ((- 2 * v * r * r / (v * (c - r) * (c - r) * (c + r))) * (v * (c - r) * (c - r) * (c + r)) = (- 2 * r * r * v)).
        field_rewrite ((c - r) * (c - r) * (c + r) + v * v * (c + r) = ((c - r) * (c - r) + v * v) * (c + r)).
        rewrite Rabs_Rle_condition.
        split.
        -- rewrite Rle_minus_iff.
           rewrite Ropp_involutive.
           field_rewrite (((c - r) * (c - r) + v * v) * (c + r) - 2 * v * c * c + - 2 * r * r * v = (c - r - v) * ((c - r - v) * (c + r)) + - ((2 * r) * ((2 * r) * v))).
           rewrite <- Rle_minus_iff.
           destruct (Req_dec (c - r) v) as [vbound | vbound].
           ** rewrite <- vbound.
              field_rewrite ((c - r - (c - r)) * ((c - r - (c - r)) * (c + r)) = 0).
              field_rewrite (2 * r * (2 * r * (c - r)) = (4 * r * r) * (c - r)).
              assert (4 * r * r > 0) by (apply Rmult_nonneg; lra).
              field_rewrite (0 = 4 * r * r * 0).
              apply Rmult_le_compat_l; try lra.
           ** field_rewrite (2 * r * (2 * r * v) = --(2 * r * (2 * r * v))).
              field_rewrite (((c - r - v) * ((c - r - v) * (c + r))) = --((c - r - v) * ((c - r - v) * (c + r)))).
              apply Ropp_le_contravar.
              field_rewrite (- ((c - r - v) * ((c - r - v) * (c + r))) = -(c - r - v) * (-(c - r - v) * - (c + r))).
              field_rewrite (- (2 * r * (2 * r * v)) = 2 * r * (2 * r * -v)).
              apply Rmult_piecewise_le; try apply Rmult_nonneg; try lra.
              apply Rmult_piecewise_le; try lra.
        -- rewrite Rle_minus_iff.
           field_rewrite (- 2 * r * r * v + - (((c - r) * (c - r) + v * v) * (c + r) - 2 * v * c * c) = ((c - r) * (c - r) + v * v - 2 * v * (c - r)) * -(c + r)).
           apply Rmult_le_pos; try lra.
           field_rewrite ((c - r) * (c - r) + v * v - 2 * v * (c - r) = (c - r - v) * (c - r - v)).
           apply Rsquare_nonneg.
Qed.

(********************************************)
(***************** Division *****************)
(********************************************)

Lemma mult_aff_const_fresh_compat a c n:
  fresh n a ->
  fresh n (mult_aff_const a c).
Proof.
  intros fr.
  unfold mult_aff_const, mult_aff.
  simpl.
  replace (radius a * 0) with 0 by lra.
  destruct Req_dec_sum as [_ | H]; try contradiction.
  apply mult_aff_aux_fresh_compat; try assumption.
  unfold fresh, get_max_index in *.
  simpl; omega.
Qed.

Lemma plus_aff_const_fresh_compat a c n:
  fresh n a ->
  fresh n (plus_aff_const a c).
Proof.
  intros fr.
  unfold plus_aff_const, plus_aff.
  apply plus_aff_fresh_compat; try assumption.
  unfold fresh, get_max_index in *.
  simpl; omega.
Qed.

Lemma inverse_aff_fresh_compat a (n: nat):
  fresh n a ->
  fresh (n + 1) (inverse_aff a n).
Proof.
  intros fr.
  unfold inverse_aff.
  destruct (Rlt_dec (IVhi (toInterval a)) 0).
  Set Default Goal Selector "all".
  apply fresh_noise; try omega.
  apply plus_aff_const_fresh_compat.
  apply mult_aff_const_fresh_compat.
  now apply fresh_inc.
  Set Default Goal Selector "1".
Qed.

Lemma above_below_nonzero a v map:
  above_zero a \/ below_zero a ->
  af_evals a v map ->
  ~ v = 0.
Proof.
  intros bounds evals.
  destruct a.
  - unfold above_zero, below_zero in bounds.
    simpl in bounds.
    apply constant_evals_unchanged in evals.
    destruct bounds as [bound | bound]; simpl in bound; lra.
  - apply bounded_af_evals_abs in evals.
    simpl radius in evals.
    simpl get_const in evals.
    destruct bounds as [bound | bound].
    + unfold above_zero in bound; simpl in bound.
      assert (get_const a > Rabs r + radius a) as constbound by lra.
      destruct (Rlt_le_dec (v - get_const a) 0).
      * rewrite Rabs_left in evals; try lra.
      * rewrite Rabs_pos_eq in evals; try lra.
    + unfold below_zero in bound; simpl in bound.
      destruct (Rlt_le_dec (v - get_const a) 0).
      * rewrite Rabs_left in evals; try lra.
      * rewrite Rabs_pos_eq in evals; try lra.
Qed.

Definition divide_aff a1 a2 n :=
  mult_aff a1 (inverse_aff a2 n) (n + 1).

Lemma divide_aff_sound a1 a2 v1 v2 n map:
  fresh n a1 ->
  fresh n a2 ->
  above_zero a2 \/ below_zero a2 ->
  af_evals a1 v1 map ->
  af_evals a2 v2 map ->
  exists qI qM, af_evals (divide_aff a1 a2 n) (v1 / v2) (updMap (updMap map n qI) (n + 1) qM).
Proof.
  intros fresh1 fresh2 bounds2 evals1 evals2.
  pose proof inverse_aff_sound as inv.
  pose proof (above_below_nonzero _ bounds2 evals2) as v2nonzero.
  specialize (inv a2 v2 map n fresh2 bounds2 evals2) as [qInv evalsi].
  pose proof mult_aff_sound as mul.
  apply inverse_aff_fresh_compat in fresh2.
  unfold af_evals in evals1.
  rewrite (eval_updMap_compat _ qInv fresh1) in evals1.
  apply fresh_inc in fresh1.
  specialize (mul a1 (inverse_aff a2 n) v1 (1 / v2) (updMap map n qInv)
                  ((n + 1)%nat) fresh1 fresh2 evals1 evalsi) as [qMul evalsm].
  unfold af_evals, divide_aff in evalsm |-*.
  exists qInv, qMul.
  Flover_compute.
  - replace r1 with r by congruence; lra.
  - replace r0 with r by congruence; lra.
Qed.

Close Scope R.
