open simpLib realTheory realLib RealArith stringTheory;
open ml_translatorTheory ml_translatorLib cfTacticsLib basis basisProgTheory;
open AbbrevsTheory ExpressionsTheory RealSimpsTheory ExpressionAbbrevsTheory
     MachineTypeTheory
     ErrorBoundsTheory IntervalArithTheory FloverTactics IntervalValidationTheory
     EnvironmentsTheory CommandsTheory ssaPrgsTheory ErrorValidationTheory
     CertificateCheckerTheory floverParserTheory;
open preambleFloVer;

val _ = new_theory "trans";

val _ = translation_extends "basisProg";

(*
val _ = temp_overload_on("abs",``real$abs``);
val _ = temp_overload_on("max",``real$max``);
val _ = temp_overload_on("min",``real$min``);

(* TODO: move this to RatProgTheory *)
val _ = add_type_inv ``REAL_TYPE`` ``:(int # num)``;

(* translation of real_div *)
val _ = (next_ml_names := ["real_div"]);
val res = translate realTheory.real_div;
val real_div_side = prove(
  let val tm = hyp res |> hd |> rand val v = rand tm
  in mk_eq(tm,``^v <> 0``) end,EVAL_TAC)
  |> update_precondition;
(* / translation of real_div *)

val check_def = Define `
check (f:real cmd) (P:precond) (A:analysisResult) Gamma (n:num) =
case n of
 | SUC n' => (case CertificateCheckerCmd f A P Gamma of | SOME _ => T |_ => F)/\ check f P A Gamma n'
 | _ => T`

val check_all_def = Define `
  check_all (num_fun:num) (iters:num) (input:Token list) =
    case num_fun of
      | SUC n =>
        (case (dParse input) of
          | SOME ((f,P,A, Gamma), residual) =>
            if (check f P A Gamma iters)
                then case residual of
                      | a:: b => check_all n iters residual
                      | NIL => "True\n"
                else "False\n"
          | NONE => "Failure: Parse\n")
      | _ => "Failure: Number of Functions in certificate\n"`;

val runChecker_def = Define `
  runChecker (input:mlstring list) =
    let inp = concat input in
    let tokList = lex (explode inp) in
      implode (case tokList of
            (* FIRST: number of iterations, SECOND: number of functions *)
          | DCONST n :: DCONST m :: tokRest => check_all m n tokRest
          | _ => "failure no num of functions")`;

(* for recursive translation *)

fun def_of_const tm = let
  val res = dest_thy_const tm handle HOL_ERR _ =>
              failwith ("Unable to translate: " ^ term_to_string tm)
  val name = (#Name res)
  fun def_from_thy thy name =
    DB.fetch thy (name ^ "_pmatch") handle HOL_ERR _ =>
    DB.fetch thy (name ^ "_def") handle HOL_ERR _ =>
    DB.fetch thy (name ^ "_DEF") handle HOL_ERR _ =>
    DB.fetch thy name
  val def = def_from_thy "termination" name handle HOL_ERR _ =>
            def_from_thy (#Thy res) name handle HOL_ERR _ =>
            failwith ("Unable to find definition of " ^ name)
  val def = def |> CONV_RULE (DEPTH_CONV BETA_CONV)
  in def end

val _ = (find_def_for_const := def_of_const);

(* / for recursive translation *)

val _ = translate lookup_def;

val _ = translate subspt_eq;

val res = translate parseExp_def;

val parseexp_side_true = prove(
  ``!x. parseexp_side x = T``,
  recInduct parseExp_ind \\ rw []
  \\ once_rewrite_tac [fetch "-" "parseexp_side_def"] \\ rw [])
  |> update_precondition;

val res = translate dParse_def;

(* improve function definitions a little *)

val const_1_over_2_pow_11_def = Define `
  const_1_over_2_pow_11 = 1 / 2 pow 11`

val const_1_over_2_pow_24_def = Define `
  const_1_over_2_pow_24 = 1 / 2 pow 24`

val const_1_over_2_pow_53_def = Define `
  const_1_over_2_pow_53 = 1 / 2 pow 53`

val const_1_over_2_pow_11_eq = EVAL ``const_1_over_2_pow_11``;
val const_1_over_2_pow_24_eq = EVAL ``const_1_over_2_pow_24``;
val const_1_over_2_pow_53_eq = EVAL ``const_1_over_2_pow_53``;

val _ = translate const_1_over_2_pow_11_eq;
val _ = translate const_1_over_2_pow_24_eq;
val _ = translate const_1_over_2_pow_53_eq;

val _ = translate (MachineTypeTheory.mTypeToR_def
  |> REWRITE_RULE [GSYM const_1_over_2_pow_11_def,
                   GSYM const_1_over_2_pow_24_def,
                   GSYM const_1_over_2_pow_53_def]);

(* val isMorePrecise_eq = prove( *)
(*   ``isMorePrecise m1 m2 = *)
(*     case m1 of *)
(*     | REAL => T *)
(*     | M64 => (case m2 of REAL => F | _ => T) *)
(*     | M32 => (case m2 of REAL => F | M64 => F | _ => T) *)
(*     | M16 => (case m2 of M16 => T | _ => F)``, *)
(*   Cases_on `m1` \\ Cases_on `m2` \\ EVAL_TAC); *)

(* val _ = translate isMorePrecise_eq; *)

fun LET_CONV var_name body =
  (UNBETA_CONV body THENC
   RATOR_CONV (ALPHA_CONV (mk_var(var_name, type_of body))) THENC
   REWR_CONV (GSYM LET_THM));

val absIntvUpd_eq =
  IntervalArithTheory.absIntvUpd_def
  |> SPEC_ALL
  |> CONV_RULE (RAND_CONV (LET_CONV "lo1" ``IVlo iv1`` THENC
                           LET_CONV "lo2" ``IVlo iv2`` THENC
                           LET_CONV "hi1" ``IVhi iv1`` THENC
                           LET_CONV "hi2" ``IVhi iv2``));

val addInterval_eq =
  IntervalArithTheory.addInterval_def
  |> REWRITE_RULE [absIntvUpd_eq]
val _ = translate addInterval_eq

val multInterval_eq =
  IntervalArithTheory.multInterval_def
  |> REWRITE_RULE [absIntvUpd_eq]
val _ = translate multInterval_eq

val maxValueREAL_def =
  let val tm = EVAL ``maxValue REAL`` |> concl |> rand in
  Define `maxValueREAL = ^tm` end |> translate;

val maxValueM16_def =
  let val tm = EVAL ``maxValue M16`` |> concl |> rand in
  Define `maxValueM16 = ^tm` end |> translate;

val maxValueM32_def =
  let val tm = EVAL ``maxValue M32`` |> concl |> rand in
  Define `maxValueM32 = ^tm` end |> translate;

val maxValueM64_def =
  let val tm = EVAL ``maxValue M64`` |> concl |> rand in
  Define `maxValueM64 = ^tm` end |> translate;

val maxValue_eq = prove(
  ``maxValue m =
    case m of
    | REAL => maxValueREAL
    | M16 => maxValueM16
    | M32 => maxValueM32
    | M64 => maxValueM64
    | F w f => &(2 ** (w − 1) − 1) * inv &(2 ** maxExponent m)``,
  Cases_on `m` \\ EVAL_TAC)
  |> translate;

val minValue_posREAL_def =
  let val tm = EVAL ``minValue_pos REAL`` |> concl |> rand in
  Define `minValue_posREAL = ^tm` end |> translate;

val minValue_posM16_def =
  let val tm = EVAL ``minValue_pos M16`` |> concl |> rand in
  Define `minValue_posM16 = ^tm` end |> translate;

val minValue_posM32_def =
  let val tm = EVAL ``minValue_pos M32`` |> concl |> rand in
  Define `minValue_posM32 = ^tm` end |> translate;

val minValue_posM64_def =
  let val tm = EVAL ``minValue_pos M64`` |> concl |> rand in
  Define `minValue_posM64 = ^tm` end |> translate;

val minValue_pos_eq = prove(
  ``minValue_pos m =
    case m of
    | REAL => minValue_posREAL
    | M16 => minValue_posM16
    | M32 => minValue_posM32
    | M64 => minValue_posM64
    | F w f => 0``,
  Cases_on `m` \\ EVAL_TAC)
  |> translate;

val res = translate CertificateCheckerCmd_def;

val invertinterval_side_def = fetch "-" "invertinterval_side_def";

val divideinterval_side_def = fetch "-" "divideinterval_side_def";

val validintervalbounds_side_def = fetch "-" "validintervalbounds_side_def";

val validintervalboundscmd_side_def = fetch "-" "validintervalboundscmd_side_def";

val validerrorbound_side_def = fetch "-" "validerrorbound_side_def";

val validerrorboundcmd_side_def = fetch "-" "validerrorboundcmd_side_def";

val certificatecheckercmd_side_def = fetch "-" "certificatecheckercmd_side_def";

val precond_validIntervalbounds_true = prove (
  ``!e A P (dVars:num_set).
      (!v. v IN (domain dVars) ==>
        ?iv err.
          FloverMapTree_find (Var v) A = SOME (iv, err) /\
          valid iv) ==>
      validintervalbounds_side e A P dVars``,
  recInduct validIntervalbounds_ind
  \\ rw[]
  \\ once_rewrite_tac [validintervalbounds_side_def]
  \\ Cases_on `f` \\ fs []
  \\ rpt (qpat_x_assum `T` kall_tac)
  >- (once_rewrite_tac [GSYM noDivzero_def]
      \\ rpt strip_tac
      \\ rveq
      \\ rename1 `FloverMapTree_find e absenv = SOME (iv_e, err_e)`
      \\ `valid iv_e`
           by (drule validIntervalbounds_validates_iv
               \\ disch_then (qspecl_then [`e`, `P`] destruct)
               \\ fs[] \\ rveq \\ fs[])
      \\ once_rewrite_tac [invertinterval_side_def]
      \\ rpt (qpat_x_assum `!v. _` kall_tac)
      \\ fs [valid_def, noDivzero_def]
      \\ REAL_ASM_ARITH_TAC)
  >- (once_rewrite_tac [GSYM noDivzero_def]
      \\ rpt strip_tac
      \\ rveq
      \\ once_rewrite_tac [divideinterval_side_def]
      \\ once_rewrite_tac [invertinterval_side_def]
      \\ rename1 `FloverMapTree_find e0 absenv = SOME (iv_e, err_e)`
      \\ `valid iv_e`
           by (drule validIntervalbounds_validates_iv
               \\ disch_then (qspecl_then [`e0`, `P`] destruct)
               \\ fs[] \\ rveq \\ fs[])
      \\ once_rewrite_tac [invertinterval_side_def]
      \\ rpt (qpat_x_assum `!v. _` kall_tac)
      \\ fs [valid_def, noDivzero_def]
      \\ REAL_ASM_ARITH_TAC));

val precond_validIntervalboundsCmd_true = prove (
  ``!f A P (dVars:num_set).
      (!v. v IN (domain dVars) ==>
        ?iv err.
          FloverMapTree_find (Var v) A = SOME (iv, err) /\
          valid iv) ==>
      validintervalboundscmd_side f A P dVars``,
  recInduct validIntervalboundsCmd_ind
  \\ rpt strip_tac
  \\ once_rewrite_tac [validintervalboundscmd_side_def]
  \\ rpt strip_tac \\ fs []
  >- (irule precond_validIntervalbounds_true \\ fs [])
  >- (first_x_assum irule
      \\ rw []
      >- (imp_res_tac validIntervalbounds_validates_iv \\ fs [] \\ rfs [])
      >- (first_x_assum match_mp_tac \\ fs []))
  >- (irule precond_validIntervalbounds_true
      \\ fs []));

val precond_validErrorbound_true = prove (``
  !P f exprTypes A (dVars:num_set).
      (!v. v IN (domain dVars) ==>
        ?iv err.
          FloverMapTree_find (Var v) A = SOME (iv, err) /\
          valid iv) /\
    validIntervalbounds f A P dVars ==>
    validerrorbound_side f exprTypes A dVars ``,
  gen_tac
  \\ recInduct validErrorbound_ind
  \\ rpt gen_tac
  \\ fs[AND_IMP_INTRO]
  \\ disch_then ASSUME_TAC
  \\ once_rewrite_tac [validerrorbound_side_def]
  \\ once_rewrite_tac [GSYM noDivzero_def]
  \\ rpt gen_tac \\ disch_then (fn thm => fs [thm] \\ ASSUME_TAC thm)
  \\ rpt gen_tac
  \\ disch_then ASSUME_TAC
  \\ rpt gen_tac
  \\ rpt (disch_then assume_tac)
  \\ rpt gen_tac
  \\ conj_tac
  >- (rpt strip_tac
      \\ rveq
      \\ fs[]
      \\ first_x_assum irule
      \\ qpat_x_assum `validIntervalbounds _ _ _ _` mp_tac
      \\ simp[SimpL “$==>”, Once validIntervalbounds_def]
      \\ ntac 2 (TOP_CASE_TAC \\ gs[]))
  \\ conj_tac
  >- (disch_then (fn thm => fs [thm] \\ ASSUME_TAC thm)
      \\ conj_tac
      >- (conj_tac \\ rpt strip_tac \\ first_x_assum match_mp_tac
          \\ fs [Once validIntervalbounds_def])
      \\ disch_then assume_tac
      \\ rpt gen_tac
      \\ disch_then (fn thm => fs [thm] \\ ASSUME_TAC thm)
      \\ rpt gen_tac
      \\ disch_then (fn thm => fs [thm] \\ ASSUME_TAC thm)
      \\ rpt (disch_then ASSUME_TAC)
      \\ ntac 2 (rpt gen_tac \\ rpt (disch_then assume_tac))
      \\ rveq
      \\ rename1 `Binop Div e1 e2`
      \\ rename1 `FloverMapTree_find e1 A = SOME (iv_e1, err1)`
      \\ rename1 `FloverMapTree_find e2 A = SOME (iv_e2, err2)`
      \\ rewrite_tac [divideinterval_side_def, widenInterval_def,
                  minAbsFun_def, invertinterval_side_def, IVlo_def,
                  IVhi_def]
      \\ `valid iv_e1 /\ valid iv_e2`
           by (conj_tac \\ drule validIntervalbounds_validates_iv
               >- (disch_then (qspecl_then [`e1`, `P`] destruct)
                   \\ fs [Once validIntervalbounds_def] \\ rveq \\ fs[])
               \\ disch_then (qspecl_then [`e2`, `P`] destruct)
               \\ fs [Once validIntervalbounds_def] \\ rveq \\ fs[])
      \\ qpat_x_assum `!v. _` kall_tac
      \\ `0 <= err2`
           by (drule err_always_positive
               \\ disch_then (fn thm => qspecl_then [`iv_e2`, `err2`] match_mp_tac thm)
               \\ fs [])
      \\ `FST iv_e2 - err2 <> 0 /\ SND iv_e2 + err2 <> 0`
         by (fs [noDivzero_def, IVlo_def, IVhi_def, widenInterval_def, valid_def]
             \\ conj_tac \\ REAL_ASM_ARITH_TAC)
      \\ `noDivzero (SND iv_e2) (FST iv_e2)`
         by (drule validIntervalbounds_noDivzero_real
             \\ fs [])
      \\ `abs (FST iv_e2 - err2) <> 0 /\ abs (SND iv_e2 + err2) <> 0`
         by (REAL_ASM_ARITH_TAC)
      \\ `min (abs (FST iv_e2 - err2)) (abs (SND iv_e2 + err2)) <> 0`
         by (fs [min_def]
             \\ Cases_on `abs (FST iv_e2 - err2) <= abs (SND iv_e2 + err2)`
             \\ fs [] \\ REAL_ASM_ARITH_TAC)
      \\ fs [noDivzero_def, valid_def, IVhi_def, IVlo_def]
      \\ REAL_ASM_ARITH_TAC)
  \\ conj_tac
  >- (rpt strip_tac
      \\ rveq
      \\ qpat_x_assum `validIntervalbounds _ _ _ _` mp_tac
      \\ simp[SimpL “$==>”, Once validIntervalbounds_def]
      \\ ntac 2 (TOP_CASE_TAC \\ gs[]))
  \\ rpt strip_tac
  \\ qpat_x_assum `validIntervalbounds _ _ _ _` mp_tac
  \\ simp[SimpL “$==>”, Once validIntervalbounds_def]
  \\ ntac 2 (TOP_CASE_TAC \\ gs[]));

val precond_errorbounds_true = prove (``
  !P f exprTypes A dVars.
    (!v. v IN (domain dVars) ==>
      ?iv err.
        FloverMapTree_find (Var v) A = SOME (iv, err) /\
        valid iv) /\
    validIntervalboundsCmd f A P dVars ==>
    validerrorboundcmd_side f exprTypes A dVars``,
  gen_tac
  \\ recInduct validErrorboundCmd_ind
  \\ rpt strip_tac
  \\ once_rewrite_tac [validerrorboundcmd_side_def]
  \\ Cases_on `f` \\ fs [Once validIntervalboundsCmd_def]
  >- (rpt strip_tac
      >- (irule precond_validErrorbound_true \\ fs[]
          \\ qexists_tac `P` \\ fs [])
      \\ first_x_assum irule
      \\ rpt (conj_tac)
      \\ fs[]
      >- (gen_tac
          \\ Cases_on `v = n` \\ fs[]
          \\ rveq
          \\ drule validIntervalbounds_validates_iv
          \\ disch_then drule \\ fs[])
      \\ Cases_on `c` \\ fs [Once validIntervalboundsCmd_def])
  \\ irule precond_validErrorbound_true \\ fs[]
  \\ qexists_tac `P` \\ fs []);

val precond_is_true = prove (
  ``!f absenv P exprTypes.
      certificatecheckercmd_side f absenv P exprTypes``,
   once_rewrite_tac [certificatecheckercmd_side_def]
   \\ rpt gen_tac
   \\ strip_tac
   \\ conj_tac
   >- (irule precond_validIntervalboundsCmd_true
       \\ fs [])
   >- (strip_tac \\ irule precond_errorbounds_true \\ fs[]
       \\ qexists_tac `P` \\ fs []))
  |> update_precondition;

val res = translate str_of_num_def;

val str_of_num_side_def = fetch "-" "str_of_num_side_def";

val str_of_num_side_true = prove (``!a. str_of_num_side a``,
recInduct str_of_num_ind
\\ rpt strip_tac \\ simp [str_of_num_side_def]
\\ rpt strip_tac
\\ `n MOD 10 < 10` by (match_mp_tac MOD_LESS \\ fs [])
\\ match_mp_tac LESS_TRANS
\\ qexists_tac `10 + 48` \\ fs []) |> update_precondition;

val res = translate runChecker_def;

val valid_runchecker_output_def = Define`
  valid_runchecker_output file_contents output =
    (runChecker file_contents = output)`;
(* Although we have defined valid_wordfreq_output as a relation between
   file_contents and output, it is actually functional (there is only one correct
   output). We prove this below: existence and uniqueness. *)

val valid_runchecker_output_exists = Q.store_thm("valid_runchecker_output_exists",
  `∃output. valid_runchecker_output file_chars output`,
  fs[valid_runchecker_output_def]);

val runchecker_output_spec_def =
  new_specification("runchecker_output_spec_def",["runchecker_output_spec"],
    GEN_ALL valid_runchecker_output_exists |> SIMP_RULE std_ss [SKOLEM_THM]);

(* -- I/O routines from here onwards -- *)

val main = process_topdecs`
  fun main u =
    case TextIO.inputLinesFrom (List.hd (CommandLine.arguments()))
    of Some lines =>
      TextIO.print (runchecker lines)`;
  (* fun main u = *)
  (*   write_list (runchecker (read_all []))`; *)

val _ = append_prog main;

(* val res = ml_prog_update(ml_progLib.add_prog main I) *)
val st = get_ml_prog_state()

(* Specification of the runchecker function:
   Running the checker on an input file inp appends the result of running
   the function to STDOUT *)

val main_spec = Q.store_thm("main_spec",
  `hasFreeFD fs ∧ inFS_fname fs fname ∧
   cl = [pname; fname] ∧
  contents = all_lines fs fname ==>
  app (p:'ffi ffi_proj) ^(fetch_v "main" st)
    [uv] (STDIO fs * COMMANDLINE cl)
    (POSTv uv. &UNIT_TYPE () uv *
      (STDIO (add_stdout fs (runchecker_output_spec contents))) * COMMANDLINE cl)`,
     (* [Conv NONE []] (STDOUT out * STDERR err * STDIN inp F) *)
     (* (POSTv uv. &UNIT_TYPE () uv * *)
     (*            STDOUT (out ++ runChecker inp) * *)
     (*            STDERR err * *)
     (*            STDIN "" T)`, *)
  xcf "main" st
  \\ xlet_auto
  >- (xcon \\ xsimpl \\ EVAL_TAC)
  \\ xlet_auto >- xsimpl
  \\ xlet_auto >- (xsimpl)
  \\ reverse(Cases_on`wfcl cl`)
  >- (rfs[COMMANDLINE_def] \\ xpull)
  \\ reverse (Cases_on `STD_streams fs`) >-(fs[STDIO_def] \\ xpull)
  \\ xlet_auto_spec (SOME inputLinesFrom_spec)
  >- (xsimpl \\ rfs[wfcl_def, validArg_def, EVERY_MEM])
  \\ xmatch \\ fs[OPTION_TYPE_def]
  \\ reverse conj_tac >- (EVAL_TAC \\ fs[])
  \\ xlet_auto
  >- (xsimpl)
  \\ xapp \\ instantiate \\ xsimpl
  \\ CONV_TAC(SWAP_EXISTS_CONV) \\ qexists_tac`fs` \\ xsimpl
  \\ qmatch_abbrev_tac`STDIO (add_stdout _ xxxx) ==>> STDIO (add_stdout _ yyyy) * GC`
  \\ `xxxx = yyyy` suffices_by xsimpl
  (* now let us unabbreviate xxxx and yyyy *)
  \\ map_every qunabbrev_tac[`xxxx`,`yyyy`] \\ simp[]
  \\ assume_tac (REWRITE_RULE [valid_runchecker_output_def] runchecker_output_spec_def)
  \\ fs[all_lines_def]);

val main_whole_prog_spec = Q.store_thm("main_whole_prog_spec",
  `hasFreeFD fs ∧ inFS_fname fs fname ∧
   cl = [pname; fname] ∧
  contents = all_lines fs fname ==>
   whole_prog_spec ^(fetch_v "main" st) cl fs NONE
         ((=) (add_stdout fs (runchecker_output_spec contents)))`,
  disch_then assume_tac
  \\ simp[whole_prog_spec_def]
  \\ qmatch_goalsub_abbrev_tac`fs1 = _ with numchars := _`
  \\ qexists_tac`fs1`
  \\ simp[Abbr`fs1`,GSYM add_stdo_with_numchars,with_same_numchars]
  \\ match_mp_tac (MP_CANON (MATCH_MP app_wgframe (UNDISCH main_spec)))
  \\ xsimpl);

val (sem_thm,prog_tm) = whole_prog_thm (get_ml_prog_state ()) "main" (UNDISCH main_whole_prog_spec);

val main_prog_def = Define `main_prog = ^prog_tm`;

val main_semantics =
  sem_thm |> ONCE_REWRITE_RULE[GSYM main_prog_def]
  |> DISCH_ALL |> Q.GENL[`cl`,`contents`]
  |> SIMP_RULE(srw_ss())[AND_IMP_INTRO,GSYM CONJ_ASSOC]
  |> curry save_thm "main_semantics";
*)

val _ = export_theory();
