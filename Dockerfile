FROM debian:latest

WORKDIR /root

USER root

#Necessary packages for opam
RUN apt-get update && \
    apt-get install -y build-essential aspcud m4 wget unzip git curl

# # Configure opam
# RUN opam init --comp=4.05.0 --auto-setup && \
#RUN eval `opam config env`

#RUN cd /home/opam/opam-repository && git pull && cd

# Install coq and dependencies
#RUN opam repo add coq-released https://coq.inria.fr/opam/released --set-default && \
#    opam update

### Currently disabled because some lemmas are missing in 8.7.2
##Install coq 8.7.2 in a switch
#RUN opam switch create coq8.7.2 ocaml-base-compiler.4.05.0 && \
#    eval `opam config env`

#RUN opam install coq.8.7.2 coq-flocq.2.6.1

#Install coq 8.8 in a switch
#RUN opam switch create coq8.8 ocaml-base-compiler.4.05.0
#RUN opam install coq.8.8.2 coq-flocq.3.1.0

# Install polyml from git
RUN git clone https://github.com/polyml/polyml.git polyml && \
    cd polyml && \
    git checkout v5.8.2
RUN cd polyml && ./configure --prefix=/usr && make && make install

COPY ./hol4/.HOLCOMMIT HOLCOMMIT

RUN git clone https://github.com/HOL-Theorem-Prover/HOL/ HOL && \
    cd HOL && \
    git checkout $(cat /root/HOLCOMMIT) && \
    poly < tools/smart-configure.sml && \
    bin/build
RUN ln -s /root/HOL/bin/Holmake /usr/bin/Holmake