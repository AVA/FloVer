# Using HOL Light #

1. Clone the latest release from the Github Repository
    https://github.com/jrh13/hol-light/
2. Make sure you have camlp5 in strict mode installed (`camlp5 -pmode` must print "strict")
    If not download camlp5 and install it as follows:
    ```
        cd software/camlp5-6.06 [or wherever you unpacked sources to]
        ./configure --strict
        make world.opt
        sudo make install       [assuming no earlier errors]
    ```
3. Got ot the directory to which you cloned HOL Light and compile it with `make`.
4. Run the `configure_ocaml.sh` script
    This script currently only creates the necessary .ocamlinit file such that it is not necessary to load HOL Light by hand.
5. Open emacs and add the following line to your customized variables in your .emacs file:
 `'(warning-suppress-types (quote ((undo discard-info))))`
    This line will suppress the warnings that the ocaml buffer is too long.
6. Add the following line to your .emacs:
    `(load "ABSOLUTE_PATH_TO_DAISY_REPO_HOL_DIR/hol-light.el")`
7. After restarting emacs, you can open any .ml file in this directory and use M-x hol-light-mode to get syntax highlighting working
    To get an interactive prompt do M-x hol-light-run-caml. When prompted for an hol-light binary simply enter ocaml.

## Keybindings ##
The file hol-light.el contains all the keybindings in the variable `hol-light-mode-map`. The most important one is C-x C-e which sends the line under the cursor to the ocaml shell.

    
