  OCaml version 4.01.0

  # #load "str.cma";;
# let mysplit (word) = split (regexp word);;
Error: Unbound value split
# open Str;;
# let mysplit (word) = split (regexp word);;
val mysplit : string -> string -> string list = <fun>
  # let test = mysplit "2.123456789e-14
      "
      ".";;
val test : string list = ["."]
    # let test = mysplit "2.123456789e-14" ".+";;
val test : string list = [".+"]
    # let test = split (regexp ".")
        "2.123456789e-14";;
val test : string list =
  [""; ""; ""; ""; ""; ""; ""; ""; ""; ""; ""; ""; ""; ""]
    # let test = mysplit
        "\." "2.123456789e-14";;
Warning 14: illegal backslash escape in string.
val test : string list = ["2"; "123456789e-14"]
    # regexp (".");;
- : Str.regexp = <abstr>
# let mysplit (word) = split (regexp_string word);;
val mysplit : string -> string -> string list = <fun>
  # let res = mysplit "." "2.1234e-14";;
val res : string list = ["2"; "1234e-14"]
    # let res2 = mysplit "e-" res[1];;
Error: This function has type string -> string -> string list
    It is applied to too many arguments; maybe you forgot a `;'.
  # let res2 = mysplit "e-" (List.nth res 2);;
Exception: Failure "nth".
# List.nth;;
- : 'a list -> int -> 'a = <fun>
  # List.nth res 1;;
- : string = "1234e-14"
  # let res2 = mysplit
      "e-" (List.nth res 1);;
val res2 : string list = ["1234"; "14"]
