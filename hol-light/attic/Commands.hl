(*
 Formalization of the Abstract Syntax Tree of a subset used in the Daisy framework
*)
needs "Expressions.hl";;
(*
  Next define what a program is.
  Currently no loops, only conditionals and assignments
  Final return statement
*)
let cmd_INDUCT, cmd_REC = define_type
  "cmd = Let num (V)exp cmd
       | Ite (V)bexp cmd cmd
       | Ret (V)exp
       | Nop ";;

(*
   Small Step semantics for Daisy language, parametric by evaluation function.
*)
let sstep_RULES,sstep_INDUCT,sstep_CASES = new_inductive_definition
  `(!x (e:(real)exp) s (env:num->real) (v:real) eps
      (env:num->real).
      eval_exp eps env e v ==>
      sstep (Let x e s) env eps s (upd_env x v env)) /\
  (!(c:(real)bexp) s t eps
     (env:num->real).
     bval eps env c T ==>
     sstep (Ite c s t) env eps s env) /\
  (!(c:(real)bexp) s t eps
     (env:num->real).
     bval eps env c F ==>
     sstep (Ite c s t) env eps t env) /\
  (!(e:(real)exp) (v:real) eps
     (env:num->real).
     eval_exp eps env e v ==>
     sstep (Ret e) env eps Nop (upd_env (0) v env))`;;

(*
  TODO: small step inversion lemmata
*)

(*
  Analogously define Big Step semantics for the Daisy language,
  parametric by the evaluation function*)
let bstep_RULES,bstep_INDUCT,bstep_CASES = new_inductive_definition
  `(!x (e:(real)exp) s s' (env:num->real) (v:real) eps
      (env2:num->real).
      eval_exp eps env e v /\
      bstep s (upd_env x v env) eps s' env2 ==>
      bstep (Let x e s) env eps s' env2) /\
  (!(c:(real)bexp) s1 s2 t eps
     (env:num->real)
     (env2:num->real).
     bval eps env c T /\
     bstep s1 env eps s2 env2 ==>
     bstep (Ite c s1 t) env eps s2 env2) /\
  (!(c:(real)bexp) s t1 t2 eps
     (env:num->real)
     (env2:num->real).
     bval eps env c F /\
     bstep t1 env eps t2 env2 ==>
     bstep (Ite c s t1) env eps t2 env2) /\
  (!(e:(real)exp) (v:real) eps
     (env:num->real).
     eval_exp eps env e v ==>
      bstep (Ret e) env eps Nop (upd_env 0 v env))`;;

(*
  Big Step Inversion Lemmata
*)
let bstep_let_inv =
  prove (
    `!x e (s1:(real)cmd) env eps (s2:(real)cmd) env2.
      bstep (Let x e s1) env eps s2 env2 ==>
      ?v. eval_exp eps env e v /\ bstep s1 (upd_env x v env) eps s2 env2`,
    intros "!x e s1 env eps s2 env2; bstep_let"
      THEN RULE_ASSUM_TAC (ONCE_REWRITE_RULE [bstep_CASES])
      THEN destruct "bstep_let" "bstep_let | bstep_itet | bstep_itef | bstep_ret"
      THENL [
        destruct "bstep_let" "@x'. @e'. @s. @v. let_eq eval_eq bstep_rec"
        THEN EXISTS_TAC `v:real`
          THEN CONJ_TAC THEN ASM_REWRITE_TAC[]
          THEN SUBGOAL_TAC "e_eq_e'" `e:(real)exp = e':(real)exp` [
            RULE_ASSUM_TAC (REWRITE_RULE[injectivity "cmd"])
              THEN ASM_REWRITE_TAC[]]
          THEN ASM_REWRITE_TAC[]
          THEN SUBGOAL_TAC "eqs" `s1:(real)cmd = s /\ x:num = x':num` [
            RULE_ASSUM_TAC (REWRITE_RULE[injectivity "cmd"])
              THEN ASM_REWRITE_TAC[]]
          THEN ASM_REWRITE_TAC[];
        destruct "bstep_itet" "@c. @s1'. @t. let_eq_if _"
          THEN lcontradiction "let_eq_if" "cmd";
        destruct "bstep_itef" "@a. @b. @c. let_eq_if _"
          THEN lcontradiction "let_eq_if" "cmd";
        destruct "bstep_ret" "@a. @b. let_eq_ret _"
          THEN lcontradiction "let_eq_ret" "cmd"]);;

let bstep_ite_inv =
  prove (
    `!c (s1:(real)cmd) t1 env eps (nop:(real)cmd) env2.
      bstep (Ite c s1 t1) env eps nop env2 ==>
      (bval eps env c T /\ bstep s1 env eps nop env2) \/
      (bval eps env c F /\ bstep t1 env eps nop env2)`,
    intros "!c s1 t1 env eps nop env2; bstep_if"
      THEN RULE_ASSUM_TAC (ONCE_REWRITE_RULE [bstep_CASES])
      THEN destruct "bstep_if" "bstep_let | bstep_itet | bstep_itef | bstep_ret"
      THENL[
        destruct "bstep_let" "@x. @e. @s. @v. ite_eq_let _"
          THEN lcontra "ite_eq_let" "cmd";
        DISJ1_TAC
          THEN destruct "bstep_itet" "@c'. @s1'. @t. ite_eq bval_true bstep_rec"
          THEN SUBGOAL_TAC "eqs" `c:(real)bexp = c' /\ s1:(real)cmd = s1' /\ t1:(real)cmd = t`
          [RULE_ASSUM_TAC (REWRITE_RULE[injectivity "cmd"]) THEN ASM_REWRITE_TAC[]]
          THEN ASM_REWRITE_TAC[];
        DISJ2_TAC
          THEN destruct "bstep_itef" "@c'. @s. @t1'. ite_eq bval_false bstep_rec"
          THEN SUBGOAL_TAC "eqs" `c:(real)bexp = c' /\ s1:(real)cmd = s /\ t1:(real)cmd = t1'`
          [RULE_ASSUM_TAC (REWRITE_RULE[injectivity "cmd"]) THEN ASM_REWRITE_TAC[]]
          THEN ASM_REWRITE_TAC[];
        destruct "bstep_ret" "@e. @v. ite_eq_ret _"
          THEN lcontra "ite_eq_ret" "cmd"]);;

let bstep_ret_inv =
  prove (
    `!e env eps (s:(real)cmd) env2.
      bstep (Ret e) env eps s env2 ==>
      ?v. eval_exp eps env e v /\ env2 = upd_env 0 v env /\ s = Nop`,
    intros "!e env eps s env2; bstep_ret"
      THEN RULE_ASSUM_TAC (ONCE_REWRITE_RULE [bstep_CASES])
      THEN destruct "bstep_ret" "bstep_let | bstep_itet | bstep_itef | bstep_ret"
      THENL[
        destruct "bstep_let" "@x. @e'. @s'. @v. ite_eq_let _"
          THEN lcontra "ite_eq_let" "cmd";
        destruct "bstep_itet" "@c. @s1. @t. let_eq_if _"
          THEN lcontradiction "let_eq_if" "cmd";
        destruct "bstep_itef" "@a. @b. @c. let_eq_if _"
          THEN lcontradiction "let_eq_if" "cmd";
        destruct "bstep_ret" "@e'. @v. ret_eq s_nop env2_eq eval_ret"
          THEN EXISTS_TAC `v:real`
          THEN SUBGOAL_TAC "eqs" `e:(real)exp = e'`
          [RULE_ASSUM_TAC (ONCE_REWRITE_RULE [injectivity "cmd"]) THEN ASM_REWRITE_TAC[]]
          THEN ASM_REWRITE_TAC[]]);;
