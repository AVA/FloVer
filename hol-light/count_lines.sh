#!/bin/bash
arr=()
while IFS=  read -r -d $'\0'; do
    arr+=("$REPLY")
done < <(find . -path ./attic -prune -o -path ./output -prune -o -path ./Infra -prune -o -name "*.hl" -print0)

for file in "${arr[@]}"
do
    wc -l $file
done
