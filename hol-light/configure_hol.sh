#!/bin/bash

###################################################
# configuration Script to set up the ocaml        #
# environment file, needed for ease of using HOL  #
###################################################

#check wether HOLLIGHT_HOME is set

PG_DIR=
CURR_DIR=`pwd`

if [ -z "$HOLLIGHT_HOME" ];
then 
    echo "Please configure HOLLIGHT_HOME to point to your local HOL Light directory";
    exit 1;
fi
#if [ -z "$1" ];
#then
#    echo "No Proof General directory supplied. Not including PG Tactics";
#    echo "If needed, provide the absolute path to the ho-light subdirectory of Proof General as first argument"
#else
#    PG_DIR=$1
#fi

echo "(* " > .ocamlinit
echo "DO NOT CHANGE THIS FILE! IF CHANGES ARE NECESSARY ADD THEM TO THE configure_hol.sh SCRIPT" >> .ocamlinit
echo "*)" >> .ocamlinit
echo "#cd \""$HOLLIGHT_HOME"\";;" >> .ocamlinit
echo "#use \"hol.ml\";;" >> .ocamlinit

echo "#cd \""$CURR_DIR"\";;" >>.ocamlinit
echo "#use \"Infra/types.ml\";;" >>.ocamlinit

# Setup dmtcp
if [ -z `command -v dmtcp_launch >/dev/null 2>&1` ]
then
    echo "DMTCP in path found generating patched launch script and init file"
    echo "Please launch HOL using \"./run_hol.sh\" and close the session after loading using Ctrl-d."
    echo "#load \"unix.cma\";;" >> .ocamlinit
    echo "if Unix.fork () = 0 then Unix.execvp \"dmtcp_command\" [|\"\"; \"--checkpoint\"|] else try ignore (Unix.select [] [] [] 0.1) with _ -> ();;" >>.ocamlinit
    echo "#!/bin/bash" > ./run_hol.sh
    echo "dmtcp_launch ocaml" >> ./run_hol.sh
    chmod +x ./run_hol.sh
else   
    echo "DMTCP not found please launch HOL using \"ocaml\""
fi
