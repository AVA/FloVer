(**
Add printers to HOL-Light to be able to print terms annotated with their terms.
Snippet obtained from HOL-Light mailing list.
https://sourceforge.net/p/hol/mailman/message/35203735/
Code taken from the Flyspeck project.
**)
let print_varandtype fmt tm =
    let hop,args = strip_comb tm in
    let s = name_of hop
          and ty = type_of hop in
    if is_var hop & args = [] then
        (pp_print_string fmt "(";
                     pp_print_string fmt s;
                         pp_print_string fmt ":";
                             pp_print_type fmt ty;
                                 pp_print_string fmt ")")
    else fail() ;;

let show_types,hide_types =
    (fun () -> install_user_printer ("Show Types",print_varandtype)),
        (fun () -> try delete_user_printer "Show Types"
        with Failure _ -> failwith ("hide_types: "^
                                                 "Types are already hidden."));;
