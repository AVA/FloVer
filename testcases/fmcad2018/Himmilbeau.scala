
import daisy.lang._
import Real._

/*
  Real2Float
  From a global optimization problem
  A Numerical Evaluation of Several Stochastic Algorithms on
  Selected Continuous Global Optimization problems,
  Ali, Khompatraporn, Zabinsky, 2005

*/
object Himmilbeau {

  //15 operations
  def himmilbeau(x1: Real, x2: Real) = {
    require(-5 <= x1 && x1 <= 5 && -5 <= x2 && x2 <= 5)

    val t1 = x1*x1 + x2
    val t2 = x1 + x2*x2
    (t1 - 11)*(t1 - 11) + (t2 - 7)*(t2 - 7)

  } //1.43e–12

}
