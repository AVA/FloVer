open  FloverTactics AbbrevsTheory MachineTypeTheory CertificateCheckerTheory FloverMapTheory;
open simpLib realTheory realLib RealArith;

val _ = new_theory "certificate_AdditionSimple";


val C12_def = Define `C12:(real expr) = Const M64 ((1657)/(5))`;
val u0_def = Define `u0:(real expr) = Var 0`;
val PlusC12u0 = Define `PlusC12u0:(real expr) = Binop Plus C12 u0`;;
val RetPlusC12u0_def = Define `RetPlusC12u0 = Ret PlusC12u0`;


val defVars_additionSimple_def = Define `
 defVars_additionSimple : typeMap = 
(FloverMapTree_insert (Var 0) (M64) (FloverMapTree_empty))`;


val thePrecondition_additionSimple_def = Define ` 
 thePreconditionadditionSimple (n:num):interval = 
if n = 0 then ( (-100)/(1), (100)/(1)) else (0,1)`;

val absenv_additionSimple_def = RIGHT_CONV_RULE EVAL (Define `
  absenv_additionSimple:analysisResult = 
FloverMapTree_insert PlusC12u0 (( (1157)/(5), (2157)/(5)), (7771411516990528329)/(81129638414606681695789005144064)) (FloverMapTree_insert u0 (( (-100)/(1), (100)/(1)), (25)/(2251799813685248)) (FloverMapTree_insert C12 (( (1657)/(5), (1657)/(5)), (1657)/(45035996273704960)) (FloverMapTree_empty)))`);

val _ = store_thm ("ErrorBound_additionSimple_Sound",
``? Gamma. CertificateCheckerCmd RetPlusC12u0 absenv_additionSimple
thePreconditionadditionSimple defVars_additionSimple = SOME Gamma``,
 flover_eval_tac \\ fs[REAL_INV_1OVER]);


 val _ = export_theory();
