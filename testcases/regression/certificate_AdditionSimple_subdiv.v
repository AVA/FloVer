Require Import Flover.CertificateChecker.
Definition C12 :expr Q := Const M64 ((1657)#(5)).
Definition u0 :expr Q := Var Q 0.
Definition e3 :expr Q := Binop Plus C12 u0.


Definition defVars_additionSimple: FloverMap.t mType :=
(FloverMap.add (Var Q 0) (M64) (FloverMap.empty mType)).


Definition thePrecondition_additionSimple_sub0 :=
(FloverMap.add u0 ((-100)#(1), (-100)#(3)) (FloverMap.empty intv),
TrueQ).

Definition thePrecondition_additionSimple_sub1 :=
(FloverMap.add u0 ((-100)#(3), (100)#(3)) (FloverMap.empty intv),
TrueQ).

Definition thePrecondition_additionSimple_sub2 :=
(FloverMap.add u0 ((100)#(3), (100)#(1)) (FloverMap.empty intv),
TrueQ).

Definition querymap_additionSimple_sub0 :=
FloverMap.empty (SMTLogic * SMTLogic).

Definition querymap_additionSimple_sub1 :=
FloverMap.empty (SMTLogic * SMTLogic).

Definition querymap_additionSimple_sub2 :=
FloverMap.empty (SMTLogic * SMTLogic).

Definition absenv_additionSimple_sub0: analysisResult :=
FloverMap.add e3 (((1157)#(5), (4471)#(15)), (19711354849075188187)#(243388915243820045087367015432192)) (FloverMap.add u0 (((-100)#(1), (-100)#(3)), (25)#(2251799813685248)) (FloverMap.add C12 (((1657)#(5), (1657)#(5)), (1657)#(45035996273704960)) (FloverMap.empty (intv * error)))).

Definition absenv_additionSimple_sub1: analysisResult :=
FloverMap.add e3 (((4471)#(15), (5471)#(15)), (21512794700023386587)#(243388915243820045087367015432192)) (FloverMap.add u0 (((-100)#(3), (100)#(3)), (25)#(2251799813685248)) (FloverMap.add C12 (((1657)#(5), (1657)#(5)), (1657)#(45035996273704960)) (FloverMap.empty (intv * error)))).

Definition absenv_additionSimple_sub2: analysisResult :=
FloverMap.add e3 (((5471)#(15), (2157)#(5)), (7771411516990528329)#(81129638414606681695789005144064)) (FloverMap.add u0 (((100)#(3), (100)#(1)), (25)#(2251799813685248)) (FloverMap.add C12 (((1657)#(5), (1657)#(5)), (1657)#(45035996273704960)) (FloverMap.empty (intv * error)))).

Definition subdivs_additionSimple: list (precond * analysisResult * usedQueries) := [
(thePrecondition_additionSimple_sub0, absenv_additionSimple_sub0, querymap_additionSimple_sub0);(thePrecondition_additionSimple_sub1, absenv_additionSimple_sub1, querymap_additionSimple_sub1);(thePrecondition_additionSimple_sub2, absenv_additionSimple_sub2, querymap_additionSimple_sub2)].

Definition thePrecondition_additionSimple :=
(FloverMap.add u0 ((-100)#(1), (100)#(1)) (FloverMap.empty intv),
TrueQ).

Definition absenv_additionSimple: analysisResult :=
FloverMap.add e3 (((1157)#(5), (2157)#(5)), (7771411516990528329)#(81129638414606681695789005144064)) (FloverMap.add u0 (((-100)#(1), (100)#(1)), (25)#(2251799813685248)) (FloverMap.add C12 (((1657)#(5), (1657)#(5)), (1657)#(45035996273704960)) (FloverMap.empty (intv * error)))).

Definition querymap_additionSimple :=
FloverMap.empty (SMTLogic * SMTLogic).

Theorem ErrorBound_additionSimple_sound :
CertificateChecker e3 absenv_additionSimple thePrecondition_additionSimple querymap_additionSimple subdivs_additionSimple defVars_additionSimple= true.
Proof. vm_compute; auto. Qed.
