

import daisy.lang._
import Real._


object DopplerFMA {

  def doppler(u: Real, v: Real, T: Real): Real = {
    require(-100.0 <= u && u <= 100 && 20 <= v && v <= 20000 && -30 <= T && T <= 50 && u + T <= 100)

    val t1 = fma(0.6, T, 331.4)
    (- (t1) *v) / ((t1 + u)*(t1 + u))

  }


}
